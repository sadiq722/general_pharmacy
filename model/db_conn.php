<?php

//CONNECTORS
// <editor-fold defaultstate="collapsed" desc="open database connection">
define('TODAY', date('Y-m-d'));

function open_conn() {
    $host = "localhost";
    $user = "root";
    $password = "";
    $databaseName = "pharmacy_db";

    $conn = new mysqli($host, $user, $password, $databaseName);
    return $conn;
}

// </editor-fold>
// <editor-fold defaultstate="collapsed" desc="close database connection">
function close_conn() {
    open_conn()->close();
}

// </editor-fold>
// INSERT INTO DATABASE
// 
include_once '../controller/utils.php';

function _isAUser($phone_number, $username) {
    $flag = FALSE;
    $query = "SELECT id FROM user_table WHERE phone_number='$phone_number' OR username='$username'";
    $conn = open_conn();
    $result = $conn->query($query);
    echo $conn->error;
    close_conn();
    if ($result->num_rows > 0) {
        $flag = true;
    }
    return $flag;
}

function _fetchUsernames() {
    $items = array();
    $query = "SELECT username FROM user_table ";
    $conn = open_conn();
    $result = $conn->query($query);
    close_conn();
    while($row=$result->fetch_assoc()){
        $items[]=$row;
    }  
    return $items;
}

function _createAccount($first_name, $last_name, $phone_number, $username, $password) {
    $query = "INSERT INTO user_table (username,password,first_name,last_name,phone_number) "
            . " VALUES('$username','$password','$first_name','$last_name','$phone_number')";
    $conn = open_conn();
    $conn->query($query);
    $id = $conn->insert_id;
    $conn->query("INSERT INTO privilege_table (user_id) VALUES('$id')");
    echo $conn->error;
    close_conn();
}



function _login($username) {
    $conn = open_conn();
    $sql = "SELECT * FROM user_table WHERE username='$username'";
    $result = $conn->query($sql);
    close_conn();
    if ($result->num_rows > 0) {
        $row = $result->fetch_assoc();
        return $row;
    } else {
        return NULL;
    }
    
}

function _fetchUser() {
    $items = array();
    $query = "SELECT * FROM user_table ";
    $conn = open_conn();
    $result = $conn->query($query);
    while ($row = $result->fetch_assoc()) {
        $items[] = $row;
    }
    echo $conn->error;
    close_conn();
    return $items;
}

function _getUser($id) {
    $query = "SELECT * FROM user_table WHERE id='$id'";
    $conn = open_conn();
    $result = $conn->query($query);
    $row = $result->fetch_assoc();
    echo $conn->error;
    close_conn();
    return $row;
}

function _getUserPrivilege($id) {
    $query = "SELECT * FROM privilege_table WHERE user_id='$id'";
    $conn = open_conn();
    $result = $conn->query($query);
    echo $conn->error;
    $row = $result->fetch_assoc();
    echo $conn->error;
    close_conn();
    return $row;
}

function _changePassword($id, $password) {
    $conn = open_conn();
    $flag = 0;
    $sql = "UPDATE user_table SET password='$password'  WHERE id='$id'";
    if ($conn->query($sql)) {
        $flag = 1;
    } else {
        echo $conn->error;
    }
    close_conn();
    return $flag;
}
function _getPrinterShareName() {
    $conn = open_conn();
   
    $sql = "SELECT * FROM printer_table WHERE id='1'";
    $result=$conn->query($sql);
    echo $conn->error;
        $row=$result->fetch_assoc();
    close_conn();
    return $row;
}
function _changePrinterShareName($name) {
    $conn = open_conn();
    $flag = 0;
    $sql = "UPDATE printer_table SET share_name='$name'";
    if ($conn->query($sql)) {
        $flag = 1;
    } else {
        echo $conn->error;
    }
    close_conn();
    return $flag;
}

function _changePrivilege($id,$record,$nurse,$doctor,$pharmacy, $pharmacy_admin,$lab,$lab_admin,$radiology,$radiology_admin,$bill,$bill_admin,$admin,$super_admin)
 {
    $conn = open_conn();
    $flag = 0;
    $sql = "UPDATE privilege_table SET record='$record', nurse='$nurse', doctor='$doctor',"
            . "pharmacy='$pharmacy', pharmacy_admin='$pharmacy_admin', lab='$lab',"
            . "lab_admin='$lab_admin', radiology='$radiology', radiology_admin='$radiology_admin',"
            . "bill='$bill', bill_admin='$bill_admin',"
            . "admin='$admin', super_admin='$super_admin'  WHERE user_id='$id'";
    if ($conn->query($sql)) {
        $flag = 1;
    } else {
        echo $conn->error;
    }
    echo $conn->error;
    close_conn();
    return $flag;
}

function _isEMRIDExist($emr_id) {
    $flag = FALSE;
    $query = "SELECT id FROM patient_table WHERE emr_id='$emr_id'";
    $conn = open_conn();
    $result = $conn->query($query);
    echo $conn->error;
    close_conn();
    if ($result->num_rows > 0) {
        $flag = true;
    }
    return $flag;
}

function _isPhoneNumberExist($phone) {
    $flag = 0;
    $query = "SELECT id FROM patient_table WHERE phone_number='$phone'";
    $conn = open_conn();
    $result = $conn->query($query);
    
    close_conn();
    if ($result->num_rows > 0) {
        $flag = $result->fetch_assoc()['id'];
        echo $conn->error;
    }
    return $flag;
}

// </editor-fold>
// <editor-fold defaultstate="collapsed" desc="Create an account for patient">
function _addPatient($full_name, $phone_number) {
    
    $query = "INSERT INTO patient_table(phone_number,full_name) VALUES "
            . "("
            . "'$phone_number',"
            . "'$full_name'"
            . ")";
    $conn = open_conn();
    $conn->query($query);
    echo $conn->error;
        $last_id = $conn->insert_id;
        return $last_id;
}
 
// </editor-fold>
// 
// 
// 
// </editor-fold>
// 
function _addPrescriptionSlip($patient_id) {
    
    $query = "INSERT INTO prescription_slip(patient_id) VALUES "
            . "("
            . "'$patient_id'"
            . ")";
    $conn = open_conn();
    $conn->query($query);
    echo $conn->error;
        $last_id = $conn->insert_id;
        return $last_id;
}
function _activeSlipCheck($id) {
    $flag = 0;
    $query = "SELECT id FROM prescription_slip WHERE patient_id='$id' AND is_cleared=0";
    $conn = open_conn();
    $result = $conn->query($query);
    
    close_conn();
    if ($result->num_rows > 0) {
        $flag = $result->fetch_assoc()['id'];
        echo $conn->error;
    }
    return $flag;
}
// <editor-fold defaultstate="collapsed" desc="Create an account for patient">
function _addExternalPatient($emr_id,$principal_emr, $full_name, $phone_number,$type,$created_by) {
    $vital_signs = array('Blood Pressure (mmHg)', 'Glucose (mg/dl)', 'Pulse (beats/min)', 'Respiration (beats/min)', 'Tempreture (oC)');

    $query = "INSERT INTO patient_table(emr_id,principal_emr,"
            . "phone_number,"
            . "full_name,type,"
            . "updated_by) "
            . "VALUES "
            . "("
            . "'$emr_id','$principal_emr',"
            . "'$phone_number',"
            . "'$full_name','$type',"
            . "'$created_by'"
            . ")";
    $conn = open_conn();
    if ($conn->query($query) == TRUE) {
        $length = count($vital_signs);
        for ($i = 0; $i < $length; $i++) {
            $conn->query("INSERT INTO last_vitals_table (emr_id, type) VALUES ('$emr_id','$vital_signs[$i]') ");
        }
        close_conn();
    }
}


function _updateLabStatus($id, $is_available) {
    $query = "UPDATE lab_category_item_table SET is_available='$is_available' WHERE id='$id'";
    $conn = open_conn();
    $conn->query($query);
    echo $conn->error;
    close_conn();
}

function _createBilingAccount($emr) {
    $query = "INSERT INTO patient_billing_account (emr_id) VALUES('$emr')";
    $conn = open_conn();
    $conn->query($query);
    echo $conn->error;
    close_conn();
}

function _updateBillingAccount($emr, $balance) {
    $query = "UPDATE patient_billing_account SET balance='$balance' WHERE emr_id='$emr'";
    $conn = open_conn();
    $conn->query($query);
    close_conn();
}

function _getBillingAccount($emr) {
    $query = "SELECT * FROM patient_billing_account WHERE emr_id='$emr'";
    $conn = open_conn();
    $result = $conn->query($query);
    echo $conn->error;
    $row = $result->fetch_assoc();
    close_conn();
    return $row;
}

function _isBillingAccountAvailable($emr) {
    $flag = FALSE;
    $query = "SELECT * FROM patient_billing_account WHERE emr_id='$emr'";
    $conn = open_conn();
    $result = $conn->query($query);
    if ($result->num_rows > 0) {
        $flag = TRUE;
    }
    return $flag;
}

function _getPatientCount() {
    $query = "SELECT id FROM patient_table";
    $conn = open_conn();
    $result = $conn->query($query);
    $count = $result->num_rows;
    close_conn();
    return $count;
}
function _getPrivatePatientCount() {
    $query = "SELECT id FROM patient_table WHERE type='Private'";
    $conn = open_conn();
    $result = $conn->query($query);
    $count = $result->num_rows;
    close_conn();
    return $count;
}
function _getFamilyPatientCount() {
    $query = "SELECT id FROM patient_table WHERE type='Family'";
    $conn = open_conn();
    $result = $conn->query($query);
    $count = $result->num_rows;
    close_conn();
    return $count;
}
function _getRetainershipPatientCount() {
    $query = "SELECT id FROM patient_table WHERE type='Retainership'";
    $conn = open_conn();
    $result = $conn->query($query);
    $count = $result->num_rows;
    close_conn();
    return $count;
}
//AND  AND"
          //  . "
function _getInsurancePatientCount() {
    $query = "SELECT id FROM patient_table WHERE type <> 'Retainership' AND type <> 'Private' AND type <> 'Family'";
    $conn = open_conn();
    $result = $conn->query($query);
    $count = $result->num_rows;
    close_conn();
    return $count;
}

function _getTodayRegisteredPatientCount() {
    $today = date('Y-m-d');
    $query = "SELECT id FROM patient_table WHERE created_at='$today'";
    $conn = open_conn();
    $result = $conn->query($query);
    $count = $result->num_rows;
    close_conn();
    return $count;
}
function _getHospital() {
    $query = "SELECT * FROM hospital_table";
    $conn = open_conn();
    $result = $conn->query($query);
    close_conn();
    return $result->fetch_assoc();
}

function _getPatient($id) {
    $query = "SELECT * FROM patient_table WHERE id='$id' OR emr_id='$id'";
    $conn = open_conn();
    $result = $conn->query($query);
    close_conn();
    return $result->fetch_assoc();
}

function _filterPatient($id) {
    $items = array();
    $query = "SELECT * FROM patient_table WHERE id='$id' OR emr_id='$id'";
    $conn = open_conn();
    $result = $conn->query($query);
    while ($row = $result->fetch_assoc()) {
        $items[] = $row;
    }
    close_conn();
    return $items;
}

function _filterPatientBy($by) {
    $items = array();
    $query = "SELECT * FROM patient_table WHERE updated_by='$by'";
    $conn = open_conn();
    $result = $conn->query($query);
    while ($row = $result->fetch_assoc()) {
        $items[] = $row;
    }
    echo $conn->error;
    close_conn();
    return $items;
}

function _filterPatientByEMRIDBy($emr_id, $by) {
    $items = array();
    $query = "SELECT * FROM patient_table WHERE updated_by='$by' AND emr_id='$emr_id'";
    $conn = open_conn();
    $result = $conn->query($query);
    while ($row = $result->fetch_assoc()) {
        $items[] = $row;
    }
    echo $conn->error;
    close_conn();
    return $items;
}

function _getTransactionCount() {
    $query = "SELECT id FROM transaction_table";
    $conn = open_conn();
    $result = $conn->query($query);
    $count = $result->num_rows;
    close_conn();
    return $count;
}
function _addAuthCodeToTransation($id,$auth_code){
   $query = "UPDATE transaction_table SET auth_code='$auth_code' WHERE id='$id'";
    $conn = open_conn();
    $conn->query($query);
       close_conn();
   
}
function _getAppointmentCount() {
    $query = "SELECT id FROM visit_table";
    $conn = open_conn();
    $result = $conn->query($query);
    $count = $result->num_rows;
    close_conn();
    return $count;
}

function _getAdmissionCount() {
    $query = "SELECT id FROM admission_table";
    $conn = open_conn();
    $result = $conn->query($query);
    $count = $result->num_rows;
    close_conn();
    return $count;
}

function _getPrescriptionCount() {
    $query = "SELECT prescription_table.id FROM prescription_table INNER JOIN prescription_slip ON prescription_table.prescription_slip_id=prescription_slip.id"
            . "  WHERE prescription_slip.is_cleared=1";
    $conn = open_conn();
    $result = $conn->query($query);
    echo $conn->error;
    $count = $result->num_rows;
    close_conn();
    return $count;
}

function _getLabCount() {
    $query = "SELECT id FROM lab_investigation_table";
    $conn = open_conn();
    $result = $conn->query($query);
    $count = $result->num_rows;
    close_conn();
    return $count;
}

function _getImagingCount() {
    $query = "SELECT id FROM imaging_request_table";
    $conn = open_conn();
    $result = $conn->query($query);
    $count = $result->num_rows;
    close_conn();
    return $count;
}

function _getProcedureCount() {
    $query = "SELECT id FROM procedure_table";
    $conn = open_conn();
    $result = $conn->query($query);
    $count = $result->num_rows;
    close_conn();
    return $count;
}

function _getTodayLabCount() {
    $today = date('Y-m-d');
    $query = "SELECT id FROM lab_investigation_table WHERE request_date>='$today'";
    $conn = open_conn();
    $result = $conn->query($query);
    echo $conn->error;
    $count = $result->num_rows;
    close_conn();
    return $count;
}

function _getTodayImagingCount() {
    $today = date('Y-m-d');
    $query = "SELECT id FROM imaging_request_table WHERE request_date>='$today'";
    $conn = open_conn();
    $result = $conn->query($query);
    echo $conn->error;
    $count = $result->num_rows;
    close_conn();
    return $count;
}

function _getTodayProcedureCount() {
    $today = date('Y-m-d');
    $query = "SELECT id FROM procedure_table WHERE schedule_date='$today'";
    $conn = open_conn();
    $result = $conn->query($query);
    echo $conn->error;
    $count = $result->num_rows;
    close_conn();
    return $count;
}

function _getCurrentPrescriptionCount() {
    $today = date('Y-m-d');
    $query = "SELECT prescription_table.id FROM prescription_table INNER JOIN prescription_slip ON prescription_table.prescription_slip_id=prescription_slip.id"
            . "  WHERE prescription_slip.is_cleared=1 AND prescription_date>='$today'";
    $conn = open_conn();
    $result = $conn->query($query);
    echo $conn->error;
    $count = $result->num_rows;
    close_conn();
    return $count;
}

function _getTodayPhPayment() {
    $today = date('Y-m-d');
    $query = "SELECT SUM(price) FROM prescription_table INNER JOIN prescription_slip ON prescription_table.prescription_slip_id=prescription_slip.id"
            . "  WHERE prescription_slip.is_cleared=1 AND prescription_date>='$today'";
    $conn = open_conn();
    $result = $conn->query($query);
    $count = $result->fetch_assoc();
    echo $conn->error;
    close_conn();
    return $count;
}

function _getTodayTransactionCount() {
    $query = "SELECT id FROM transaction_table WHERE transaction_date_=" . TODAY;
    $conn = open_conn();
    $result = $conn->query($query);
    $count = $result->num_rows;
    close_conn();
    return $count;
}

function _getCurrentAdmissionCount() {
    $query = "SELECT id FROM admission_table WHERE is_admitted=1 AND is_discharged=0";
    $conn = open_conn();
    $result = $conn->query($query);
    echo $conn->error;
    $count = $result->num_rows;
    close_conn();
    return $count;
}

function _getTodayAppointmentCount() {
    $query = "SELECT id FROM visit_table WHERE visit_date=" . TODAY;
    $conn = open_conn();
    $result = $conn->query($query);
    $count = $result->num_rows;
    close_conn();
    return $count;
}

function _getLastVitals($emr_id) {
    $signs = array();
    $query = "SELECT type, reading "
            . "FROM last_vitals_table "
            . "WHERE emr_id='$emr_id'";
    $conn = open_conn();

    $result = $conn->query($query);
    echo $conn->error;
    while ($row = $result->fetch_array()) {
        $signs[] = $row;
    }

    close_conn();
    return $signs;
}

function _fetchClinic() {
    $clinics = array();
    $query = "SELECT * FROM clinic_table ";
    $conn = open_conn();
    $result = $conn->query($query);
    while ($row = $result->fetch_assoc()) {
        $clinics[] = $row;
    }
    close_conn();
    return $clinics;
}

function _fetchHMO() {
    $items = array();
    $query = "SELECT * FROM insurance_table";
    $conn = open_conn();
    $result = $conn->query($query);
    while ($row = $result->fetch_assoc()) {
        $items[] = $row;
    }
    close_conn();
    return $items;
}

function _fetchClinicRoom($id) {
    $clinics = array();
    $query = "SELECT * FROM clinic_room_table WHERE clinic_id='$id'";
    $conn = open_conn();
    $result = $conn->query($query);
    while ($row = $result->fetch_assoc()) {
        $clinics[] = $row;
    }
    close_conn();
    return $clinics;
}

function _fetchClinicWard($id) {
    $clinics = array();
    $query = "SELECT * FROM ward_table WHERE clinic_id='$id'";
    $conn = open_conn();
    $result = $conn->query($query);
    while ($row = $result->fetch_assoc()) {
        $clinics[] = $row;
    }
    close_conn();
    return $clinics;
}

function _fetchWardLocation($id) {
    $clinics = array();
    $query = "SELECT * FROM ward_location_table WHERE ward_id='$id'";
    $conn = open_conn();
    $result = $conn->query($query);
    while ($row = $result->fetch_assoc()) {
        $clinics[] = $row;
    }
    close_conn();
    return $clinics;
}

function _getClinicId($name) {

    $query = "SELECT id FROM clinic_table WHERE clinic='$name' ";
    $conn = open_conn();
    $result = $conn->query($query);
    close_conn();
    return $result->fetch_assoc();
}

function _getWardId($name) {

    $query = "SELECT * FROM ward_table WHERE ward='$name' ";
    $conn = open_conn();
    $result = $conn->query($query);
    close_conn();
    return $result->fetch_assoc();
}

function _fetchAppointmentTypes() {
    $items = array();
    $query = "SELECT * FROM service_table WHERE category='Appointment'";
    $conn = open_conn();
    $result = $conn->query($query);
    while ($row = $result->fetch_assoc()) {
        $items[] = $row;
    }
    close_conn();
    return $items;
}

function _requestAdmission($emr_id, $visit_id, $reason_for_admission, $admission_date, $clinic, $requested_by) {
    $flag = FALSE;
    $query = "INSERT INTO admission_table(emr_id,visit_id,"
            . "reason_for_admission,"
            . "admission_date,"
            . "clinic,"
            . "requested_by) "
            . "VALUES "
            . "("
            . "'$emr_id','$visit_id',"
            . "'$reason_for_admission',"
            . "'$admission_date',"
            . "'$clinic',"
            . "'$requested_by'"
            . ")";
    $conn = open_conn();
    $conn->query($query);

    echo $conn->error;
    close_conn();
}

function _isAdmissionRequestExist($emr_id) {
    $row = 0;
    $query = "SELECT id FROM admission_table WHERE emr_id='$emr_id' AND is_admitted=0";
    $conn = open_conn();
    $result = $conn->query($query);
    close_conn();
    if ($result->num_rows > 0) {
        $row = $result->fetch_assoc()['id'];
    }
    return $row;
}

function _isSimilarAppointmentExist($emr_id,$visit_type, $clinic) {
    $row = 0;
    $query = "SELECT id FROM visit_table WHERE emr_id='$emr_id' AND visit_type='$visit_type' AND clinic='$clinic' AND is_signed=0";
    $conn = open_conn();
    $result = $conn->query($query);
    close_conn();
    if ($result->num_rows > 0) {
        $row = $result->fetch_assoc()['id'];
    }
    return $row;
}

function _updateAdmission($id, $emr_id, $visit_id, $reason_for_admission, $admission_date, $clinic, $requested_by) {
    $query = "UPDATE admission_table SET emr_id='$emr_id',visit_id='$visit_id',"
            . "reason_for_admission='$reason_for_admission',"
            . "admission_date='$admission_date',"
            . "clinic='$clinic',"
            . "requested_by='$requested_by' WHERE id='$id'";
    $conn = open_conn();
    $conn->query($query);

    echo $conn->error;
    close_conn();
}

function _assignBed($id, $clinic, $ward, $location, $admission_date, $requested_by) {
    $query = "UPDATE admission_table SET is_admitted=1, clinic='$clinic',ward='$ward',"
            . "location='$location',admission_date='$admission_date',"
            . "assign_bed_by='$requested_by' WHERE id='$id'";
    $conn = open_conn();
    $conn->query($query);

    echo $conn->error;
    close_conn();
}

function _changeBed($id, $clinic, $ward, $location, $requested_by) {
    $query = "UPDATE admission_table SET  clinic='$clinic',ward='$ward',"
            . "location='$location',"
            . "assign_bed_by='$requested_by' WHERE id='$id'";
    $conn = open_conn();
    $conn->query($query);

    echo $conn->error;
    close_conn();
}

function _updateAppointment($id, $emr_id, $visit_type, $visit_date, $clinic, $room, $booked_by) {
    $flag = FALSE;
    $query = "UPDATE visit_table SET emr_id='$emr_id',visit_type='$visit_type',"
            . "visit_date='$visit_date',"
            . "clinic='$clinic',"
            . "room='$room', booked_by='$booked_by'"
            . " WHERE id='$id'";
    $conn = open_conn();
    $conn->query($query);

    echo $conn->error;
    close_conn();
}

function _bookAppointment($emr_id, $visit_type,$price, $visit_date, $clinic, $room, $booked_by) {
    $query = "INSERT INTO visit_table(emr_id,"
            . "visit_type,price, visit_date,"
            . "clinic,room,booked_by) "
            . "VALUES "
            . "("
            . "'$emr_id',"
            . "'$visit_type','$price','$visit_date',"
            . "'$clinic', '$room','$booked_by'"
            . ")";
    $conn = open_conn();
    $conn->query($query);
    $last_id = $conn->insert_id;
    echo $conn->error;
    close_conn();
    return $last_id;
}

function _getAppointment($id) {
    $query = "SELECT emr_id,visit_type,visit_date,clinic,room FROM visit_table WHERE id='$id' ";
    $conn = open_conn();
    $result = $conn->query($query);
    $row = $result->fetch_assoc();
    close_conn();
    return $row;
}

function _getPrincipalEMRID($emr) {
    $query = "SELECT principal_emr FROM patient_table WHERE emr_id='$emr' ";
    $conn = open_conn();
    $result = $conn->query($query);
    $row = $result->fetch_assoc()['principal_emr'];
    close_conn();
    return $row;
}

function _fetchLabCategory() {
    $items = array();
    $query = "SELECT * FROM lab_category_table ";
    $conn = open_conn();
    $result = $conn->query($query);
    while ($row = $result->fetch_assoc()) {
        $items[] = $row;
    }
    close_conn();
    return $items;
}

function _fetchDrugMeasure() {
    $items = array();
    $query = "SELECT * FROM drug_measure_table ";
    $conn = open_conn();
    $result = $conn->query($query);
    while ($row = $result->fetch_assoc()) {
        $items[] = $row;
    }
    close_conn();
    return $items;
}

function _getLabCategory($id) {

    $query = "SELECT name FROM lab_category_table WHERE id='$id' ";
    $conn = open_conn();
    $result = $conn->query($query);
    $row = $result->fetch_assoc();
    $item = $row != NULL ? $row['name'] : NULL;
    close_conn();
    return $item;
}

function _fetchProcedureList() {
    $items = array();
    $query = "SELECT * FROM procedure_list_table ";
    $conn = open_conn();
    $result = $conn->query($query);
    while ($row = $result->fetch_assoc()) {
        $items[] = $row;
    }
    close_conn();
    return $items;
}

function _fetchLabCategoryItem($category_id) {
    $items = array();
    $query = "SELECT * FROM lab_category_item_table WHERE lab_category_id='$category_id' AND is_available=1";
    $conn = open_conn();
    $result = $conn->query($query);
    while ($row = $result->fetch_assoc()) {
        $items[] = $row;
    }
    close_conn();
    return $items;
}

function _fetchDrug() {
    $stocks = array();
    $query = "SELECT * FROM drug_table ";
    $conn = open_conn();

    $result = $conn->query($query);
    echo $conn->error;
    while ($row = $result->fetch_assoc()) {
        $stocks[] = $row;
    }

    close_conn();
    return $stocks;
}
function _fetchDrugBatch() {
    $stocks = array();
    $query = "SELECT * FROM drug_batch_table ";
    $conn = open_conn();

    $result = $conn->query($query);
    echo $conn->error;
    while ($row = $result->fetch_assoc()) {
        $stocks[] = $row;
    }

    close_conn();
    return $stocks;
}
function _fetchAvailableDrugBatch() {
    $stocks = array();
    $query = "SELECT * FROM drug_batch_table WHERE quantity > 1";
    $conn = open_conn();

    $result = $conn->query($query);
    echo $conn->error;
    while ($row = $result->fetch_assoc()) {
        $stocks[] = $row;
    }

    close_conn();
    return $stocks;
}
function _fetchRadiologyScan() {
    $items = array();
    $query = "SELECT * FROM radiology_table ";
    $conn = open_conn();
    $result = $conn->query($query);
    echo $conn->error;
    while ($row = $result->fetch_assoc()) {
        $items[] = $row;
    }
    close_conn();
    return $items;
}

function _fetchVitalsType() {
    $items = array();
    $query = "SELECT * FROM vitals_type_table ";
    $conn = open_conn();
    $result = $conn->query($query);
    echo $conn->error;
    while ($row = $result->fetch_assoc()) {
        $items[] = $row;
    }
    close_conn();
    return $items;
}

function _getPatientVitals($emr_id, $type) {
    $items = array();
    $query = "SELECT id, emr_id, type, reading, checked_by, DATE_FORMAT(reading_date, '%Y %e %b, %a  %h:%i:%p') AS reading_date FROM vital_sign_table WHERE emr_id='$emr_id' AND type='$type' ORDER BY id DESC";
    $conn = open_conn();
    $result = $conn->query($query);
    echo $conn->error;
    while ($row = $result->fetch_assoc()) {
        $items[] = $row;
    }
    close_conn();
    return $items;
}

function _getLastVital($emr_id, $type) {

    $query = "SELECT reading FROM last_vitals_table WHERE emr_id='$emr_id' AND type='$type'";
    $conn = open_conn();
    $result = $conn->query($query);
    echo $conn->error;
    close_conn();
    return $result->fetch_assoc();
}

function _requestLab($visit_id, $emr_id,$price, $lab_test, $request_note, $requested_by) {
    $last_id = "";
    $query = "INSERT INTO lab_investigation_table(visit_id,emr_id,"
            . "price,lab_test,"
            . "request_note, requested_by) "
            . "VALUES "
            . "("
            . "'$visit_id',"
            . "'$emr_id','$price',"
            . "'$lab_test','$request_note','$requested_by'"
            . ")";
    $conn = open_conn();
    if ($conn->query($query) == TRUE) {
        $last_id = $conn->insert_id;
    } else {
        echo $conn->error;
    }
    close_conn();
    return $last_id;
}
function _initiateLabSeperateSettlement($id) {
    $query = "UPDATE lab_investigation_table SET seperate_settlement=1 "
            . "WHERE id='$id'";
    $conn = open_conn();
    $conn->query($query);
    echo $conn->error;
    close_conn();
}

function _initiateRecordSeperateSettlement($id) {
    $query = "UPDATE visit_table SET seperate_settlement=1 "
            . "WHERE id='$id'";
    $conn = open_conn();
    $conn->query($query);
    echo $conn->error;
    close_conn();
}
function _initiatePharmacySeperateSettlement($id) {
    $query = "UPDATE prescription_table SET seperate_settlement=1 "
            . "WHERE id='$id'";
    $conn = open_conn();
    $conn->query($query);
    echo $conn->error;
    close_conn();
}
function _fillLabResult($id, $result, $conducted_by) {
    $query = "UPDATE lab_investigation_table SET result='$result', conducted_by='$conducted_by' "
            . "WHERE id='$id'";
    $conn = open_conn();
    $conn->query($query);
    echo $conn->error;
    close_conn();
}
function _uploadLabResult($id, $result, $conducted_by) {
    $query = "UPDATE lab_investigation_table SET attachment='$result', conducted_by='$conducted_by' "
            . "WHERE id='$id'";
    $conn = open_conn();
    $conn->query($query);
    echo $conn->error;
    close_conn();
}

function _fillPrescription($id,$price,$quantity, $served_by) {
    $query = "UPDATE prescription_table SET is_served=1, price='$price', served_quantity='$quantity', served_by='$served_by' "
            . "WHERE id='$id'";
    $conn = open_conn();
    $conn->query($query);
    echo $conn->error;
    close_conn();
}

function _addVitals($emr_id, $type, $reading, $checked_by) {
    $query = "INSERT INTO vital_sign_table(emr_id,"
            . "type,"
            . "reading, checked_by) "
            . "VALUES "
            . "("
            . "'$emr_id',"
            . "'$type','$reading','$checked_by'"
            . ")";
    $conn = open_conn();
    $conn->query($query);
    echo $conn->error;
    close_conn();
}

function _addLabCategory($name) {
    $query = "INSERT INTO lab_category_table(name) VALUES ('$name')";
    $conn = open_conn();
    $conn->query($query);
    echo $conn->error;
    close_conn();
}
function _addHMO($name,$abbr,$phone_number) {
    $query = "INSERT INTO insurance_table(hmo,hmo_abbr,phone_number) VALUES ('$name','$abbr','$phone_number')";
    $conn = open_conn();
    $conn->query($query);
    echo $conn->error;
    close_conn();
}

function _isLabExist($name) {
    $row = 0;
    $query = "SELECT id FROM lab_category_item_table WHERE name='$name'";
    $conn = open_conn();
    $result = $conn->query($query);
    close_conn();
    if ($result->num_rows > 0) {
        $row = $result->fetch_assoc()['id'];
    }
    return $row;
}

function _isClinicExist($name) {
    $row = 0;
    $query = "SELECT id FROM clinic_table WHERE clinic='$name'";
    $conn = open_conn();
    $result = $conn->query($query);
    close_conn();
    if ($result->num_rows > 0) {
        $row = $result->fetch_assoc()['id'];
    }
    return $row;
}

function _isRoomExist($id, $name) {
    $row = 0;
    $query = "SELECT id FROM clinic_room_table WHERE room='$name' AND clinic_id='$id'";
    $conn = open_conn();
    $result = $conn->query($query);
    close_conn();
    if ($result->num_rows > 0) {
        $row = $result->fetch_assoc()['id'];
    }
    return $row;
}


function _isWardExist($id, $name) {
    $row = 0;
    $query = "SELECT id FROM ward_table WHERE ward='$name' AND clinic_id='$id'";
    $conn = open_conn();
    $result = $conn->query($query);
    close_conn();
    if ($result->num_rows > 0) {
        $row = $result->fetch_assoc()['id'];
    }
    return $row;
}

function _isBedRoomExist($id, $name) {
    $row = 0;
    $query = "SELECT id FROM ward_location_table WHERE location='$name' AND ward_id='$id'";
    $conn = open_conn();
    $result = $conn->query($query);
    close_conn();
    if ($result->num_rows > 0) {
        $row = $result->fetch_assoc()['id'];
    }
    return $row;
}

function _isMeasureExist($name) {
    $row = 0;
    $query = "SELECT id FROM drug_measure_table WHERE name='$name'";
    $conn = open_conn();
    $result = $conn->query($query);
    close_conn();
    if ($result->num_rows > 0) {
        $row = $result->fetch_assoc()['id'];
    }
    return $row;
}

function _isdrugExist($name) {
    $row = 0;
    $query = "SELECT id FROM drug_table WHERE name='$name'";
    $conn = open_conn();
    $result = $conn->query($query);
    close_conn();
    if ($result->num_rows > 0) {
        $row = $result->fetch_assoc()['id'];
    }
    return $row;
}

function _isBatchExist($no) {
    $row = 0;
    $query = "SELECT id FROM drug_batch_table WHERE batch_no='$no'";
    $conn = open_conn();
    $result = $conn->query($query);
    echo $conn->error;
    close_conn();
    if ($result->num_rows > 0) {
        $row = $result->fetch_assoc()['id'];
    }
    return $row;
}

function _isScanExist($name) {
    $row = 0;
    $query = "SELECT id FROM radiology_table WHERE name='$name'";
    $conn = open_conn();
    $result = $conn->query($query);
    close_conn();
    if ($result->num_rows > 0) {
        $row = $result->fetch_assoc()['id'];
    }
    return $row;
}

function _isProcedureServiceExist($name) {
    $row = 0;
    $query = "SELECT id FROM procedure_list_table WHERE name='$name'";
    $conn = open_conn();
    $result = $conn->query($query);
    close_conn();
    if ($result->num_rows > 0) {
        $row = $result->fetch_assoc()['id'];
    }
    return $row;
}

function _addMeasure($name) {
    $query = "INSERT INTO drug_measure_table(name) VALUES('$name')";
    $conn = open_conn();
    $conn->query($query);
    close_conn();
}

function _addDrug($name, $unit_of_measure, $reorder_level, $updated_by) {
   
    $query = "INSERT INTO drug_table(name,unit_of_measure,reorder_level,updated_by) "
            . "VALUES('$name','$unit_of_measure',$reorder_level,'$updated_by')";
    $conn = open_conn();
    $conn->query($query);
    echo $conn->error;
    close_conn();
}
function _addBatch($drug_id,$name,$batch, $quantity, $unit_price,$expire_date,$updated_by){
    $query = "INSERT INTO drug_batch_table(drug_id,drug_name,batch_no,quantity,unit_price,expiration_date,created_by) "
            . "VALUES('$drug_id','$name','$batch','$quantity','$unit_price','$expire_date','$updated_by')";
    $conn = open_conn();
    $conn->query($query);
    echo $conn->error;
    close_conn();  
}
function _addClinic($name, $updated_by) {
    $query = "INSERT INTO clinic_table(clinic,updated_by) "
            . "VALUES('$name','$updated_by')";
    $conn = open_conn();
    $conn->query($query);
    close_conn();
}

function _addClinicRoom($id, $name, $updated_by) {
    $query = "INSERT INTO clinic_room_table(clinic_id,room,updated_by) "
            . "VALUES('$id','$name','$updated_by')";
    $conn = open_conn();
    $conn->query($query);
    echo $conn->error;
    close_conn();
}

function _addWard($id, $name, $price, $updated_by) {
    $query = "INSERT INTO ward_table(clinic_id,ward,price,updated_by) "
            . "VALUES('$id','$name','$price','$updated_by')";
    $conn = open_conn();
    $conn->query($query);
    echo $conn->error;
    close_conn();
}

function _addBed($id, $name, $updated_by) {
    $query = "INSERT INTO ward_location_table(ward_id,location,updated_by) "
            . "VALUES('$id','$name','$updated_by')";
    $conn = open_conn();
    $conn->query($query);
    echo $conn->error;
    close_conn();
}

function _updateBedRoom($id, $name, $updated_by) {
    $query = "UPDATE ward_location_table SET location='$name',updated_by='$updated_by' "
            . "WHERE id='$id'";
    $conn = open_conn();
    $conn->query($query);
    echo $conn->error;
    close_conn();
}

function _updateWard($id, $name, $price, $updated_by) {
    $query = "UPDATE ward_table SET ward='$name',updated_by='$updated_by', price='$price' "
            . "WHERE id='$id'";
    $conn = open_conn();
    $conn->query($query);
    echo $conn->error;
    close_conn();
}

function _updateClinicRoom($id, $name, $updated_by) {
    $query = "UPDATE clinic_room_table SET room='$name',updated_by='$updated_by' "
            . "WHERE id='$id'";
    $conn = open_conn();
    $conn->query($query);
    echo $conn->error;
    close_conn();
}
function _updateHMO($id,$name,$abbr,$phone_number){
  $query = "UPDATE insurance_table SET hmo='$name',hmo_abbr='$abbr',"
          . "phone_number='$phone_number' "
            . "WHERE id='$id'";
    $conn = open_conn();
    $conn->query($query);
    echo $conn->error;
    close_conn();  
}
function _updateClinic($id, $name, $updated_by) {
    $query = "UPDATE clinic_table SET clinic='$name',updated_by='$updated_by' WHERE id='$id'";
    $conn = open_conn();
    $conn->query($query);
    close_conn();
}

function _addRadiologyScan($name, $price, $last_update) {
    $query = "INSERT INTO radiology_table(name,price,last_update) "
            . "VALUES('$name','$price','$last_update')";
    $conn = open_conn();
    $conn->query($query);
    close_conn();
}

function _addProcedureService($name, $price, $last_update) {
    $query = "INSERT INTO procedure_list_table(name,price,updated_by) "
            . "VALUES('$name','$price','$last_update')";
    $conn = open_conn();
    $conn->query($query);
    close_conn();
}

function _updateRadiologyScan($id, $name, $price, $last_update) {
    $query = "UPDATE radiology_table SET name='$name',price='$price',last_update='$last_update' WHERE id='$id' ";
    $conn = open_conn();
    $conn->query($query);
    echo $conn->error;
    close_conn();
}

function _updateProcedureService($id, $name, $price, $last_update) {
    $query = "UPDATE procedure_list_table SET name='$name',price='$price',updated_by='$last_update' WHERE id='$id' ";
    $conn = open_conn();
    $conn->query($query);
    echo $conn->error;
    close_conn();
}

function _changeRecordServicePrice($id, $price, $signature) {
    $query = "UPDATE service_table SET price='$price', updated_by='$signature' WHERE id='$id' ";
    $conn = open_conn();
    $conn->query($query);
    echo $conn->error;
    close_conn();
}

function _isLabCategoryExist($name) {
    $row = 0;
    $query = "SELECT id FROM lab_category_table WHERE name='$name'";
    $conn = open_conn();
    $result = $conn->query($query);
    close_conn();
    if ($result->num_rows > 0) {
        $row = $result->fetch_assoc()['id'];
    }
    return $row;
}

function _isHMOExist($name,$abbr,$phone_number) {
   
    $query = "SELECT id FROM insurance_table WHERE hmo='$name' OR hmo_abbr='$abbr' OR phone_number='$phone_number'";
    $conn = open_conn();
    $result = $conn->query($query);
    close_conn();
    return $result->num_rows;
}

function _addLabType($name, $category,$result, $price, $updated_by) {
    $query = "INSERT INTO lab_category_item_table(name,lab_category_id,result_template,price,updated_by) VALUES ('$name','$category','$result','$price','$updated_by')";
    $conn = open_conn();
    $conn->query($query);
    echo $conn->error;
    close_conn();
}

function _editLabType($id, $name, $category,$result, $price, $signature) {
    $query = "UPDATE lab_category_item_table SET name='$name', lab_category_id='$category',"
            . " result_template='$result',price='$price',updated_by='$signature' "
            . "WHERE id='$id'";
    $conn = open_conn();
    $conn->query($query);
    echo $conn->error;
    close_conn();
}

function _updateVitals($emr_id, $type, $reading, $checked_by) {
    $query = "UPDATE last_vitals_table SET reading='$reading', checked_by='$checked_by' "
            . "WHERE emr_id='$emr_id' AND type='$type'";
    $conn = open_conn();
    $conn->query($query);
    echo $conn->error;
    close_conn();
}

function _requestImaging($visit_id, $emr_id, $name,$price, $request_note, $requested_by) {
    $last_id = "";
    $query = "INSERT INTO imaging_request_table(visit_id,emr_id,"
            . "name,price,"
            . "request_note, requested_by) "
            . "VALUES "
            . "("
            . "'$visit_id',"
            . "'$emr_id',"
            . "'$name','$price','$request_note','$requested_by'"
            . ")";
    $conn = open_conn();
    if ($conn->query($query) == TRUE) {
        $last_id = $conn->insert_id;
    } else {
        echo $conn->error;
    }
    close_conn();
    return $last_id;
}

function _requestProcedure($visit_id, $emr_id, $name,$price, $primary_diagnosis, $request_note, $schedule_date, $bill, $requested_by) {
    $last_id = "";
    $query = "INSERT INTO procedure_table(visit_id,emr_id,"
            . "name,price,primary_diagnosis,schedule_date,"
            . "request_note, requested_by,bill) "
            . "VALUES "
            . "("
            . "'$visit_id',"
            . "'$emr_id',"
            . "'$name','$price','$primary_diagnosis','$schedule_date','$request_note',"
            . "'$requested_by','$bill'"
            . ")";
    $conn = open_conn();
    if ($conn->query($query) == TRUE) {
        $last_id = $conn->insert_id;
    } else {
        echo $conn->error;
    }
    close_conn();
    return $last_id;
}
function _getPrescriptionPrice($id){
    $query = "SELECT SUM(price) FROM prescription_table WHERE prescription_slip_id='$id'";
    
    $conn = open_conn();
    $result = $conn->query($query);
    echo $conn->error;
    $row = $result->fetch_assoc();
    close_conn();
    return $row;
}
function _requestPrescription($ps_id, $drug_name,$unit,$batch_no, $qty,$price) {
   
    $query = "INSERT INTO prescription_table(prescription_slip_id,drug_name,unit,batch_no,served_quantity, price)" 
            . "VALUES "
            . "("
            . "'$ps_id',"
            . "'$drug_name','$unit','$batch_no','$qty','$price'"
            . ")";
    $conn = open_conn();
    $conn->query($query);
     echo $conn->error;
        $last_id = $conn->insert_id;

       

    close_conn();
    return $last_id;
}

function _addProcedureMedication($procedure_id, $drug_name, $dose, $duration, $requested_by) {
    $last_id = "";
    $query = "INSERT INTO procedure_medication_table(procedure_id,"
            . "drug_name,dose,"
            . "duration, prescribed_by) "
            . "VALUES "
            . "("
            . "'$procedure_id',"
            . "'$drug_name','$dose','$duration','$requested_by'"
            . ")";
    $conn = open_conn();
    $conn->query($query);
    echo $conn->error;
    close_conn();
    return $last_id;
}

function _deleteLab($id) {
    $query = "DELETE  FROM lab_investigation_table WHERE id='$id'";
    $conn = open_conn();
    $conn->query($query);
    echo $conn->error;
    close_conn();
}
function _cancelVisit($id) {
    $query = "DELETE  FROM visit_table WHERE id='$id'";
    $conn = open_conn();
    $conn->query($query);
    echo $conn->error;
    close_conn();
}
function _cancelAdmission($id){
   $query = "DELETE  FROM admission_table WHERE id='$id'";
    $conn = open_conn();
    $conn->query($query);
    echo $conn->error;
    close_conn();  
}

function  _deleteDrugBatch($id) {
    $query = "DELETE  FROM drug_batch_table WHERE id='$id'";
    $conn = open_conn();
    $conn->query($query);
    echo $conn->error;
    close_conn();
}

function  _deleteDrug($id) {
    $query = "DELETE  FROM drug_table WHERE id='$id'";
    $conn = open_conn();
    $conn->query($query);
    echo $conn->error;
    close_conn();
}


function _deleteUser($id) {
    $query = "DELETE  FROM user_table WHERE id='$id'";
    $query1 = "DELETE  FROM privilege_table WHERE user_id='$id'";
    $conn = open_conn();
    $conn->query($query);
     $conn->query($query1);
    echo $conn->error;
    close_conn();
}


function _deletePrescription($id) {
    $query = "DELETE  FROM prescription_table WHERE id='$id'";
    $conn = open_conn();
    $conn->query($query);
    echo $conn->error;
    close_conn();
}


 function _getPrescription($id){
     $query = "SELECT batch_no, served_quantity  FROM prescription_table WHERE id='$id'";
   $conn = open_conn();
    $result = $conn->query($query);
    echo $conn->error;
    $row = $result->fetch_assoc();
       
    return $row;  
 }
function _fetchPrescription($id){
    $query = "SELECT * FROM prescription_table WHERE prescription_slip_id='$id'";
   $conn = open_conn();
    $result = $conn->query($query);
    echo $conn->error;
    while ($row = $result->fetch_assoc()) {
        $items[] = $row;
    }
    close_conn();
    return $items;
}
 function _getPtInfo($id){
     $query = "SELECT full_name, phone_number FROM patient_table INNER JOIN prescription_slip"
             . " ON patient_table.id=prescription_slip.patient_id WHERE prescription_slip.id='$id'";
   $conn = open_conn();
    $result = $conn->query($query);
    echo $conn->error;
    $row = $result->fetch_assoc();
        
    close_conn();
    return $row;  
 }
function servePrescription($id){
     $query = "UPDATE prescription_slip SET is_cleared=1  WHERE id='$id' ";
    $conn = open_conn();
    $conn->query($query);
    echo $conn->error;
    close_conn();
}
function _getVisitInvestigation($visit_id) {
    $items = array();
    $query = "SELECT *, DATE_FORMAT(request_date, '%Y %e %b, %a  %h:%i:%p') AS request_date FROM lab_investigation_table WHERE visit_id='$visit_id' ORDER BY request_date DESC";
    $conn = open_conn();
    $result = $conn->query($query);
    echo $conn->error;
    while ($row = $result->fetch_assoc()) {
        $items[] = $row;
    }
    close_conn();
    return $items;
}

function _getVisitPrescription($id) {
    $items = array();
    $query = "SELECT * FROM prescription_table WHERE prescription_slip_id='$id'";
    $conn = open_conn();
    $result = $conn->query($query);
    echo $conn->error;
    while ($row = $result->fetch_assoc()) {
        $items[] = $row;
    }
    close_conn();
    return $items;
}

function _getVisitImagingRequest($visit_id) {
    $items = array();
    $query = "SELECT *, DATE_FORMAT(request_date, '%Y %e %b, %a  %h:%i:%p') AS request_date FROM imaging_request_table WHERE visit_id='$visit_id' ORDER BY id DESC";
    $conn = open_conn();
    $result = $conn->query($query);
    echo $conn->error;
    while ($row = $result->fetch_assoc()) {
        $items[] = $row;
    }
    close_conn();
    return $items;
}

function _getVisit($emr_id) {
    $items = array();
    $query = "SELECT *, DATE_FORMAT(visit_date, '%Y %e %b, %a  %h:%i:%p') AS visit_date FROM visit_table WHERE emr_id='$emr_id' ORDER BY id DESC";
    $conn = open_conn();
    $result = $conn->query($query);
    echo $conn->error;
    while ($row = $result->fetch_assoc()) {
        $items[] = $row;
    }
    close_conn();
    return $items;
}

function _getPrescriptions($drug_name) {
    $items = array();
    $query = "SELECT *, DATE_FORMAT(prescription_date, '%Y %e %b, %a  %h:%i:%p') AS prescription_date FROM prescription_table "
            . "INNER JOIN prescription_slip ON prescription_table.prescription_slip_id=prescription_slip.id"
            . "  WHERE prescription_slip.is_cleared=1 AND drug_name='$drug_name' ORDER BY prescription_table.id DESC";
    $conn = open_conn();
    $result = $conn->query($query);
    echo $conn->error;
    while ($row = $result->fetch_assoc()) {
        $items[] = $row;
    }
    close_conn();
    return $items;
}

function _getPrescriptionsOutstanding($emr_id) {
    $items = array();
    $query = "SELECT *, DATE_FORMAT(prescription_date, '%Y %e %b, %a  %h:%i:%p') AS prescription_date FROM prescription_table WHERE emr_id='$emr_id' AND is_served=1 AND seperate_settlement=0 ORDER BY id DESC";
    $conn = open_conn();
    $result = $conn->query($query);
    echo $conn->error;
    while ($row = $result->fetch_assoc()) {
        $items[] = $row;
    }
    close_conn();
    return $items;
}

function _fetchPrescriptions($id) {
    $items = array();
    $query = "SELECT * FROM prescription_table WHERE prescription_slip_id='$id' ";
    $conn = open_conn();
    $result = $conn->query($query);
    echo $conn->error;
    while ($row = $result->fetch_assoc()) {
        $items[] = $row;
    }
    close_conn();
    return $items;
}

function _getProcedureMedication($procedure_id) {
    $items = array();
    $query = "SELECT *, DATE_FORMAT(prescribed_date, '%Y %e %b, %a  %h:%i:%p') AS prescribed_date FROM procedure_medication_table WHERE procedure_id='$procedure_id' ORDER BY id DESC";
    $conn = open_conn();
    $result = $conn->query($query);
    echo $conn->error;
    while ($row = $result->fetch_assoc()) {
        $items[] = $row;
    }
    close_conn();
    return $items;
}

function _getProcedureAttachment($procedure_id) {
    $items = array();
    $query = "SELECT *, DATE_FORMAT(attached_date, '%Y %e %b, %a  %h:%i:%p') AS attached_date FROM procedure_attachment_table WHERE procedure_id='$procedure_id' ORDER BY id DESC";
    $conn = open_conn();
    $result = $conn->query($query);
    echo $conn->error;
    while ($row = $result->fetch_assoc()) {
        $items[] = $row;
    }
    close_conn();
    return $items;
}

function _uploadPassport($emr, $passport, $updated_by) {
    $query = "UPDATE patient_table SET passport='$passport', updated_by='$updated_by' WHERE emr_id='$emr' ";
    $conn = open_conn();
    $conn->query($query);
    echo $conn->error;
    close_conn();
}

function _uploadAttachment($procedure_id, $description, $attachment, $attached_by) {
    $query = "INSERT INTO procedure_attachment_table (procedure_id,description,attachment,attached_by) "
            . "VALUES('$procedure_id','$description','$attachment','$attached_by')";
    $conn = open_conn();
    $conn->query($query);
    echo $conn->error;
    close_conn();
}

function _uploadRadiologyResult($id, $result, $result_note, $uploaded_by) {
    $query = "UPDATE imaging_request_table SET result='$result', result_note='$result_note', "
            . "uploaded_by='$uploaded_by' WHERE id='$id'";
    $conn = open_conn();
    $conn->query($query);
    echo $conn->error;
    close_conn();
}

function _getlabInvestigations($emr_id) {
    $items = array();
    $query = "SELECT *, DATE_FORMAT(request_date, '%Y %e %b, %a  %h:%i:%p') AS request_date FROM lab_investigation_table WHERE emr_id='$emr_id' ORDER BY id DESC";
    $conn = open_conn();
    $result = $conn->query($query);
    echo $conn->error;
    while ($row = $result->fetch_assoc()) {
        $items[] = $row;
    }
    close_conn();
    return $items;
}

function _fetchlabRequest() {
    $items = array();
    $query = "SELECT lab_investigation_table.price, lab_investigation_table.id, lab_test, requested_by, lab_investigation_table.emr_id,patient_table.full_name, DATE_FORMAT(request_date, '%Y %e %b, %a  %h:%i:%p') AS request_date "
            . " FROM lab_investigation_table INNER JOIN patient_table "
            . " ON lab_investigation_table.emr_id=patient_table.emr_id WHERE result='' AND attachment='' ORDER BY id ASC";
    $conn = open_conn();
    $result = $conn->query($query);
    echo $conn->error;
    while ($row = $result->fetch_assoc()) {
        $items[] = $row;
    }
    close_conn();
    return $items;
}
function _getlabRequest($emr_id) {
    $items = array();
    $query = "SELECT lab_investigation_table.price, lab_investigation_table.id, lab_test, requested_by, lab_investigation_table.emr_id,patient_table.full_name, DATE_FORMAT(request_date, '%Y %e %b, %a  %h:%i:%p') AS request_date "
            . " FROM lab_investigation_table INNER JOIN patient_table "
            . " ON lab_investigation_table.emr_id=patient_table.emr_id WHERE  lab_investigation_table.emr_id='$emr_id' AND seperate_settlement=0 AND result='' AND attachment='' ORDER BY id ASC";
    $conn = open_conn();
    $result = $conn->query($query);
    echo $conn->error;
    while ($row = $result->fetch_assoc()) {
        $items[] = $row;
    }
    close_conn();
    return $items;
}

function _fetchLab() {
    $items = array();
    $query = "SELECT * FROM lab_category_item_table";
    $conn = open_conn();
    $result = $conn->query($query);
    while ($row = $result->fetch_assoc()) {
        $items[] = $row;
    }
    close_conn();
    return $items;
}

function _fetchBiller() {
    $items = array();
    $query = "SELECT patient_billing_account.emr_id, patient_table.full_name FROM patient_billing_account "
            . "INNER JOIN patient_table ON patient_billing_account.emr_id=patient_table.emr_id";
    $conn = open_conn();
    
    $result = $conn->query($query);
    echo $conn->error;
    while ($row = $result->fetch_assoc()) {
        $items[] = $row;
    }
    close_conn();
    return $items;
}

function _fetchInsurance() {
    $items = array();
    $query = "SELECT * FROM insurance_table";
    $conn = open_conn();
    $result = $conn->query($query);
    echo $conn->error;
    while ($row = $result->fetch_assoc()) {
        $items[] = $row;
    }
    close_conn();
    return $items;
}
function _getInsuranceAbbr($hmo){
  $query = "SELECT hmo_abbr FROM insurance_table WHERE id='$hmo'";
    $conn = open_conn();
    $result = $conn->query($query);
    echo $conn->error;
   $row = $result->fetch_assoc();
    close_conn();
    return $row; 
}
function _getbill($emr_id) {
    $items = array();
    $query = "SELECT *, DATE_FORMAT(transaction_date, '%Y %e %b, %a  %h:%i:%p') AS transaction_date FROM transaction_table WHERE emr_id='$emr_id' ORDER BY id DESC";
    $conn = open_conn();
    $result = $conn->query($query);
    echo $conn->error;
    while ($row = $result->fetch_assoc()) {
        $items[] = $row;
    }
    close_conn();
    return $items;
}

function _getPrescriptionBill($emr_id) {
    $items = array();
    $query = "SELECT item_type,description,price,item_id, signed_by, "
            . "DATE_FORMAT(transaction_date, '%Y %e %b, %a  %h:%i:%p') AS transaction_date "
            . "FROM transaction_table INNER JOIN  prescription_table "
            . "ON  transaction_table.item_id=prescription_table.id "
            . "WHERE type='Debit' AND is_served=0";
    $conn = open_conn();
    $result = $conn->query($query);
    echo $conn->error;
    while ($row = $result->fetch_assoc()) {
        $items[] = $row;
    }
    close_conn();
    return $items;
}
function _getTotalDebit($emr_id) {

    $query = "SELECT SUM(price) FROM transaction_table WHERE emr_id='$emr_id' AND type='Debit'";
    $conn = open_conn();
    $result = $conn->query($query);
    $row = $result->fetch_assoc();
    close_conn();
    return $row['SUM(price)'];
}

function _getTotalPayment($emr_id) {

    $query = "SELECT SUM(price) FROM transaction_table WHERE emr_id='$emr_id' AND (type='Credit' OR type='Reversed')";
    $conn = open_conn();
    $result = $conn->query($query);
    $row = $result->fetch_assoc();
    close_conn();
    return $row['SUM(price)'];
}

function _filterBillByDate($emr_id, $from, $to) {
    $items = array();
    $query = "SELECT *, DATE_FORMAT(transaction_date, '%Y %e %b, %a  %h:%i:%p') AS transaction_date FROM transaction_table WHERE emr_id='$emr_id' "
            . "AND transaction_date BETWEEN '$from' AND '$to'  ORDER BY id DESC";
    $conn = open_conn();
    $result = $conn->query($query);
    echo $conn->error;
    while ($row = $result->fetch_assoc()) {
        $items[] = $row;
    }
    close_conn();
    return $items;
}

function _filterBillByType($emr_id, $type) {
    $items = array();
    $query = "SELECT *, DATE_FORMAT(transaction_date, '%Y %e %b, %a  %h:%i:%p') AS transaction_date FROM transaction_table WHERE emr_id='$emr_id' "
            . "AND type='$type'  ORDER BY id DESC";
    $conn = open_conn();
    $result = $conn->query($query);
    echo $conn->error;
    while ($row = $result->fetch_assoc()) {
        $items[] = $row;
    }
    close_conn();
    return $items;
}

function _filterBillBySource($emr_id, $source) {
    $items = array();
    $query = "SELECT *, DATE_FORMAT(transaction_date, '%Y %e %b, %a  %h:%i:%p') AS transaction_date FROM transaction_table WHERE emr_id='$emr_id' "
            . "AND item_type='$source'  ORDER BY id DESC";
    $conn = open_conn();
    $result = $conn->query($query);
    echo $conn->error;
    while ($row = $result->fetch_assoc()) {
        $items[] = $row;
    }
    close_conn();
    return $items;
}

function _filterBillByDateAndType($emr_id, $from, $to, $type) {
    $items = array();
    $query = "SELECT *, DATE_FORMAT(transaction_date, '%Y %e %b, %a  %h:%i:%p') AS transaction_date FROM transaction_table WHERE emr_id='$emr_id' "
            . "AND type='$type' AND transaction_date BETWEEN '$from' AND '$to'  ORDER BY id DESC";
    $conn = open_conn();
    $result = $conn->query($query);
    echo $conn->error;
    while ($row = $result->fetch_assoc()) {
        $items[] = $row;
    }
    close_conn();
    return $items;
}

function _filterBillByDateAndSource($emr_id, $from, $to, $source) {
    $items = array();
    $query = "SELECT *, DATE_FORMAT(transaction_date, '%Y %e %b, %a  %h:%i:%p') AS transaction_date FROM transaction_table WHERE emr_id='$emr_id' "
            . "AND item_type='$source' AND transaction_date BETWEEN '$from' AND '$to'  ORDER BY id DESC";
    $conn = open_conn();
    $result = $conn->query($query);
    echo $conn->error;
    while ($row = $result->fetch_assoc()) {
        $items[] = $row;
    }
    close_conn();
    return $items;
}

function _filterBillByTypeAndSource($emr_id, $type, $source) {
    $items = array();
    $query = "SELECT *, DATE_FORMAT(transaction_date, '%Y %e %b, %a  %h:%i:%p') AS transaction_date FROM transaction_table WHERE emr_id='$emr_id' "
            . "AND item_type='$source' AND type='$type' ORDER BY id DESC";
    $conn = open_conn();
    $result = $conn->query($query);
    echo $conn->error;
    while ($row = $result->fetch_assoc()) {
        $items[] = $row;
    }
    close_conn();
    return $items;
}

function _filterBillByDateTypeAndSource($emr_id, $from, $to, $type, $source) {
    $items = array();
    $query = "SELECT *, DATE_FORMAT(transaction_date, '%Y %e %b, %a  %h:%i:%p') AS transaction_date FROM transaction_table WHERE emr_id='$emr_id' "
            . "AND item_type='$source' AND type='$type' AND transaction_date BETWEEN '$from' AND '$to' ORDER BY id DESC";
    $conn = open_conn();
    $result = $conn->query($query);
    echo $conn->error;
    while ($row = $result->fetch_assoc()) {
        $items[] = $row;
    }
    close_conn();
    return $items;
}

function _filterLabRequestByDateAndEMRID($emr_id, $from, $to) {
    $items = array();
    $query = "SELECT *, DATE_FORMAT(request_date, '%d %m %Y %h:%i:%p') AS request_date FROM lab_investigation_table WHERE emr_id='$emr_id' "
            . " AND request_date BETWEEN '$from' AND '$to' ORDER BY id DESC";
    $conn = open_conn();
    $result = $conn->query($query);
    echo $conn->error;
    while ($row = $result->fetch_assoc()) {
        $items[] = $row;
    }
    close_conn();
    return $items;
}

function _filterLabRequestByDate($from, $to) {
    $items = array();
    $query = "SELECT *, DATE_FORMAT(request_date, '%d %m %Y %h:%i:%p') AS request_date FROM lab_investigation_table WHERE "
            . " request_date BETWEEN '$from' AND '$to' ORDER BY id DESC";
    $conn = open_conn();
    $result = $conn->query($query);
    echo $conn->error;
    while ($row = $result->fetch_assoc()) {
        $items[] = $row;
    }
    close_conn();
    return $items;
}

function _filterPrescriptionRequestByDateAndEMRID($drug_name, $from, $to) {
    $items = array();
    $query = "SELECT *, DATE_FORMAT(prescription_date, '%d %m %Y %h:%i:%p') AS prescription_date FROM prescription_table "
            . "INNER JOIN prescription_slip ON prescription_table.prescription_slip_id=prescription_slip.id"
            . "  WHERE prescription_slip.is_cleared=1 AND drug_name='$drug_name' "
            . " AND prescription_date BETWEEN '$from' AND '$to' ORDER BY prescription_table.id DESC";
    $conn = open_conn();
    $result = $conn->query($query);
    echo $conn->error;
    while ($row = $result->fetch_assoc()) {
        $items[] = $row;
    }
    close_conn();
    return $items;
}

function _filterPrescriptionRequestByDate($from, $to) {
    $items = array();
    $query = "SELECT *, DATE_FORMAT(prescription_date, '%d %m %Y %h:%i:%p') AS prescription_date FROM prescription_table"
            . " INNER JOIN prescription_slip ON prescription_table.prescription_slip_id=prescription_slip.id"
            . "  WHERE prescription_slip.is_cleared=1 AND "
            . "prescription_date BETWEEN '$from' AND '$to' ORDER BY prescription_table.id DESC";
    $conn = open_conn();
    $result = $conn->query($query);
    echo $conn->error;
    while ($row = $result->fetch_assoc()) {
        $items[] = $row;
    }
    close_conn();
    return $items;
}

function _filterRadiologyRequestByDateAndEMRID($emr_id, $from, $to) {
    $items = array();
    $query = "SELECT *, DATE_FORMAT(request_date, '%d %m %Y %h:%i:%p') AS request_date FROM imaging_request_table WHERE emr_id='$emr_id' "
            . " AND request_date BETWEEN '$from' AND '$to' ORDER BY id DESC";
    $conn = open_conn();
    $result = $conn->query($query);
    echo $conn->error;
    while ($row = $result->fetch_assoc()) {
        $items[] = $row;
    }
    close_conn();
    return $items;
}

function _filterRadiologyRequestByDate($from, $to) {
    $items = array();
    $query = "SELECT *, DATE_FORMAT(request_date, '%d %m %Y %h:%i:%p') AS request_date FROM imaging_request_table WHERE "
            . "request_date BETWEEN '$from' AND '$to' ORDER BY id DESC";
    $conn = open_conn();
    $result = $conn->query($query);
    echo $conn->error;
    while ($row = $result->fetch_assoc()) {
        $items[] = $row;
    }
    close_conn();
    return $items;
}

function _getProcedure($emr_id) {
    $items = array();
    $query = "SELECT * FROM procedure_table WHERE emr_id='$emr_id' ORDER BY id DESC";
    $conn = open_conn();
    $result = $conn->query($query);
    echo $conn->error;
    while ($row = $result->fetch_assoc()) {
        $items[] = $row;
    }
    close_conn();
    return $items;
}

function _getAProcedure($id) {
    $query = "SELECT * FROM procedure_table WHERE id='$id'";
    $conn = open_conn();
    $result = $conn->query($query);
    close_conn();
    return $result->fetch_assoc();
}

function _getRadiologyInvestigations($emr_id) {
    $items = array();
    $query = "SELECT *, DATE_FORMAT(request_date, '%a, %e %b %y %h:%i:%p') AS request_date FROM imaging_request_table WHERE emr_id='$emr_id' ORDER BY id DESC";
    $conn = open_conn();
    $result = $conn->query($query);
    echo $conn->error;
    while ($row = $result->fetch_assoc()) {
        $items[] = $row;
    }
    close_conn();
    return $items;
}

function _getRadiologyInvestigation($id) {

    $query = "SELECT name,result,result_note FROM imaging_request_table WHERE id='$id'";
    $conn = open_conn();
    $result = $conn->query($query);
    echo $conn->error;
    $row = $result->fetch_assoc();

    close_conn();
    return $row;
}

function _fetchRadiologyInvestigations() {
    $items = array();
    $query = "SELECT imaging_request_table.id,imaging_request_table.emr_id,imaging_request_table.name, "
            . "result,requested_by,patient_table.full_name, DATE_FORMAT(request_date, '%Y %e %b, %a  %h:%i:%p') AS request_date "
            . "FROM imaging_request_table INNER JOIN patient_table ON imaging_request_table.emr_id=patient_table.emr_id "
            . " WHERE result='' ORDER BY id ASC";
    $conn = open_conn();
    $result = $conn->query($query);
    echo $conn->error;
    while ($row = $result->fetch_assoc()) {
        $items[] = $row;
    }
    close_conn();
    return $items;
}

function _fetchRecordServices() {
    $items = array();
    $query = "SELECT * FROM service_table";
    $conn = open_conn();
    $result = $conn->query($query);
    echo $conn->error;
    while ($row = $result->fetch_assoc()) {
        $items[] = $row;
    }
    close_conn();
    return $items;
}

function _getInPatientNotes($admission_id) {
    $items = array();
    $query = "SELECT *, DATE_FORMAT(date_taken, '%Y %e %b,%a  %h:%i:%p') AS date_taken FROM in_patient_note_table WHERE admission_id='$admission_id' ORDER BY id DESC";
    $conn = open_conn();
    $result = $conn->query($query);
    echo $conn->error;
    while ($row = $result->fetch_assoc()) {
        $items[] = $row;
    }
    close_conn();
    return $items;
}

function _fetchAllPatients() {
    $items = array();
    $query = "SELECT emr_id, full_name, phone_number, dob, gender FROM patient_table ORDER BY id DESC";
    $conn = open_conn();
    $result = $conn->query($query);
    echo $conn->error;
    while ($row = $result->fetch_assoc()) {
        $items[] = $row;
    }
    close_conn();
    return $items;
}

function _fetchAllTransactions() {
    $items = array();
    $query = "SELECT emr_id, description,auth_code, quantity, price,type,balance, DATE_FORMAT(transaction_date, '%Y %e %b, %a  %h:%i:%p') AS transaction_date"
            . " FROM transaction_table";
    $conn = open_conn();
    $result = $conn->query($query);
    echo $conn->error;
    while ($row = $result->fetch_assoc()) {
        $items[] = $row;
    }
    close_conn();
    return $items;
}

function _fetchParmacyTransactions() {
    $today= date('Y-m-d');
    $items = array();
    $query = "SELECT emr_id, description, quantity, price,type,balance, DATE_FORMAT(transaction_date, '%Y %e %b, %a  %h:%i:%p') AS transaction_date"
            . " FROM transaction_table WHERE item_type='Pharmacy' AND type='Credit' AND transaction_date_='$today'";
    $conn = open_conn();
    $result = $conn->query($query);
    echo $conn->error;
    while ($row = $result->fetch_assoc()) {
        $items[] = $row;
    }
    close_conn();
    return $items;
}

function _fetchAllAdmission() {
    $items = array();
    $query = "SELECT emr_id,reason_for_admission, admission_date,clinic, ward,location "
            . " FROM admission_table";
    $conn = open_conn();
    $result = $conn->query($query);
    echo $conn->error;
    while ($row = $result->fetch_assoc()) {
        $items[] = $row;
    }
    close_conn();
    return $items;
}

function _fetchAllPrescription() {
    $items = array();
    $query = "SELECT *, DATE_FORMAT(prescription_date, '%Y %e %b,%a  %h:%i:%p') AS prescription_date FROM prescription_table"
            . " INNER JOIN prescription_slip ON prescription_table.prescription_slip_id=prescription_slip.id"
            . "  WHERE prescription_slip.is_cleared=1";
    $conn = open_conn();
    $result = $conn->query($query);
    echo $conn->error;
    while ($row = $result->fetch_assoc()) {
        $items[] = $row;
    }
    close_conn();
    return $items;
}

function _fetchAllLabInvestigation() {
    $items = array();
    $query = "SELECT emr_id, DATE_FORMAT(request_date, '%Y %e %b,%a  %h:%i:%p') AS request_date, lab_test, requested_by "
            . " FROM lab_investigation_table";
    $conn = open_conn();
    $result = $conn->query($query);
    echo $conn->error;
    while ($row = $result->fetch_assoc()) {
        $items[] = $row;
    }
    close_conn();
    return $items;
}

function _fetchAllRadiologyInvestigation() {
    $items = array();
    $query = "SELECT emr_id, DATE_FORMAT(request_date, '%Y %e %b,%a  %h:%i:%p') AS request_date, name, requested_by "
            . " FROM imaging_request_table";
    $conn = open_conn();
    $result = $conn->query($query);
    echo $conn->error;
    while ($row = $result->fetch_assoc()) {
        $items[] = $row;
    }
    close_conn();
    return $items;
}

function _fetchAllProcedure() {
    $items = array();
    $query = "SELECT emr_id, schedule_date, name, requested_by "
            . " FROM procedure_table";
    $conn = open_conn();
    $result = $conn->query($query);
    echo $conn->error;
    while ($row = $result->fetch_assoc()) {
        $items[] = $row;
    }
    close_conn();
    return $items;
}

function _fetchAllVisit() {
    $items = array();
    $query = "SELECT emr_id, visit_date, visit_type, seen_by,clinic,room"
            . " FROM visit_table ORDER BY id DESC";
    $conn = open_conn();
    $result = $conn->query($query);
    echo $conn->error;
    while ($row = $result->fetch_assoc()) {
        $items[] = $row;
    }
    close_conn();
    return $items;
}

function _filterVisitByDateAndEMRIDBy($emr_id, $from, $to, $by) {
    $items = array();
    $query = "SELECT emr_id, visit_date, visit_type, seen_by,clinic,room"
            . " FROM visit_table "
            . "WHERE emr_id='$emr_id' AND booked_by='$by' AND visit_date BETWEEN '$from' AND '$to' ";
    $conn = open_conn();
    $result = $conn->query($query);
    echo $conn->error;
    while ($row = $result->fetch_assoc()) {
        $items[] = $row;
    }
    close_conn();
    return $items;
}

function _filterAdmissionByDateAndEMRIDBy($emr_id, $from, $to, $by) {
    ///THIS PLACE
    $items = array();
    $query = "SELECT emr_id,reason_for_admission, admission_date,clinic, ward,location "
            . " FROM admission_table "
            . "WHERE emr_id='$emr_id' AND requested_by='$by' AND admission_date BETWEEN '$from' AND '$to' ";
    $conn = open_conn();
    $result = $conn->query($query);
    echo $conn->error;
    while ($row = $result->fetch_assoc()) {
        $items[] = $row;
    }
    close_conn();
    return $items;
}

function _filterPrescriptionByDateAndEMRIDBy($emr_id, $from, $to, $by) {
    ///THIS PLACE
    $items = array();
    $query = "SELECT emr_id, DATE_FORMAT(prescription_date, '%Y %e %b,%a  %h:%i:%p') AS prescription_date, drug_name,duration, dose, served_quantity, requested_by "
            . " FROM prescription_table "
            . "WHERE emr_id='$emr_id' AND requested_by='$by' AND prescription_date BETWEEN '$from' AND '$to' ";
    $conn = open_conn();
    $result = $conn->query($query);
    echo $conn->error;
    while ($row = $result->fetch_assoc()) {
        $items[] = $row;
    }
    close_conn();
    return $items;
}

function _filterLabByDateAndEMRIDBy($emr_id, $from, $to, $by) {
    ///THIS PLACE
    $items = array();
    $query = "SELECT emr_id, DATE_FORMAT(request_date, '%Y %e %b,%a  %h:%i:%p') AS request_date, lab_test, requested_by "
            . " FROM lab_investigation_table "
            . "WHERE emr_id='$emr_id' AND conducted_by='$by' AND request_date BETWEEN '$from' AND '$to' ";
    $conn = open_conn();
    $result = $conn->query($query);
    echo $conn->error;
    while ($row = $result->fetch_assoc()) {
        $items[] = $row;
    }
    close_conn();
    return $items;
}

function _filterRadiologyByDateAndEMRIDBy($emr_id, $from, $to, $by) {
    ///THIS PLACE
    $items = array();
    $query = "SELECT emr_id, DATE_FORMAT(request_date, '%Y %e %b,%a  %h:%i:%p') AS request_date, name, requested_by "
            . " FROM imaging_request_table "
            . "WHERE emr_id='$emr_id' AND uploaded_by='$by' AND request_date BETWEEN '$from' AND '$to' ";
    $conn = open_conn();
    $result = $conn->query($query);
    echo $conn->error;
    while ($row = $result->fetch_assoc()) {
        $items[] = $row;
    }
    close_conn();
    return $items;
}

function _filterProcedureByDateAndEMRIDBy($emr_id, $from, $to, $by) {
    ///THIS PLACE
    $items = array();
    $query = "SELECT emr_id, schedule_date, name, requested_by "
            . " FROM procedure_table "
            . "WHERE emr_id='$emr_id' AND requested_by='$by' AND schedule_date BETWEEN '$from' AND '$to' ";
    $conn = open_conn();
    $result = $conn->query($query);
    echo $conn->error;
    while ($row = $result->fetch_assoc()) {
        $items[] = $row;
    }
    close_conn();
    return $items;
}

function _filterAdmissionByDateAndEMRID($emr_id, $from, $to) {
    ///THIS PLACE
    $items = array();
    $query = "SELECT emr_id,reason_for_admission, admission_date,clinic, ward,location "
            . " FROM admission_table "
            . "WHERE emr_id='$emr_id' AND admission_date BETWEEN '$from' AND '$to' ";
    $conn = open_conn();
    $result = $conn->query($query);
    echo $conn->error;
    while ($row = $result->fetch_assoc()) {
        $items[] = $row;
    }
    close_conn();
    return $items;
}

function _filterPrescriptionByDateAndEMRID($emr_id, $from, $to) {

    $items = array();
    $query = "SELECT emr_id, DATE_FORMAT(prescription_date, '%Y %e %b,%a  %h:%i:%p') AS prescription_date, drug_name,duration, dose, served_quantity, requested_by "
            . " FROM prescription_table "
            . "WHERE emr_id='$emr_id' AND prescription_date BETWEEN '$from' AND '$to' ";
    $conn = open_conn();
    $result = $conn->query($query);
    echo $conn->error;
    while ($row = $result->fetch_assoc()) {
        $items[] = $row;
    }
    close_conn();
    return $items;
}

function _filterLabByDateAndEMRID($emr_id, $from, $to) {
    ///THIS PLACE
    $items = array();
    $query = "SELECT emr_id, DATE_FORMAT(request_date, '%Y %e %b,%a  %h:%i:%p') AS request_date, lab_test, requested_by "
            . " FROM lab_investigation_table "
            . "WHERE emr_id='$emr_id' AND request_date BETWEEN '$from' AND '$to' ";
    $conn = open_conn();
    $result = $conn->query($query);
    echo $conn->error;
    while ($row = $result->fetch_assoc()) {
        $items[] = $row;
    }
    close_conn();
    return $items;
}

function _filterRadiologyByDateAndEMRID($emr_id, $from, $to) {
    ///THIS PLACE
    $items = array();
    $query = "SELECT emr_id, DATE_FORMAT(request_date, '%Y %e %b,%a  %h:%i:%p') AS request_date, name, requested_by "
            . " FROM imaging_request_table "
            . "WHERE emr_id='$emr_id' AND request_date BETWEEN '$from' AND '$to' ";
    $conn = open_conn();
    $result = $conn->query($query);
    echo $conn->error;
    while ($row = $result->fetch_assoc()) {
        $items[] = $row;
    }
    close_conn();
    return $items;
}

function _filterProcedureByDateAndEMRID($emr_id, $from, $to) {
    ///THIS PLACE
    $items = array();
    $query = "SELECT emr_id, schedule_date, name, requested_by "
            . " FROM procedure_table "
            . "WHERE emr_id='$emr_id' AND schedule_date BETWEEN '$from' AND '$to' ";
    $conn = open_conn();
    $result = $conn->query($query);
    echo $conn->error;
    while ($row = $result->fetch_assoc()) {
        $items[] = $row;
    }
    close_conn();
    return $items;
}

function _filterAdmissionByDateAndBy($by, $from, $to) {
    ///THIS PLACE
    $items = array();
    $query = "SELECT emr_id,reason_for_admission, admission_date,clinic, ward,location "
            . " FROM admission_table "
            . "WHERE requested_by='$by' AND admission_date BETWEEN '$from' AND '$to' ";
    $conn = open_conn();
    $result = $conn->query($query);
    echo $conn->error;
    while ($row = $result->fetch_assoc()) {
        $items[] = $row;
    }
    close_conn();
    return $items;
}

function _filterPrescriptionByDateAndBy($by, $from, $to) {
    ///THIS PLACE
    $items = array();
    $query = "SELECT emr_id, DATE_FORMAT(prescription_date, '%Y %e %b,%a  %h:%i:%p') AS prescription_date, drug_name,duration, dose, served_quantity, requested_by "
            . " FROM prescription_table "
            . "WHERE requested_by='$by' AND prescription_date BETWEEN '$from' AND '$to' ";
    $conn = open_conn();
    $result = $conn->query($query);
    echo $conn->error;
    while ($row = $result->fetch_assoc()) {
        $items[] = $row;
    }
    close_conn();
    return $items;
}

function _filterLabByDateAndBy($by, $from, $to) {
    ///THIS PLACE
    $items = array();
    $query = "SELECT emr_id, DATE_FORMAT(request_date, '%Y %e %b,%a  %h:%i:%p') AS request_date, lab_test, requested_by "
            . " FROM lab_investigation_table "
            . "WHERE conducted_by='$by' AND request_date BETWEEN '$from' AND '$to' ";
    $conn = open_conn();
    $result = $conn->query($query);
    echo $conn->error;
    while ($row = $result->fetch_assoc()) {
        $items[] = $row;
    }
    close_conn();
    return $items;
}

function _filterRadiologyByDateAndBy($by, $from, $to) {
    ///THIS PLACE
    $items = array();
    $query = "SELECT emr_id, DATE_FORMAT(request_date, '%Y %e %b,%a  %h:%i:%p') AS request_date, name, requested_by "
            . " FROM imaging_request_table "
            . "WHERE uploaded_by='$by' AND request_date BETWEEN '$from' AND '$to' ";
    $conn = open_conn();
    $result = $conn->query($query);
    echo $conn->error;
    while ($row = $result->fetch_assoc()) {
        $items[] = $row;
    }
    close_conn();
    return $items;
}

function _filterProcedureByDateAndBy($by, $from, $to) {
    ///THIS PLACE
    $items = array();
    $query = "SELECT emr_id, schedule_date, name, requested_by "
            . " FROM procedure_table "
            . "WHERE requested_by='$by' AND schedule_date BETWEEN '$from' AND '$to' ";
    $conn = open_conn();
    $result = $conn->query($query);
    echo $conn->error;
    while ($row = $result->fetch_assoc()) {
        $items[] = $row;
    }
    close_conn();
    return $items;
}

function _filterAdmissionByEMRIDAndBy($emr_id, $by) {
    ///THIS PLACE
    $items = array();
    $query = "SELECT emr_id,reason_for_admission, admission_date,clinic, ward,location "
            . " FROM admission_table "
            . "WHERE requested_by='$by' AND emr_id='$emr_id' ";
    $conn = open_conn();
    $result = $conn->query($query);
    echo $conn->error;
    while ($row = $result->fetch_assoc()) {
        $items[] = $row;
    }
    close_conn();
    return $items;
}

function _filterPrescriptionByEMRIDAndBy($emr_id, $by) {
    ///THIS PLACE
    $items = array();
    $query = "SELECT emr_id, DATE_FORMAT(prescription_date, '%Y %e %b,%a  %h:%i:%p') AS prescription_date, drug_name,duration, dose, served_quantity, requested_by "
            . " FROM prescription_table "
            . "WHERE emr_id='$emr_id' AND requested_by='$by'";
    $conn = open_conn();
    $result = $conn->query($query);
    echo $conn->error;
    while ($row = $result->fetch_assoc()) {
        $items[] = $row;
    }
    close_conn();
    return $items;
}

function _filterLabByEMRIDAndBy($emr_id, $by) {
    ///THIS PLACE
    $items = array();
    $query = "SELECT emr_id, DATE_FORMAT(request_date, '%Y %e %b,%a  %h:%i:%p') AS request_date, lab_test, requested_by "
            . " FROM lab_investigation_table "
            . "WHERE emr_id='$emr_id' AND conducted_by='$by'";
    $conn = open_conn();
    $result = $conn->query($query);
    echo $conn->error;
    while ($row = $result->fetch_assoc()) {
        $items[] = $row;
    }
    close_conn();
    return $items;
}

function _filterRadiologyByEMRIDAndBy($emr_id, $by) {
    ///THIS PLACE
    $items = array();
    $query = "SELECT emr_id, DATE_FORMAT(request_date, '%Y %e %b,%a  %h:%i:%p') AS request_date, name, requested_by "
            . " FROM imaging_request_table "
            . "WHERE emr_id='$emr_id' AND uploaded_by='$by'";
    $conn = open_conn();
    $result = $conn->query($query);
    echo $conn->error;
    while ($row = $result->fetch_assoc()) {
        $items[] = $row;
    }
    close_conn();
    return $items;
}

function _filterProcedureByEMRIDAndBy($emr_id, $by) {
    ///THIS PLACE
    $items = array();
    $query = "SELECT emr_id, schedule_date, name, requested_by "
            . " FROM procedure_table "
            . "WHERE emr_id='$emr_id' AND requested_by='$by'";
    $conn = open_conn();
    $result = $conn->query($query);
    echo $conn->error;
    while ($row = $result->fetch_assoc()) {
        $items[] = $row;
    }
    close_conn();
    return $items;
}

function _filterAdmissionByEMRID($emr_id) {
    ///THIS PLACE
    $items = array();
    $query = "SELECT emr_id,reason_for_admission, admission_date,clinic, ward,location "
            . " FROM admission_table "
            . "WHERE emr_id='$emr_id' ";
    $conn = open_conn();
    $result = $conn->query($query);
    echo $conn->error;
    while ($row = $result->fetch_assoc()) {
        $items[] = $row;
    }
    close_conn();
    return $items;
}

function _filterPrescriptionByEMRID($emr_id) {
    ///THIS PLACE
    $items = array();
    $query = "SELECT emr_id, DATE_FORMAT(prescription_date, '%Y %e %b,%a  %h:%i:%p') AS prescription_date, drug_name,duration, dose, served_quantity, requested_by "
            . " FROM prescription_table "
            . "WHERE emr_id='$emr_id' ";
    $conn = open_conn();
    $result = $conn->query($query);
    echo $conn->error;
    while ($row = $result->fetch_assoc()) {
        $items[] = $row;
    }
    close_conn();
    return $items;
}

function _filterLabByEMRID($emr_id) {
    ///THIS PLACE
    $items = array();
    $query = "SELECT emr_id, DATE_FORMAT(request_date, '%Y %e %b,%a  %h:%i:%p') AS request_date, lab_test, requested_by "
            . " FROM lab_investigation_table "
            . "WHERE emr_id='$emr_id' ";
    $conn = open_conn();
    $result = $conn->query($query);
    echo $conn->error;
    while ($row = $result->fetch_assoc()) {
        $items[] = $row;
    }
    close_conn();
    return $items;
}

function _filterRadiologyByEMRID($emr_id) {
    ///THIS PLACE
    $items = array();
    $query = "SELECT emr_id, DATE_FORMAT(request_date, '%Y %e %b,%a  %h:%i:%p') AS request_date, name, requested_by "
            . " FROM imaging_request_table "
            . "WHERE emr_id='$emr_id' ";
    $conn = open_conn();
    $result = $conn->query($query);
    echo $conn->error;
    while ($row = $result->fetch_assoc()) {
        $items[] = $row;
    }
    close_conn();
    return $items;
}

function _filterProcedureByEMRID($emr_id) {
    ///THIS PLACE
    $items = array();
    $query = "SELECT emr_id, schedule_date, name, requested_by "
            . " FROM procedure_table "
            . "WHERE emr_id='$emr_id' ";
    $conn = open_conn();
    $result = $conn->query($query);
    echo $conn->error;
    while ($row = $result->fetch_assoc()) {
        $items[] = $row;
    }
    close_conn();
    return $items;
}

function _filterAdmissionBY($by) {
    ///THIS PLACE
    $items = array();
    $query = "SELECT emr_id,reason_for_admission, admission_date,clinic, ward,location "
            . " FROM admission_table "
            . "WHERE requested_by='$by'";
    $conn = open_conn();
    $result = $conn->query($query);
    echo $conn->error;
    while ($row = $result->fetch_assoc()) {
        $items[] = $row;
    }
    close_conn();
    return $items;
}

function _filterPrescriptionBY($by) {
    ///THIS PLACE
    $items = array();
    $query = "SELECT emr_id, DATE_FORMAT(prescription_date, '%Y %e %b,%a  %h:%i:%p') AS prescription_date, drug_name,duration, dose, served_quantity, requested_by "
            . " FROM prescription_table "
            . "WHERE requested_by='$by'";
    $conn = open_conn();
    $result = $conn->query($query);
    echo $conn->error;
    while ($row = $result->fetch_assoc()) {
        $items[] = $row;
    }
    close_conn();
    return $items;
}

function _filterLabBY($by) {
    ///THIS PLACE
    $items = array();
    $query = "SELECT emr_id, DATE_FORMAT(request_date, '%Y %e %b,%a  %h:%i:%p') AS request_date, lab_test, requested_by "
            . " FROM lab_investigation_table "
            . "WHERE conducted_by='$by'";
    $conn = open_conn();
    $result = $conn->query($query);
    echo $conn->error;
    while ($row = $result->fetch_assoc()) {
        $items[] = $row;
    }
    close_conn();
    return $items;
}

function _filterProcedureBY($by) {
    ///THIS PLACE
    $items = array();
    $query = "SELECT emr_id, schedule_date, name, requested_by "
            . " FROM procedure_table "
            . "WHERE requested_by='$by'";
    $conn = open_conn();
    $result = $conn->query($query);
    echo $conn->error;
    while ($row = $result->fetch_assoc()) {
        $items[] = $row;
    }
    close_conn();
    return $items;
}

function _filterRadiologyBY($by) {
    ///THIS PLACE
    $items = array();
    $query = "SELECT emr_id, DATE_FORMAT(request_date, '%Y %e %b,%a  %h:%i:%p') AS request_date, name, requested_by "
            . " FROM imaging_request_table "
            . "WHERE uploaded_by='$by'";
    $conn = open_conn();
    $result = $conn->query($query);
    echo $conn->error;
    while ($row = $result->fetch_assoc()) {
        $items[] = $row;
    }
    close_conn();
    return $items;
}

function _filterAdmissionByDate($from, $to) {
    ///THIS PLACE
    $items = array();
    $query = "SELECT emr_id,reason_for_admission, admission_date,clinic, ward,location "
            . " FROM admission_table "
            . "WHERE admission_date BETWEEN '$from' AND '$to' ";
    $conn = open_conn();
    $result = $conn->query($query);
    echo $conn->error;
    while ($row = $result->fetch_assoc()) {
        $items[] = $row;
    }
    close_conn();
    return $items;
}

function _filterPrescriptionByDate($from, $to) {
    ///THIS PLACE
    $items = array();
    $query = "SELECT emr_id, DATE_FORMAT(prescription_date, '%Y %e %b,%a  %h:%i:%p') AS prescription_date, drug_name,duration, dose, served_quantity, requested_by "
            . " FROM prescription_table "
            . "WHERE prescription_date BETWEEN '$from' AND '$to' ";
    $conn = open_conn();
    $result = $conn->query($query);
    echo $conn->error;
    while ($row = $result->fetch_assoc()) {
        $items[] = $row;
    }
    close_conn();
    return $items;
}

function _filterLabByDate($from, $to) {
    ///THIS PLACE
    $items = array();
    $query = "SELECT emr_id, DATE_FORMAT(request_date, '%Y %e %b,%a  %h:%i:%p') AS request_date, lab_test, requested_by "
            . " FROM lab_investigation_table "
            . "WHERE request_date BETWEEN '$from' AND '$to' ";
    $conn = open_conn();
    $result = $conn->query($query);
    echo $conn->error;
    while ($row = $result->fetch_assoc()) {
        $items[] = $row;
    }
    close_conn();
    return $items;
}

function _filterRadiologyByDate($from, $to) {
    ///THIS PLACE
    $items = array();
    $query = "SELECT emr_id, DATE_FORMAT(request_date, '%Y %e %b,%a  %h:%i:%p') AS request_date, name, requested_by "
            . " FROM imaging_request_table "
            . "WHERE request_date BETWEEN '$from' AND '$to' ";
    $conn = open_conn();
    $result = $conn->query($query);
    echo $conn->error;
    while ($row = $result->fetch_assoc()) {
        $items[] = $row;
    }
    close_conn();
    return $items;
}

function _filterProcedureByDate($from, $to) {
    ///THIS PLACE
    $items = array();
    $query = "SELECT emr_id, schedule_date, name, requested_by "
            . " FROM procedure_table "
            . "WHERE request_date BETWEEN '$from' AND '$to' ";
    $conn = open_conn();
    $result = $conn->query($query);
    echo $conn->error;
    while ($row = $result->fetch_assoc()) {
        $items[] = $row;
    }
    close_conn();
    return $items;
}

function _filterVisitByDateAndBy($by, $from, $to) {
    $items = array();
    $query = "SELECT emr_id, visit_date, visit_type, seen_by,clinic,room"
            . " FROM visit_table "
            . "WHERE booked_by='$by' AND visit_date BETWEEN '$from' AND '$to' ";
    $conn = open_conn();
    $result = $conn->query($query);
    echo $conn->error;
    while ($row = $result->fetch_assoc()) {
        $items[] = $row;
    }
    close_conn();
    return $items;
}

function _filterVisitByDate($from, $to) {
    $items = array();
    $query = "SELECT emr_id, visit_date, visit_type, seen_by,clinic,room"
            . " FROM visit_table "
            . "WHERE visit_date BETWEEN '$from' AND '$to' ";
    $conn = open_conn();
    $result = $conn->query($query);
    echo $conn->error;
    while ($row = $result->fetch_assoc()) {
        $items[] = $row;
    }
    close_conn();
    return $items;
}

function _filterVisitByDateAndEMRID($emr_id, $from, $to) {
    $items = array();
    $query = "SELECT emr_id, visit_date, visit_type, seen_by,clinic,room"
            . " FROM visit_table "
            . "WHERE emr_id='$emr_id' AND visit_date BETWEEN '$from' AND '$to' ";
    $conn = open_conn();
    $result = $conn->query($query);
    echo $conn->error;
    while ($row = $result->fetch_assoc()) {
        $items[] = $row;
    }
    close_conn();
    return $items;
}

function _filterVisitByEMRIDAndBy($by, $emr_id) {
    $items = array();
    $query = "SELECT emr_id, visit_date, visit_type, seen_by,clinic,room"
            . " FROM visit_table "
            . "WHERE booked_by='$by' AND emr_id='$emr_id' ";
    $conn = open_conn();
    $result = $conn->query($query);
    echo $conn->error;
    while ($row = $result->fetch_assoc()) {
        $items[] = $row;
    }
    close_conn();
    return $items;
}

function _filterVisitByEMRID($emr_id) {
    $items = array();
    $query = "SELECT emr_id, visit_date, visit_type, seen_by,clinic,room"
            . " FROM visit_table "
            . "WHERE emr_id='$emr_id' ";
    $conn = open_conn();
    $result = $conn->query($query);
    echo $conn->error;
    while ($row = $result->fetch_assoc()) {
        $items[] = $row;
    }
    close_conn();
    return $items;
}

function _filterVisitBY($by) {
    $items = array();
    $query = "SELECT emr_id, visit_date, visit_type, seen_by,clinic,room"
            . " FROM visit_table "
            . "WHERE booked_by='$by' ";
    $conn = open_conn();
    $result = $conn->query($query);
    echo $conn->error;
    while ($row = $result->fetch_assoc()) {
        $items[] = $row;
    }
    close_conn();
    return $items;
}

function _filterTransactionByDateAndEMRIDBy($emr_id, $from, $to, $by) {
    $items = array();
    $query = "SELECT emr_id, description,auth_code, quantity, price,type,balance, DATE_FORMAT(transaction_date, '%Y %e %b,%a  %h:%i:%p') AS transaction_date"
            . " FROM transaction_table "
            . "WHERE emr_id='$emr_id' AND signed_by='$by' AND transaction_date BETWEEN '$from' AND '$to' ";
    $conn = open_conn();
    $result = $conn->query($query);
    echo $conn->error;
    while ($row = $result->fetch_assoc()) {
        $items[] = $row;
    }
    close_conn();
    return $items;
}

function _filterTransactionByDateAndEMRID($emr_id, $from, $to) {
    $items = array();
    $query = "SELECT emr_id, description, quantity,auth_code, price,type,balance, DATE_FORMAT(transaction_date, '%Y %e %b,%a  %h:%i:%p') AS transaction_date"
            . " FROM transaction_table "
            . "WHERE emr_id='$emr_id' AND transaction_date BETWEEN '$from' AND '$to'";
    $conn = open_conn();
    $result = $conn->query($query);
    echo $conn->error;
    while ($row = $result->fetch_assoc()) {
        $items[] = $row;
    }
    close_conn();
    return $items;
}

function _filterTransactionByDate($from, $to) {
    $items = array();
    $query = "SELECT emr_id, description, quantity,auth_code, price,type,balance, DATE_FORMAT(transaction_date, '%Y %e %b,%a  %h:%i:%p') AS transaction_date"
            . " FROM transaction_table "
            . "WHERE transaction_date BETWEEN '$from' AND '$to'";
    $conn = open_conn();
    $result = $conn->query($query);
    echo $conn->error;
    while ($row = $result->fetch_assoc()) {
        $items[] = $row;
    }
    close_conn();
    return $items;
}

function _filterTransactionByDateBy($by, $from, $to) {
    $items = array();
    $query = "SELECT emr_id, description, quantity,auth_code, price,type,balance, DATE_FORMAT(transaction_date, '%Y %e %b,%a  %h:%i:%p') AS transaction_date"
            . " FROM transaction_table "
            . "WHERE signed_by='$by' AND transaction_date BETWEEN '$from' AND '$to'";
    $conn = open_conn();
    $result = $conn->query($query);
    echo $conn->error;
    while ($row = $result->fetch_assoc()) {
        $items[] = $row;
    }
    close_conn();
    return $items;
}

function _filterTransaction($id) {
    $items = array();
    $query = "SELECT emr_id, description,auth_code, quantity, price,type,balance, DATE_FORMAT(transaction_date, '%Y %e %b,%a  %h:%i:%p') AS transaction_date"
            . " FROM transaction_table "
            . "WHERE emr_id='$id'";
    $conn = open_conn();
    $result = $conn->query($query);
    while ($row = $result->fetch_assoc()) {
        $items[] = $row;
    }
    close_conn();
    return $items;
}

function _filterTransactionEMRIDBy($id, $by) {
    $items = array();
    $query = "SELECT emr_id, description,auth_code, quantity, price,type,balance, DATE_FORMAT(transaction_date, '%Y %e %b,%a  %h:%i:%p') AS transaction_date"
            . " FROM transaction_table "
            . "WHERE signed_by='$by' AND  emr_id='$id'";
    $conn = open_conn();
    $result = $conn->query($query);
    while ($row = $result->fetch_assoc()) {
        $items[] = $row;
    }
    close_conn();
    return $items;
}

function _filterTransactionBy($by) {
    $items = array();
    $query = "SELECT emr_id, description, quantity,auth_code, price,type,balance, DATE_FORMAT(transaction_date, '%Y %e %b,%a  %h:%i:%p') AS transaction_date"
            . " FROM transaction_table "
            . "WHERE signed_by='$by'";
    $conn = open_conn();
    $result = $conn->query($query);
    while ($row = $result->fetch_assoc()) {
        $items[] = $row;
    }
    echo $conn->error;
    close_conn();
    return $items;
}

function _filterPatientByDateAndEMRIDBy($emr_id, $from, $to, $by) {
    $items = array();
    $query = "SELECT emr_id, full_name, phone_number, dob, gender FROM patient_table "
            . "WHERE emr_id='$emr_id' AND updated_by='$by' AND created_at BETWEEN '$from' AND '$to' ";
    $conn = open_conn();
    $result = $conn->query($query);
    echo $conn->error;
    while ($row = $result->fetch_assoc()) {
        $items[] = $row;
    }
    close_conn();
    return $items;
}

function _filterPatientByDateAndEMRID($emr_id, $from, $to) {
    $items = array();
    $query = "SELECT emr_id, full_name, phone_number, dob, gender FROM patient_table "
            . "WHERE emr_id='$emr_id' AND created_at BETWEEN '$from' AND '$to'";
    $conn = open_conn();
    $result = $conn->query($query);
    echo $conn->error;
    while ($row = $result->fetch_assoc()) {
        $items[] = $row;
    }
    close_conn();
    return $items;
}

function _filterPatientByDateAndBy($by, $from, $to) {
    $items = array();
    $query = "SELECT emr_id, full_name, phone_number, dob, gender FROM patient_table "
            . "WHERE updated_by='$by' AND created_at BETWEEN '$from' AND '$to'";
    $conn = open_conn();
    $result = $conn->query($query);
    echo $conn->error;
    while ($row = $result->fetch_assoc()) {
        $items[] = $row;
    }
    close_conn();
    return $items;
}

function _filterPatientByDate($from, $to) {
    $items = array();
    $query = "SELECT emr_id, full_name, phone_number, dob, gender FROM patient_table "
            . "WHERE created_at BETWEEN '$from' AND '$to'";
    $conn = open_conn();
    $result = $conn->query($query);
    echo $conn->error;
    while ($row = $result->fetch_assoc()) {
        $items[] = $row;
    }
    close_conn();
    return $items;
}

function _getNurseObservations($admission_id) {
    $items = array();
    $query = "SELECT *, DATE_FORMAT(note_date, '%a, %e %b %y %h:%i:%p') AS note_date FROM nurse_observation_table WHERE admission_id='$admission_id' ORDER BY id DESC";
    $conn = open_conn();
    $result = $conn->query($query);
    echo $conn->error;
    while ($row = $result->fetch_assoc()) {
        $items[] = $row;
    }
    close_conn();
    return $items;
}

function _addInPatientNote($admission_id, $note, $noted_by) {
    $last_id = "";
    $query = "INSERT INTO in_patient_note_table(admission_id,note,noted_by) "
            . "VALUES "
            . "("
            . "'$admission_id',"
            . "'$note',"
            . "'$noted_by'"
            . ")";
    $conn = open_conn();
    if ($conn->query($query) == TRUE) {
        $last_id = $conn->insert_id;
    } else {
        echo $conn->error;
    }
    close_conn();
    return $last_id;
}

function _addNurseObservation($admission_id, $note, $noted_by) {
    $last_id = "";
    $query = "INSERT INTO nurse_observation_table(admission_id,note,noted_by) "
            . "VALUES "
            . "("
            . "'$admission_id',"
            . "'$note',"
            . "'$noted_by'"
            . ")";
    $conn = open_conn();
    if ($conn->query($query) == TRUE) {
        $last_id = $conn->insert_id;
    } else {
        echo $conn->error;
    }
    close_conn();
    return $last_id;
}

function _addProcedureSpeciality($name) {
    $query = "INSERT INTO procedure_speciality_table(name) VALUES ('$name')";
    $conn = open_conn();
    $conn->query($query);
    close_conn();
}

function _addProcedureNote($procedure_id, $speciality, $note_type, $note, $noted_by) {
    $query = "INSERT INTO procedure_note_table(procedure_id, speciality, note_type, note, noted_by) "
            . "VALUES ('$procedure_id', '$speciality','$note_type','$note','$noted_by')";
    $conn = open_conn();
    $conn->query($query);
    close_conn();
}

function _fetchProcedureNote($procedure_id) {
    $items = array();
    $query = "SELECT *, DATE_FORMAT(note_date, '%Y %e %b, %a  %h:%i:%p') AS note_date FROM procedure_note_table WHERE procedure_id='$procedure_id' ORDER BY id DESC";
    $conn = open_conn();
    $result = $conn->query($query);
    echo $conn->error;
    while ($row = $result->fetch_array()) {
        $items[] = $row;
    }
    close_conn();
    return $items;
}

function _fetchWaitingList() {
    $today = date('Y-m-d');
    $items = array();
    $query = "SELECT patient_table.id, visit_table.emr_id, full_name, visit_table.id,  visit_table.visit_type  FROM "
            . "visit_table INNER JOIN patient_table ON visit_table.emr_id=patient_table.emr_id WHERE visit_date='$today' AND is_signed=0 ORDER BY visit_table.id ASC";
    $conn = open_conn();
    $result = $conn->query($query);
    echo $conn->error;
    while ($row = $result->fetch_array()) {
        $items[] = $row;
    }
    close_conn();
    return $items;
}

function _getWaitingList($emr_id) {
    $today = date('Y-m-d');
    $items = array();
    $query = "SELECT DATE_FORMAT(visit_date, '%Y %e %b, %a  %h:%i:%p') AS visit_date,booked_by, price, visit_table.emr_id, visit_table.id,  visit_table.visit_type  FROM "
            . "visit_table  WHERE emr_id='$emr_id' AND seperate_settlement=0 AND is_signed=0 ";
    $conn = open_conn();
    $result = $conn->query($query);
    echo $conn->error;
    while ($row = $result->fetch_assoc()) {
        $items[] = $row;
    }
    close_conn();
    return $items;
}

function _fetchProcedureSpeciality() {
    $items = array();
    $query = "SELECT * FROM procedure_speciality_table";
    $conn = open_conn();
    $result = $conn->query($query);
    echo $conn->error;
    while ($row = $result->fetch_array()) {
        $items[] = $row;
    }
    close_conn();
    return $items;
}

function _filterWaitingList($clinic, $room) {
    $today = date('Y-m-d');
    $items = array();
    $query = "SELECT patient_table.id, visit_table.emr_id, full_name, visit_table.id,  visit_table.visit_type FROM "
            . "visit_table INNER JOIN patient_table ON visit_table.emr_id=patient_table.emr_id "
            . "WHERE visit_date='$today' AND clinic='$clinic' AND room='$room' AND is_signed=0 ";
    $conn = open_conn();
    $result = $conn->query($query);
    echo $conn->error;
    while ($row = $result->fetch_array()) {
        $items[] = $row;
    }
    close_conn();
    return $items;
}

function _fetchAllAppointment() {
    $items = array();
    $query = "SELECT full_name, visit_table.emr_id, visit_table.visit_type, visit_table.clinic, "
            . " visit_table.room, visit_table.visit_date FROM visit_table INNER JOIN patient_table ON visit_table.emr_id=patient_table.emr_id";
    $conn = open_conn();
    $result = $conn->query($query);
    echo $conn->error;
    while ($row = $result->fetch_assoc()) {
        $items[] = $row;
    }
    close_conn();
    return $items;
}

function _fetchMissedAppointment() {
    $today = date('Y-m-d');
    $items = array();
    $query = "SELECT full_name, visit_table.emr_id, visit_table.visit_type, visit_table.clinic, "
            . " visit_table.room, visit_table.visit_date,visit_table.id FROM visit_table INNER JOIN patient_table ON visit_table.emr_id=patient_table.emr_id "
            . " WHERE visit_table.visit_date<'$today' AND is_signed=0";
    $conn = open_conn();
    $result = $conn->query($query);
    echo $conn->error;
    while ($row = $result->fetch_assoc()) {
        $items[] = $row;
    }
    close_conn();
    return $items;
}

function _updatePatientProfile($id, $full_name, $phone_number, $dob, $gender, $blood_group, $genotype, $address, $occupation, $NOK_name, $NOK_address, $NOK_phone_number) {
    $query = "UPDATE patient_table SET full_name='$full_name', gender='$gender', address='$address',
	blood_group='$blood_group',
	phone_number='$phone_number',
	occupation='$occupation',
	dob='$dob',
	genotype='$genotype',
	NOK_name='$NOK_name',NOK_address='$NOK_address' , NOK_phone_number='$NOK_phone_number'
	WHERE id='$id'";
    $conn = open_conn();
    $conn->query($query);
    echo $conn->error;


    close_conn();
}

function _saveVisit($visit_id, $seen_by, $visit_note, $diagnosis) {

    $query = "UPDATE visit_table SET
	visit_note='$visit_note',
	diagnosis='$diagnosis',
	seen_by='$seen_by', is_signed=1 
	WHERE id='$visit_id'";
    $conn = open_conn();
    $conn->query($query);
    echo $conn->error;
    close_conn();
}

function _startProcedure($id) {

    $query = "UPDATE procedure_table SET status=1 WHERE id='$id'";
    $conn = open_conn();
    $conn->query($query);
    echo $conn->error;
    close_conn();
}

function _concludeProcedure($id) {

    $query = "UPDATE procedure_table SET status=2 WHERE id='$id'";
    $conn = open_conn();
    $conn->query($query);
    echo $conn->error;
    close_conn();
}

function _deleteProcedure($id) {

    $query = "DELETE FROM procedure_table WHERE id='$id'";
    $conn = open_conn();
    $conn->query($query);
    echo $conn->error;
    close_conn();
}

function _fetchLastVisitID($emr_id) {

    $query = "SELECT id FROM visit_table  WHERE emr_id='$emr_id' ORDER BY id DESC LIMIT 1";
    $conn = open_conn();
    $result = $conn->query($query);
    $row = $result->fetch_assoc();
    echo $conn->error;
    close_conn();
    return ($row != NULL) ? $row['id'] : 0;
}

function _getPatientName($emr_id) {
    $query = "SELECT full_name FROM patient_table WHERE emr_id='$emr_id'";
    $conn = open_conn();
    $result = $conn->query($query);
    echo $conn->error;
    $row = $result->fetch_assoc();
    close_conn();
    return $row;
}

// <editor-fold defaultstate="collapsed" desc="">




function is_book_appointment($emr_id) {
    $last_id = "";
    $query = "SELECT emr_id FROM visit_table WHERE emr_id='$emr_id' AND is_signed='0'";
    $conn = open_conn();
    $result = $conn->query($query);
    echo $conn->error;

    if ($result->num_rows > 0) {
        close_conn();
        return 1;
    } else {
        close_conn();
        return 0;
    }
}

// </editor-fold>
function _fetchInboundPatient() {
    $items = array();
    $query = "SELECT patient_table.full_name, admission_table.emr_id, reason_for_admission, requested_date, requested_by, admission_table.id "
            . "FROM admission_table INNER JOIN patient_table ON admission_table.emr_id = patient_table.emr_id "
            . "  WHERE is_admitted=0 AND is_discharged=0";
    $conn = open_conn();

    $result = $conn->query($query);
    echo $conn->error;
    while ($row = $result->fetch_assoc()) {
        $items[] = $row;
    }

    close_conn();
    return $items;
}

function _fetchScheduledProcedure() {
    $items = array();
    $query = "SELECT patient_table.full_name, procedure_table.emr_id, procedure_table.id, name, primary_diagnosis, schedule_date,bill, request_note,requested_by"
            . " FROM procedure_table INNER JOIN patient_table ON procedure_table.emr_id = patient_table.emr_id "
            . "  WHERE status=0 ORDER BY procedure_table.id DESC";
    $conn = open_conn();

    $result = $conn->query($query);
    echo $conn->error;
    while ($row = $result->fetch_assoc()) {
        $items[] = $row;
    }

    close_conn();
    return $items;
}

function _fetchOnGoingProcedure() {
    $items = array();
    $query = "SELECT patient_table.full_name, procedure_table.emr_id, procedure_table.id, name, primary_diagnosis, schedule_date,bill, request_note,requested_by"
            . " FROM procedure_table INNER JOIN patient_table ON procedure_table.emr_id = patient_table.emr_id "
            . "  WHERE status=1 ORDER BY procedure_table.id DESC";
    $conn = open_conn();

    $result = $conn->query($query);
    echo $conn->error;
    while ($row = $result->fetch_assoc()) {
        $items[] = $row;
    }

    close_conn();
    return $items;
}

function _filterInboundPatient($clinic) {
    $items = array();
    $query = "SELECT patient_table.full_name, admission_table.emr_id, reason_for_admission, requested_date, requested_by, admission_table.id "
            . "FROM admission_table INNER JOIN patient_table ON admission_table.emr_id = patient_table.emr_id "
            . "  WHERE is_admitted=0 AND is_discharged=0 AND clinic='$clinic'";
    $conn = open_conn();

    $result = $conn->query($query);
    echo $conn->error;
    while ($row = $result->fetch_assoc()) {
        $items[] = $row;
    }

    close_conn();
    return $items;
}

function _fetchAdmitedPatient() {
    $items = array();
    $query = "SELECT patient_table.id, admission_table.id, admission_date, admission_table.emr_id,  patient_table.full_name, reason_for_admission, clinic, ward, location, requested_by "
            . "FROM admission_table INNER JOIN patient_table ON admission_table.emr_id = patient_table.emr_id "
            . "  WHERE is_admitted=1 AND is_discharged=0";
    $conn = open_conn();

    $result = $conn->query($query);
    echo $conn->error;
    while ($row = $result->fetch_array()) {
        $items[] = $row;
    }

    close_conn();
    return $items;
}

function _filterAdmittedPatient($clinic, $ward) {
    $items = array();
    $query = "SELECT patient_table.id, admission_table.id, admission_date, admission_table.emr_id,  patient_table.full_name, reason_for_admission, clinic, ward, location, requested_by "
            . "FROM admission_table INNER JOIN patient_table ON admission_table.emr_id = patient_table.emr_id "
            . "  WHERE is_admitted=1 AND is_discharged=0 AND clinic='$clinic' AND ward='$ward'";
    $conn = open_conn();

    $result = $conn->query($query);
    echo $conn->error;
    while ($row = $result->fetch_array()) {
        $items[] = $row;
    }

    close_conn();
    return $items;
}

function _fetchDischargedPatient() {
    $items = array();
    $query = "SELECT patient_table.id,  admission_table.id, admission_date,discharge_date, admission_table.emr_id,  patient_table.full_name, reason_for_admission, clinic, ward, location, discharged_by "
            . "FROM admission_table INNER JOIN patient_table ON admission_table.emr_id = patient_table.emr_id "
            . "  WHERE is_admitted=1 AND is_discharged=1";
    $conn = open_conn();

    $result = $conn->query($query);
    echo $conn->error;
    while ($row = $result->fetch_array()) {
        $items[] = $row;
    }

    close_conn();
    return $items;
}

function _filterDischargdPatient($clinic, $ward) {
    $items = array();
    $query = "SELECT patient_table.id,  admission_table.id, admission_date,discharge_date, admission_table.emr_id,  patient_table.full_name, reason_for_admission, clinic, ward, location, discharged_by "
            . "FROM admission_table INNER JOIN patient_table ON admission_table.emr_id = patient_table.emr_id "
            . "  WHERE is_admitted=1 AND is_discharged=1 AND clinic='$clinic' AND ward='$ward'";
    $conn = open_conn();

    $result = $conn->query($query);
    echo $conn->error;
    while ($row = $result->fetch_array()) {
        $items[] = $row;
    }

    close_conn();
    return $items;
}

function _isPatientAdmitted($emr_id) {

    $query = "SELECT id, ward, admission_date FROM admission_table WHERE emr_id='$emr_id' AND is_admitted=1 AND is_discharged=0";
    $conn = open_conn();

    $result = $conn->query($query);
    echo $conn->error;
    $row = $result->fetch_assoc();

    close_conn();
    return $row;
}

function _findPatient($query) {
    $patients = array();
    $query = "SELECT * FROM patient_table WHERE full_name LIKE'%$query%' OR phone_number LIKE'%$query%' OR emr_id LIKE'%$query%' LIMIT 3";
    $conn = open_conn();
    $result = $conn->query($query);
    echo $conn->error;
    while ($row = $result->fetch_assoc()) {
        $patients[] = $row;
    }
    close_conn();
    return $patients;
}

function _findPatients($s_query) {
    $patients = array();
    $query = "SELECT * FROM patient_table WHERE full_name LIKE'%$s_query%' OR phone_number LIKE'%$s_query%' OR emr_id LIKE'%$s_query%' LIMIT 3";
    $conn = open_conn();
    $result = $conn->query($query);
    echo $conn->error;
    while ($row = $result->fetch_assoc()) {
        $patients[] = $row;
    }
    close_conn();
    return $patients;
}
function _findPatientForBill($emr) {
    $patients = array();
    $query = "SELECT * FROM patient_table WHERE  emr_id ='$emr'";
    $conn = open_conn();
    $result = $conn->query($query);
    echo $conn->error;
    while ($row = $result->fetch_assoc()) {
        $patients[] = $row;
    }
    close_conn();
    return $patients;
}
function _findExternalPatient($phone) {
    $patients = array();
    $query = "SELECT * FROM patient_table WHERE  phone_number ='$phone'";
    $conn = open_conn();
    $result = $conn->query($query);
    echo $conn->error;
    while ($row = $result->fetch_assoc()) {
        $patients[] = $row;
    }
    close_conn();
    return $patients;
}

function _dischargePatient($id, $discharge_note, $discharged_by, $discharge_date) {
    $query = "UPDATE admission_table"
            . " SET is_discharged=1,"
            . " discharge_date='$discharge_date',"
            . " discharged_by='$discharged_by',"
            . "discharge_note='$discharge_note'"
            . " WHERE id='$id'"
    ;
    $conn = open_conn();
    $conn->query($query);
    echo $conn->error;

    close_conn();
}

function _documentTransaction($biller_id, $emr_id, $item_id, $item_type, $description, $price, $type, $balance, $signed_by, $quantity = 1, $transaction_date = transaction_date, $transaction_date_ = transaction_date_) {
    $conn = open_conn();
    $last_id = "";
    $query = "INSERT INTO transaction_table (biller_id,emr_id, item_id, item_type, description, price, type,balance, signed_by,quantity,transaction_date,transaction_date_)"
            . "	VALUES('$biller_id','$emr_id', '$item_id', '$item_type', '$description', '$price', '$type','$balance', '$signed_by','$quantity','$transaction_date','$transaction_date_')";

    if ($conn->query($query)) {
        $last_id = $conn->insert_id;
    }
    close_conn();
    return $last_id;
}

//AUTHENTICATORS

// <editor-fold defaultstate="collapsed" desc="Authenticate Customeradmin">
function _getServicePrice($id) {
    $conn = open_conn();
    $sql = "SELECT id, name,price FROM service_table WHERE id='$id' OR name='$id'";
    $result = $conn->query($sql);
    close_conn();
    echo $conn->error;
    return $result->fetch_assoc();
}

function _getLabPrice($id) {
    $conn = open_conn();
    $sql = "SELECT * FROM lab_category_item_table WHERE id='$id' OR name='$id'";
    $result = $conn->query($sql);
    close_conn();
    echo $conn->error;
    return $result->fetch_assoc();
}

function _getDrugInfo($id) {
    $conn = open_conn();
    $sql = "SELECT * FROM drug_table WHERE id='$id' OR name='$id'";
    $result = $conn->query($sql);
  
    echo $conn->error;
      close_conn();
    return $result->fetch_assoc();
}
function _getDrugId($name) {
    $conn = open_conn();
    $sql = "SELECT id FROM drug_table WHERE name='$name'";
    $result = $conn->query($sql);
  
    echo $conn->error;
      close_conn();
    return $result->fetch_assoc();
}
function _getDrugBatchCount($id) {
    $conn = open_conn();
    $sql = "SELECT id FROM drug_batch_table WHERE drug_id='$id' ";
    $result = $conn->query($sql);
  
    echo $conn->error;
      close_conn();
    return $result->num_rows;
}
function _getBatchDrugInfo($id) {
    $conn = open_conn();
    $sql = "SELECT * FROM drug_batch_table WHERE id='$id' OR batch_no='$id'";
    $result = $conn->query($sql);
    
    echo $conn->error;
    close_conn();
    return $result->fetch_assoc();
}

function _getBatchDrugInfoByBatchNo($id) {
    $conn = open_conn();
    $sql = "SELECT * FROM drug_batch_table WHERE  batch_no='$id'";
    $result = $conn->query($sql);
    close_conn();
    echo $conn->error;
    return $result->fetch_assoc();
}

function _getinventorySettings() {
    $conn = open_conn();
    $sql = "SELECT * FROM inventory_setting WHERE id=1";
    $result = $conn->query($sql);
    close_conn();
    echo $conn->error;
    return $result->fetch_assoc();
}

function _incrementDrugQuantity($id, $quantity) {
    $conn = open_conn();
    $sql = "UPDATE drug_table SET quantity='$quantity' WHERE id='$id'";
    $conn->query($sql);
    close_conn();
}
function _updateDrug($id, $name,$unit_of_measure,$reorder_level, $updated_by) {
    $conn = open_conn();
    $sql = "UPDATE drug_table SET name='$name', unit_of_measure='$unit_of_measure',reorder_level='$reorder_level',updated_by='$updated_by' WHERE id='$id'";
    $conn->query($sql);
    echo $conn->error;
    close_conn();
}
function _updateDrugBatch($id, $name,$expiration_date,$batch_no, $unit_price, $quantity, $updated_by) {
    $conn = open_conn();
    $sql = "UPDATE drug_batch_table SET drug_name='$name', expiration_date='$expiration_date',batch_no='$batch_no',unit_price='$unit_price',"
            . "quantity='$quantity',updated_by='$updated_by' WHERE id='$id'";
    $conn->query($sql);
    echo $conn->error;
    close_conn();
}
function _updateInventorySettings($item_expiration, $copay_rate, $low_qty_threshold){
        $conn = open_conn();
    $sql = "UPDATE inventory_setting SET item_expiration='$item_expiration',copay_rate='$copay_rate', low_qty_threshold='$low_qty_threshold' WHERE id=1";
    $conn->query($sql);
    close_conn();
}//need refactoring. only one ID either id or batch_no
function _updateDrugQty($quantity, $id) {

    $query = "UPDATE drug_batch_table SET quantity='$quantity' WHERE id='$id'";
    $conn = open_conn();
    $conn->query($query);
    close_conn();
}
function _increaseDrugQty($quantity, $id) {

    $query = "UPDATE drug_batch_table SET quantity='$quantity' WHERE batch_no='$id'";
    $conn = open_conn();
    $conn->query($query);
    close_conn();
}

function _getImagingPrice($id) {
    $conn = open_conn();
    $sql = "SELECT id,name,price FROM radiology_table WHERE id='$id' OR name='$id'";
    $result = $conn->query($sql);
    close_conn();
    echo $conn->error;
    return $result->fetch_assoc();
}

function _getProcedurePrice($id) {
    $conn = open_conn();
    $sql = "SELECT id,name,price FROM procedure_list_table WHERE id='$id' OR name='$id'";
    $result = $conn->query($sql);
    close_conn();
    echo $conn->error;
    return $result->fetch_assoc();
}
