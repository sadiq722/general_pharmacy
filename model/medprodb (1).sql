-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 01, 2021 at 02:14 PM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `medprodb`
--

-- --------------------------------------------------------

--
-- Table structure for table `admission_table`
--

CREATE TABLE `admission_table` (
  `id` int(11) NOT NULL,
  `emr_id` varchar(11) NOT NULL,
  `visit_id` int(11) DEFAULT NULL,
  `reason_for_admission` varchar(150) NOT NULL,
  `requested_date` datetime NOT NULL DEFAULT current_timestamp(),
  `admission_date` date NOT NULL DEFAULT current_timestamp(),
  `clinic` varchar(45) NOT NULL,
  `ward` varchar(45) NOT NULL,
  `is_admitted` int(1) NOT NULL DEFAULT 0,
  `requested_by` varchar(45) NOT NULL,
  `assign_bed_by` varchar(45) NOT NULL,
  `is_discharged` int(1) NOT NULL DEFAULT 0,
  `discharge_date` date NOT NULL,
  `discharged_by` varchar(25) NOT NULL,
  `location` varchar(75) NOT NULL,
  `discharge_note` varchar(2000) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admission_table`
--

INSERT INTO `admission_table` (`id`, `emr_id`, `visit_id`, `reason_for_admission`, `requested_date`, `admission_date`, `clinic`, `ward`, `is_admitted`, `requested_by`, `assign_bed_by`, `is_discharged`, `discharge_date`, `discharged_by`, `location`, `discharge_note`) VALUES
(1, '00000000006', 8, 'fever', '2021-04-24 10:50:13', '2021-04-24', 'MALE GOPD', 'AMENITY WARD', 1, 'test', 'test', 1, '2021-04-27', 'test', 'BED 2', 'pt discharged'),
(2, '00000000007', 9, 'test', '2021-04-26 15:49:30', '2021-04-27', 'MALE GOPD', 'AMENITY WARD', 1, 'test', 'test', 0, '0000-00-00', '', 'BED 2', '');

-- --------------------------------------------------------

--
-- Table structure for table `clinic_room_table`
--

CREATE TABLE `clinic_room_table` (
  `id` int(11) NOT NULL,
  `clinic_id` int(11) NOT NULL,
  `room` varchar(45) NOT NULL,
  `updated_by` varchar(75) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `clinic_room_table`
--

INSERT INTO `clinic_room_table` (`id`, `clinic_id`, `room`, `updated_by`) VALUES
(1, 1, 'CONSULTATION ROOM 2', 'y.sadiq'),
(2, 1, 'CONSULTATION ROOM 1', 'y.sadiq');

-- --------------------------------------------------------

--
-- Table structure for table `clinic_table`
--

CREATE TABLE `clinic_table` (
  `id` int(11) NOT NULL,
  `clinic` varchar(75) NOT NULL,
  `updated_by` varchar(75) NOT NULL,
  `last_update` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `clinic_table`
--

INSERT INTO `clinic_table` (`id`, `clinic`, `updated_by`, `last_update`) VALUES
(1, 'MALE GOPD', 'y.sadiq', '2021-02-13 17:25:57'),
(2, 'FEMALE GOPD', 'y.sadiq', '2021-02-13 17:25:29');

-- --------------------------------------------------------

--
-- Table structure for table `drug_measure_table`
--

CREATE TABLE `drug_measure_table` (
  `id` int(11) NOT NULL,
  `name` varchar(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `drug_measure_table`
--

INSERT INTO `drug_measure_table` (`id`, `name`) VALUES
(1, 'tablet');

-- --------------------------------------------------------

--
-- Table structure for table `drug_table`
--

CREATE TABLE `drug_table` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `unit_of_measure` varchar(45) NOT NULL,
  `quantity` int(11) NOT NULL,
  `unit_price` double NOT NULL,
  `updated_by` varchar(65) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `drug_table`
--

INSERT INTO `drug_table` (`id`, `name`, `unit_of_measure`, `quantity`, `unit_price`, `updated_by`) VALUES
(1, 'Panadol', 'tablet', 11, 150, 'test');

-- --------------------------------------------------------

--
-- Table structure for table `hospital_table`
--

CREATE TABLE `hospital_table` (
  `hospital_name` varchar(60) NOT NULL,
  `address` varchar(120) NOT NULL,
  `phone_number` varchar(12) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hospital_table`
--

INSERT INTO `hospital_table` (`hospital_name`, `address`, `phone_number`) VALUES
('Turai Yaradua Mertinaty Hospital Katsina', 'ADDRESS', '08000000000');

-- --------------------------------------------------------

--
-- Table structure for table `imaging_request_table`
--

CREATE TABLE `imaging_request_table` (
  `id` int(11) NOT NULL,
  `name` varchar(75) NOT NULL,
  `price` double NOT NULL,
  `seperate_settlement` int(1) NOT NULL DEFAULT 0,
  `request_note` varchar(400) NOT NULL,
  `emr_id` varchar(11) NOT NULL,
  `visit_id` int(11) NOT NULL,
  `result` varchar(600) NOT NULL,
  `result_note` varchar(1000) NOT NULL,
  `request_date` datetime NOT NULL DEFAULT current_timestamp(),
  `requested_by` varchar(75) NOT NULL,
  `uploaded_by` varchar(70) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `imaging_request_table`
--

INSERT INTO `imaging_request_table` (`id`, `name`, `price`, `seperate_settlement`, `request_note`, `emr_id`, `visit_id`, `result`, `result_note`, `request_date`, `requested_by`, `uploaded_by`) VALUES
(3, 'CT SCAN', 0, 0, '                                \r\n                            test', '00000000001', 0, '', '', '2021-02-14 12:25:23', 'test', ''),
(4, 'X-RAY Abdomen', 0, 0, 'ASAP', '00000000001', 0, '', '', '2021-03-03 17:25:09', 'test', ''),
(5, 'Ultrasound', 0, 0, 'TEST', '00000000001', 0, '', '', '2021-03-03 17:25:22', 'test', ''),
(6, 'X-RAY Abdomen', 0, 0, 'test', '00000000007', 0, '', '', '2021-04-27 12:47:15', 'test', ''),
(8, 'CT SCAN', 0, 0, 'mm', '00000000007', 0, '', '', '2021-04-29 12:01:03', 'test', ''),
(9, 'CT SCAN', 0, 0, 'www', '00000000007', 0, '', '', '2021-05-01 14:55:33', 'test', ''),
(10, 'CT SCAN', 25000, 0, 'www', '00000000007', 0, '', '', '2021-05-01 14:56:03', 'test', '');

-- --------------------------------------------------------

--
-- Table structure for table `insurance_table`
--

CREATE TABLE `insurance_table` (
  `id` int(11) NOT NULL,
  `hmo` varchar(111) NOT NULL,
  `hmo_abbr` varchar(25) NOT NULL,
  `phone_number` varchar(21) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `insurance_table`
--

INSERT INTO `insurance_table` (`id`, `hmo`, `hmo_abbr`, `phone_number`) VALUES
(2, 'STATE NATIONAL HEALTH INSURANCE SCHEME', 'GNHIS', '08056568978');

-- --------------------------------------------------------

--
-- Table structure for table `in_patient_note_table`
--

CREATE TABLE `in_patient_note_table` (
  `id` int(11) NOT NULL,
  `admission_id` int(11) NOT NULL,
  `note` varchar(5000) NOT NULL,
  `noted_by` varchar(45) NOT NULL,
  `date_taken` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `in_patient_note_table`
--

INSERT INTO `in_patient_note_table` (`id`, `admission_id`, `note`, `noted_by`, `date_taken`) VALUES
(1, 2, '<p>bbjhbjvhbhjd</p>', 'test', '2021-04-27 11:31:09');

-- --------------------------------------------------------

--
-- Table structure for table `lab_category_item_table`
--

CREATE TABLE `lab_category_item_table` (
  `id` int(11) NOT NULL,
  `lab_category_id` int(11) NOT NULL,
  `name` varchar(75) NOT NULL,
  `result_template` varchar(700) DEFAULT NULL,
  `is_available` int(1) NOT NULL DEFAULT 1,
  `price` double NOT NULL,
  `updated_by` varchar(75) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `lab_category_item_table`
--

INSERT INTO `lab_category_item_table` (`id`, `lab_category_id`, `name`, `result_template`, `is_available`, `price`, `updated_by`) VALUES
(1, 1, 'MPS', 'RESULT', 1, 400, 'test'),
(2, 1, 'URINALYSIS', 'HB,PROTEIN,PH,KETONE,GLUCOSE,BILLIRUBEN', 0, 500, 'test'),
(3, 1, 'yau', 'kaza kaza', 0, 100, 'test'),
(4, 1, 'new test', 'result1,result2', 0, 2500, 'test');

-- --------------------------------------------------------

--
-- Table structure for table `lab_category_table`
--

CREATE TABLE `lab_category_table` (
  `id` int(11) NOT NULL,
  `name` varchar(75) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `lab_category_table`
--

INSERT INTO `lab_category_table` (`id`, `name`) VALUES
(1, 'MICRBIOLOGY');

-- --------------------------------------------------------

--
-- Table structure for table `lab_investigation_table`
--

CREATE TABLE `lab_investigation_table` (
  `id` int(11) NOT NULL,
  `emr_id` varchar(11) NOT NULL,
  `visit_id` int(11) NOT NULL DEFAULT 0,
  `request_date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `price` double NOT NULL,
  `seperate_settlement` int(1) NOT NULL DEFAULT 0,
  `lab_test` varchar(25) NOT NULL,
  `request_note` varchar(120) NOT NULL,
  `requested_by` varchar(45) NOT NULL,
  `result` varchar(500) NOT NULL,
  `attachment` varchar(500) NOT NULL,
  `conducted_by` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lab_investigation_table`
--

INSERT INTO `lab_investigation_table` (`id`, `emr_id`, `visit_id`, `request_date`, `price`, `seperate_settlement`, `lab_test`, `request_note`, `requested_by`, `result`, `attachment`, `conducted_by`) VALUES
(1, '00000000001', 1, '2021-03-03 16:03:54', 0, 0, 'MPS', 'TEST', 'test', 'positive', '', 'test'),
(2, '00000000001', 1, '2021-03-03 16:04:14', 0, 0, 'URINALYSIS', 'EE', 'test', '+,++,+,+,+++,+', '', 'test'),
(5, '00000000001', 1, '2021-04-24 09:57:20', 0, 0, 'MPS', 'yy', 'test', 'positive', '', 'test'),
(6, '00000000001', 0, '2021-02-14 11:06:35', 0, 0, 'MPS', 'test', 'test', '', '', ''),
(7, '00000000001', 2, '2021-02-14 11:25:03', 0, 0, 'URINALYSIS', 'jjh', 'test', '', '', ''),
(8, '00000000002', 3, '2021-05-06 16:46:44', 0, 1, 'MPS', 'jhhjh', 'test', '-VE', '', 'test'),
(9, '00000000006', 8, '2021-04-24 09:44:38', 0, 0, 'MPS', 'bnb', 'test', '', '', ''),
(11, '00000000007', 10, '2021-04-27 11:46:09', 0, 0, 'MPS', 'note', 'test', '', '', ''),
(12, '00000000007', 10, '2021-04-27 11:46:14', 0, 0, 'URINALYSIS', 'note', 'test', '', '', ''),
(14, '00000000001', 0, '2021-05-01 14:13:51', 0, 1, 'MPS', 'hhh', 'test', '', '', ''),
(15, '00000000002', 0, '2021-04-30 13:31:47', 120, 1, 'MPS', 'hh', 'test', '', '', ''),
(18, '00000000002', 0, '2021-04-30 13:38:22', 120, 1, 'MPS', 'w', 'test', '', '', ''),
(19, '00000000007', 0, '2021-05-01 13:57:21', 120, 0, 'MPS', 'www', 'test', '', '', ''),
(20, '00000000010', 0, '2021-05-02 09:52:29', 120, 0, 'MPS', 'nnn', 'test', '', '', ''),
(21, '00000000002', 0, '2021-05-02 10:02:54', 400, 0, 'MPS', 'nnn', 'test', '', '', ''),
(22, '00000000002', 17, '2021-05-05 11:40:25', 400, 0, 'MPS', 'vnvhn', 'test', '', '', ''),
(24, '00000000002', 17, '2021-05-06 16:44:48', 400, 0, 'MPS', 'TEST', 'test', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `last_vitals_table`
--

CREATE TABLE `last_vitals_table` (
  `id` int(11) NOT NULL,
  `emr_id` varchar(11) NOT NULL,
  `type` varchar(95) NOT NULL,
  `reading` varchar(45) NOT NULL DEFAULT '0',
  `checked_by` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `last_vitals_table`
--

INSERT INTO `last_vitals_table` (`id`, `emr_id`, `type`, `reading`, `checked_by`) VALUES
(1, '00000000001', 'Blood Pressure (mmHg)', '12', 'test'),
(2, '00000000001', 'Glucose (mg/dl)', '0', ''),
(3, '00000000001', 'Pulse (beats/min)', '0', ''),
(4, '00000000001', 'Respiration (beats/min)', '0', ''),
(5, '00000000001', 'Tempreture (oC)', '35', 'test'),
(6, '00000000002', 'Blood Pressure (mmHg)', '120/80', 'test'),
(7, '00000000002', 'Glucose (mg/dl)', '0', ''),
(8, '00000000002', 'Pulse (beats/min)', '0', ''),
(9, '00000000002', 'Respiration (beats/min)', '0', ''),
(10, '00000000002', 'Tempreture (oC)', '0', ''),
(11, '00000000003', 'Blood Pressure (mmHg)', '0', ''),
(12, '00000000003', 'Glucose (mg/dl)', '0', ''),
(13, '00000000003', 'Pulse (beats/min)', '0', ''),
(14, '00000000003', 'Respiration (beats/min)', '0', ''),
(15, '00000000003', 'Tempreture (oC)', '0', ''),
(16, '00000000004', 'Blood Pressure (mmHg)', '120/80', 'test'),
(17, '00000000004', 'Glucose (mg/dl)', '0', ''),
(18, '00000000004', 'Pulse (beats/min)', '0', ''),
(19, '00000000004', 'Respiration (beats/min)', '0', ''),
(20, '00000000004', 'Tempreture (oC)', '0', ''),
(21, '00000000005', 'Blood Pressure (mmHg)', '0', ''),
(22, '00000000005', 'Glucose (mg/dl)', '0', ''),
(23, '00000000005', 'Pulse (beats/min)', '0', ''),
(24, '00000000005', 'Respiration (beats/min)', '0', ''),
(25, '00000000005', 'Tempreture (oC)', '0', ''),
(26, '00000000006', 'Blood Pressure (mmHg)', '180/20', 'test'),
(27, '00000000006', 'Glucose (mg/dl)', '0', ''),
(28, '00000000006', 'Pulse (beats/min)', '0', ''),
(29, '00000000006', 'Respiration (beats/min)', '0', ''),
(30, '00000000006', 'Tempreture (oC)', '0', ''),
(31, '00000000007', 'Blood Pressure (mmHg)', '0', ''),
(32, '00000000007', 'Glucose (mg/dl)', '0', ''),
(33, '00000000007', 'Pulse (beats/min)', '0', ''),
(34, '00000000007', 'Respiration (beats/min)', '0', ''),
(35, '00000000007', 'Tempreture (oC)', '0', ''),
(36, '00000000008', 'Blood Pressure (mmHg)', '0', ''),
(37, '00000000008', 'Glucose (mg/dl)', '0', ''),
(38, '00000000008', 'Pulse (beats/min)', '0', ''),
(39, '00000000008', 'Respiration (beats/min)', '0', ''),
(40, '00000000008', 'Tempreture (oC)', '0', ''),
(41, '00000000009', 'Blood Pressure (mmHg)', '0', ''),
(42, '00000000009', 'Glucose (mg/dl)', '0', ''),
(43, '00000000009', 'Pulse (beats/min)', '0', ''),
(44, '00000000009', 'Respiration (beats/min)', '0', ''),
(45, '00000000009', 'Tempreture (oC)', '0', ''),
(46, '00000000010', 'Blood Pressure (mmHg)', '0', ''),
(47, '00000000010', 'Glucose (mg/dl)', '0', ''),
(48, '00000000010', 'Pulse (beats/min)', '0', ''),
(49, '00000000010', 'Respiration (beats/min)', '0', ''),
(50, '00000000010', 'Tempreture (oC)', '0', ''),
(51, '00000000011', 'Blood Pressure (mmHg)', '0', ''),
(52, '00000000011', 'Glucose (mg/dl)', '0', ''),
(53, '00000000011', 'Pulse (beats/min)', '0', ''),
(54, '00000000011', 'Respiration (beats/min)', '0', ''),
(55, '00000000011', 'Tempreture (oC)', '0', '');

-- --------------------------------------------------------

--
-- Table structure for table `nurse_observation_table`
--

CREATE TABLE `nurse_observation_table` (
  `id` int(11) NOT NULL,
  `admission_id` int(11) NOT NULL,
  `note` varchar(5000) NOT NULL,
  `noted_by` varchar(100) NOT NULL,
  `note_date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `nurse_observation_table`
--

INSERT INTO `nurse_observation_table` (`id`, `admission_id`, `note`, `noted_by`, `note_date`) VALUES
(1, 2, '<p>hjbhjbb</p>', 'test', '2021-04-27 11:31:20');

-- --------------------------------------------------------

--
-- Table structure for table `patient_billing_account`
--

CREATE TABLE `patient_billing_account` (
  `id` int(11) NOT NULL,
  `emr_id` varchar(11) NOT NULL,
  `balance` double NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `patient_billing_account`
--

INSERT INTO `patient_billing_account` (`id`, `emr_id`, `balance`) VALUES
(1, '00000000001', 539),
(2, '00000000002', 2405),
(3, '00000000003', 2500),
(4, '00000000004', 0),
(5, '00000000006', -14500),
(6, '00000000007', -12120),
(7, '00000000008', 500),
(8, '00000000009', -500),
(9, '00000000010', -2620),
(10, '00000000011', -500);

-- --------------------------------------------------------

--
-- Table structure for table `patient_table`
--

CREATE TABLE `patient_table` (
  `id` int(11) NOT NULL,
  `emr_id` varchar(11) NOT NULL,
  `phone_number` varchar(14) NOT NULL,
  `occupation` varchar(15) NOT NULL,
  `full_name` varchar(45) NOT NULL,
  `type` varchar(100) NOT NULL,
  `principal_emr` varchar(11) NOT NULL,
  `blood_group` varchar(5) DEFAULT NULL,
  `dob` date NOT NULL,
  `gender` varchar(6) NOT NULL,
  `genotype` varchar(3) DEFAULT NULL,
  `address` varchar(60) NOT NULL,
  `NOK_name` varchar(45) NOT NULL,
  `NOK_address` varchar(65) NOT NULL,
  `NOK_phone_number` varchar(15) NOT NULL,
  `last_update` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_by` varchar(75) NOT NULL,
  `passport` varchar(200) NOT NULL,
  `created_at` date NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `patient_table`
--

INSERT INTO `patient_table` (`id`, `emr_id`, `phone_number`, `occupation`, `full_name`, `type`, `principal_emr`, `blood_group`, `dob`, `gender`, `genotype`, `address`, `NOK_name`, `NOK_address`, `NOK_phone_number`, `last_update`, `updated_by`, `passport`, `created_at`) VALUES
(1, '00000000001', '08000000000', 'TEST OCCUPATION', 'TEST PATIENT', 'Private', '00000000001', 'TEST ', '1976-02-13', 'Male', 'TE1', 'NO TEST 1 TEST ADDRESS', 'TEST NEXT OF KIN FULLNAME', 'NO TEST 1TEST NEXT OF KIN ADDRESS', '08025250000', '2021-02-13 16:42:27', 'y.sadiq', '00000000001.jpg', '2021-02-13'),
(2, '00000000002', '08022222222', '', 'TEST 2', 'Retainership', '00000000002', '', '1996-02-13', 'Female', '', 'TEST2', 'TEST2', 'TEST ADDRESS', '08022225555', '2021-02-13 17:44:37', 'y.sadiq', '00000000002.jpg', '2021-02-13'),
(3, '00000000003', '08036365958', '', 'TEST 3', '2', '00000000003', '', '1993-02-13', 'Male', '', 'TEST ADDRESS', 'TEST NAME', 'TEST ADDRESS NOK', '08025256363', '2021-02-13 17:46:52', 'y.sadiq', '00000000003.jpg', '2021-02-13'),
(4, '00000000004', '08025253636', '', 'TESTNG', 'Family', '00000000004', '', '1977-02-13', 'Female', '', 'TESTING', 'TEST', 'TESTING', '08025256363', '2021-02-13 21:57:42', 'y.sadiq', '00000000004.jpg', '2021-02-13'),
(5, '00000000005', '08055225522', '', 'TEST', 'Family', '00000000004', '', '1993-02-13', 'Female', '', 'TEST ADDRESS', 'TEST', 'TEST', '08055212145', '2021-02-13 22:10:36', 'y.sadiq', '00000000005.jpg', '2021-02-13'),
(6, '00000000006', '08025256363', 'student', 'sadiq', 'Private', '00000000006', '', '1996-04-24', 'Male', '', 'katsina', 'sadiq', 'kt', '08020202020', '2021-04-24 09:40:59', 'test', '00000000006.jpg', '2021-04-24'),
(7, '00000000007', '08000000000', 'test oc', 'Last Test', 'Private', '00000000007', 'O', '1996-03-26', 'Male', 'AA', 'test addess', 'test kin name', 'test kin address', '08000000000', '2021-04-26 14:50:40', 'test', '00000000007.jpg', '2021-04-26'),
(8, '00000000008', '07063490700', '', 'sadiq', '', '00000000008', NULL, '0000-00-00', '', NULL, '', '', '', '', '2021-05-02 09:43:47', 'test', '', '2021-05-02'),
(9, '00000000009', '09099614141', '', 'abubakar', '', '00000000009', NULL, '0000-00-00', '', NULL, '', '', '', '', '2021-05-02 09:46:22', 'test', '', '2021-05-02'),
(10, '00000000010', '07063490701', '', 'daura', 'Private', '00000000010', NULL, '0000-00-00', '', NULL, '', '', '', '', '2021-05-02 09:50:09', 'test', '', '2021-05-02'),
(11, '00000000011', '677787', 'HHVH', 'TEVGVH', 'Private', '00000000011', 'VHJBJ', '2021-12-30', 'Female', 'VHG', 'VGHV', 'GHVGH', 'BHBJJB', '67687', '2021-05-06 16:43:08', 'test', '', '2021-05-06');

-- --------------------------------------------------------

--
-- Table structure for table `prescription_table`
--

CREATE TABLE `prescription_table` (
  `id` int(11) NOT NULL,
  `emr_id` varchar(11) NOT NULL,
  `visit_id` int(11) DEFAULT NULL,
  `prescription_date` datetime NOT NULL DEFAULT current_timestamp(),
  `drug_name` varchar(25) NOT NULL,
  `duration` varchar(45) NOT NULL,
  `dose` varchar(45) NOT NULL,
  `requested_by` varchar(45) NOT NULL,
  `is_served` int(1) NOT NULL DEFAULT 0,
  `note` varchar(120) NOT NULL,
  `served_by` varchar(75) NOT NULL,
  `served_quantity` int(11) NOT NULL,
  `price` double NOT NULL,
  `seperate_settlement` int(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `prescription_table`
--

INSERT INTO `prescription_table` (`id`, `emr_id`, `visit_id`, `prescription_date`, `drug_name`, `duration`, `dose`, `requested_by`, `is_served`, `note`, `served_by`, `served_quantity`, `price`, `seperate_settlement`) VALUES
(1, '00000000001', 1, '2021-02-14 10:23:15', 'Panadol', '7', '500mg', 'test', 1, '', 'test', 0, 0, 0),
(3, '00000000001', 4, '2021-02-14 11:14:52', 'Panadol', '5', '100mg', 'test', 1, '', 'test', 0, 0, 0),
(4, '00000000001', 1, '2021-02-14 11:39:09', 'Panadol', '4days', '15mg', 'test', 1, '', 'test', 0, 300, 0),
(5, '00000000001', 4, '2021-02-14 12:25:14', 'Panadol', '4', '20mg', 'test', 0, '', '', 0, 0, 0),
(6, '00000000007', 4, '2021-04-27 12:46:48', 'Panadol', '4', '200mg', 'test', 0, '', '', 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `printer_table`
--

CREATE TABLE `printer_table` (
  `id` int(11) NOT NULL,
  `share_name` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `printer_table`
--

INSERT INTO `printer_table` (`id`, `share_name`) VALUES
(1, 'XP-76C');

-- --------------------------------------------------------

--
-- Table structure for table `privilege_table`
--

CREATE TABLE `privilege_table` (
  `user_id` int(1) NOT NULL DEFAULT 0,
  `record` int(1) NOT NULL DEFAULT 0,
  `nurse` int(1) NOT NULL DEFAULT 0,
  `doctor` int(1) NOT NULL DEFAULT 0,
  `pharmacy` int(1) NOT NULL DEFAULT 0,
  `pharmacy_admin` int(1) NOT NULL DEFAULT 0,
  `lab` int(1) NOT NULL DEFAULT 0,
  `lab_admin` int(1) NOT NULL DEFAULT 0,
  `radiology` int(1) NOT NULL DEFAULT 0,
  `radiology_admin` int(1) NOT NULL DEFAULT 0,
  `bill` int(1) NOT NULL DEFAULT 0,
  `bill_admin` int(1) NOT NULL DEFAULT 0,
  `admin` int(1) NOT NULL DEFAULT 0,
  `super_admin` int(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `privilege_table`
--

INSERT INTO `privilege_table` (`user_id`, `record`, `nurse`, `doctor`, `pharmacy`, `pharmacy_admin`, `lab`, `lab_admin`, `radiology`, `radiology_admin`, `bill`, `bill_admin`, `admin`, `super_admin`) VALUES
(2, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1),
(4, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `procedure_attachment_table`
--

CREATE TABLE `procedure_attachment_table` (
  `id` int(11) NOT NULL,
  `procedure_id` int(11) NOT NULL,
  `description` varchar(100) NOT NULL,
  `attachment` varchar(500) NOT NULL,
  `attached_by` varchar(65) NOT NULL,
  `attached_date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `procedure_list_table`
--

CREATE TABLE `procedure_list_table` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `price` double NOT NULL,
  `updated_by` varchar(75) NOT NULL,
  `last_update` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `procedure_list_table`
--

INSERT INTO `procedure_list_table` (`id`, `name`, `price`, `updated_by`, `last_update`) VALUES
(1, 'CIRCUMCISION', 2000, 'y.sadiq', '2021-02-13 17:20:56');

-- --------------------------------------------------------

--
-- Table structure for table `procedure_medication_table`
--

CREATE TABLE `procedure_medication_table` (
  `id` int(11) NOT NULL,
  `procedure_id` int(11) NOT NULL,
  `drug_name` varchar(75) NOT NULL,
  `dose` varchar(45) NOT NULL,
  `duration` varchar(75) NOT NULL,
  `prescribed_by` varchar(75) NOT NULL,
  `prescribed_date` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `procedure_medication_table`
--

INSERT INTO `procedure_medication_table` (`id`, `procedure_id`, `drug_name`, `dose`, `duration`, `prescribed_by`, `prescribed_date`) VALUES
(1, 1, 'Panadol', 'bb', '66', 'test', '2021-02-14 12:26:39');

-- --------------------------------------------------------

--
-- Table structure for table `procedure_note_table`
--

CREATE TABLE `procedure_note_table` (
  `id` int(11) NOT NULL,
  `procedure_id` int(11) NOT NULL,
  `note_type` varchar(45) NOT NULL,
  `note` varchar(10000) NOT NULL,
  `speciality` varchar(75) NOT NULL,
  `noted_by` varchar(75) NOT NULL,
  `note_date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `procedure_note_table`
--

INSERT INTO `procedure_note_table` (`id`, `procedure_id`, `note_type`, `note`, `speciality`, `noted_by`, `note_date`) VALUES
(1, 1, 'Pre-Procedure', 'vhghg', 'surgeion', 'test', '2021-04-24 09:55:16'),
(2, 2, 'Pre-Procedure', 'ghghcg', 'dentalist', 'test', '2021-05-05 15:26:54'),
(3, 2, 'Pre-Procedure', 'BBJBDSV ', 'ANESTHETIC', 'test', '2021-05-06 16:49:48');

-- --------------------------------------------------------

--
-- Table structure for table `procedure_speciality_table`
--

CREATE TABLE `procedure_speciality_table` (
  `id` int(11) NOT NULL,
  `name` varchar(75) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `procedure_speciality_table`
--

INSERT INTO `procedure_speciality_table` (`id`, `name`) VALUES
(1, 'surgeion'),
(2, 'dentalist'),
(3, 'ANESTHETIC');

-- --------------------------------------------------------

--
-- Table structure for table `procedure_table`
--

CREATE TABLE `procedure_table` (
  `id` int(11) NOT NULL,
  `emr_id` varchar(11) NOT NULL,
  `visit_id` int(11) NOT NULL,
  `name` varchar(75) NOT NULL,
  `price` double NOT NULL,
  `seperate_settlement` int(1) NOT NULL DEFAULT 0,
  `primary_diagnosis` varchar(200) NOT NULL,
  `schedule_date` date NOT NULL,
  `request_note` varchar(100) NOT NULL,
  `requested_by` varchar(75) NOT NULL,
  `status` int(1) NOT NULL DEFAULT 0,
  `bill` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `procedure_table`
--

INSERT INTO `procedure_table` (`id`, `emr_id`, `visit_id`, `name`, `price`, `seperate_settlement`, `primary_diagnosis`, `schedule_date`, `request_note`, `requested_by`, `status`, `bill`) VALUES
(1, '00000000001', 1, 'CIRCUMCISION', 0, 0, 'TEST', '2021-02-14', 'TEST', 'test', 1, 1),
(2, '00000000006', 8, 'CIRCUMCISION', 0, 0, 'ghgh', '2021-04-24', 'hjhj', 'test', 1, 1),
(3, '00000000007', 10, 'CIRCUMCISION', 0, 0, 'test diag', '2021-04-27', 'note', 'test', 2, 1),
(5, '00000000007', 10, 'CIRCUMCISION', 2000, 0, 'u', '2021-04-30', 'u', 'test', 0, 1),
(6, '00000000007', 10, 'CIRCUMCISION', 2000, 0, 'www', '2021-05-01', 'wwww', 'test', 0, 1),
(7, '00000000004', 18, 'CIRCUMCISION', 2000, 0, 'jhyy', '2021-05-05', 'hjhh', 'test', 0, 1),
(8, '00000000002', 17, 'CIRCUMCISION', 2000, 0, 'JHHJ', '2021-05-06', 'JHJ', 'test', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `radiology_table`
--

CREATE TABLE `radiology_table` (
  `id` int(11) NOT NULL,
  `name` varchar(70) NOT NULL,
  `last_update` varchar(500) NOT NULL,
  `price` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `radiology_table`
--

INSERT INTO `radiology_table` (`id`, `name`, `last_update`, `price`) VALUES
(1, 'CT SCAN', 'test', 25000),
(2, 'X-RAY Abdomen', 'test', 4500),
(3, 'Ultrasound', 'test', 2500);

-- --------------------------------------------------------

--
-- Table structure for table `service_table`
--

CREATE TABLE `service_table` (
  `id` int(11) NOT NULL,
  `category` varchar(25) NOT NULL,
  `last_update` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `name` varchar(25) NOT NULL,
  `price` int(11) NOT NULL,
  `updated_by` varchar(75) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `service_table`
--

INSERT INTO `service_table` (`id`, `category`, `last_update`, `name`, `price`, `updated_by`) VALUES
(1, 'Record', '2021-02-13 17:16:33', 'Account Creation', 500, 'y.sadiq'),
(2, 'Appointment', '2020-12-14 10:55:17', 'Regular Appointment', 2000, '0'),
(3, 'Appointment', '2020-12-14 10:55:17', 'Brief/Follow Up', 1000, '0'),
(4, 'Appointment', '2020-12-14 10:55:17', 'Investigation Review', 500, '0');

-- --------------------------------------------------------

--
-- Table structure for table `transaction_table`
--

CREATE TABLE `transaction_table` (
  `id` int(11) NOT NULL,
  `auth_code` varchar(50) NOT NULL,
  `biller_id` int(11) NOT NULL,
  `emr_id` varchar(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `item_type` varchar(45) NOT NULL,
  `description` varchar(60) NOT NULL,
  `quantity` int(6) NOT NULL,
  `price` double NOT NULL,
  `type` varchar(21) NOT NULL,
  `balance` double NOT NULL,
  `transaction_date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `signed_by` varchar(45) NOT NULL,
  `is_reverse` int(1) NOT NULL DEFAULT 0,
  `is_settle` int(1) NOT NULL DEFAULT 0,
  `transaction_date_` date NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `transaction_table`
--

INSERT INTO `transaction_table` (`id`, `auth_code`, `biller_id`, `emr_id`, `item_id`, `item_type`, `description`, `quantity`, `price`, `type`, `balance`, `transaction_date`, `signed_by`, `is_reverse`, `is_settle`, `transaction_date_`) VALUES
(1, '', 1, '00000000001', 1, 'Record', 'Account Creation', 1, 1, 'Debit', -1, '2021-02-13 04:30:34', 'y.sadiq', 0, 0, '2021-02-13'),
(2, '', 1, '00000000001', 2, 'Record', 'Regular Appointment', 1, 2000, 'Debit', -2001, '2021-02-13 05:38:59', 'y.sadiq', 0, 0, '2021-02-13'),
(3, '', 2, '00000000002', 1, 'Record', 'Account Creation', 1, 500, 'Debit', -500, '2021-02-13 05:44:14', 'y.sadiq', 0, 0, '2021-02-13'),
(4, '', 3, '00000000003', 1, 'Record', 'Account Creation', 1, 500, 'Debit', -500, '2021-02-13 05:46:45', 'y.sadiq', 0, 0, '2021-02-13'),
(5, '', 4, '00000000004', 1, 'Record', 'Account Creation', 1, 500, 'Debit', -500, '2021-02-13 09:57:36', 'y.sadiq', 0, 0, '2021-02-13'),
(6, '', 4, '00000000005', 1, 'Record', 'Account Creation', 1, 500, 'Debit', -1000, '2021-02-13 10:10:29', 'y.sadiq', 0, 0, '2021-02-13'),
(7, '', 1, '00000000001', 1, 'Procedure', 'CIRCUMCISION', 1, 2000, 'Debit', -4001, '2021-02-14 09:53:52', 'test', 0, 0, '2021-02-14'),
(8, '', 1, '00000000001', 2, 'Record', 'Regular Appointment', 1, 2000, 'Debit', -6001, '2021-02-14 11:09:25', 'test', 0, 0, '2021-02-14'),
(9, '', 2, '00000000002', 2, 'Record', 'Regular Appointment', 1, 2000, 'Debit', -2500, '2021-02-14 11:11:57', 'test', 0, 0, '2021-02-14'),
(10, '', 3, '00000000003', 2, 'Record', 'Regular Appointment', 1, 2000, 'Debit', -2500, '2021-02-14 11:13:10', 'test', 0, 0, '2021-02-14'),
(11, '', 4, '00000000004', 2, 'Record', 'Regular Appointment', 1, 2000, 'Debit', -3000, '2021-02-14 11:14:43', 'test', 0, 0, '2021-02-14'),
(12, '', 1, '00000000001', 2, 'Record', 'Regular Appointment', 1, 2000, 'Debit', -8001, '2021-02-14 11:46:32', 'test', 0, 0, '2021-02-14'),
(13, '', 1, '00000000001', 2, 'Record', 'Regular Appointment', 1, 2000, 'Debit', -10001, '2021-02-21 11:01:43', 'test', 0, 0, '2021-02-21'),
(14, '0', 1, '00000000001', 0, 'Pharmacy', 'MMT', 1, 500, 'Credit', -9501, '2021-02-21 11:14:33', 'test', 0, 0, '2021-02-21'),
(15, '', 1, '00000000001', 1, 'Pharmacy', 'Panadol', 10, 1500, 'Debit', -11001, '2021-02-24 10:52:57', 'test', 0, 0, '2021-02-24'),
(16, '', 1, '00000000001', 1, 'Lab', 'MPS', 1, 120, 'Debit', -11121, '2021-03-03 04:03:54', 'test', 0, 0, '2021-03-03'),
(17, '', 1, '00000000001', 2, 'Lab', 'URINALYSIS', 1, 500, 'Debit', -11621, '2021-03-03 04:04:14', 'test', 0, 0, '2021-03-03'),
(18, '', 1, '00000000001', 3, 'Pharmacy', 'Panadol', 2, 300, 'Debit', -11921, '2021-03-03 04:04:31', 'test', 0, 0, '2021-03-03'),
(19, '', 5, '00000000006', 1, 'Record', 'Account Creation', 1, 500, 'Debit', -500, '2021-04-24 10:40:43', 'test', 0, 0, '2021-04-24'),
(20, '', 5, '00000000006', 2, 'Record', 'Regular Appointment', 1, 2000, 'Debit', -2500, '2021-04-24 10:42:52', 'test', 0, 0, '2021-04-24'),
(21, '', 5, '00000000006', 2, 'Procedure', 'CIRCUMCISION', 1, 2000, 'Debit', -4500, '2021-04-24 10:49:19', 'test', 0, 0, '2021-04-24'),
(22, '', 5, '00000000006', 1, 'Admission', 'AMENITY WARD bed charges', 1, 2500, 'Debit', -7000, '2021-04-24 10:52:25', 'test', 0, 0, '2021-04-24'),
(23, '', 1, '00000000001', 5, 'Lab', 'MPS', 1, 120, 'Debit', -12041, '2021-04-24 10:57:20', 'test', 0, 0, '2021-04-24'),
(24, '', 6, '00000000007', 1, 'Record', 'Account Creation', 1, 500, 'Debit', -500, '2021-04-26 03:31:51', 'test', 0, 0, '2021-04-26'),
(25, '', 6, '00000000007', 2, 'Record', 'Regular Appointment', 1, 2000, 'Debit', -2500, '2021-04-26 03:43:49', 'test', 0, 0, '2021-04-26'),
(26, '', 6, '00000000007', 3, 'Record', 'Brief/Follow Up', 1, 1000, 'Debit', -3500, '2021-04-26 04:36:40', 'test', 0, 0, '2021-04-26'),
(27, '', 6, '00000000007', 2, 'Record', 'Regular Appointment', 1, 2000, 'Debit', -5500, '2021-04-26 05:02:31', 'test', 0, 0, '2021-04-26'),
(28, '', 6, '00000000007', 12, 'Record', 'Investigation Review', 1, 500, 'Debit', -6000, '2021-04-26 05:25:12', 'test', 0, 0, '2021-04-26'),
(29, '', 0, '', 0, '', '', 1, 0, 'Reversed', 0, '2021-04-26 05:42:26', 'test', 0, 0, '2021-04-26'),
(30, '', 0, '', 0, '', '', 1, 0, 'Reversed', 0, '2021-04-26 05:51:11', 'test', 0, 0, '2021-04-26'),
(31, '', 4, '00000000004', 13, 'Record', 'Regular Appointment', 1, 2000, 'Debit', -5000, '2021-04-26 17:03:04', 'test', 1, 0, '2021-04-26'),
(32, '', 4, '00000000004', 13, 'Record', 'Regular Appointment', 1, 2000, 'Reversed', -3000, '2021-04-26 17:05:30', 'test', 1, 0, '2021-04-26'),
(33, '', 4, '00000000004', 13, 'Record', 'Regular Appointment', 1, 2000, 'Reversed', -1000, '2021-04-26 06:05:30', 'test', 0, 0, '2021-04-26'),
(34, '', 5, '00000000006', 1, 'Admission', 'AMENITY WARD bed charges', 1, 2500, 'Debit', -9500, '2021-04-25 11:00:00', 'test', 0, 0, '2021-04-25'),
(35, '', 5, '00000000006', 1, 'Admission', 'AMENITY WARD bed charges', 1, 2500, 'Debit', -12000, '2021-04-26 11:00:00', 'test', 0, 0, '2021-04-26'),
(36, '', 5, '00000000006', 1, 'Admission', 'AMENITY WARD bed charges', 1, 2500, 'Debit', -14500, '2021-04-27 11:00:00', 'test', 0, 0, '2021-04-27'),
(37, '', 6, '00000000007', 2, 'Admission', 'AMENITY WARD bed charges', 1, 2500, 'Debit', -8500, '2021-04-27 00:30:56', 'test', 0, 0, '2021-04-27'),
(38, '', 6, '00000000007', 3, 'Procedure', 'CIRCUMCISION', 1, 2000, 'Debit', -10500, '2021-04-27 00:40:34', 'test', 0, 0, '2021-04-27'),
(39, '', 6, '00000000007', 4, 'Procedure', 'CIRCUMCISION', 1, 2000, 'Debit', -12500, '2021-04-27 11:44:16', 'test', 1, 0, '2021-04-27'),
(40, '', 6, '00000000007', 4, 'Procedure', 'CIRCUMCISION', 1, 2000, 'Reversed', -10500, '2021-04-27 00:44:16', 'test', 0, 0, '2021-04-27'),
(41, '', 6, '00000000007', 2, 'Admission', 'AMENITY WARD bed charges', 1, 2500, 'Debit', -13000, '2021-04-28 11:00:00', 'test', 0, 0, '2021-04-28'),
(42, '', 6, '00000000007', 2, 'Admission', 'AMENITY WARD bed charges', 1, 2500, 'Debit', -15500, '2021-04-29 11:00:00', 'test', 0, 0, '2021-04-29'),
(43, '', 1, '00000000001', 14, 'Lab', 'MPS', 1, 120, 'Debit', -12161, '2021-04-29 11:56:23', 'test', 0, 0, '2021-04-29'),
(44, '', 6, '00000000007', 8, 'imaging', 'CT SCAN', 1, 25000, 'Debit', -40500, '2021-04-29 00:01:03', 'test', 0, 0, '2021-04-29'),
(45, '0', 2, '00000000002', 0, 'Admission', '200', 1, 2005, 'Credit', 2005, '2021-04-29 13:44:25', 'test', 0, 0, '2021-04-29'),
(46, '0', 2, '00000000002', 0, 'Admission', 'jjj', 1, 200, 'Credit', 2205, '2021-04-29 13:50:17', 'test', 0, 0, '2021-04-29'),
(47, '', 2, '00000000002', 15, 'Lab', 'MPS', 1, 120, 'Debit', 2085, '2021-04-29 05:24:16', 'test', 0, 0, '2021-04-29'),
(48, '0', 2, '00000000002', 0, 'Lab', 'MPS', 1, 0, 'Credit', 2085, '2021-04-30 13:29:10', 'test', 0, 0, '2021-04-30'),
(49, '0', 2, '00000000002', 0, 'Lab', 'MPS', 1, 120, 'Credit', 2205, '2021-04-30 13:31:47', 'test', 0, 0, '2021-04-30'),
(50, '', 0, '', 0, '', '', 1, 0, 'Reversed', 0, '2021-04-30 02:35:23', 'test', 0, 0, '2021-04-30'),
(51, '', 2, '00000000002', 16, 'Lab', 'MPS', 1, 120, 'Debit', 2085, '2021-04-30 13:37:07', 'test', 1, 0, '2021-04-30'),
(52, '', 2, '00000000002', 17, 'Lab', 'MPS', 1, 120, 'Debit', 1965, '2021-04-30 13:37:59', 'test', 1, 0, '2021-04-30'),
(53, '', 2, '00000000002', 18, 'Lab', 'MPS', 1, 120, 'Debit', 1845, '2021-04-30 02:36:55', 'test', 0, 0, '2021-04-30'),
(54, '', 2, '00000000002', 16, 'Lab', 'MPS', 1, 120, 'Reversed', 1965, '2021-04-30 02:37:06', 'test', 0, 0, '2021-04-30'),
(55, '', 2, '00000000002', 17, 'Lab', 'MPS', 1, 120, 'Reversed', 2085, '2021-04-30 02:37:58', 'test', 0, 0, '2021-04-30'),
(56, '0', 2, '00000000002', 0, 'Lab', 'MPS', 1, 120, 'Credit', 2205, '2021-04-30 13:38:22', 'test', 0, 0, '2021-04-30'),
(57, '', 2, '00000000002', 14, 'Record', 'Regular Appointment', 1, 2000, 'Debit', 205, '2021-05-01 14:30:55', 'test', 1, 0, '2021-04-30'),
(58, '', 2, '00000000002', 15, 'Record', 'Investigation Review', 1, 500, 'Debit', -295, '2021-04-30 03:37:15', 'test', 0, 0, '2021-04-30'),
(59, '', 6, '00000000007', 2, 'Admission', 'AMENITY WARD bed charges', 1, 2500, 'Debit', -43000, '2021-04-30 11:00:00', 'test', 0, 0, '2021-04-30'),
(60, '', 6, '00000000007', 5, 'Procedure', 'CIRCUMCISION', 1, 2000, 'Debit', -45000, '2021-04-30 03:49:00', 'test', 0, 0, '2021-04-30'),
(61, '', 1, '00000000001', 4, 'Pharmacy', 'Panadol', 2, 300, 'Debit', -12461, '2021-04-30 05:41:27', 'test', 0, 0, '2021-04-30'),
(62, '', 6, '00000000007', 2, 'Admission', 'AMENITY WARD bed charges', 1, 2500, 'Debit', -47500, '2021-05-01 11:00:00', 'test', 0, 0, '2021-05-01'),
(63, '', 6, '00000000007', 9, 'imaging', 'CT SCAN', 1, 25000, 'Debit', -72500, '2021-05-01 02:55:33', 'test', 0, 0, '2021-05-01'),
(64, '', 6, '00000000007', 10, 'imaging', 'CT SCAN', 1, 25000, 'Debit', -97500, '2021-05-01 02:56:03', 'test', 0, 0, '2021-05-01'),
(65, '', 6, '00000000007', 19, 'Lab', 'MPS', 1, 120, 'Debit', -97620, '2021-05-01 02:57:21', 'test', 0, 0, '2021-05-01'),
(66, '', 6, '00000000007', 6, 'Procedure', 'CIRCUMCISION', 1, 2000, 'Debit', -99620, '2021-05-01 03:02:01', 'test', 0, 0, '2021-05-01'),
(67, '0', 2, '00000000002', 0, 'Record', 'Regular Appointment', 1, 0, 'Credit', -295, '2021-05-01 14:13:50', 'test', 0, 0, '2021-05-01'),
(68, '0', 2, '00000000002', 0, 'Record', 'Investigation Review', 1, 500, 'Credit', 205, '2021-05-01 14:27:11', 'test', 0, 0, '2021-05-01'),
(69, '', 2, '00000000002', 14, 'Record', 'Regular Appointment', 1, 2000, 'Reversed', 2205, '2021-05-01 03:30:55', 'test', 0, 0, '2021-05-01'),
(70, '', 7, '00000000008', 1, '', 'Account Creation', 1, 500, '', 500, '2021-05-02 10:43:47', 'test', 0, 0, '2021-05-02'),
(71, '', 8, '00000000009', 1, 'Record', 'Account Creation', 1, 500, 'Debit', -500, '2021-05-02 10:46:22', 'test', 0, 0, '2021-05-02'),
(72, '', 9, '00000000010', 1, 'Record', 'Account Creation', 1, 500, 'Debit', -500, '2021-05-02 10:50:09', 'test', 0, 0, '2021-05-02'),
(73, '', 9, '00000000010', 20, 'Lab', 'MPS', 1, 120, 'Debit', -620, '2021-05-02 10:52:29', 'test', 0, 0, '2021-05-02'),
(74, '', 9, '00000000010', 16, 'Record', 'Regular Appointment', 1, 2000, 'Debit', -2620, '2021-05-02 11:01:24', 'test', 0, 0, '2021-05-02'),
(75, '', 2, '00000000002', 17, 'Record', 'Regular Appointment', 1, 2000, 'Debit', 205, '2021-05-02 11:01:50', 'test', 0, 0, '2021-05-02'),
(76, '', 2, '00000000002', 21, 'Lab', 'MPS', 1, 400, 'Debit', -195, '2021-05-02 11:02:54', 'test', 0, 0, '2021-05-02'),
(77, '0', 2, '00000000002', 0, 'Lab', 'lab test mps', 1, 400, 'Credit', 205, '2021-05-02 10:03:51', 'test', 0, 0, '2021-05-02'),
(78, '', 2, '00000000002', 22, 'Lab', 'MPS', 1, 400, 'Debit', -195, '2021-05-05 00:40:25', 'test', 0, 0, '2021-05-05'),
(79, '', 4, '00000000004', 18, 'Record', 'Regular Appointment', 1, 2000, 'Debit', -3000, '2021-05-05 02:28:38', 'test', 0, 0, '2021-05-05'),
(80, '0', 6, '00000000007', 0, 'others', 'test', 1, 100000, 'Credit', 380, '2021-05-05 13:33:04', 'test', 0, 0, '2021-05-05'),
(81, '', 6, '00000000007', 2, 'Admission', 'AMENITY WARD bed charges', 1, 2500, 'Debit', -2120, '2021-05-02 11:00:00', 'test', 0, 0, '2021-05-02'),
(82, '', 6, '00000000007', 2, 'Admission', 'AMENITY WARD bed charges', 1, 2500, 'Debit', -4620, '2021-05-03 11:00:00', 'test', 0, 0, '2021-05-03'),
(83, '', 6, '00000000007', 2, 'Admission', 'AMENITY WARD bed charges', 1, 2500, 'Debit', -7120, '2021-05-04 11:00:00', 'test', 0, 0, '2021-05-04'),
(84, '', 6, '00000000007', 2, 'Admission', 'AMENITY WARD bed charges', 1, 2500, 'Debit', -9620, '2021-05-05 11:00:00', 'test', 0, 0, '2021-05-05'),
(85, '0', 4, '00000000004', 0, 'Admission', 'mmm', 1, 5000, 'Credit', 2000, '2021-05-05 13:37:43', 'test', 0, 0, '2021-05-05'),
(86, '', 4, '00000000004', 23, 'Lab', 'MPS', 1, 400, 'Debit', 1600, '2021-05-05 15:21:05', 'test', 1, 0, '2021-05-05'),
(87, '', 4, '00000000004', 23, 'Lab', 'MPS', 1, 400, 'Reversed', 2000, '2021-05-05 04:21:05', 'test', 0, 0, '2021-05-05'),
(88, '', 4, '00000000004', 7, 'Procedure', 'CIRCUMCISION', 1, 2000, 'Debit', 0, '2021-05-05 04:24:14', 'test', 0, 0, '2021-05-05'),
(89, '0', 1, '00000000001', 0, 'Lab', 'test', 1, 13000, 'Credit', 539, '2021-05-05 15:28:31', 'test', 0, 0, '2021-05-05'),
(90, '0', 2, '00000000002', 0, 'Record', 'APPOINTMENT', 1, 5000, 'Credit', 4805, '2021-05-06 16:41:59', 'test', 0, 0, '2021-05-06'),
(91, '', 10, '00000000011', 1, 'Record', 'Account Creation', 1, 500, 'Debit', -500, '2021-05-06 05:43:08', 'test', 0, 0, '2021-05-06'),
(92, '', 2, '00000000002', 24, 'Lab', 'MPS', 1, 400, 'Debit', 4405, '2021-05-06 05:44:48', 'test', 0, 0, '2021-05-06'),
(93, '', 2, '00000000002', 8, 'Procedure', 'CIRCUMCISION', 1, 2000, 'Debit', 2405, '2021-05-06 05:47:31', 'test', 0, 0, '2021-05-06'),
(94, '', 6, '00000000007', 2, 'Admission', 'AMENITY WARD bed charges', 1, 2500, 'Debit', -12120, '2021-05-06 11:00:00', 'test', 0, 0, '2021-05-06');

-- --------------------------------------------------------

--
-- Table structure for table `user_table`
--

CREATE TABLE `user_table` (
  `id` int(11) NOT NULL,
  `username` varchar(15) NOT NULL,
  `password` varchar(255) NOT NULL,
  `first_name` varchar(25) NOT NULL,
  `last_name` varchar(25) NOT NULL,
  `phone_number` varchar(15) NOT NULL,
  `role` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_table`
--

INSERT INTO `user_table` (`id`, `username`, `password`, `first_name`, `last_name`, `phone_number`, `role`) VALUES
(2, 'y.sadiq', '700c8b805a3e2a265b01c77614cd8b21', 'Sadiq', 'Yusuf', '07063490700', ''),
(4, 'test', '700c8b805a3e2a265b01c77614cd8b21', 'TEST', 'USER', '08012345678', '');

-- --------------------------------------------------------

--
-- Table structure for table `visit_table`
--

CREATE TABLE `visit_table` (
  `id` int(11) NOT NULL,
  `emr_id` varchar(11) NOT NULL,
  `visit_date` date NOT NULL,
  `visit_type` varchar(69) NOT NULL,
  `price` double NOT NULL,
  `seperate_settlement` int(1) NOT NULL DEFAULT 0,
  `visit_note` varchar(10000) NOT NULL,
  `seen_by` varchar(45) NOT NULL,
  `booked_by` varchar(45) NOT NULL,
  `is_signed` int(1) NOT NULL DEFAULT 0,
  `clinic` varchar(75) NOT NULL,
  `room` varchar(45) NOT NULL,
  `diagnosis` varchar(5000) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `visit_table`
--

INSERT INTO `visit_table` (`id`, `emr_id`, `visit_date`, `visit_type`, `price`, `seperate_settlement`, `visit_note`, `seen_by`, `booked_by`, `is_signed`, `clinic`, `room`, `diagnosis`) VALUES
(1, '00000000001', '2021-02-14', 'Brief/Follow Up', 0, 0, '<p><u>Presenting complain</u></p><p>pt has fever and headach</p>', 'test', 'test', 1, 'MALE GOPD', 'CONSULTATION ROOM 2', '<p>pt diagnosis</p>'),
(2, '00000000001', '2021-02-14', 'Regular Appointment', 0, 0, '<p>pt has fever and headach</p>', 'test', 'test', 1, 'MALE GOPD', 'CONSULTATION ROOM 2', '<p>typhoid</p>'),
(3, '00000000002', '2021-02-14', 'Regular Appointment', 0, 0, '<p>follow up</p>', 'test', 'test', 1, 'MALE GOPD', 'CONSULTATION ROOM 1', '<p>pt diagnosis</p>'),
(4, '00000000003', '2021-02-15', 'Regular Appointment', 0, 0, '', '', 'test', 0, 'MALE GOPD', 'CONSULTATION ROOM 2', ''),
(5, '00000000004', '2021-02-14', 'Regular Appointment', 0, 0, '<p>Presenting complain </p>\r\n<p>History of presenting complain </p>', 'test', 'test', 1, 'MALE GOPD', 'CONSULTATION ROOM 2', '<p>pt Diagnosis</p>'),
(6, '00000000001', '2021-02-14', 'Regular Appointment', 0, 0, '<p>pt complain here</p>\r\n<p>hx of pt complain here</p>', 'test', 'test', 1, 'MALE GOPD', 'CONSULTATION ROOM 2', '<p>pt diagnosis</p>'),
(7, '00000000001', '2021-02-28', 'Regular Appointment', 0, 0, '', '', 'test', 0, 'MALE GOPD', 'CONSULTATION ROOM 2', ''),
(8, '00000000006', '2021-04-24', 'Regular Appointment', 0, 0, '<p>bjbjbjjk</p>', 'test', 'test', 1, 'MALE GOPD', 'CONSULTATION ROOM 1', '<p>vvhjvhj</p>'),
(9, '00000000007', '2021-04-26', 'Brief/Follow Up', 0, 0, '', '', 'test', 0, 'MALE GOPD', 'CONSULTATION ROOM 1', ''),
(10, '00000000007', '2021-04-27', 'Brief/Follow Up', 0, 0, '<p>pt has fever and headach</p>', 'test', 'test', 1, 'FEMALE GOPD', '', '<p>faver</p>'),
(15, '00000000002', '2021-04-30', 'Investigation Review', 500, 1, '', '', 'test', 0, 'MALE GOPD', 'CONSULTATION ROOM 1', ''),
(16, '00000000010', '2021-05-07', 'Regular Appointment', 2000, 0, '', '', 'test', 0, 'MALE GOPD', 'CONSULTATION ROOM 1', ''),
(17, '00000000002', '2021-05-06', 'Regular Appointment', 2000, 0, '', '', 'test', 0, 'MALE GOPD', 'CONSULTATION ROOM 2', ''),
(18, '00000000004', '2021-05-05', 'Regular Appointment', 2000, 0, '', '', 'test', 0, 'MALE GOPD', 'CONSULTATION ROOM 1', '');

-- --------------------------------------------------------

--
-- Table structure for table `vitals_type_table`
--

CREATE TABLE `vitals_type_table` (
  `id` int(11) NOT NULL,
  `type` varchar(75) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `vitals_type_table`
--

INSERT INTO `vitals_type_table` (`id`, `type`) VALUES
(1, 'Blood Pressure (mmHg)'),
(2, 'Glucose (mg/dl)'),
(3, 'Pulse (beats/min)'),
(4, 'Respiration (beats/min)'),
(5, 'Tempreture (oC)');

-- --------------------------------------------------------

--
-- Table structure for table `vital_sign_table`
--

CREATE TABLE `vital_sign_table` (
  `id` int(11) NOT NULL,
  `emr_id` varchar(11) NOT NULL,
  `type` varchar(45) NOT NULL,
  `reading` varchar(45) NOT NULL,
  `checked_by` varchar(45) NOT NULL,
  `reading_date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vital_sign_table`
--

INSERT INTO `vital_sign_table` (`id`, `emr_id`, `type`, `reading`, `checked_by`, `reading_date`) VALUES
(1, '00000000001', 'Blood Pressure (mmHg)', '100/10', 'y.sadiq', '2021-02-13 22:21:10'),
(2, '00000000001', 'Blood Pressure (mmHg)', '250/23', 'y.sadiq', '2021-02-13 22:43:54'),
(3, '00000000001', 'Blood Pressure (mmHg)', '10/10', 'y.sadiq', '2021-02-13 22:51:36'),
(4, '00000000001', 'Blood Pressure (mmHg)', '12', 'test', '2021-02-13 22:59:54'),
(5, '00000000001', 'Tempreture (oC)', '35', 'test', '2021-03-04 09:46:21'),
(6, '00000000006', 'Blood Pressure (mmHg)', '180/20', 'test', '2021-04-24 09:45:48'),
(7, '00000000002', 'Blood Pressure (mmHg)', '120/80', 'test', '2021-05-05 11:41:07'),
(8, '00000000004', 'Blood Pressure (mmHg)', '120/80', 'test', '2021-05-05 15:21:48');

-- --------------------------------------------------------

--
-- Table structure for table `ward_location_table`
--

CREATE TABLE `ward_location_table` (
  `id` int(11) NOT NULL,
  `ward_id` int(11) NOT NULL,
  `location` varchar(120) NOT NULL,
  `type` varchar(70) NOT NULL,
  `updated_by` varchar(85) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `ward_location_table`
--

INSERT INTO `ward_location_table` (`id`, `ward_id`, `location`, `type`, `updated_by`) VALUES
(1, 2, 'BED 2', '', 'y.sadiq'),
(2, 2, 'BED 1', '', 'y.sadiq');

-- --------------------------------------------------------

--
-- Table structure for table `ward_table`
--

CREATE TABLE `ward_table` (
  `id` int(11) NOT NULL,
  `clinic_id` int(11) NOT NULL,
  `ward` varchar(45) NOT NULL,
  `price` double NOT NULL,
  `updated_by` varchar(75) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `ward_table`
--

INSERT INTO `ward_table` (`id`, `clinic_id`, `ward`, `price`, `updated_by`) VALUES
(1, 1, 'MALE WARD 1', 5001, 'y.sadiq'),
(2, 1, 'AMENITY WARD', 2500, 'y.sadiq');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admission_table`
--
ALTER TABLE `admission_table`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `clinic_room_table`
--
ALTER TABLE `clinic_room_table`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `clinic_table`
--
ALTER TABLE `clinic_table`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `drug_measure_table`
--
ALTER TABLE `drug_measure_table`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `drug_table`
--
ALTER TABLE `drug_table`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `imaging_request_table`
--
ALTER TABLE `imaging_request_table`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `insurance_table`
--
ALTER TABLE `insurance_table`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `in_patient_note_table`
--
ALTER TABLE `in_patient_note_table`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lab_category_item_table`
--
ALTER TABLE `lab_category_item_table`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lab_category_table`
--
ALTER TABLE `lab_category_table`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lab_investigation_table`
--
ALTER TABLE `lab_investigation_table`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `last_vitals_table`
--
ALTER TABLE `last_vitals_table`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `nurse_observation_table`
--
ALTER TABLE `nurse_observation_table`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `patient_billing_account`
--
ALTER TABLE `patient_billing_account`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `patient_table`
--
ALTER TABLE `patient_table`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `prescription_table`
--
ALTER TABLE `prescription_table`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `printer_table`
--
ALTER TABLE `printer_table`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `privilege_table`
--
ALTER TABLE `privilege_table`
  ADD UNIQUE KEY `user_id` (`user_id`);

--
-- Indexes for table `procedure_attachment_table`
--
ALTER TABLE `procedure_attachment_table`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `procedure_list_table`
--
ALTER TABLE `procedure_list_table`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `procedure_medication_table`
--
ALTER TABLE `procedure_medication_table`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `procedure_note_table`
--
ALTER TABLE `procedure_note_table`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `procedure_speciality_table`
--
ALTER TABLE `procedure_speciality_table`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `procedure_table`
--
ALTER TABLE `procedure_table`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `radiology_table`
--
ALTER TABLE `radiology_table`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `service_table`
--
ALTER TABLE `service_table`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `transaction_table`
--
ALTER TABLE `transaction_table`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_table`
--
ALTER TABLE `user_table`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `visit_table`
--
ALTER TABLE `visit_table`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vitals_type_table`
--
ALTER TABLE `vitals_type_table`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vital_sign_table`
--
ALTER TABLE `vital_sign_table`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ward_location_table`
--
ALTER TABLE `ward_location_table`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ward_table`
--
ALTER TABLE `ward_table`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admission_table`
--
ALTER TABLE `admission_table`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `clinic_room_table`
--
ALTER TABLE `clinic_room_table`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `clinic_table`
--
ALTER TABLE `clinic_table`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `drug_measure_table`
--
ALTER TABLE `drug_measure_table`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `drug_table`
--
ALTER TABLE `drug_table`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `imaging_request_table`
--
ALTER TABLE `imaging_request_table`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `insurance_table`
--
ALTER TABLE `insurance_table`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `in_patient_note_table`
--
ALTER TABLE `in_patient_note_table`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `lab_category_item_table`
--
ALTER TABLE `lab_category_item_table`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `lab_category_table`
--
ALTER TABLE `lab_category_table`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `lab_investigation_table`
--
ALTER TABLE `lab_investigation_table`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `last_vitals_table`
--
ALTER TABLE `last_vitals_table`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=56;

--
-- AUTO_INCREMENT for table `nurse_observation_table`
--
ALTER TABLE `nurse_observation_table`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `patient_billing_account`
--
ALTER TABLE `patient_billing_account`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `patient_table`
--
ALTER TABLE `patient_table`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `prescription_table`
--
ALTER TABLE `prescription_table`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `printer_table`
--
ALTER TABLE `printer_table`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `procedure_attachment_table`
--
ALTER TABLE `procedure_attachment_table`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `procedure_list_table`
--
ALTER TABLE `procedure_list_table`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `procedure_medication_table`
--
ALTER TABLE `procedure_medication_table`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `procedure_note_table`
--
ALTER TABLE `procedure_note_table`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `procedure_speciality_table`
--
ALTER TABLE `procedure_speciality_table`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `procedure_table`
--
ALTER TABLE `procedure_table`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `radiology_table`
--
ALTER TABLE `radiology_table`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `service_table`
--
ALTER TABLE `service_table`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `transaction_table`
--
ALTER TABLE `transaction_table`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=95;

--
-- AUTO_INCREMENT for table `user_table`
--
ALTER TABLE `user_table`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `visit_table`
--
ALTER TABLE `visit_table`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `vitals_type_table`
--
ALTER TABLE `vitals_type_table`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `vital_sign_table`
--
ALTER TABLE `vital_sign_table`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `ward_location_table`
--
ALTER TABLE `ward_location_table`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `ward_table`
--
ALTER TABLE `ward_table`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
