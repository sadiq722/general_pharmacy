<?php

define('transaction_date', date('Y-m-d h:i:s'));
define('transaction_date_', date('Y-m-d'));
$transaction_date_ = date('Y-m-d');

function getEMRID($emr_id) {

    $id = $emr_id;
    $length = strlen($id);
    $emr = '';
    switch ($length) {
        case 1:
            $emr = "0000000000" . $id;
            break;
        case 2:
            $emr = "000000000" . $id;
            break;
        case 3:
            $emr = "00000000" . $id;
            break;
        case 4:
            $emr = "0000000" . $id;
            break;
        case 5:
            $emr = "000000" . $id;
            break;
        case 6:
            $emr = "00000" . $id;
            break;
        case 7:
            $emr = "0000" . $id;
            break;
        case 8:
            $emr = "000" . $id;
            break;
        case 9:
            $emr = "00" . $id;
            break;
        case 10:
            $emr = "0" . $id;
            break;
        default:
            $emr = $id;
    }
    return $emr;
}

function documentTransaction($emr_id, $item_id, $item_type, $transaction_type, $service, $requested_by, $quantity = 1, $transaction_date = transaction_date, $transaction_date_ = transaction_date_) {
    //ensure that $service variable is of type of array (associative)

    $principal = _getPrincipalEMRID($emr_id);
      

    // echo $principal ;

    $bill_account = _getBillingAccount($principal);
//echo $emr_id;
//echo $service['price'];
    $balance = ($transaction_type == 'Debit') ? ($bill_account['balance'] - $service['price']) : ($bill_account['balance'] + $service['price']);
    _updateBillingAccount($principal, $balance);
    $id = _documentTransaction($bill_account['id'], $emr_id, $item_id, $item_type, $service['name'], $service['price'], $transaction_type, $balance, $requested_by, $quantity, $transaction_date, $transaction_date_);
//echo $id;
    return $id;
}

function reverseTransaction($requested_by, $transaction_type, $id = null, $item_id = null, $type = NULL, $name = NULL, $quantity = 1) {
    $transaction_date = date('Y-m-d h:i:s');
    $transaction_date_ = date('Y-m-d');
    $transaction = _getTransaction($id, $item_id, $type, $name);
    $principal = _getPrincipalEMRID($transaction['emr_id']);
    $bill_account = _getBillingAccount($principal);

    $balance = $bill_account['balance'] + $transaction['price'];
    _updateBillingAccount($principal, $balance);
    _documentTransaction($bill_account['id'], $transaction['emr_id'], $transaction['item_id'], $transaction['item_type'], $transaction['description'], $transaction['price'], $transaction_type, $balance, $requested_by, $quantity, $transaction_date, $transaction_date_);
    _updateTransactionToReverse($transaction['id']);
}

function filter_data($data) {
    $conn = open_conn();
    trim($data);
    htmlspecialchars($data);
    mysqli_real_escape_string($conn, $data);
    $conn->close();
    return $data;
}

function validateNumber($data) {
    if (!ctype_digit($data) || $data == '') {
        echo '<script> alert("invalid number given");history.back(); </script>';
        exit();
    } else {
        return $data;
    }
}

function validateName($data) {
    if (!preg_match('/^[a-zA-Z ]*$/', $data) || $data == '') {
        echo '<script> alert("invalid name given");history.back(); </script>';
        exit();
    } else {
        return $data;
    }
}

function getMonthOfTheYear() {
    $months = array('Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec');
    return $months;
}

function getBillAccount($emr_id) {
    $principal = _getPrincipalEMRID($emr_id);
    $bill_account = _getBillingAccount($principal);
    return $bill_account;
}
