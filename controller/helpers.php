<?php
session_start();
//include '../res/includes/sweet.php';
include '../model/db_conn.php';
require_once "../assets/PHPEXCEL/PHPExcel.php";
$GLOBALS['item_type'] = array('r' => 'Record', 'l' => 'Lab', 'ph' => 'Pharmacy', 'i' => 'imaging', 'p' => 'Procedure', 'a' => 'Admission');
$GLOBALS['transaction_type'] = array('d' => 'Debit', 'c' => 'Credit', 'r' => 'Reversed');

function createAccount($first_name, $last_name, $phone_number, $username, $password) {

    if (!_isAUser($phone_number, $username)) {
        $hash_pass = md5(hash('sha1', $password));
        _createAccount($first_name, $last_name, $phone_number, $username, $hash_pass);
        echo 'User Account Created';
    } else {
        echo 0;
    }
}

function changePassword($id, $password, $new_password) {
    $user = _getUser($id);
    $hash_pass = md5(hash('sha1', $password));
    if ($user['password'] == $hash_pass) {
        $hash_pass = md5(hash('sha1', $new_password));
        _changePassword($id, $hash_pass);
        echo 'Password Changed';
    } else {
        echo 0;
    }
}

function resetPassword($id, $new_password) {
    $hash_pass = md5(hash('sha1', $new_password));
    _changePassword($id, $hash_pass);
    echo 'Password Reset';
}

function changePrivilege($id, $record, $nurse, $doctor, $pharmacy, $pharmacy_admin, $lab, $lab_admin, $radiology, $radiology_admin,$bill,$bill_admin, $admin, $super_admin) {

    _changePrivilege($id, $record, $nurse, $doctor, $pharmacy, $pharmacy_admin, $lab, $lab_admin, $radiology, $radiology_admin,$bill,$bill_admin, $admin, $super_admin);

    echo 'Privilege Saved!';
}

function login($username, $password) {
    $user = _login($username);
    if ($user != NULL) {
        $hash_pass = md5(hash('sha1', $password));
        if ($user['password'] == $hash_pass) {
            $_SESSION['signature'] = $user['username'];
            $_SESSION['id'] = $user['id'];
            echo 1;
        } else {
            echo 0;
        }
    } else {
        echo 0;
    }
}

function addPatient($emr_id, $full_name, $phone_number, $dob, $gender, $blood_group, $genotype, $address, $occupation, $NOK_name, $NOK_address, $NOK_phone_number, $type, $principal_emr, $created_by, $insurance, $is_principal) {
    if (!_isEMRIDExist($emr_id)) {
     ($insurance=="")? _addPatient($emr_id, $full_name, $phone_number, $dob, $gender, $blood_group, $genotype, $address, $occupation, $NOK_name, $NOK_address, $NOK_phone_number, $type, $principal_emr, $created_by)
             :_addPatient($emr_id, $full_name, $phone_number, $dob, $gender, $blood_group, $genotype, $address, $occupation, $NOK_name, $NOK_address, $NOK_phone_number,$insurance, $principal_emr, $created_by);
        if ($is_principal != 'No') {
            _createBilingAccount($emr_id);
        }
        $service = _getServicePrice(1);
        documentTransaction($emr_id, $service['id'], $GLOBALS['item_type']['r'], $GLOBALS['transaction_type']['d'], $service, $created_by);
        echo  $emr_id;
    } else {
        echo 0;
    }
}

function requestAdmission($emr_id, $visit_id, $reason_for_admission, $admission_date, $clinic, $requested_by) {
    $visitid = ($visit_id == 0) ? _fetchLastVisitID($emr_id) : $visit_id;
    if (_isEMRIDExist($emr_id)) {
        if (_isAdmissionRequestExist($emr_id) > 0) {
            $id = _isAdmissionRequestExist($emr_id);
            _updateAdmission($id, $emr_id, $visitid, $reason_for_admission, $admission_date, $clinic, $requested_by);
            echo 'Admission request was updated';
        } else {
            _requestAdmission($emr_id, $visitid, $reason_for_admission, $admission_date, $clinic, $requested_by);
            echo 'Admission request was succesful';
        }
    } else {
        echo 'Failed to request, verify patient account and try again!';
    }
}

function assignBed($id, $emr_id, $price, $clinic, $ward, $location) {
    $emr_id = getEMRID($emr_id);
    $today = date('Y-m-d');
    _assignBed($id, $clinic, $ward, $location, $today, $_SESSION['signature']);

    $service = array('price' => $price, 'name' => $ward . ' bed charges');
    documentTransaction($emr_id, $id, $GLOBALS['item_type']['a'], $GLOBALS['transaction_type']['d'], $service, $_SESSION['signature']);
    echo $location . ' assigned';
}

function changeBed($id, $emr_id, $price, $clinic, $ward, $location) {
    $emr_id = getEMRID($emr_id);

    _changeBed($id, $clinic, $ward, $location, $_SESSION['signature']);
    $service = array('price' => $price, 'name' => $ward . ' bed charges');
    documentTransaction($emr_id, $id, $GLOBALS['item_type']['a'], $GLOBALS['transaction_type']['d'], $service, $_SESSION['signature']);
    echo $location . ' assigned';
}

function dischargePatient($id, $note) {
    $today = date('Y-m-d');
    _dischargePatient($id, $note, $_SESSION['signature'], $today);
    echo 'Discharged Sucessful';
}

function bookAppointment($emr_id, $visit_type, $visit_date, $clinic, $room, $booked_by) {
    $emr_id = getEMRID($emr_id);
    if (_isEMRIDExist($emr_id)) {
        $id = _isSimilarAppointmentExist($emr_id,$visit_type, $clinic);
        if ($id > 0) {
            _updateAppointment($id, $emr_id, $visit_type, $visit_date, $clinic, $room, $booked_by);
            echo 'Previous appointment has been updated';
        } else {
            $service = _getServicePrice($visit_type);
         $item_id=   _bookAppointment($emr_id, $visit_type,$service['price'], $visit_date, $clinic, $room, $booked_by);
            $principal = _getPrincipalEMRID($emr_id);
            $bill_account = _getBillingAccount($principal);
            
            $balance = $bill_account['balance'] - $service['price'];
            _updateBillingAccount($principal, $balance);
            _documentTransaction($bill_account['id'], $emr_id, $item_id, $GLOBALS['item_type']['r'], $service['name'], $service['price'], $GLOBALS['transaction_type']['d'], $balance, $booked_by);
            echo 'Appointment booked succesfully';
        }
    } else {
        echo 'Failed to book appointment, verify patient account and try again!';
    }
}

function updateAppointment($id, $emr_id, $visit_type, $visit_date, $clinic, $room, $booked_by) {
    $emr_id = getEMRID($emr_id);
    if (_isEMRIDExist($emr_id)) {
        _updateAppointment($id, $emr_id, $visit_type, $visit_date, $clinic, $room, $booked_by);
        echo 'Appointment has been updated';
    } else {
        echo 'Failed to update appointment, verify patient account and try again!';
    }
}

function updatePatientProfile($id, $emr_id, $full_name, $phone_number, $dob, $gender, $blood_group, $genotype, $address, $occupation, $NOK_name, $NOK_address, $NOK_phone_number) {
    if (_isEMRIDExist($emr_id)) {
        _updatePatientProfile($id, $full_name, $phone_number, $dob, $gender, $blood_group, $genotype, $address, $occupation, $NOK_name, $NOK_address, $NOK_phone_number);
        echo 'Profile Updated';
    } else {
        echo 'Failed to update profile, verify patient account and try again!';
    }
}

function fillLabResult($id,$lab_test, $result,$emr_id) {
    if (!is_array($result)) {
        _fillLabResult($id, $result, $_SESSION['signature']);
    } else {
        $results = implode(',', $result);
        _fillLabResult($id, $results, $_SESSION['signature']);
    }
     //-- patient should be bill after service
//       $service = _getLabPrice($lab_test);
//        documentTransaction($emr_id, $id, $GLOBALS['item_type']['l'], $GLOBALS['transaction_type']['d'], $service, $_SESSION['signature']);

    echo 'Result Saved';
}

//function updateDrugQty($id) {
//
//    foreach (_fetchPrescription($id) as $pres){
//        $stock_quantity= _getBatchDrugInfo($pres['batch_no']); //_getDrugInfo($pres['drug_name']);
//       $remainingQuantity = $stock_quantity['quantity'] - $pres['served_quantity'];
//       _updateDrugQty($remainingQuantity, $pres['batch_no']);
//    }
//  servePrescription($id);
//}

function addLabCategory($name) {
    if (_isLabCategoryExist($name) == 0) {
        _addLabCategory($name);
        echo 'Category Saved';
    } else {
        echo 0;
    }
}
function addHMO($name,$abbr,$phone_number){
    if (_isHMOExist($name,$abbr,$phone_number) == 0) {
        _addHMO($name,$abbr,$phone_number);
        echo 'HMO Saved';
    } else {
        echo 0;
    }  
}
function addLabType($name, $category,$result, $price) {
    if (_isLabExist($name) == 0) {
        _addLabType($name, $category,$result, $price, $_SESSION['signature']);
        echo 'Lab Saved';
    } else {
        echo 0;
    }
}

function addMeasure($name) {
    if (_isMeasureExist($name) == 0) {
        _addMeasure($name);
        echo 'Saved';
    } else {
        echo 0;
    }
}

function addDrug($name, $unit_of_measure, $reorder_level) {
    if (_isdrugExist($name) == 0) {
         $conn = open_conn();
   
        _addDrug(mysqli_real_escape_string($conn, $name), mysqli_real_escape_string($conn, $unit_of_measure), mysqli_real_escape_string($conn, $reorder_level), $_SESSION['signature']);
        echo 'Item Added';
    } else {
        echo 0;
    }
}
function  editDrug($id,$name, $unit_of_measure, $reorder_level) {
    $drug_id = _isdrugExist($name);
    if ($drug_id == 0 || $clinic_id == $id) {
        _updateDrug($id, $name, $unit_of_measure, $reorder_level,  $_SESSION['signature']);
       
        echo 'Drug Updated';
    } else {
        echo 0;
    }
}
 function addBatch($drug_id,$name,$batch, $quantity, $unit_price,$expire_date){
   if ( _isBatchExist($batch) == 0) {
       $conn = open_conn();
   
        _addBatch(mysqli_real_escape_string($conn, $drug_id),mysqli_real_escape_string($conn, $name),mysqli_real_escape_string($conn, $batch), mysqli_real_escape_string($conn, $quantity), $unit_price,$expire_date,$_SESSION['signature']);
        echo 'Batch Added';
    } else {
        echo 0;
    }
 
 }
 $GLOBALS['i']=0;
 $GLOBALS['errored_batches']=array();
 function addBatches($name,$batch, $quantity, $unit_price,$expire_date){
 
   $drug_id=(is_null(_getDrugId($name)))?null:_getDrugId($name)['id'] ;
   if ( _isBatchExist($batch) == 0) {
    if($drug_id!=null){
        $conn = open_conn();
        _addBatch(mysqli_real_escape_string($conn, $drug_id),mysqli_real_escape_string($conn, $name),mysqli_real_escape_string($conn, $batch), mysqli_real_escape_string($conn, $quantity), $unit_price,$expire_date,$_SESSION['signature']);
   
    } else{
        $GLOBALS['i']++;
        $GLOBALS['errored_batches'][$GLOBALS['i']]=$batch;
    }
   
 }
    
  
  }
  function addDrugs($name,$unit, $reorder_level){
 
    if ( _isdrugExist($name) == 0) {
  
         $conn = open_conn();
         _addDrug(mysqli_real_escape_string($conn,$name), mysqli_real_escape_string($conn,$unit), mysqli_real_escape_string($conn,$reorder_level), $_SESSION['signature']);
     
     } else{
         $GLOBALS['i']++;
         $GLOBALS['errored_batches'][$GLOBALS['i']]=$name;  
  }
     
   
   }
function addClinic($name) {
    if (_isClinicExist($name) == 0) {
        _addClinic($name, $_SESSION['signature']);
        echo 'Clinic Saved';
    } else {
        echo 0;
    }
}

function addRoom($id, $name) {
    if (_isRoomExist($id, $name) == 0) {
        _addClinicRoom($id, $name, $_SESSION['signature']);
        echo 'Room Saved';
    } else {
        echo 0;
    }
}

function addWard($id, $name, $price) {
    if (_isWardExist($id, $name) == 0) {
        _addWard($id, $name, $price, $_SESSION['signature']);
        echo 'Ward Saved!';
    } else {
        echo 0;
    }
}

function addBed($id, $name) {
    if (_isBedRoomExist($id, $name) == 0) {
        _addBed($id, $name, $_SESSION['signature']);
        echo 'Bed Saved!';
    } else {
        echo 0;
    }
}

function editBed($ward_id, $id, $name) {
    $bed_id = _isBedRoomExist($ward_id, $name);
    if ($bed_id == 0 || $bed_id == $id) {
        _updateBedRoom($id, $name, $_SESSION['signature']);
        echo 'Bed Updated';
    } else {
        echo 0;
    }
}

function editRoom($clinic_id, $id, $name) {
    $room_id = _isRoomExist($clinic_id, $name);
    if ($room_id == 0 || $room_id == $id) {
        _updateClinicRoom($id, $name, $_SESSION['signature']);
        echo 'Room Updated';
    } else {
        echo 0;
    }
}
function updateHMO($id,$name,$abbr,$phone_number){
   $_id= _isHMOExist($name,$abbr,$phone_number);
  
    if ($_id == 0 || $_id == 1) {
        _updateHMO($id,$name,$abbr,$phone_number);
        echo 'HMO Updated';
    } else {
        echo 0;
    }  
}
function editWard($clinic_id, $id, $name, $price) {
    $ward_id = _isWardExist($clinic_id, $name);
    if ($ward_id == 0 || $ward_id == $id) {
        _updateWard($id, $name, $price, $_SESSION['signature']);
        echo 'Ward Updated';
    } else {
        echo 0;
    }
}



function addScan($name, $price) {
    if (_isScanExist($name) == 0) {
        _addRadiologyScan($name, $price, $_SESSION['signature']);
        echo 'Item Added';
    } else {
        echo 0;
    }
}

function addProcedureService($name, $price) {
    if (_isProcedureServiceExist($name) == 0) {
        _addProcedureService($name, $price, $_SESSION['signature']);
        echo 'Procedure Saved';
    } else {
        echo 0;
    }
}

function updateImaging($id, $name, $price) {
    $scan_id = _isScanExist($name);
    if ($scan_id == 0 || $scan_id == $id) {
        _updateRadiologyScan($id, $name, $price, $_SESSION['signature']);
        echo 'Scan Updated';
    } else {
        echo 0;
    }
}

function updateProcedureService($id, $name, $price) {
    $procedure_id = _isProcedureServiceExist($name);
    if ($procedure_id == 0 || $procedure_id == $id) {
        _updateProcedureService($id, $name, $price, $_SESSION['signature']);
        echo 'Procedure Updated';
    } else {
        echo 0;
    }
}

function changeRecordServicePrice($id, $price) {
    _changeRecordServicePrice($id, $price, $_SESSION['signature']);
    echo 'Price Updated';
}

function addQuantity($id, $quantity) {
    $drug = _getDrugInfo($id);
    $new_quantity = $drug['quantity'] + $quantity;
    _incrementDrugQuantity($id, $new_quantity);
    echo 'Quantity Updated!';
}

function updateDrug($id, $name,$expiration_date,$batch_no, $unit_price, $quantity) {
   
        _updateDrugBatch($id, $name,$expiration_date,$batch_no, $unit_price, $quantity,  $_SESSION['signature']);
        echo 'Item Updated';
    
}
function updateInventorySettings($item_expiration, $copay_rate, $low_qty_threshold){
        _updateInventorySettings($item_expiration, $copay_rate, $low_qty_threshold);
        echo 'Settings Updated';
}
function editLabType($id, $name, $category,$result, $price) {
    $lab = _isLabExist($name);
    if ($lab > 0 && $lab != $id) {
        echo 0;
    } else {
        _editLabType($id, $name, $category,$result, $price, $_SESSION['signature']);
        echo 'Lab Updated';
    }
}

function requestLab($visit_id, $emr_id, $lab_test, $request_note, $requested_by) {

    if (_isEMRIDExist($emr_id)) {
        $service = _getLabPrice($lab_test);
        $request_id = _requestLab($visit_id, $emr_id,$service['price'], $lab_test, $request_note, $requested_by);
        
         // patient should be bill before service
        documentTransaction($emr_id, $request_id, $GLOBALS['item_type']['l'], $GLOBALS['transaction_type']['d'], $service, $requested_by);
        $div_id = 'id' . rand(0, 999);
        echo '\'' . $div_id . '\',\'' . $service['name'] . '\',\'' . $GLOBALS['item_type']['l'] . '\',' . $request_id;
    } else {
        echo 0;
    }
}

function addLab($emr_id, $lab_test, $request_note, $requested_by) {

    if (_isEMRIDExist($emr_id)) {
        $visit_id =0;// _fetchLastVisitID($emr_id);
        $service = _getLabPrice($lab_test);
        $request_id = _requestLab($visit_id, $emr_id,$service['price'], $lab_test, $request_note, $requested_by);
        // patient should be bill before service
        
        documentTransaction($emr_id, $request_id, $GLOBALS['item_type']['l'], $GLOBALS['transaction_type']['d'], $service, $requested_by);

        echo "Lab Request Added";
    } else {
        echo 'Failed to request lab, verify patient account and try again!';
    }
}


function decreaseDrugQty($stockQty, $servedQty,$id) {
       $remainingQuantity = $stockQty - $servedQty;
       _updateDrugQty($remainingQuantity, $id);
}
function increaseDrugQty($stockQty, $servedQty,$id) {
       $remainingQuantity = $stockQty + $servedQty;
       _updateDrugQty($remainingQuantity, $id);
}

function requestPrescription($ps_id, $drug_batch_id, $unit, $qty) {
    $drug=_getBatchDrugInfo($drug_batch_id);
    if ($drug['quantity']>$qty) {
        $copay_rate=  (_getinventorySettings()['copay_rate'])/100;
        $price=($drug['unit_price']*$copay_rate) * $qty;
       $drug_batch=  _getBatchDrugInfo($drug_batch_id);
       decreaseDrugQty($drug['quantity'], $qty,$drug_batch_id);
      echo   _requestPrescription($ps_id, $drug_batch['drug_name'],$unit,$drug_batch['batch_no'], $qty,$price);
             //   echo $ps_id;
    } else {
        echo 0;
    }
}

function addPrescription($phone_no, $full_name) {
    $patient_check=_isPhoneNumberExist($phone_no);

   if ($patient_check>0) {
       $active_slip=_activeSlipCheck($patient_check);
       if($active_slip>0){
           echo $active_slip;
       }else{
     echo _addPrescriptionSlip($patient_check);
       }
   } else {
      $id= _addPatient($full_name, $phone_no);
       echo _addPrescriptionSlip($id);
    }
}

function addProcedureMedication($procedure_id, $drug_name, $dose, $duration, $requested_by) {
    _addProcedureMedication($procedure_id, $drug_name, $dose, $duration, $requested_by);
    echo 'Medication Added';
}

function deleteLab($name, $type, $item_id) {
//reverseTransaction($requested_by, $transaction_type, $id, $item_id, $type, $name)
    reverseTransaction($_SESSION['signature'], $GLOBALS['transaction_type']['r'], null, $item_id, $type, $name);
    _deleteLab($item_id);
}

function deleteImagingRequest($name, $type, $item_id) {
//reverseTransaction($requested_by, $transaction_type, $id, $item_id, $type, $name)
    reverseTransaction($_SESSION['signature'], $GLOBALS['transaction_type']['r'], null, $item_id, $type, $name);
    _deleteImagingRequest($item_id);
}

function saveVisit($emr_id, $visit_id, $seen_by, $visit_note, $diagnosis) {
    if (_isEMRIDExist($emr_id)) {
        _saveVisit($visit_id, $seen_by, $visit_note, $diagnosis);
        unset($_SESSION['visit_id']);
        echo 'Session Saved';
    } else {
        echo 'Failed to save session, verify patient account and try again!';
    }
}

function cancelProcedure($item_id, $type, $name, $bill) {
    if ($bill != 1) {
        _deleteProcedure($item_id);
    } else {
        reverseTransaction($_SESSION['signature'], $GLOBALS['transaction_type']['r'], NULL, $item_id, $type, $name);
        _deleteProcedure($item_id);
    }
    echo 'Procedure Canceled';
}

function cancelLabRequest($item_id, $name) {

    reverseTransaction($_SESSION['signature'], $GLOBALS['transaction_type']['r'], NULL, $item_id, $GLOBALS['item_type']['l'], $name);
    _deleteLab($item_id);
    echo 'Investigation Canceled';
}
function cancelPrescription($item_id, $name) {

    reverseTransaction($_SESSION['signature'], $GLOBALS['transaction_type']['r'], NULL, $item_id, $GLOBALS['item_type']['ph'], $name);
    _deletePrescription($item_id);
    echo 'Prescription Canceled';
}
function cancelAppointment($item_id, $name) {
    reverseTransaction($_SESSION['signature'], $GLOBALS['transaction_type']['r'], NULL, $item_id, $GLOBALS['item_type']['r'], $name);
  _cancelVisit($item_id);
    echo 'Appointment Canceled';
}
function cancelRadiologyRequest($item_id, $name) {

   reverseTransaction($_SESSION['signature'], $GLOBALS['transaction_type']['r'], NULL, $item_id, $GLOBALS['item_type']['i'], $name);
    _deleteRadiologyRequest($item_id);
    echo 'Request Canceled';
}

function deleteUser($id) {
    _deleteUser($id);
    echo 'User Deleted';
}
function deleteHMO($id){
     _deleteHMO($id);
    echo 'HMO Deleted';
}
function cancelPrescriptionRequest($id) {
    $prescription=    _getPrescription($id);
      $drug=  _getBatchDrugInfoByBatchNo($prescription['batch_no']);
      increaseDrugQty($drug['quantity'], $prescription['served_quantity'], $drug['id']);
    //decreaseDrugQty($stockQty, $servedQty, $id);
    _deletePrescription($id);
    echo 'Prescription Canceled';
}

function addProcedureSpeciality($name) {
    _addProcedureSpeciality($name);
    echo 'Speciality Added';
}

function addProcedureNote($procedure_id, $speciality, $note_type, $note, $noted_by) {
    _addProcedureNote($procedure_id, $speciality, $note_type, $note, $noted_by);
    echo 'Note Added';
}

function uploadPassport( $emr, $file) {
    //echo $emr;
    $test = explode('.', $file["name"]);
    $ext = end($test);
    $name = $emr . '.' . $ext;
    $location = '../uploads/passport/' . $name;
    
    $old_passport = _getPatient($emr)['passport'];
    
    if ($old_passport != '') {
        unlink('../uploads/passport/' . $old_passport);
    }
    move_uploaded_file($file["tmp_name"], $location);
    _uploadPassport($emr, $name, $_SESSION['signature']);
    echo '<i class="text-success">Passport Uploaded..</i>';
}

function uploadAttachment($upload_attachment, $file) {
    $today = date('ymdhis');
    $test = explode('.', $file["name"]);
    $ext = end($test);
    $name = $upload_attachment . $today . '.' . $ext;

    $location = '../uploads/attachment/' . $name;

    move_uploaded_file($file["tmp_name"], $location);
    _uploadAttachment($_SESSION['procedure']['id'], $upload_attachment, $name, $_SESSION['signature']);
    echo '<i>Attachment Uploaded..</i>';
}
function uploadLabResult($lab_test,$id,$emr,$file){
    $today = date('ymdhis');
    $test = explode('.', $file["name"]);
    $ext = end($test);
    $name = $emr . $today . '.' . $ext;

   $location = '../uploads/lab/' . $name;
   move_uploaded_file($file["tmp_name"], $location);
    
   _uploadLabResult($id, $location, $_SESSION['signature']);
     // --patient should be bill after service
       //$service = _getLabPrice($lab_test);
        //documentTransaction($emr, $id, $GLOBALS['item_type']['l'], $GLOBALS['transaction_type']['d'], $service, $_SESSION['signature']);
    echo '<i class="text-success">Result Uploaded..</i>';
}
function uploadRadiologyResult($id,$imgname, $note, $emr_id, $file) {
    $today = date('ymdhis');
    $test = explode('.', $file["name"]);
    $ext = end($test);
    $name = $emr_id . $today . '.' . $ext;

    $location = '../uploads/radiology/' . $name;

    move_uploaded_file($file["tmp_name"], $location);
    _uploadRadiologyResult($id, $name, $note, $_SESSION['signature']);

 // $service = _getImagingPrice($imgname);
        //documentTransaction($emr_id, $id, $GLOBALS['item_type']['i'], $GLOBALS['transaction_type']['d'], $service, $_SESSION['signature']);

    echo '<i class="text-success">Result Uploaded..</i>';
}

function addVitals($emr_id, $type, $reading, $checked_by) {
    _addVitals($emr_id, $type, $reading, $checked_by);
    _updateVitals($emr_id, $type, $reading, $checked_by);
    echo 'Record taken succesfully';
}

function addPayment($emr_id, $description, $source, $amount, $signature,$auth_code) {

    if (_isEMRIDExist($emr_id)) {
        $service = array('price' => $amount, 'name' => $description);
      $id=  documentTransaction($emr_id, 0, $source, $GLOBALS['transaction_type']['c'], $service, $signature);
      _addAuthCodeToTransation($id,$auth_code);  
      echo $id;
    } else {
        echo 'Failed, verify patient account and try again!';
    }
}

function addDebit($emr_id, $description, $source, $amount, $signature) {

    if (_isEMRIDExist($emr_id)) {
        $service = array('price' => $amount, 'name' => $description);
        documentTransaction($emr_id, 0, $source, $GLOBALS['transaction_type']['d'], $service, $signature);
        echo 'Sucessful';
    } else {
        echo 'Failed, verify patient account and try again!';
    }
}

function addInPatientNote($admission_id, $note, $noted_by) {

    _addInPatientNote($admission_id, $note, $noted_by);
    echo 'Note Saved';
}

function addNurseObservation($admission_id, $note, $noted_by) {

    _addNurseObservation($admission_id, $note, $noted_by);
    echo 'Note Saved';
}
function uploadDrug($file){
    $path=$file["tmp_name"];

    $reader=PHPExcel_IOFactory::createReaderForFile($path);
    
    $excel_obj=$reader->load($path);
    
    $worksheet=$excel_obj->getSheet('0'); 
    $rowCount=$worksheet->getHighestRow();
    $colCount=$worksheet->getHighestDataColumn();
    $colCountNumber=PHPExcel_cell::columnIndexFromString($colCount);
    $db_data=array();
    
    //echo $worksheet->getCell('C2')->getValue().'<br>';
        for($row=2; $row<=$rowCount;$row++){
            for($col=0; $col<$colCountNumber; $col++){
               
                $db_data[$row-2][$col]=$worksheet->getCell(PHPExcel_cell::stringFromColumnIndex($col).$row)->getValue();
          
            }
        }
        foreach($db_data as $data){
                   addDrugs($data[0],$data[1], $data[2]);
             }  
             echo count($GLOBALS['errored_batches']).' drug name(s) already exist. <br>';
             foreach($GLOBALS['errored_batches'] as $batch){
                 echo $batch.'<br>';
             }
}
 function uploadBatch($file){

$path=$file["tmp_name"];

$reader=PHPExcel_IOFactory::createReaderForFile($path);

$excel_obj=$reader->load($path);

$worksheet=$excel_obj->getSheet('0'); 
$rowCount=$worksheet->getHighestRow();
$colCount=$worksheet->getHighestDataColumn();
$colCountNumber=PHPExcel_cell::columnIndexFromString($colCount);
$db_data=array();

//echo $worksheet->getCell('C2')->getValue().'<br>';
    for($row=2; $row<=$rowCount;$row++){
        for($col=0; $col<$colCountNumber; $col++){
            if($col==2){
              $db_data[$row-2][$col]=date('Y-m-d',PHPExcel_Shared_Date::ExcelToPHP($worksheet->getCell(PHPExcel_cell::stringFromColumnIndex($col).$row)->getValue()));   
      
            }else{
            $db_data[$row-2][$col]=$worksheet->getCell(PHPExcel_cell::stringFromColumnIndex($col).$row)->getValue();
        }
        }
    }
    foreach($db_data as $data){
               addBatches($data[1],$data[0],$data[3],$data[4],date($data[2]));
         }  
         echo count($GLOBALS['errored_batches']).' batch item(s) dont have a drug name. <br>';
         foreach($GLOBALS['errored_batches'] as $batch){
             echo $batch.'<br>';
         }
 }