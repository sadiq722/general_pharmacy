<?php

include 'includes.php';
$GLOBALS['errors'] = array();
$GLOBALS['info'] = array();

if (isset($_POST['create_patient'])) {


    $vital_signs = array();
    $emr_id = filter_data($_POST['emr_id']);
    $phone_number = validateNumber(filter_data($_POST['phone_number']));
    $occupation = ucwords(filter_data($_POST['occupation']));
    $full_name = validateName(ucwords(filter_data($_POST['full_name'])));
    $date_registered = date('Y-m-d');
    $blood_group = filter_data($_POST['blood_group']);
    $age = filter_data($_POST['age']);
    $gender = filter_data($_POST['gender']);
    $genotype = filter_data($_POST['genotype']);
    $address = filter_data($_POST['address']);
    $NOK_name = validateName(filter_data($_POST['NOK_name']));
    $NOK_phone_number = validateNumber(filter_data($_POST['NOK_phone_number']));
    $NOK_address = filter_data($_POST['NOK_address']);
    $location = $_GET['location'];
    $service_name = $_GET['service_name'];
    $price = get_service_price($service_name);
    $today = date('Y-m-d');
    if (create_Patient($emr_id, $phone_number, $occupation, $full_name, $date_registered, $blood_group, $age, $gender, $genotype, $address, $NOK_name, $NOK_address, $NOK_phone_number, $_SESSION['name'])) {
        document_transaction($emr_id, $service_name, $location, 1, $price, $today);

        header('location: ../view/nurse/patient_print.php?emr_id=' . $emr_id . '&full_name=' . $full_name . '&phone_number=' . $phone_number . '&age=' . $age . '&gender=' . $gender . '&date_registered=' . $date_registered);
    } else {

        echo '<script> alert("Try again"); history.back();</script>';
    }
} elseif (isset($_POST['find_patient'])) {
    $query = filter_data($_POST['query']);
    if (!empty($query)) {
        if (ctype_digit($query) && strlen($query) < 11) {
            $id = $query;
            $length = strlen($query);
            switch ($length) {
                case 1:
                    $query = "0000000000" . $id;
                    break;
                case 2:
                    $query = "000000000" . $id;
                    break;
                case 3:
                    $query = "00000000" . $id;
                    break;
                case 4:
                    $query = "0000000" . $id;
                    break;
                case 5:
                    $query = "000000" . $id;
                    break;
                case 6:
                    $query = "00000" . $id;
                    break;
                case 7:
                    $query = "0000" . $id;
                    break;
                case 8:
                    $query = "000" . $id;
                    break;
                case 9:
                    $query = "00" . $id;
                    break;
                case 10:
                    $query = "0" . $id;
                    break;
                default:
                    $query = $id;
            }
        }

        $_SESSION['patients'] = find_patient_by_query($query);
        echo '<script> history.back();</script>';
    } else {
        echo '<script> history.back();</script>';
    }
} elseif (isset($_POST['order_admission'])) {
    if (!is_admited($_SESSION['VISIT_ID'])) {
        $now = date('Y-m-d');
        $service_location = $_GET['location'];
        $location = filter_data($_POST['location']);
        $facility = $_POST['facility'];
        if ($_SESSION['visit_date'] != $now) {
            $_SESSION['VISIT_ID'] = save_follow_up($_SESSION['EMR_ID'], $now, $_SESSION['name']);
            $_SESSION['visit_date'] = $now;
        }
        order_admission($_SESSION['VISIT_ID'], $now, $facility, $location, $_SESSION['name']);
        $price = get_service_price($facility);
        document_transaction($_SESSION['EMR_ID'], $facility . ' for ' . $now, $service_location, 1, $price, $now);
        echo '<script> alert("Admission order registered"); history.back();</script>';
    } else {
        echo '<script> alert("Patient already registered for admission"); history.back();</script>';
    }
} elseif (isset($_GET['admit_patient'])) {

    $now = date('Y-m-d');
    admit_patient($_GET['admit_patient'], $now);
    echo '<script> alert("Admission order registered"); history.back();</script>';
} elseif (isset($_GET['cancel_admission'])) {
    cancel_admission($_GET['cancel_admission']);
    echo '<script> alert("Admission cancled succesfully"); history.back();</script>';
} elseif (isset($_POST['order_procedure'])) {
    $now = date('Y-m-d');
    $start_date = filter_data($_POST['start_date']);
    $service_location = $_GET['location'];
    $procedure_name = filter_data($_POST['procedure_name']);
    $primary_diagnoses = filter_data($_POST['primary_diagnoses']);
    $procedure_fee = filter_data($_POST['procedure_fee']);
    empty($procedure_fee) ? $price = get_service_price($procedure_name) : $price = $procedure_fee;
    if ($_SESSION['visit_date'] != $now) {
        $_SESSION['VISIT_ID'] = save_follow_up($_SESSION['EMR_ID'], $now, $_SESSION['name']);
        $_SESSION['visit_date'] = $now;
    }
    order_procedure($_SESSION['VISIT_ID'], $procedure_name, $primary_diagnoses, $start_date, $_SESSION['name']);
    // $price = get_service_price($procedure_name);
    document_transaction($_SESSION['EMR_ID'], $procedure_name, $service_location, 1, $price, $now);
    echo '<script> alert("Procedure order registered"); history.back();</script>';
} elseif (isset($_POST['open_procedure'])) {

    $procedure_id = $_POST['procedure_id'];
    $past_medical_history = filter_data($_POST['past_medical_history']);
    $present_medical_history = filter_data($_POST['present_medical_history']);
    $anesthetic_medical_history = filter_data($_POST['anesthetic_medical_history']);
    $current_medication = filter_data($_POST['current_medication']);
    $allergy_reaction = filter_data($_POST['allergy_reaction']);
    $dental_history = filter_data($_POST['dental_history']);
    $family_social_gynea_history = filter_data($_POST['family_social_gynea_history']);
    $examination = filter_data($_POST['examination']);
    $airway_assessment = filter_data($_POST['airway_assessment']);
    $mouth = filter_data($_POST['mouth']);
    $neck = filter_data($_POST['neck']);
    $thyroid_mental_distance = filter_data($_POST['thyroid_mental_distance']);
    $mallamphati_score = filter_data($_POST['mallamphati_score']);
    $Plan = filter_data($_POST['Plan']);
    $anesthetist = filter_data($_POST['anesthetist']);
    echo $procedure_id;
    open_procedure($procedure_id, $past_medical_history, $present_medical_history, $anesthetic_medical_history, $current_medication, $allergy_reaction, $dental_history, $family_social_gynea_history, $examination, $airway_assessment, $mouth, $neck, $thyroid_mental_distance, $mallamphati_score, $Plan, $anesthetist);
    echo '<script> alert("Procedure has been open"); history.back();</script>';
} elseif (isset($_POST['conclude_anesthesia'])) {
    $procedure_id = $_GET['procedure_id'];
    $theater = filter_data($_POST['theater']);
    $position = filter_data($_POST['position']);
    $pre_op_assessment = filter_data($_POST['pre_op_assessment']);
    $HR_PR = filter_data($_POST['HR_PR']);
    $urinalysis = filter_data($_POST['urinalysis']);
    $HR_PVC = filter_data($_POST['HR_PVC']);
    $allergies = filter_data($_POST['allergies']);
    $weight = filter_data($_POST['weight']);
    $asa = filter_data($_POST['asa']);
    $temperature = filter_data($_POST['temperature']);
    $time_given = filter_data($_POST['time_given']);
    $time_of_last_food = filter_data($_POST['time_of_last_food']);
    $iv_line_site = filter_data($_POST['iv_line_site']);
    $canula_size = filter_data($_POST['canula_size']);
    $other_result = filter_data($_POST['other_result']);
    $techniques = filter_data($_POST['techniques']);
    $fluid_and_blood = filter_data($_POST['fluid_and_blood']);
    $total_input = filter_data($_POST['total_input']);
    $urine_input = filter_data($_POST['urine_input']);
    $blood_loss = filter_data($_POST['blood_loss']);
    $post_operative_instruction = filter_data($_POST['post_operative_instruction']);
    conclude_anesthesia($procedure_id, $theater, $position, $pre_op_assessment, $HR_PR, $urinalysis, $HR_PVC, $allergies, $weight, $asa, $temperature, $time_given, $time_of_last_food, $iv_line_site, $canula_size, $other_result, $techniques, $fluid_and_blood, $total_input, $urine_input, $blood_loss, $post_operative_instruction);
    echo '<script> alert("Anaesthesia record saved"); history.back();</script>';
} elseif (isset($_POST['conclude_procedure'])) {
    $procedure_id = $_GET['procedure_id'];
    $operative_diagnosis = filter_data($_POST['operative_diagnosis']);
    $surgeon = filter_data($_POST['surgeon']);
    $assistant = filter_data($_POST['assistant']);
    $operation_date = filter_data($_POST['operation_date']);
    $post_operative_treatment = filter_data($_POST['post_operative_treatment']);

    conclude_procedure($procedure_id, $operative_diagnosis, $surgeon, $assistant, $operation_date, $post_operative_treatment);
    echo '<script> alert("Procedure record saved"); history.back();</script>';
} elseif (isset($_POST['find_prescription'])) {
    $query = filter_data($_POST['query']);
    if (!empty($query)) {
        if (ctype_digit($query) && strlen($query) < 11) {
            $id = $query;
            $length = strlen($query);
            switch ($length) {
                case 1:
                    $query = "0000000000" . $id;
                    break;
                case 2:
                    $query = "000000000" . $id;
                    break;
                case 3:
                    $query = "00000000" . $id;
                    break;
                case 4:
                    $query = "0000000" . $id;
                    break;
                case 5:
                    $query = "000000" . $id;
                    break;
                case 6:
                    $query = "00000" . $id;
                    break;
                case 7:
                    $query = "0000" . $id;
                    break;
                case 8:
                    $query = "000" . $id;
                    break;
                case 9:
                    $query = "00" . $id;
                    break;
                case 10:
                    $query = "0" . $id;
                    break;
                default:
                    $query = $id;
            }
        }
        $_SESSION['patients'] = find_prescription_by_query($query);
        header('location: ../view/pharmacy/prescription_order.php');
    } else {
        echo '<script> history.back();</script>';
    }
}
//elseif (isset($_POST['find_admission'])) {
//    $query = filter_data($_POST['query']);
//    if (!empty($query)) {
//        if (ctype_digit($query) && strlen($query) < 11) {
//            $id = $query;
//            $length = strlen($query);
//            switch ($length) {
//                case 1:
//                    $query = "0000000000" . $id;
//                    break;
//                case 2:
//                    $query = "000000000" . $id;
//                    break;
//                case 3:
//                    $query = "00000000" . $id;
//                    break;
//                case 4:
//                    $query = "0000000" . $id;
//                    break;
//                case 5:
//                    $query = "000000" . $id;
//                    break;
//                case 6:
//                    $query = "00000" . $id;
//                    break;
//                case 7:
//                    $query = "0000" . $id;
//                    break;
//                case 8:
//                    $query = "000" . $id;
//                    break;
//                case 9:
//                    $query = "00" . $id;
//                    break;
//                case 10:
//                    $query = "0" . $id;
//                    break;
//                default:
//                    $query = $id;
//            }
//        }
//        $_SESSION['patients'] = find_admission_by_query($query);
//        header('location: ../view/nurse/admission_list.php');
//    } else {
//        echo '<script> history.back();</script>';
//    }
//} 
elseif (isset($_POST['edit_patient'])) {
    $emr_id = filter_data($_POST['emr_id']);
    $phone_number = filter_data($_POST['phone_number']);
    $occupation = filter_data($_POST['occupation']);
    $full_name = ucwords(filter_data($_POST['full_name']));
    $date_registered = filter_data($_POST['date_registered']);
    $blood_group = filter_data($_POST['blood_group']);
    $age = filter_data($_POST['age']);
    $gender = filter_data($_POST['gender']);
    $genotype = filter_data($_POST['genotype']);
    edit_patient($emr_id, $full_name, $gender, $date_registered, $phone_number, $occupation, $blood_group, $age, $genotype, $_SESSION['name']);
    header('location: ../view/nurse/patient_print.php?edit=1&emr_id=' . $emr_id . '&full_name=' . $full_name . '&phone_number=' . $phone_number . '&age=' . $age . '&gender=' . $gender . '&date_registered=' . $date_registered);
} elseif (isset($_POST['reprint_card'])) {
    $today = date('Y-m-d');
    $emr_id = filter_data($_GET['emr_id']);
    $phone_number = filter_data($_GET['phone_number']);
    $location = filter_data($_GET['location']);
    $service_name = filter_data($_GET['service_name']);
    $full_name = ucwords(filter_data($_GET['full_name']));
    $date_registered = filter_data($_GET['date_registered']);
    $age = filter_data($_GET['age']);
    $gender = filter_data($_GET['gender']);
    $price = get_service_price($service_name);
    $transaction_id = document_transaction($emr_id, $service_name, $location, 1, $price, $today);
    update_transaction($transaction_id);
    header('location: ../view/nurse/patient_print.php?reprint=1&emr_id=' . $emr_id . '&full_name=' . $full_name . '&phone_number=' . $phone_number . '&age=' . $age . '&gender=' . $gender . '&date_registered=' . $date_registered);
} elseif (isset($_POST['update_patient'])) {
    $emr_id = filter_data($_POST['emr_id']);
    $phone_number = filter_data($_POST['phone_number']);
    $occupation = filter_data($_POST['occupation']);
    $full_name = filter_data($_POST['full_name']);
    $date_registered = filter_data($_POST['date_registered']);
    $blood_group = filter_data($_POST['blood_group']);
    $age = filter_data($_POST['age']);
    $gender = filter_data($_POST['gender']);
    $genotype = filter_data($_POST['genotype']);

    //  update item
    update_patient($emr_id, $phone_number, $occupation, $blood_group, $age, $genotype, $_SESSION['name']);
    echo '<script> alert("Patient Updated Succesfully"); history.back();</script>';
} elseif (isset($_POST['save_vital_sign'])) {

    $emr_id = filter_data($_GET['emr_id']);

    $type = $_POST['type'];
    $reading = $_POST['reading'];
    $count = count($type);
    for ($i = 0; $i < $count; $i++) {
        //  update item
        update_vital_sign($emr_id, $type[$i], $reading[$i], $_SESSION['name']);
    }

    echo '<script> alert("Vital Sign Updated Succesfully"); history.back();</script>';
} elseif (isset($_GET['book_appointment'])) {
    $emr_id = filter_data($_GET['book_appointment']);
    //  update item
    $now = date("Y-m-d h:m:s");

    if (is_book_appointment($emr_id) == 0) {
        $price = get_service_price("Consultation");
        $id = document_transaction($emr_id, 'Consultation', 'Doctor', 1, $price, $now);
        if (book_appointment($emr_id, $now, $_SESSION['name'], $id) > 0) {

            echo '<script> alert("Appointment Booked Succesfully"); history.back();</script>';
        } else {
            echo '<script> alert("Error"); history.back();</script>';
        }
    } else {
        echo '<script> alert("An appointment has already been booked for this patient"); history.back();</script>';
    }
} elseif (isset($_GET['discharge'])) {

    $visit_id = filter_data($_GET['discharge']);
    $now = date("Y-m-d h:m:s");
    if (discharge_patient($visit_id, $now, $_SESSION['name'])) {
        echo '<script> alert("Patient Discharged Succesfully"); history.back();</script>';
    } else {
        //echo '<script> alert("Error"); history.back();</script>';
    }
} elseif (isset($_POST['change_room'])) {
    $visit_id = filter_data($_GET['change_room']);
    $admission_date = filter_data($_POST['admission_date']);
    $facility = filter_data($_POST['facility']);
    $location = filter_data($_POST['location']);
    $now = date("Y-m-d h:m:s");
    if (discharge_patient($visit_id, $now, $_SESSION['name'])) {
        order_admission($visit_id, $now, $facility, $location, $_SESSION['name']);
        admit_patient($visit_id, $admission_date, $facility, $location, $_SESSION['name']);
        echo '<script> alert("Facility Changed Succesfully"); history.back();</script>';
    } else {
        echo '<script> alert("Error"); history.back();</script>';
    }
} elseif (isset($_POST['save_visit'])) {
    $visit_id = $_SESSION['VISIT_ID'];
    $clinical_notes = filter_data($_POST['clinical_notes']);
    $details = filter_data($_POST['details']);
    $requested_test = empty($_POST['test']) ? NULL : $_POST['test'];
    $_SESSION['visit_date'] = date('Y-m-d');
    $drugs_count = $_POST['count'];
    $drug_name = '';
    $dose = '';
    $duration = '';
    if ($drugs_count > 0) {
        $drug_name = $_POST['drug_name'];

        $dose = $_POST['dose'];

        $duration = $_POST['duration'];
    }

    sign_visit($visit_id, $clinical_notes, $_SESSION['name']);
    add_diagnosis($visit_id, $details, $_SESSION['visit_date'], $_SESSION['name']);
    if (!empty($requested_test) && count($requested_test) > 0) {
        $no_of_test = count($requested_test);
        for ($i = 0; $i < $no_of_test; $i++) {
            add_lab_investigation($visit_id, $_SESSION['visit_date'], $requested_test[$i], $_SESSION['name']);
            $price = get_service_price($requested_test[$i]);
            document_transaction($_SESSION['EMR_ID'], $requested_test[$i], 'Lab', 1, $price, $_SESSION['visit_date']);
        }
    }
    if ($drugs_count > 0) {
        for ($i = 0; $i < $drugs_count; $i++) {
            add_prescription($visit_id, $_SESSION['visit_date'], $drug_name[$i], $dose[$i], $duration[$i], $_SESSION['name']);
        }
    }
    update_transaction($_SESSION['transaction_id']);
    unset($_SESSION['new_visit']);
    echo '<script> alert("Session Saved Succesfully"); window.location="../view/doctor/patient_profile.php";</script>';
} elseif (isset($_POST['order_investigation'])) {
    $visit_id = $_GET['visit_id'];
    $visit_date = $_GET['visit_date'];
    $emr_id = $_GET['emr_id'];

    $requested_test = $_POST['test'];
    $now = date('Y-m-d');

    if (count($requested_test) > 0) {

        $no_of_test = count($requested_test);
        if ($visit_date != $now) {

            $visit_id = save_follow_up($emr_id, $now, $_SESSION['name']);
            $visit_date = $now;
        }

        for ($i = 0; $i < $no_of_test; $i++) {
            add_lab_investigation($visit_id, $now, $requested_test[$i], $_SESSION['name']);
            $price = get_service_price($requested_test[$i]);
            document_transaction($emr_id, $requested_test[$i], 'Lab', 1, $price, $now);
        }
    }
    $_SESSION['VISIT_ID'] = $visit_id;
    $_SESSION['visit_date'] = $visit_date;

    echo '<script> alert("Investigation Ordered Succesfully"); history.back();</script>';
} elseif (isset($_POST['order_prescription'])) {
    $visit_id = $_GET['visit_id'];
    $visit_date = $_GET['visit_date'];
    $emr_id = $_GET['emr_id'];
   
    $drug_name = $_POST['drug_name'];

    $dose = $_POST['dose'];

    $duration = $_POST['duration'];
    $drugs_count = $_POST['count'];
    $now = date('Y-m-d');
    if ($visit_date != $now) {
        $visit_id = save_follow_up($emr_id, $now, $_SESSION['name']);
        $visit_date = $now;
    }
    if ($drugs_count > 0) {
        for ($i = 0; $i < $drugs_count; $i++) {
            add_prescription($visit_id, $now, $drug_name[$i], $dose[$i], $duration[$i], $_SESSION['name']);
        }
    }
    $_SESSION['VISIT_ID'] = $visit_id;
    $_SESSION['visit_date'] = $visit_date;
   
    echo '<script> alert("Prescription Ordered Succesfully"); history.back();</script>';
} elseif (isset($_POST['fill_lab_result'])) {
    $visit_id = $_GET['visit_id'];
    $test = $_POST['test'];
    $result = $_POST['result'];
    echo $result[0];
    echo $test[0];
    $count = count($result);

    for ($i = 0; $i < $count; $i++) {
        if ($result[$i] != "") {
            fill_lab_result($result[$i], $test[$i], $visit_id, $_SESSION['name']);
        }
    }
    echo '<script> alert("Result Submited Succesfully"); history.back();</script>';
} elseif (isset($_POST['acknowledge_prescription'])) {
    $today = date('Y-m-d');
    $prescription_id = $_GET['prescription_id'];
    $emr_id = $_GET['emr_id'];
    $location = $_GET['location'];
    $drug_name = $_POST['drug_name'];
    $quantity = $_POST['quantity'];

    acknowledge_prescription($prescription_id);
    $drug = get_drug_info($drug_name);
    $stock_price = $drug['unit_price'];

    document_transaction($emr_id, $drug_name, $location, $quantity, $stock_price, $today);

    echo '<script> alert("Prescription Acknowledged Succesfully"); history.back();</script>';
} elseif (isset($_POST['acknowledge_prescriptions'])) {
    $today = date('Y-m-d');

    $emr_id = $_POST['emr_id'];
    $prescription_id = $_POST['prescription_id'];
    $location = $_GET['location'];
    $drug_name = $_POST['drug_name'];
    $quantity = $_POST['quantity'];
    $count = count($quantity);

    for ($i = 0; $i < $count; $i++) {
        if ($quantity[$i] != "") {
            acknowledge_prescription($prescription_id[$i]);
            $drug = get_drug_info($drug_name[$i]);
            $stock_price = $drug['unit_price'];

            document_transaction($emr_id[$i], $drug_name[$i], $location, $quantity[$i], $stock_price, $today);
        }
    }
    echo '<script> alert("Prescription Acknowledged Succesfully"); history.back();</script>';
} elseif (isset($_POST['dispense_prescription'])) {
    $prescription_id = $_POST['prescription_id'];
    $count = count($prescription_id);
    for ($i = 0; $i < $count; $i++) {

        dispense_prescription($prescription_id[$i]);
    }
    echo '<script> alert("Prescription Dispensed Succesfully"); history.back();</script>';
} elseif (isset($_GET['dispense'])) {
    dispense_prescription($_GET['dispense']);
    echo '<script> alert("Prescription Dispensed Succesfully"); history.back();</script>';
} 
