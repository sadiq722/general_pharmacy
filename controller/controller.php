<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
require_once 'helpers.php';
include_once 'utils.php';



// $sec_key = $_GET['sec_key'];
switch (true) {

    case isset($_POST['login']):
        
        login($_POST['username'], $_POST['password']);

        break;
    
    case isset($_POST['new_prescription']):
        
        requestPrescription( $_POST['prescription_slip_id'], $_POST['drug_name'],$_POST['unit'], $_POST['quantity']);
        break;
    case isset($_POST['add_prescription']):
        addPrescription($_POST['phone_no'], $_POST['full_name']);
        break;
    case isset($_POST['add_procedure_medication']):
        addProcedureMedication($_POST['procedure_id'], $_POST['drug_name'], $_POST['dose'], $_POST['duration'], $_SESSION['signature']);
        break;
    case isset($_POST['new_Imaging']):
        $visit_id = isset($_POST['visit_id']) ? $_POST['visit_id'] : "";
        requestImaging($visit_id, getEMRID($_POST['emr_id']), $_POST['name'], $_POST['request_note'], $_POST['requested_by']);
        break;
    case isset($_POST['add_imaging']):
        addImaging(getEMRID($_POST['emr_id']), $_POST['name'], $_POST['request_note'], $_SESSION['signature']);
        break;
    case isset($_POST['add_procedure']):
        addProcedure($_POST['emr_id'], $_POST['name'], $_POST['primary_diagnosis'], $_POST['request_note'], $_POST['schedule_date'], $_POST['bill'], $_SESSION['signature']);
        break;
    case isset($_POST['new_vitals']):

        addVitals($_POST['emr_id'], $_POST['type'], $_POST['reading'], $_SESSION['signature']);
        break;
    case isset($_POST['add_payment']):
        $auth_code = isset($_POST['auth_code']) ? $_POST['auth_code'] : 0;
        addPayment($_POST['emr_id'], $_POST['description'], $_POST['source'], $_POST['amount'], $_SESSION['signature'], $auth_code);
        break;
    case isset($_POST['debit']):
        addDebit(getEMRID($_POST['emr_id']), $_POST['description'], $_POST['source'], $_POST['amount'], $_SESSION['signature']);
        break;
    case isset($_POST['assign_bed']):
        assignBed($_POST['id'], $_POST['emr_id'], $_POST['price'], $_POST['clinic'], $_POST['ward'], $_POST['location']);
        break;
    case isset($_POST['change_bed']):
        changeBed($_POST['id'], $_POST['emr_id'], $_POST['price'], $_POST['clinic'], $_POST['ward'], $_POST['location']);
        break;
    case isset($_POST['discharge']):
        dischargePatient($_POST['id'], $_POST['note']);
        break;
    case isset($_POST['add_in_patient_note']):
        addInPatientNote($_POST['admission_id'], $_POST['note'], $_SESSION['signature']);
        break;
    case isset($_POST['add_nurse_observation']):
        addNurseObservation($_POST['admission_id'], $_POST['note'], $_SESSION['signature']);
        break;
    case isset($_POST['add_speciality']):
        addProcedureSpeciality($_POST['name']);
        break;
    case isset($_POST['add_procedure_note']):
        addProcedureNote($_POST['procedure_id'], $_POST['speciality'], $_POST['note_type'], $_POST['note'], $_SESSION['signature']);
        break;
    case isset($_POST['upload_attachment']):
        uploadAttachment($_POST['upload_attachment'], $_FILES['file']);
        break;
    case isset($_POST['fill_lab_result']):
        fillLabResult($_POST['id'], $_POST['lab_test'], $_POST['result'], $_POST['emr']);
        break;
    case isset($_POST['upload_lab_result']):
        uploadLabResult($_POST['lab'], $_POST['id'], $_POST['emr'], $_FILES['file']);
        break;
    case isset($_POST['add_lab_category']):
        addLabCategory($_POST['name']);
        break;
    case isset($_POST['add_lab_type']):
        addLabType($_POST['name'], $_POST['category'], $_POST['result'], $_POST['price']);
        break;
    case isset($_POST['edit_lab_type']):
        editLabType($_POST['id'], $_POST['name'], $_POST['category'], $_POST['result'], $_POST['price']);
        break;
    case isset($_POST['fill_prescription']):
        $bill_patient = isset($_POST['bill_patient']) ? $_POST['bill_patient'] : 0;
        fillPescription(getEMRID($_POST['emr_id']), $_POST['id'], $_POST['drug_name'], $_POST['quantity'], $_POST['unit_price'], $_POST['stock_quantity'], $bill_patient);
        break;
    case isset($_POST['add_measure']):
        addMeasure($_POST['name']);
        break;
    case isset($_POST['add_drug']):
        addDrug($_POST['name'], $_POST['unit_of_measure'], $_POST['reorder_level']);
        break;
    case isset($_POST['edit_drug']):
        editDrug($_POST['id'],$_POST['name'], $_POST['unit_of_measure'], $_POST['reorder_level']);
        break;
    case isset($_POST['add_batch']):
   
        addBatch($_POST['drug_id'],$_POST['drug_name'],$_POST['batch'],  $_POST['quantity'], $_POST['unit_price'],$_POST['expire_date']);
        break;
    case isset($_POST['add_quantity']):
        addQuantity($_POST['id'], $_POST['quantity']);
        break;
    case isset($_POST['update_drug']):
        updateDrug($_POST['id'], $_POST['drug_name'],$_POST['expiration_date'],$_POST['batch_no'], $_POST['unit_price'], $_POST['quantity']);
        break;
    case isset($_POST['update_inventory_setting']):
        updateInventorySettings($_POST['item_expiration'], $_POST['copay_rate'], $_POST['low_qty_threshold']);
        break;
    case isset($_POST['upload_radiology_result']):
        uploadRadiologyResult($_POST['id'], $_POST['name'], $_POST['note'], $_POST['upload_radiology_result'], $_FILES['file']);
        break;
    case isset($_POST['add_scan']):
        addScan($_POST['name'], $_POST['price']);
        break;
    case isset($_POST['update_imaging']):
        updateImaging($_POST['id'], $_POST['name'], $_POST['price']);
        break;
    case isset($_POST['change_record_service_price']):
        changeRecordServicePrice($_POST['id'], $_POST['price']);
        break;
    case isset($_POST['add_procedure_service']):
        addProcedureService($_POST['name'], $_POST['price']);
        break;
    case isset($_POST['update_procedure_service']):
        updateProcedureService($_POST['id'], $_POST['name'], $_POST['price']);
        break;
    case isset($_POST['add_clinic']):
        addClinic($_POST['name']);
        break;
    case isset($_POST['edit_clinic']):
        editClinic($_POST['id'], $_POST['name']);
        break;
    case isset($_POST['add_room']):
        addRoom($_POST['id'], $_POST['name']);
        break;
    case isset($_POST['edit_room']):
        editRoom($_POST['clinic_id'], $_POST['id'], $_POST['name']);
        break;
    case isset($_POST['add_ward']):
        addWard($_POST['id'], $_POST['name'], $_POST['price']);
        break;
    case isset($_POST['edit_ward']):
        editWard($_POST['clinic_id'], $_POST['id'], $_POST['name'], $_POST['price']);
        break;
    case isset($_POST['add_bed_room']):
        addBed($_POST['id'], $_POST['name']);
        break;
    case isset($_POST['rename_bed_room']):
        editBed($_POST['ward_id'], $_POST['id'], $_POST['name']);
        break;
    case isset($_POST['create_account']):
        createAccount($_POST['first_name'], $_POST['last_name'], $_POST['phone_number'], $_POST['username'], $_POST['password']);
        break;
    case isset($_POST['change_password']):
        changePassword($_POST['id'], $_POST['password'], $_POST['new_password']);
        break;
    case isset($_POST['reset_password']):
        resetPassword($_POST['id'], $_POST['new_password']);
        break;
    case isset($_POST['change_privilege']):
        $record = isset($_POST['record']) ? 1 : 0;
        $nurse = isset($_POST['nurse']) ? 1 : 0;
        $doctor = isset($_POST['doctor']) ? 1 : 0;
        $pharmacy = isset($_POST['pharmacy']) ? 1 : 0;
        $pharmacy_admin = isset($_POST['pharmacy_admin']) ? 1 : 0;
        $lab = isset($_POST['lab']) ? 1 : 0;
        $lab_admin = isset($_POST['lab_admin']) ? 1 : 0;
        $radiology = isset($_POST['radiology']) ? 1 : 0;
        $radiology_admin = isset($_POST['radiology_admin']) ? 1 : 0;
        $bill = isset($_POST['bill']) ? 1 : 0;
        $bill_admin = isset($_POST['bill_admin']) ? 1 : 0;
        $admin = isset($_POST['admin']) ? 1 : 0;
        $super_admin = isset($_POST['super_admin']) ? 1 : 0;
        changePrivilege($_POST['id'], $record, $nurse, $doctor, $pharmacy, $pharmacy_admin, $lab, $lab_admin, $radiology, $radiology_admin, $bill, $bill_admin, $admin, $super_admin);
        break;
    case isset($_POST['add_hmo']):
        addHMO($_POST['name'], $_POST['abbr'], $_POST['phone_number']);
        break;
    case isset($_POST['update_hmo']):
        updateHMO($_POST['id'], $_POST['name'], $_POST['abbr'], $_POST['phone_number']);
        break;
        case isset($_FILES['file']):
        
            uploadBatch($_FILES['file']);
            break;
            case isset($_FILES['drugFile']):
                uploadDrug($_FILES['drugFile']);
                break;  
        
}