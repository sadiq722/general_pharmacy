<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
require_once 'helpers.php';
include_once 'utils.php';

switch (true) {
    case isset($_GET['dispense_prescription']):
        servePrescription($_GET['dispense_prescription']);
       // updateDrugQty($_GET['dispense_prescription']);
       
        break;
    case isset($_GET['get_drug_quantity']):

       $drug= _getDrugInfo($_GET['get_drug_quantity']);
      echo $drug['quantity'];

        break;
    case isset($_GET['cancel_prescription_request']):
        cancelPrescriptionRequest($_GET['cancel_prescription_request']); 
        break;
    case isset($_GET['cancel_prescription']):
        cancelPrescription($_GET['id'], $_GET['prescription']);
        break;
    case isset($_GET['delete_drug_batch']):
        _deleteDrugBatch($_GET['delete_drug_batch']);
        
        break;
      case isset($_GET['delete_drug']):
        _deleteDrug($_GET['delete_drug']);
        
        break;
    case isset($_GET['delete_user']):
        deleteUser($_GET['id']);
        break;
    case isset($_GET['delete_HMO']):
        deleteHMO($_GET['id']);
        break;
    case isset($_GET['make_payment']):
        $auth_code = 0;
        addPayment($_GET['emr'],$_GET['desc'], $_GET['type'], $_GET['price'], $_SESSION['signature'], $auth_code);
 if($_GET['type']=='Lab'){
        _initiateLabSeperateSettlement($_GET['make_payment']);
 } elseif ($_GET['type']=='Record') {
     _initiateRecordSeperateSettlement($_GET['make_payment']);
 
} elseif ($_GET['type']=='Pharmacy') {
    _initiatePharmacySeperateSettlement($_GET['make_payment']);

}
        break;
}