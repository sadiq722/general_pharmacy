-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 13, 2021 at 02:07 PM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pharmacy_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `drug_batch_table`
--

CREATE TABLE `drug_batch_table` (
  `id` int(11) NOT NULL,
  `batch_no` varchar(45) NOT NULL,
  `quantity` int(11) NOT NULL,
  `unit_price` int(11) NOT NULL,
  `expiration_date` date NOT NULL,
  `drug_name` varchar(45) NOT NULL,
  `created_by` varchar(45) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_by` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `drug_batch_table`
--

INSERT INTO `drug_batch_table` (`id`, `batch_no`, `quantity`, `unit_price`, `expiration_date`, `drug_name`, `created_by`, `created_at`, `updated_at`, `updated_by`) VALUES
(1, '00000008', -7, 450, '2023-05-12', 'Pentazosin', 'test', '2021-09-28 10:37:20', '2021-09-28 10:37:20', ''),
(2, '00000001', 207, 550, '2023-05-02', 'Pentazosin', 'test', '2021-09-28 10:40:42', '2021-09-28 10:40:42', ''),
(3, '000000002', 85, 80, '2025-11-11', 'Panadol', 'test', '2021-09-28 11:38:46', '2021-09-28 11:38:46', '');

-- --------------------------------------------------------

--
-- Table structure for table `drug_measure_table`
--

CREATE TABLE `drug_measure_table` (
  `id` int(11) NOT NULL,
  `name` varchar(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `drug_measure_table`
--

INSERT INTO `drug_measure_table` (`id`, `name`) VALUES
(1, 'Tablet'),
(2, 'Capsule'),
(3, 'Bottle'),
(4, 'Pant');

-- --------------------------------------------------------

--
-- Table structure for table `drug_table`
--

CREATE TABLE `drug_table` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `unit_of_measure` varchar(45) NOT NULL,
  `reorder_level` int(11) NOT NULL,
  `updated_by` varchar(65) NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `expire_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `drug_table`
--

INSERT INTO `drug_table` (`id`, `name`, `unit_of_measure`, `reorder_level`, `updated_by`, `updated_at`, `expire_date`) VALUES
(1, 'Panadol', 'Tablet', 28, 'test', '2021-09-19 11:30:50', '2021-11-18'),
(2, 'Menthodex', 'Bottle', 77, 'test', '2021-09-19 11:30:50', '2021-11-18'),
(3, 'pentasoson', 'Bottle', 36, 'test', '2021-09-19 11:43:11', '2022-05-28'),
(4, 'Paracetamol', 'Tablet', 150, 'test', '2021-09-22 09:56:34', '2021-09-22'),
(5, 'Pentazosin', 'Bottle', 20, 'test', '2021-09-28 10:23:27', '0000-00-00');

-- --------------------------------------------------------

--
-- Table structure for table `hospital_table`
--

CREATE TABLE `hospital_table` (
  `hospital_name` varchar(60) NOT NULL,
  `address` varchar(120) NOT NULL,
  `phone_number` varchar(12) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hospital_table`
--

INSERT INTO `hospital_table` (`hospital_name`, `address`, `phone_number`) VALUES
('Turai Yaradua Mertinaty Hospital Katsina', 'ADDRESS', '08000000000');

-- --------------------------------------------------------

--
-- Table structure for table `insurance_table`
--

CREATE TABLE `insurance_table` (
  `id` int(11) NOT NULL,
  `hmo` varchar(111) NOT NULL,
  `hmo_abbr` varchar(25) NOT NULL,
  `phone_number` varchar(21) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `insurance_table`
--

INSERT INTO `insurance_table` (`id`, `hmo`, `hmo_abbr`, `phone_number`) VALUES
(2, 'STATE NATIONAL HEALTH INSURANCE SCHEME', 'GNHIS', '08056568978'),
(3, 'KATSINA STATE CONTRIBUTORY HEALTH INSURRANCE ', 'KACHIMA', '09099999999');

-- --------------------------------------------------------

--
-- Table structure for table `inventory_setting`
--

CREATE TABLE `inventory_setting` (
  `id` int(1) NOT NULL,
  `item_expiration` int(3) NOT NULL,
  `copay_rate` int(3) NOT NULL,
  `low_qty_threshold` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `inventory_setting`
--

INSERT INTO `inventory_setting` (`id`, `item_expiration`, `copay_rate`, `low_qty_threshold`) VALUES
(1, 3, 10, 25);

-- --------------------------------------------------------

--
-- Table structure for table `lab_category_table`
--

CREATE TABLE `lab_category_table` (
  `id` int(11) NOT NULL,
  `name` varchar(75) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lab_category_table`
--

INSERT INTO `lab_category_table` (`id`, `name`) VALUES
(1, 'MICRBIOLOGY');

-- --------------------------------------------------------

--
-- Table structure for table `lab_investigation_table`
--

CREATE TABLE `lab_investigation_table` (
  `id` int(11) NOT NULL,
  `emr_id` varchar(11) NOT NULL,
  `visit_id` int(11) NOT NULL DEFAULT 0,
  `request_date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `price` double NOT NULL,
  `seperate_settlement` int(1) NOT NULL DEFAULT 0,
  `lab_test` varchar(25) NOT NULL,
  `request_note` varchar(120) NOT NULL,
  `requested_by` varchar(45) NOT NULL,
  `result` varchar(500) DEFAULT '',
  `attachment` varchar(500) DEFAULT '',
  `conducted_by` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lab_investigation_table`
--

INSERT INTO `lab_investigation_table` (`id`, `emr_id`, `visit_id`, `request_date`, `price`, `seperate_settlement`, `lab_test`, `request_note`, `requested_by`, `result`, `attachment`, `conducted_by`) VALUES
(1, '00000000022', 0, '2018-01-28 20:39:21', 400, 0, 'MPS', 'rrr', 'test', NULL, NULL, NULL),
(2, '00000000023', 0, '2018-01-28 16:51:45', 400, 0, 'MPS', 'dd', 'test', NULL, NULL, NULL),
(3, '00000000002', 0, '2018-01-28 17:55:05', 400, 0, 'MPS', 'r', 'test', NULL, NULL, NULL),
(4, '00000000002', 0, '2018-01-28 17:55:21', 400, 0, 'MPS', 't', 'test', NULL, NULL, NULL),
(5, '00000000002', 0, '2018-01-28 17:55:43', 400, 0, 'MPS', 'gg', 'test', NULL, NULL, NULL),
(6, '00000000002', 0, '2018-01-28 17:56:00', 400, 0, 'MPS', 'b', 'test', NULL, NULL, NULL),
(7, '00000000002', 0, '2018-01-28 17:56:21', 400, 0, 'MPS', 'gggg', 'test', NULL, NULL, NULL),
(8, '00000000002', 0, '2018-01-28 17:56:40', 400, 0, 'MPS', 'hnn', 'test', NULL, NULL, NULL),
(9, '33', 0, '2018-01-28 19:28:48', 33, 0, '', '', '', '', '', NULL),
(11, '00000000002', 0, '2018-01-28 19:33:00', 400, 1, 'MPS', 'f', 'test', 'ttt', '', 'test'),
(14, '00000000002', 0, '2021-08-31 07:26:47', 400, 1, 'MPS', '200', 'test', '', '', NULL),
(15, '00000000002', 0, '2021-08-31 09:17:44', 400, 1, 'MPS', 'nnn', 'test', '', '', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `last_vitals_table`
--

CREATE TABLE `last_vitals_table` (
  `id` int(11) NOT NULL,
  `emr_id` varchar(11) NOT NULL,
  `type` varchar(95) NOT NULL,
  `reading` varchar(45) NOT NULL DEFAULT '0',
  `checked_by` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `last_vitals_table`
--

INSERT INTO `last_vitals_table` (`id`, `emr_id`, `type`, `reading`, `checked_by`) VALUES
(1, '00000000022', 'Blood Pressure (mmHg)', '65/88', 'test'),
(2, '00000000022', 'Glucose (mg/dl)', '3', 'test'),
(3, '00000000022', 'Pulse (beats/min)', '0', NULL),
(4, '00000000022', 'Respiration (beats/min)', '0', NULL),
(5, '00000000022', 'Tempreture (oC)', '0', NULL),
(6, '00000000023', 'Blood Pressure (mmHg)', '0', NULL),
(7, '00000000023', 'Glucose (mg/dl)', '0', NULL),
(8, '00000000023', 'Pulse (beats/min)', '0', NULL),
(9, '00000000023', 'Respiration (beats/min)', '0', NULL),
(10, '00000000023', 'Tempreture (oC)', '0', NULL),
(11, '00000000024', 'Blood Pressure (mmHg)', '0', NULL),
(12, '00000000024', 'Glucose (mg/dl)', '0', NULL),
(13, '00000000024', 'Pulse (beats/min)', '0', NULL),
(14, '00000000024', 'Respiration (beats/min)', '0', NULL),
(15, '00000000024', 'Tempreture (oC)', '0', NULL),
(16, '00000000025', 'Blood Pressure (mmHg)', '0', NULL),
(17, '00000000025', 'Glucose (mg/dl)', '0', NULL),
(18, '00000000025', 'Pulse (beats/min)', '0', NULL),
(19, '00000000025', 'Respiration (beats/min)', '0', NULL),
(20, '00000000025', 'Tempreture (oC)', '0', NULL),
(21, '00000000026', 'Blood Pressure (mmHg)', '0', NULL),
(22, '00000000026', 'Glucose (mg/dl)', '0', NULL),
(23, '00000000026', 'Pulse (beats/min)', '0', NULL),
(24, '00000000026', 'Respiration (beats/min)', '0', NULL),
(25, '00000000026', 'Tempreture (oC)', '0', NULL),
(26, '00000000027', 'Blood Pressure (mmHg)', '0', NULL),
(27, '00000000027', 'Glucose (mg/dl)', '0', NULL),
(28, '00000000027', 'Pulse (beats/min)', '0', NULL),
(29, '00000000027', 'Respiration (beats/min)', '0', NULL),
(30, '00000000027', 'Tempreture (oC)', '0', NULL),
(31, '00000000028', 'Blood Pressure (mmHg)', '0', NULL),
(32, '00000000028', 'Glucose (mg/dl)', '0', NULL),
(33, '00000000028', 'Pulse (beats/min)', '0', NULL),
(34, '00000000028', 'Respiration (beats/min)', '0', NULL),
(35, '00000000028', 'Tempreture (oC)', '0', NULL),
(36, '00000000029', 'Blood Pressure (mmHg)', '0', NULL),
(37, '00000000029', 'Glucose (mg/dl)', '0', NULL),
(38, '00000000029', 'Pulse (beats/min)', '0', NULL),
(39, '00000000029', 'Respiration (beats/min)', '0', NULL),
(40, '00000000029', 'Tempreture (oC)', '0', NULL),
(41, '00000000030', 'Blood Pressure (mmHg)', '0', NULL),
(42, '00000000030', 'Glucose (mg/dl)', '0', NULL),
(43, '00000000030', 'Pulse (beats/min)', '0', NULL),
(44, '00000000030', 'Respiration (beats/min)', '0', NULL),
(45, '00000000030', 'Tempreture (oC)', '0', NULL),
(46, '00000000031', 'Blood Pressure (mmHg)', '0', NULL),
(47, '00000000031', 'Glucose (mg/dl)', '0', NULL),
(48, '00000000031', 'Pulse (beats/min)', '0', NULL),
(49, '00000000031', 'Respiration (beats/min)', '0', NULL),
(50, '00000000031', 'Tempreture (oC)', '0', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `patient_billing_account`
--

CREATE TABLE `patient_billing_account` (
  `id` int(11) NOT NULL,
  `emr_id` varchar(11) NOT NULL,
  `balance` double NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `patient_billing_account`
--

INSERT INTO `patient_billing_account` (`id`, `emr_id`, `balance`) VALUES
(1, '00000000001', -500),
(4, '00000000002', 6222),
(5, '00000000003', -1000),
(6, '00000000004', -1500),
(7, '00000000006', -2000),
(8, '00000000007', -2500),
(9, '00000000008', -3000),
(10, '00000000009', -3500),
(11, '00000000010', -4000),
(12, '00000000011', -4500),
(13, '00000000012', -5000),
(14, '00000000013', -5500),
(15, '00000000014', -6000),
(16, '00000000015', -6500),
(17, '66', 0),
(18, '00000000016', -500),
(19, '00000000017', -2060),
(20, '00000000018', -15503),
(21, '00000000019', -43008),
(22, '00000000020', -5501),
(23, '00000000021', -500),
(24, '00000000022', -49900),
(25, '00000000023', -5901),
(28, '00000000024', -300),
(29, '00000000024', -300),
(30, '00000000024', -300),
(31, '00000000024', -300),
(32, '00000000024', -300),
(33, '00000000024', -300),
(34, '00000000024', -300),
(35, '00000000024', -300),
(36, '00000000024', -300),
(37, '00000000024', -300),
(38, '00000000024', -300),
(39, '00000000024', -300),
(40, '00000000024', -300),
(41, '00000000024', -300),
(42, '00000000024', -300),
(43, '00000000024', -300),
(44, '00000000024', -300),
(45, '00000000025', -1100),
(46, '00000000026', -500),
(47, '00000000027', -500),
(48, '00000000028', -500),
(49, '00000000029', -500),
(50, '00000000030', -500),
(51, '00000000031', -500);

-- --------------------------------------------------------

--
-- Table structure for table `patient_table`
--

CREATE TABLE `patient_table` (
  `id` int(11) NOT NULL,
  `phone_number` varchar(14) NOT NULL,
  `full_name` varchar(45) NOT NULL,
  `created_at` date NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `patient_table`
--

INSERT INTO `patient_table` (`id`, `phone_number`, `full_name`, `created_at`) VALUES
(1, '07063490700', 'yusuf abubakar', '0000-00-00'),
(2, '09099614141', 'Abubakar Yusuf', '0000-00-00'),
(3, '09099614144', 'Abubakar Yusuf', '2021-09-20'),
(4, '08022065050', 'garba isah', '2021-09-20'),
(5, '08022', 'abba', '2021-09-20'),
(6, '777', 'ww', '2021-09-20'),
(7, '77', 'q', '2021-09-20'),
(8, '00', 't', '2021-09-20'),
(9, '66', 't', '2021-09-20'),
(10, '88', 'y', '2021-09-20'),
(11, '888', 'hh', '2021-09-20'),
(12, '4', 'hh', '2021-09-20'),
(13, '0909977', 'qwe', '2021-09-20'),
(14, '090887666', 'jj', '2021-09-20'),
(15, '666', 'kk', '2021-09-20'),
(16, '8885', 'uu', '2021-09-20'),
(17, '08900', 'yyu', '2021-09-20'),
(18, '6', 'e', '2021-09-20'),
(19, '7', 'q', '2021-09-20'),
(20, '99', 'q', '2021-09-20'),
(21, '55', 'q', '2021-09-20'),
(22, '5', 'q', '2021-09-20'),
(23, '999', 'o', '2021-09-21'),
(24, '2', 'q', '2021-09-21'),
(25, '22', 'q', '2021-09-21'),
(26, '555', 'kk', '2021-09-21'),
(27, '33', 'w', '2021-09-21'),
(28, '25', 'm', '2021-09-21'),
(29, '20', '2', '2021-09-21'),
(30, '3', 'www', '2021-09-21'),
(31, '020', 'q', '2021-09-28'),
(32, '21', 'q', '2021-09-28'),
(33, '5445', 'gh', '2021-10-09'),
(34, '122', 'kk', '2021-10-10'),
(35, '121', 'l', '2021-10-10'),
(36, '12', 'l', '2021-10-11'),
(37, '0201', 'Yusuf Abubakar Sadiq', '2021-10-12'),
(38, '778', 'yttytu', '2021-10-12'),
(39, '767', 'h', '2021-10-12'),
(40, '55555', 'QQQ', '2021-10-13');

-- --------------------------------------------------------

--
-- Table structure for table `prescription_slip`
--

CREATE TABLE `prescription_slip` (
  `id` int(11) NOT NULL,
  `patient_id` varchar(11) NOT NULL,
  `is_cleared` int(1) DEFAULT 0,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `prescription_slip`
--

INSERT INTO `prescription_slip` (`id`, `patient_id`, `is_cleared`, `created_at`) VALUES
(1, '1', NULL, '2021-09-20 10:36:02'),
(2, '2', NULL, '2021-09-20 10:37:41'),
(3, '3', NULL, '2021-09-20 10:40:06'),
(4, '4', NULL, '2021-09-20 10:40:27'),
(5, '5', NULL, '2021-09-20 10:45:57'),
(6, '6', NULL, '2021-09-20 10:46:38'),
(7, '7', NULL, '2021-09-20 10:48:21'),
(8, '8', NULL, '2021-09-20 10:49:35'),
(9, '9', NULL, '2021-09-20 10:51:05'),
(10, '6', NULL, '2021-09-20 10:53:01'),
(11, '10', NULL, '2021-09-20 10:53:25'),
(12, '7', NULL, '2021-09-20 10:54:18'),
(13, '11', NULL, '2021-09-20 10:55:35'),
(14, '12', NULL, '2021-09-20 10:57:07'),
(15, '6', NULL, '2021-09-20 10:59:10'),
(16, '7', NULL, '2021-09-20 11:00:51'),
(17, '9', NULL, '2021-09-20 11:01:52'),
(18, '13', NULL, '2021-09-20 11:02:47'),
(19, '7', NULL, '2021-09-20 11:05:02'),
(20, '14', NULL, '2021-09-20 11:05:51'),
(21, '9', NULL, '2021-09-20 11:06:28'),
(22, '15', NULL, '2021-09-20 11:06:52'),
(23, '16', NULL, '2021-09-20 11:07:53'),
(24, '6', NULL, '2021-09-20 11:10:47'),
(25, '12', NULL, '2021-09-20 11:11:36'),
(26, '17', NULL, '2021-09-20 11:47:03'),
(27, '12', NULL, '2021-09-20 12:04:51'),
(28, '7', NULL, '2021-09-20 12:06:29'),
(29, '18', NULL, '2021-09-20 12:07:04'),
(30, '11', NULL, '2021-09-20 12:07:38'),
(31, '19', NULL, '2021-09-20 12:08:27'),
(32, '20', NULL, '2021-09-20 12:10:54'),
(33, '21', NULL, '2021-09-20 12:11:37'),
(34, '7', NULL, '2021-09-20 12:18:49'),
(35, '7', NULL, '2021-09-20 12:22:03'),
(36, '9', NULL, '2021-09-20 12:24:28'),
(37, '10', NULL, '2021-09-20 12:26:01'),
(38, '15', NULL, '2021-09-20 12:32:19'),
(39, '11', NULL, '2021-09-20 12:32:57'),
(40, '9', NULL, '2021-09-20 12:33:51'),
(41, '7', NULL, '2021-09-20 14:34:16'),
(42, '9', NULL, '2021-09-20 14:43:36'),
(43, '12', NULL, '2021-09-20 14:46:15'),
(44, '22', NULL, '2021-09-20 14:48:48'),
(45, '23', NULL, '2021-09-21 07:32:37'),
(46, '11', NULL, '2021-09-21 07:34:15'),
(47, '10', NULL, '2021-09-21 07:36:00'),
(48, '10', NULL, '2021-09-21 07:38:30'),
(49, '20', NULL, '2021-09-21 08:08:28'),
(50, '6', NULL, '2021-09-21 08:19:10'),
(51, '6', NULL, '2021-09-21 08:19:19'),
(52, '24', NULL, '2021-09-21 09:11:00'),
(53, '24', NULL, '2021-09-21 09:11:08'),
(54, '10', NULL, '2021-09-21 09:18:57'),
(55, '23', NULL, '2021-09-21 09:24:47'),
(56, '23', NULL, '2021-09-21 09:24:56'),
(57, '23', NULL, '2021-09-21 09:25:39'),
(58, '10', NULL, '2021-09-21 09:25:54'),
(59, '10', NULL, '2021-09-21 09:26:01'),
(60, '21', NULL, '2021-09-21 09:27:54'),
(61, '21', NULL, '2021-09-21 09:28:02'),
(62, '21', NULL, '2021-09-21 09:28:14'),
(63, '21', NULL, '2021-09-21 09:28:16'),
(64, '21', NULL, '2021-09-21 09:28:17'),
(65, '21', NULL, '2021-09-21 09:28:33'),
(66, '21', NULL, '2021-09-21 09:28:34'),
(67, '21', NULL, '2021-09-21 09:28:36'),
(68, '21', NULL, '2021-09-21 09:29:20'),
(69, '21', NULL, '2021-09-21 09:29:21'),
(70, '21', NULL, '2021-09-21 09:29:23'),
(71, '21', NULL, '2021-09-21 09:29:24'),
(72, '25', NULL, '2021-09-21 09:29:53'),
(73, '25', NULL, '2021-09-21 09:30:00'),
(74, '25', NULL, '2021-09-21 09:30:01'),
(75, '20', NULL, '2021-09-21 09:31:34'),
(76, '20', NULL, '2021-09-21 09:31:41'),
(77, '20', NULL, '2021-09-21 09:31:47'),
(78, '20', NULL, '2021-09-21 09:32:13'),
(79, '26', NULL, '2021-09-21 10:01:39'),
(80, '27', NULL, '2021-09-21 10:03:26'),
(81, '7', NULL, '2021-09-21 10:04:12'),
(82, '28', NULL, '2021-09-21 10:09:14'),
(83, '25', 1, '2021-09-21 10:20:49'),
(84, '28', 0, '2021-09-21 10:24:59'),
(85, '29', 0, '2021-09-21 11:23:14'),
(86, '30', 1, '2021-09-21 11:25:57'),
(87, '30', 1, '2021-09-21 11:30:46'),
(88, '22', 0, '2021-09-21 11:45:50'),
(89, '25', 0, '2021-09-21 11:49:13'),
(90, '4', 1, '2021-09-22 09:57:38'),
(91, '31', 1, '2021-09-28 10:54:38'),
(92, '32', 1, '2021-09-28 11:09:08'),
(93, '10', 1, '2021-09-28 11:21:55'),
(94, '24', 1, '2021-09-28 11:39:03'),
(95, '33', 0, '2021-10-09 17:50:06'),
(96, '34', 0, '2021-10-10 17:48:26'),
(97, '35', 1, '2021-10-10 18:03:24'),
(98, '7', 1, '2021-10-10 20:43:49'),
(99, '35', 1, '2021-10-11 13:18:38'),
(100, '35', 1, '2021-10-11 13:20:38'),
(101, '35', 1, '2021-10-11 13:21:26'),
(102, '35', 1, '2021-10-11 13:22:16'),
(103, '35', 1, '2021-10-11 13:23:46'),
(104, '36', 0, '2021-10-11 13:59:36'),
(105, '35', 1, '2021-10-11 14:11:01'),
(106, '35', 1, '2021-10-11 14:11:51'),
(107, '35', 1, '2021-10-11 14:16:04'),
(108, '35', 1, '2021-10-11 14:35:09'),
(109, '37', 1, '2021-10-12 10:05:00'),
(110, '38', 1, '2021-10-12 10:30:25'),
(111, '39', 1, '2021-10-12 10:32:22'),
(112, '40', 1, '2021-10-13 12:02:30');

-- --------------------------------------------------------

--
-- Table structure for table `prescription_table`
--

CREATE TABLE `prescription_table` (
  `id` int(11) NOT NULL,
  `prescription_slip_id` varchar(11) NOT NULL,
  `prescription_date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `drug_name` varchar(45) NOT NULL,
  `served_quantity` int(11) NOT NULL DEFAULT 0,
  `price` double NOT NULL DEFAULT 0,
  `batch_no` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `prescription_table`
--

INSERT INTO `prescription_table` (`id`, `prescription_slip_id`, `prescription_date`, `drug_name`, `served_quantity`, `price`, `batch_no`) VALUES
(3, '93', '2021-09-28 11:28:50', 'Pentazosin', 2, 90, '00000008'),
(4, '94', '2021-09-28 11:39:11', 'Panadol', 15, 120, '000000002'),
(5, '94', '2021-09-28 11:39:16', 'Pentazosin', 15, 675, '00000008'),
(6, '94', '2021-09-28 11:40:13', 'Pentazosin', 90, 4050, '00000008'),
(10, '97', '2021-10-10 20:40:31', 'Panadol', 10, 80, '000000002'),
(11, '97', '2021-10-10 20:43:13', 'Pentazosin', 3, 165, '00000001'),
(13, '98', '2021-10-10 20:44:57', 'Pentazosin', 4, 220, '00000001'),
(14, '99', '2021-10-11 13:18:45', 'Pentazosin', 2, 110, '00000001'),
(15, '100', '2021-10-11 13:20:44', 'Pentazosin', 3, 165, '00000001'),
(16, '101', '2021-10-11 13:21:34', 'Pentazosin', 1, 55, '00000001'),
(17, '102', '2021-10-11 13:22:23', 'Pentazosin', 1, 55, '00000001'),
(18, '103', '2021-10-11 13:59:02', 'Panadol', 1, 8, '000000002'),
(19, '105', '2021-10-11 14:11:16', 'Pentazosin', 2, 110, '00000001'),
(20, '106', '2021-10-11 14:15:29', 'Pentazosin', 2, 110, '00000001'),
(21, '107', '2021-10-11 14:16:09', 'Pentazosin', 5, 275, '00000001'),
(22, '108', '2021-10-11 14:35:16', 'Pentazosin', 2, 110, '00000001'),
(23, '91', '2021-10-12 10:02:59', 'Pentazosin', 5, 275, '00000001'),
(24, '91', '2021-10-12 10:03:06', 'Panadol', 2, 16, '000000002'),
(25, '109', '2021-10-12 10:05:06', 'Pentazosin', 1, 55, '00000001'),
(26, '109', '2021-10-12 10:05:13', 'Pentazosin', 2, 110, '00000001'),
(27, '109', '2021-10-12 10:05:27', 'Panadol', 1, 8, '000000002'),
(28, '109', '2021-10-12 10:05:34', 'Panadol', 2, 16, '000000002'),
(29, '110', '2021-10-12 10:30:31', 'Pentazosin', 4, 220, '00000001'),
(30, '110', '2021-10-12 10:30:36', 'Panadol', 2, 16, '000000002'),
(31, '111', '2021-10-12 10:32:28', 'Pentazosin', 4, 220, '00000001'),
(32, '111', '2021-10-12 10:32:33', 'Panadol', 94, 752, '000000002'),
(34, '112', '2021-10-13 12:02:36', 'Pentazosin', 2, 110, '00000001'),
(35, '112', '2021-10-13 12:02:43', 'Panadol', 3, 24, '000000002');

-- --------------------------------------------------------

--
-- Table structure for table `printer_table`
--

CREATE TABLE `printer_table` (
  `id` int(11) NOT NULL,
  `share_name` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `printer_table`
--

INSERT INTO `printer_table` (`id`, `share_name`) VALUES
(1, 'XP-76C');

-- --------------------------------------------------------

--
-- Table structure for table `privilege_table`
--

CREATE TABLE `privilege_table` (
  `user_id` int(1) NOT NULL DEFAULT 0,
  `record` int(1) NOT NULL DEFAULT 0,
  `nurse` int(1) NOT NULL DEFAULT 0,
  `doctor` int(1) NOT NULL DEFAULT 0,
  `pharmacy` int(1) NOT NULL DEFAULT 0,
  `pharmacy_admin` int(1) NOT NULL DEFAULT 0,
  `lab` int(1) NOT NULL DEFAULT 0,
  `lab_admin` int(1) NOT NULL DEFAULT 0,
  `radiology` int(1) NOT NULL DEFAULT 0,
  `radiology_admin` int(1) NOT NULL DEFAULT 0,
  `bill` int(1) NOT NULL DEFAULT 0,
  `bill_admin` int(1) NOT NULL DEFAULT 0,
  `admin` int(1) NOT NULL DEFAULT 0,
  `super_admin` int(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `privilege_table`
--

INSERT INTO `privilege_table` (`user_id`, `record`, `nurse`, `doctor`, `pharmacy`, `pharmacy_admin`, `lab`, `lab_admin`, `radiology`, `radiology_admin`, `bill`, `bill_admin`, `admin`, `super_admin`) VALUES
(2, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1),
(4, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1),
(5, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `procedure_attachment_table`
--

CREATE TABLE `procedure_attachment_table` (
  `id` int(11) NOT NULL,
  `procedure_id` int(11) NOT NULL,
  `description` varchar(100) NOT NULL,
  `attachment` varchar(500) NOT NULL,
  `attached_by` varchar(65) NOT NULL,
  `attached_date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `procedure_list_table`
--

CREATE TABLE `procedure_list_table` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `price` double NOT NULL,
  `updated_by` varchar(75) NOT NULL,
  `last_update` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `procedure_list_table`
--

INSERT INTO `procedure_list_table` (`id`, `name`, `price`, `updated_by`, `last_update`) VALUES
(1, 'CIRCUMCISION', 2000, 'y.sadiq', '2021-02-13 17:20:56'),
(2, 'CS', 50000, 'test', '2018-01-28 20:02:28');

-- --------------------------------------------------------

--
-- Table structure for table `procedure_medication_table`
--

CREATE TABLE `procedure_medication_table` (
  `id` int(11) NOT NULL,
  `procedure_id` int(11) NOT NULL,
  `drug_name` varchar(75) NOT NULL,
  `dose` varchar(45) NOT NULL,
  `duration` varchar(75) NOT NULL,
  `prescribed_by` varchar(75) NOT NULL,
  `prescribed_date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `procedure_note_table`
--

CREATE TABLE `procedure_note_table` (
  `id` int(11) NOT NULL,
  `procedure_id` int(11) NOT NULL,
  `note_type` varchar(45) NOT NULL,
  `note` varchar(10000) NOT NULL,
  `speciality` varchar(75) NOT NULL,
  `noted_by` varchar(75) NOT NULL,
  `note_date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `procedure_note_table`
--

INSERT INTO `procedure_note_table` (`id`, `procedure_id`, `note_type`, `note`, `speciality`, `noted_by`, `note_date`) VALUES
(1, 1, 'Findings', 'ff', 'surgeion', 'test', '2018-01-28 17:52:22');

-- --------------------------------------------------------

--
-- Table structure for table `procedure_speciality_table`
--

CREATE TABLE `procedure_speciality_table` (
  `id` int(11) NOT NULL,
  `name` varchar(75) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `procedure_speciality_table`
--

INSERT INTO `procedure_speciality_table` (`id`, `name`) VALUES
(1, 'surgeion'),
(2, 'dentalist'),
(3, 'ANESTHETIC');

-- --------------------------------------------------------

--
-- Table structure for table `radiology_table`
--

CREATE TABLE `radiology_table` (
  `id` int(11) NOT NULL,
  `name` varchar(70) NOT NULL,
  `last_update` varchar(500) NOT NULL,
  `price` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `radiology_table`
--

INSERT INTO `radiology_table` (`id`, `name`, `last_update`, `price`) VALUES
(1, 'CT SCAN', 'test', 25000),
(2, 'X-RAY Abdomen', 'test', 4500),
(3, 'Ultrasound', 'test', 2500);

-- --------------------------------------------------------

--
-- Table structure for table `service_table`
--

CREATE TABLE `service_table` (
  `id` int(11) NOT NULL,
  `category` varchar(25) NOT NULL,
  `last_update` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `name` varchar(25) NOT NULL,
  `price` int(11) NOT NULL,
  `updated_by` varchar(75) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `service_table`
--

INSERT INTO `service_table` (`id`, `category`, `last_update`, `name`, `price`, `updated_by`) VALUES
(1, 'Record', '2021-02-13 17:16:33', 'Account Creation', 500, 'y.sadiq'),
(2, 'Appointment', '2020-12-14 10:55:17', 'Regular Appointment', 2000, '0'),
(3, 'Appointment', '2020-12-14 10:55:17', 'Brief/Follow Up', 1000, '0'),
(4, 'Appointment', '2020-12-14 10:55:17', 'Investigation Review', 500, '0');

-- --------------------------------------------------------

--
-- Table structure for table `transaction_table`
--

CREATE TABLE `transaction_table` (
  `id` int(11) NOT NULL,
  `auth_code` varchar(50) DEFAULT NULL,
  `biller_id` int(11) NOT NULL,
  `emr_id` varchar(11) NOT NULL,
  `item_id` int(11) DEFAULT NULL,
  `item_type` varchar(45) DEFAULT NULL,
  `description` varchar(60) DEFAULT NULL,
  `quantity` int(6) NOT NULL DEFAULT 0,
  `price` double NOT NULL DEFAULT 0,
  `type` varchar(21) DEFAULT NULL,
  `balance` double NOT NULL DEFAULT 0,
  `transaction_date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `signed_by` varchar(45) DEFAULT NULL,
  `is_reverse` int(1) NOT NULL DEFAULT 0,
  `is_settle` int(1) NOT NULL DEFAULT 0,
  `transaction_date_` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `transaction_table`
--

INSERT INTO `transaction_table` (`id`, `auth_code`, `biller_id`, `emr_id`, `item_id`, `item_type`, `description`, `quantity`, `price`, `type`, `balance`, `transaction_date`, `signed_by`, `is_reverse`, `is_settle`, `transaction_date_`) VALUES
(1, NULL, 22, '00000000020', 1, 'Record', 'Account Creation', 1, 500, 'Debit', -500, '2018-01-28 07:40:55', 'test', 0, 0, '2018-01-28'),
(2, NULL, 22, '00000000020', 1, 'Record', 'Account Creation', 1, 500, 'Debit', -500, '2018-01-28 07:40:55', 'test', 0, 0, '2018-01-28'),
(10, NULL, 4, '00000000002', 3, 'Record', 'Regular Appointment', 1, 2000, 'Debit', -2000, '2018-01-28 08:55:16', 'test', 0, 0, '2018-01-28'),
(11, '0', 4, '00000000002', 0, 'Record', 'Regular Appointment', 1, 2000, 'Credit', 0, '2018-01-28 19:56:43', 'test', 0, 0, '2018-01-28'),
(12, NULL, 23, '00000000021', 1, 'Record', 'Account Creation', 1, 500, 'Debit', -500, '2018-01-28 09:03:50', 'test', 0, 0, '2018-01-28'),
(13, NULL, 24, '00000000022', 1, 'Record', 'Account Creation', 1, 500, 'Debit', -500, '2018-01-28 09:05:27', 'test', 0, 0, '2018-01-28'),
(14, NULL, 24, '00000000022', 1, 'Lab', 'MPS', 1, 400, 'Debit', -1700, '2018-01-28 09:39:21', 'test', 0, 0, '2018-01-28'),
(15, NULL, 24, '00000000022', 1, 'Procedure', 'CIRCUMCISION', 1, 2000, 'Debit', -28700, '2018-01-28 09:40:00', 'test', 0, 0, '2018-01-28'),
(16, NULL, 24, '00000000022', 11, 'imaging', 'CT SCAN', 1, 25000, 'Debit', -53700, '2018-01-28 09:41:24', 'test', 0, 0, '2018-01-28'),
(17, NULL, 24, '00000000022', 4, 'Record', 'Regular Appointment', 1, 2000, 'Debit', -55700, '2018-01-28 16:22:25', 'test', 1, 0, '2018-01-28'),
(18, NULL, 24, '00000000022', 4, 'Record', 'Regular Appointment', 1, 2000, 'Reversed', -53700, '2018-01-28 05:22:25', 'test', 0, 0, '2018-01-28'),
(19, NULL, 20, '00000000018', 1, 'Admission', 'MALE WARD bed charges', 1, 5001, 'Debit', -5501, '2018-01-28 05:24:10', 'test', 0, 0, '2018-01-28'),
(20, NULL, 24, '00000000022', 2, 'Procedure', 'CIRCUMCISION', 1, 2000, 'Debit', -55700, '2018-01-28 17:50:28', 'test', 1, 0, '2018-01-28'),
(21, NULL, 25, '00000000023', 1, 'Record', 'Account Creation', 1, 500, 'Debit', -500, '2018-01-28 05:49:59', 'test', 0, 0, '2018-01-28'),
(22, NULL, 25, '00000000023', 2, 'Lab', 'MPS', 1, 400, 'Debit', -900, '2018-01-28 05:51:45', 'test', 0, 0, '2018-01-28'),
(23, NULL, 25, '00000000023', 3, 'Procedure', 'CIRCUMCISION', 1, 2000, 'Debit', -2900, '2018-01-28 17:50:04', 'test', 1, 0, '2018-01-28'),
(24, NULL, 20, '00000000018', 3, 'Admission', 'MALE WARD bed charges', 1, 5001, 'Debit', -10502, '2018-01-28 05:54:13', 'test', 0, 0, '2018-01-28'),
(25, NULL, 20, '00000000018', 3, 'Admission', 'MALE WARD bed charges', 1, 5001, 'Debit', -15503, '2018-01-28 05:57:16', 'test', 0, 0, '2018-01-28'),
(26, NULL, 21, '00000000019', 4, 'Admission', 'MALE WARD bed charges', 1, 5001, 'Debit', -5501, '2018-01-28 05:04:32', 'test', 0, 0, '2018-01-28'),
(27, NULL, 21, '00000000019', 5, 'Admission', 'MALE WARD bed charges', 1, 5001, 'Debit', -10502, '2018-01-28 05:06:50', 'test', 0, 0, '2018-01-28'),
(28, NULL, 21, '00000000019', 5, 'Admission', 'MALE WARD bed charges', 1, 5001, 'Debit', -15503, '2018-01-28 05:07:37', 'test', 0, 0, '2018-01-28'),
(29, NULL, 21, '00000000019', 5, 'Admission', 'MALE WARD bed charges', 1, 5001, 'Debit', -20504, '2018-01-28 05:09:46', 'test', 0, 0, '2018-01-28'),
(30, NULL, 21, '00000000019', 6, 'Admission', 'MALE WARD bed charges', 1, 5001, 'Debit', -25505, '2018-01-28 05:10:40', 'test', 0, 0, '2018-01-28'),
(31, NULL, 21, '00000000019', 6, 'Admission', 'MALE WARD bed charges', 1, 5001, 'Debit', -30506, '2018-01-28 05:12:05', 'test', 0, 0, '2018-01-28'),
(32, NULL, 21, '00000000019', 6, 'Admission', 'AMENITY WARD bed charges', 1, 2500, 'Debit', -33006, '2018-01-28 06:03:58', 'test', 0, 0, '2018-01-28'),
(33, NULL, 21, '00000000019', 7, 'Admission', 'MALE WARD bed charges', 1, 5001, 'Debit', -38007, '2018-01-28 06:04:43', 'test', 0, 0, '2018-01-28'),
(34, NULL, 21, '00000000019', 8, 'Admission', 'MALE WARD bed charges', 1, 5001, 'Debit', -43008, '2018-01-28 06:16:59', 'test', 0, 0, '2018-01-28'),
(35, NULL, 25, '00000000023', 9, 'Admission', 'MALE WARD bed charges', 1, 5001, 'Debit', -7901, '2018-01-28 06:42:17', 'test', 0, 0, '2018-01-28'),
(36, NULL, 22, '00000000020', 10, 'Admission', 'MALE WARD bed charges', 1, 5001, 'Debit', -5501, '2018-01-28 06:45:48', 'test', 0, 0, '2018-01-28'),
(37, NULL, 4, '00000000002', 5, 'Record', 'Regular Appointment', 1, 2000, 'Debit', -2400, '2018-01-28 06:47:22', 'test', 0, 0, '2018-01-28'),
(38, NULL, 25, '00000000023', 3, 'Procedure', 'CIRCUMCISION', 1, 2000, 'Reversed', -5901, '2018-01-28 06:50:04', 'test', 0, 0, '2018-01-28'),
(39, NULL, 24, '00000000022', 2, 'Procedure', 'CIRCUMCISION', 1, 2000, 'Reversed', -53700, '2018-01-28 17:50:31', 'test', 1, 0, '2018-01-28'),
(40, NULL, 24, '00000000022', 2, 'Procedure', 'CIRCUMCISION', 1, 2000, 'Reversed', -51700, '2018-01-28 17:50:35', 'test', 1, 0, '2018-01-28'),
(41, NULL, 24, '00000000022', 2, 'Procedure', 'CIRCUMCISION', 1, 2000, 'Reversed', -49700, '2018-01-28 06:50:35', 'test', 0, 0, '2018-01-28'),
(42, NULL, 4, '00000000002', 3, 'Lab', 'MPS', 1, 400, 'Debit', -2800, '2018-01-28 06:55:05', 'test', 0, 0, '2018-01-28'),
(43, NULL, 4, '00000000002', 4, 'Lab', 'MPS', 1, 400, 'Debit', -3200, '2018-01-28 06:55:21', 'test', 0, 0, '2018-01-28'),
(44, NULL, 4, '00000000002', 5, 'Lab', 'MPS', 1, 400, 'Debit', -3600, '2018-01-28 06:55:43', 'test', 0, 0, '2018-01-28'),
(45, NULL, 4, '00000000002', 6, 'Lab', 'MPS', 1, 400, 'Debit', -4000, '2018-01-28 06:56:00', 'test', 0, 0, '2018-01-28'),
(46, NULL, 4, '00000000002', 7, 'Lab', 'MPS', 1, 400, 'Debit', -4400, '2018-01-28 06:56:21', 'test', 0, 0, '2018-01-28'),
(47, NULL, 4, '00000000002', 8, 'Lab', 'MPS', 1, 400, 'Debit', -4800, '2018-01-28 06:56:40', 'test', 0, 0, '2018-01-28'),
(48, '0', 4, '00000000002', 0, 'Lab', 'a', 1, 5000, 'Credit', 200, '2018-01-28 18:00:04', 'test', 0, 0, '2018-01-28'),
(49, NULL, 4, '00000000002', 10, 'Lab', 'MPS', 1, 400, 'Debit', -200, '2018-01-28 19:31:51', 'test', 1, 0, '2018-01-28'),
(50, NULL, 4, '00000000002', 10, 'Lab', 'MPS', 1, 400, 'Reversed', 200, '2018-01-28 08:31:51', 'test', 0, 0, '2018-01-28'),
(51, NULL, 4, '00000000002', 11, 'Lab', 'MPS', 1, 400, 'Debit', -200, '2018-01-28 08:32:33', 'test', 0, 0, '2018-01-28'),
(52, '0', 4, '00000000002', 0, 'Lab', 'MPS', 1, 400, 'Credit', 200, '2018-01-28 19:32:47', 'test', 0, 0, '2018-01-28'),
(53, NULL, 28, '00000000024', 1, 'Record', 'Account Creation', 1, 500, 'Debit', -500, '2018-01-28 08:57:57', 'test', 0, 0, '2018-01-28'),
(54, '0', 28, '00000000024', 0, 'Admission', 'hh', 1, 200, 'Credit', -300, '2018-01-28 20:00:43', 'test', 0, 0, '2018-01-28'),
(55, NULL, 4, '00000000002', 12, 'Lab', 'MPS', 1, 400, 'Debit', -200, '2018-01-28 21:28:02', 'test', 1, 0, '2018-01-28'),
(56, NULL, 4, '00000000002', 12, 'Lab', 'MPS', 1, 400, 'Reversed', 200, '2018-01-28 10:28:02', 'test', 0, 0, '2018-01-28'),
(57, NULL, 4, '00000000002', 13, 'Lab', 'URINALYSIS', 1, 500, 'Debit', -300, '2018-01-28 21:28:51', 'test', 1, 0, '2018-01-28'),
(58, '0', 4, '00000000002', 0, 'Lab', 'URINALYSIS', 1, 500, 'Credit', 200, '2018-01-28 21:28:35', 'test', 0, 0, '2018-01-28'),
(59, NULL, 4, '00000000002', 13, 'Lab', 'URINALYSIS', 1, 500, 'Reversed', 700, '2018-01-28 10:28:51', 'test', 0, 0, '2018-01-28'),
(60, NULL, 4, '00000000002', 4, 'Procedure', 'CIRCUMCISION', 1, 2000, 'Debit', -1300, '2018-01-28 21:41:46', 'test', 1, 0, '2018-01-28'),
(61, NULL, 4, '00000000002', 11, 'Admission', 'MALE WARD bed charges', 1, 5001, 'Debit', -6301, '2018-01-28 10:41:35', 'test', 0, 0, '2018-01-28'),
(62, NULL, 4, '00000000002', 4, 'Procedure', 'CIRCUMCISION', 1, 2000, 'Reversed', -4301, '2018-01-28 10:41:46', 'test', 0, 0, '2018-01-28'),
(63, NULL, 45, '00000000025', 1, 'Record', 'Account Creation', 1, 500, 'Debit', -500, '2021-08-29 10:54:01', 'test', 0, 0, '2021-08-29'),
(64, NULL, 46, '00000000026', 1, 'Record', 'Account Creation', 1, 500, 'Debit', -500, '2021-08-29 10:57:50', 'test', 0, 0, '2021-08-29'),
(65, NULL, 47, '00000000027', 1, 'Record', 'Account Creation', 1, 500, 'Debit', -500, '2021-08-29 11:02:14', 'test', 0, 0, '2021-08-29'),
(66, NULL, 48, '00000000028', 1, 'Record', 'Account Creation', 1, 500, 'Debit', -500, '2021-08-29 11:05:24', 'test', 0, 0, '2021-08-29'),
(67, NULL, 49, '00000000029', 1, 'Record', 'Account Creation', 1, 500, 'Debit', -500, '2021-08-29 11:08:18', 'test', 0, 0, '2021-08-29'),
(68, NULL, 50, '00000000030', 1, 'Record', 'Account Creation', 1, 500, 'Debit', -500, '2021-08-29 11:09:43', 'test', 0, 0, '2021-08-29'),
(69, NULL, 51, '00000000031', 1, 'Record', 'Account Creation', 1, 500, 'Debit', -500, '2021-08-30 07:42:15', 'test', 0, 0, '2021-08-30'),
(70, NULL, 45, '00000000025', 2, 'Pharmacy', 'Menthodex', 1, 600, 'Debit', -1100, '2021-08-30 08:20:49', 'test', 0, 0, '2021-08-30'),
(71, NULL, 19, '00000000017', 3, 'Pharmacy', 'Panadol', 1, 120, 'Debit', -620, '2021-08-30 08:44:58', 'test', 0, 0, '2021-08-30'),
(72, NULL, 4, '00000000002', 14, 'Lab', 'MPS', 1, 400, 'Debit', -4701, '2021-08-30 08:54:05', 'test', 0, 0, '2021-08-30'),
(73, '0', 4, '00000000002', 0, 'others', 'clear', 1, 2000, 'Credit', -2701, '2021-08-30 07:55:25', 'test', 0, 0, '2021-08-30'),
(74, '0', 4, '00000000002', 0, 'others', 'clear bill', 1, 2701, 'Credit', 0, '2021-08-30 07:56:14', 'test', 0, 0, '2021-08-30'),
(75, '0', 4, '00000000002', 0, 'others', 'saving', 1, 2000, 'Credit', 2000, '2021-08-30 07:57:02', 'test', 0, 0, '2021-08-30'),
(76, '0', 23, '00000000021', 0, 'Pharmacy', 'Panadol X 1', 1, 120, 'Credit', -380, '2021-08-31 07:05:23', 'test', 0, 0, '2021-08-31'),
(77, NULL, 19, '00000000017', 4, 'Pharmacy', 'Panadol', 2, 240, 'Debit', -860, '2021-08-31 08:08:19', 'test', 0, 0, '2021-08-31'),
(78, NULL, 19, '00000000017', 5, 'Pharmacy', 'Menthodex', 2, 1200, 'Debit', -2060, '2021-08-31 08:12:46', 'test', 0, 0, '2021-08-31'),
(79, NULL, 4, '00000000002', 6, 'Pharmacy', 'Panadol', 1, 120, 'Debit', 1880, '2021-08-31 08:15:06', 'test', 0, 0, '2021-08-31'),
(80, '0', 4, '00000000002', 0, 'Lab', 'MPS', 1, 400, 'Credit', 2280, '2021-08-31 07:26:47', 'test', 0, 0, '2021-08-31'),
(81, NULL, 23, '00000000021', 7, 'Pharmacy', 'Panadol', 1, 120, 'Debit', -500, '2021-08-31 08:34:56', 'test', 0, 0, '2021-08-31'),
(82, NULL, 4, '00000000002', 8, 'Pharmacy', 'Panadol', 1, 120, 'Debit', 2160, '2021-08-31 09:11:46', 'test', 1, 0, '2021-08-31'),
(83, NULL, 4, '00000000002', 15, 'Lab', 'MPS', 1, 400, 'Debit', 1760, '2021-08-31 09:44:56', 'test', 0, 0, '2021-08-31'),
(84, NULL, 4, '00000000002', 10, 'Pharmacy', 'Menthodex', 1, 600, 'Debit', 1160, '2021-08-31 09:52:18', 'test', 0, 0, '2021-08-31'),
(85, '0', 4, '00000000002', 0, 'Pharmacy', 'Menthodex X 1', 1, 600, 'Credit', 1760, '2021-08-31 08:53:15', 'test', 0, 0, '2021-08-31'),
(86, '0', 4, '00000000002', 0, 'Pharmacy', 'Menthodex X 1', 1, 600, 'Credit', 2360, '2021-08-31 08:54:07', 'test', 0, 0, '2021-08-31'),
(87, NULL, 4, '00000000002', 8, 'Pharmacy', 'Panadol', 1, 120, 'Reversed', 2480, '2021-08-31 10:11:46', 'test', 0, 0, '2021-08-31'),
(88, '0', 4, '00000000002', 0, 'Pharmacy', 'Panadol', 1, 120, 'Credit', 2600, '2021-08-31 09:12:42', 'test', 0, 0, '2021-08-31'),
(89, '0', 4, '00000000002', 0, 'Record', 'Brief/Follow Up', 1, 1000, 'Credit', 3600, '2021-08-31 09:13:00', 'test', 0, 0, '2021-08-31'),
(90, '0', 4, '00000000002', 0, 'Lab', 'MPS', 1, 400, 'Credit', 4000, '2021-08-31 09:17:44', 'test', 0, 0, '2021-08-31'),
(91, '0', 4, '00000000002', 0, 'Record', 'Regular Appointment', 1, 2000, 'Credit', 6000, '2021-08-31 09:19:59', 'test', 0, 0, '2021-08-31'),
(92, '0', 4, '00000000002', 0, 'Admission', '66', 1, 222, 'Credit', 6222, '2021-08-31 09:20:53', 'test', 0, 0, '2021-08-31'),
(93, NULL, 24, '00000000022', 0, 'Pharmacy', 'jj', 1, 200, 'Debit', -49900, '2021-08-31 10:21:46', 'test', 0, 0, '2021-08-31');

-- --------------------------------------------------------

--
-- Table structure for table `user_table`
--

CREATE TABLE `user_table` (
  `id` int(11) NOT NULL,
  `username` varchar(15) NOT NULL,
  `password` varchar(255) NOT NULL,
  `first_name` varchar(25) NOT NULL,
  `last_name` varchar(25) NOT NULL,
  `phone_number` varchar(15) NOT NULL,
  `role` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_table`
--

INSERT INTO `user_table` (`id`, `username`, `password`, `first_name`, `last_name`, `phone_number`, `role`) VALUES
(2, 'y.sadiq', '700c8b805a3e2a265b01c77614cd8b21', 'Sadiq', 'Yusuf', '07063490700', ''),
(4, 'test', '700c8b805a3e2a265b01c77614cd8b21', 'TEST', 'USER', '08012345678', ''),
(5, 'ab', '700c8b805a3e2a265b01c77614cd8b21', 'Abu', 'yusuf', '07063490525', '');

-- --------------------------------------------------------

--
-- Table structure for table `visit_table`
--

CREATE TABLE `visit_table` (
  `id` int(11) NOT NULL,
  `emr_id` varchar(11) NOT NULL,
  `visit_date` date NOT NULL,
  `visit_type` varchar(69) NOT NULL,
  `price` double NOT NULL,
  `seperate_settlement` int(1) NOT NULL DEFAULT 0,
  `visit_note` varchar(10000) DEFAULT NULL,
  `seen_by` varchar(45) DEFAULT NULL,
  `booked_by` varchar(45) NOT NULL,
  `is_signed` int(1) NOT NULL DEFAULT 0,
  `clinic` varchar(75) NOT NULL,
  `room` varchar(45) NOT NULL,
  `diagnosis` varchar(5000) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `visit_table`
--

INSERT INTO `visit_table` (`id`, `emr_id`, `visit_date`, `visit_type`, `price`, `seperate_settlement`, `visit_note`, `seen_by`, `booked_by`, `is_signed`, `clinic`, `room`, `diagnosis`) VALUES
(2, '00000000002', '2018-01-30', 'Brief/Follow Up', 1000, 1, NULL, NULL, 'test', 0, 'GOPD', 'CONSULTATION ROOM 1', NULL),
(3, '00000000002', '2018-01-28', 'Regular Appointment', 2000, 1, '<p>rr</p>', 'test', 'test', 1, 'GOPD', 'CONSULTATION ROOM 1', '<p>rr</p>'),
(5, '00000000002', '2018-01-29', 'Regular Appointment', 2000, 1, NULL, NULL, 'test', 0, 'GOPD', 'CONSULTATION ROOM 1', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `vitals_type_table`
--

CREATE TABLE `vitals_type_table` (
  `id` int(11) NOT NULL,
  `type` varchar(75) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vitals_type_table`
--

INSERT INTO `vitals_type_table` (`id`, `type`) VALUES
(1, 'Blood Pressure (mmHg)'),
(2, 'Glucose (mg/dl)'),
(3, 'Pulse (beats/min)'),
(4, 'Respiration (beats/min)'),
(5, 'Tempreture (oC)');

-- --------------------------------------------------------

--
-- Table structure for table `vital_sign_table`
--

CREATE TABLE `vital_sign_table` (
  `id` int(11) NOT NULL,
  `emr_id` varchar(11) NOT NULL,
  `type` varchar(45) NOT NULL,
  `reading` varchar(45) NOT NULL,
  `checked_by` varchar(45) NOT NULL,
  `reading_date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vital_sign_table`
--

INSERT INTO `vital_sign_table` (`id`, `emr_id`, `type`, `reading`, `checked_by`, `reading_date`) VALUES
(1, '00000000022', 'Blood Pressure (mmHg)', '234/70', 'test', '2018-01-28 20:05:54'),
(2, '00000000022', 'Blood Pressure (mmHg)', '65/88', 'test', '2018-01-28 20:06:05'),
(3, '00000000022', 'Glucose (mg/dl)', '3', 'test', '2018-01-28 20:06:15');

-- --------------------------------------------------------

--
-- Table structure for table `ward_location_table`
--

CREATE TABLE `ward_location_table` (
  `id` int(11) NOT NULL,
  `ward_id` int(11) NOT NULL,
  `location` varchar(120) NOT NULL,
  `type` varchar(70) DEFAULT NULL,
  `updated_by` varchar(85) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ward_location_table`
--

INSERT INTO `ward_location_table` (`id`, `ward_id`, `location`, `type`, `updated_by`) VALUES
(1, 2, 'BED 2', '', 'y.sadiq'),
(2, 2, 'BED 1', '', 'y.sadiq'),
(3, 3, 'BED 1', NULL, 'test'),
(4, 3, 'BED 2', NULL, 'test'),
(5, 1, 'BED 1', NULL, 'test');

-- --------------------------------------------------------

--
-- Table structure for table `ward_table`
--

CREATE TABLE `ward_table` (
  `id` int(11) NOT NULL,
  `clinic_id` int(11) NOT NULL,
  `ward` varchar(45) NOT NULL,
  `price` double NOT NULL,
  `updated_by` varchar(75) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ward_table`
--

INSERT INTO `ward_table` (`id`, `clinic_id`, `ward`, `price`, `updated_by`) VALUES
(1, 1, 'MALE WARD', 5001, 'test'),
(2, 1, 'AMENITY WARD', 2500, 'y.sadiq'),
(3, 1, 'FEMALE WARD', 5000, 'test');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `drug_batch_table`
--
ALTER TABLE `drug_batch_table`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `drug_measure_table`
--
ALTER TABLE `drug_measure_table`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `drug_table`
--
ALTER TABLE `drug_table`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `insurance_table`
--
ALTER TABLE `insurance_table`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `inventory_setting`
--
ALTER TABLE `inventory_setting`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lab_category_table`
--
ALTER TABLE `lab_category_table`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lab_investigation_table`
--
ALTER TABLE `lab_investigation_table`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `last_vitals_table`
--
ALTER TABLE `last_vitals_table`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `patient_billing_account`
--
ALTER TABLE `patient_billing_account`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `patient_table`
--
ALTER TABLE `patient_table`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `prescription_slip`
--
ALTER TABLE `prescription_slip`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `prescription_table`
--
ALTER TABLE `prescription_table`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `printer_table`
--
ALTER TABLE `printer_table`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `privilege_table`
--
ALTER TABLE `privilege_table`
  ADD UNIQUE KEY `user_id` (`user_id`);

--
-- Indexes for table `procedure_attachment_table`
--
ALTER TABLE `procedure_attachment_table`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `procedure_list_table`
--
ALTER TABLE `procedure_list_table`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `procedure_medication_table`
--
ALTER TABLE `procedure_medication_table`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `procedure_note_table`
--
ALTER TABLE `procedure_note_table`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `procedure_speciality_table`
--
ALTER TABLE `procedure_speciality_table`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `radiology_table`
--
ALTER TABLE `radiology_table`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `service_table`
--
ALTER TABLE `service_table`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `transaction_table`
--
ALTER TABLE `transaction_table`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_table`
--
ALTER TABLE `user_table`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `visit_table`
--
ALTER TABLE `visit_table`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vitals_type_table`
--
ALTER TABLE `vitals_type_table`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vital_sign_table`
--
ALTER TABLE `vital_sign_table`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ward_location_table`
--
ALTER TABLE `ward_location_table`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ward_table`
--
ALTER TABLE `ward_table`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `drug_batch_table`
--
ALTER TABLE `drug_batch_table`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `drug_measure_table`
--
ALTER TABLE `drug_measure_table`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `drug_table`
--
ALTER TABLE `drug_table`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `insurance_table`
--
ALTER TABLE `insurance_table`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `inventory_setting`
--
ALTER TABLE `inventory_setting`
  MODIFY `id` int(1) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `lab_category_table`
--
ALTER TABLE `lab_category_table`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `lab_investigation_table`
--
ALTER TABLE `lab_investigation_table`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `last_vitals_table`
--
ALTER TABLE `last_vitals_table`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;

--
-- AUTO_INCREMENT for table `patient_billing_account`
--
ALTER TABLE `patient_billing_account`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=52;

--
-- AUTO_INCREMENT for table `patient_table`
--
ALTER TABLE `patient_table`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

--
-- AUTO_INCREMENT for table `prescription_slip`
--
ALTER TABLE `prescription_slip`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=113;

--
-- AUTO_INCREMENT for table `prescription_table`
--
ALTER TABLE `prescription_table`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT for table `printer_table`
--
ALTER TABLE `printer_table`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `procedure_attachment_table`
--
ALTER TABLE `procedure_attachment_table`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `procedure_list_table`
--
ALTER TABLE `procedure_list_table`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `procedure_medication_table`
--
ALTER TABLE `procedure_medication_table`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `procedure_note_table`
--
ALTER TABLE `procedure_note_table`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `procedure_speciality_table`
--
ALTER TABLE `procedure_speciality_table`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `radiology_table`
--
ALTER TABLE `radiology_table`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `service_table`
--
ALTER TABLE `service_table`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `transaction_table`
--
ALTER TABLE `transaction_table`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=94;

--
-- AUTO_INCREMENT for table `user_table`
--
ALTER TABLE `user_table`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `visit_table`
--
ALTER TABLE `visit_table`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `vitals_type_table`
--
ALTER TABLE `vitals_type_table`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `vital_sign_table`
--
ALTER TABLE `vital_sign_table`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `ward_location_table`
--
ALTER TABLE `ward_location_table`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `ward_table`
--
ALTER TABLE `ward_table`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
