<?php
session_start();
if (!isset($_SESSION['signature']) || $_SESSION['signature'] == '') {
    header('location: ../index.php');
}
include_once '../assets/includes/header.php';
include_once 'sidebar.php';
include_once '../model/db_conn.php';


?>
<link rel="stylesheet" type="text/css" href="../assets/libs/quill/dist/quill.snow.css">
<link rel="stylesheet" type="text/css" href="../assets/libs/select2/dist/css/select2.min.css">
<!--<link href="../assets/libs/datatables.net-bs4/css/dataTables.bootstrap4.css" rel="stylesheet">-->

<link href="dataTable/dataTable.css" rel="stylesheet">
<link href="dataTable/buttons.datatables.min.css" rel="stylesheet">
<link href="dataTable/jquery.dataTables.min.css" rel="stylesheet">

<link href="../dist/css/mymodal.css" rel="stylesheet">
<script src="../assets/libs/select2/dist/js/select2.full.min.js"></script>
<script src="../assets/libs/select2/dist/js/select2.min.js"></script>

<!-- ============================================================== -->
<!-- Page wrapper  -->
<!-- ============================================================== -->
<div class="page-wrapper">
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Sales Cards  -->
        <!-- ============================================================== -->

        <div class="row" id="contentHolder">

        </div>
        <!-- END MODAL -->
        <?php include '../assets/includes/footer.php'; ?>

        <script src="../assets/extra-libs/DataTables/datatables.min.js"></script>
        <script type="text/javascript" src="../dist/js/sweetalert.js"></script>
        <script src="../assets/libs/toastr/build/toastr.min.js"></script>
        <script type="text/javascript">

            jQuery('document').ready(function () {
                $body = jQuery("body");

                jQuery(document).on({
                    ajaxStart: function () {
                        $body.addClass("loading");
                    },
                    ajaxStop: function () {
                        $body.removeClass("loading");
                    }
                });
              loadReportBackup();
            });

            function loadReportBackup() {
                jQuery('#contentHolder').load('report_backup.php');
            }
            function loadOtherSetting(){
                                                                               jQuery('#contentHolder').load('other_setting.php');
                                    }
                                    
                                    function loadAccountManagement(){
                                        jQuery('#contentHolder').load('account_management.php'); 
                                    }
        </script>
        </body>

        </html>