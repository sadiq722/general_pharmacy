<?php
session_start();
include_once '../model/db_conn.php';
include_once '../controller/utils.php';
?> 
<div class="container-fluid">
    <div class="row" id="accountContent">

<div class=" col-xs-12 col-md-4">
    
                        <div class="card">
                            <form id="userAccountForm" method="post" action="javascript:createAccount()">
                                <div class="card-body">
                                    <h4 class="card-title">User Info</h4>
                                    <div class="form-group row">
                                        <label >First Name</label>
                                       
                                        <input required="" type="text" class="form-control" id="full_name" name="first_name"
                                                placeholder="First Name">
                                    </div>
                                    <div class="form-group row">
                                        <label >Last Name</label>
                                        <input required="" type="text" class="form-control" id="lname" name="last_name"
                                                placeholder="Last Name">
                                       
                                    </div>
                                    <div class="form-group row">
                                        <label >Phone Number</label>
                                        <input required="" type="number" class="form-control" id="lname" name="phone_number"
                                                placeholder="Phone Number">
                                                                            </div>
                                     <div class="form-group row">
                                        <label >Username</label>
                                        <input type="text" required="" class="form-control" id="lname" name="username"
                                                placeholder="Username">
                                                                           </div>
                                    <div class="form-group row">
                                        <label >Password</label>
                                        
                                        <div class="input-group">
                                            <input type="password" required="" id="password" class="form-control" placeholder="Password"
                                                   name="password">
                                            <div class="input-group-append">
                                                <span class="input-group-text"><i id="showPassword" class="fa  fa-eye"></i></span>
                                            </div>
                                        </div>
                                  
                                        
                                    </div>
                                    
                                    
                                </div>
                                <div class="border-top">
                                    <div class="card-body right">
                                        <input type="hidden" name="create_account">
                                        <button type="submit" class="btn btn-success">Create Account</button>
                                        <button type="button" class="btn btn-secondary" onclick="resetAccounForm()">Reset</button>
                                    </div>
                                </div>
                            </form>
                        </div>



    </div>
<div class=" col-xs-12 col-md-8">
    <div class="card" >
        <div class="card-body" >

           

            <div id="radiologyRequestTable">
                <div class="table-responsive">

                    <table id="zero_config" class="table table-striped table-bordered">
                        <thead>

                            <tr>
                                <th>First Name</th>
                                <th>Last Name</th>
                                <th>Phone Number</th>
                                <th>Username</th>
                                <th>Action</th>   
                            </tr>

                        </thead>
                        <tbody>
                            <?php foreach (_fetchUser() as $item) { ?>
                                <tr>
                                   
                                    <td><?php echo $item['first_name']; ?></td>
                                    <td><?php echo $item['last_name']; ?></td>
                                    <td><?php echo $item['phone_number']; ?></td>
                                    <td><?php echo $item['username']; ?></td>
                                    <td>
                                    <div class="btn-group col-sm-12 col-md-2 ">
                                <button type="button" class="btn btn-sm btn-success dropdown-toggle"
                                        data-toggle="dropdown" aria-haspopup="true"
                                        aria-expanded="false">Action</button>
                                <div class="dropdown-menu">

                                    <a href="#" class="dropdown-item" onclick="jQuery('#accountContent').load('view_account.php?id=<?php echo $item['id']; ?>')" >View Account</a>
                                  <?php if($_SESSION['role']['super_admin']==1){ ?>
                                    <a href="#" class="dropdown-item" onclick="deleteUser(<?php echo $item['id']; ?>)" >Delete</a>
                                  <?php } ?>
                                </div></div>
                                    </td>
                                </tr>
                            <?php }
                            ?>
                        </tbody></table>

                </div>
            </div>
        </div>
    </div>
</div>




<script src="../assets/extra-libs/DataTables/datatables.min.js"></script>
<script>
                      $('#zero_config').DataTable();

                                      $("#showPassword").click(function () {


                var input = $('#password');
                if (input.attr("type") == "password") {
                    input.attr("type", "text");
                    $('#showPassword').removeClass("fa-eye");
                    $('#showPassword').addClass("fa-eye-slash");
                } else {
                    input.attr("type", "password");
                    $('#showPassword').addClass("fa-eye");
                    $('#showPassword').removeClass("fa-eye-slash");
                }
            });
                                        function resetAccounForm(){
                                         jQuery('#userAccountForm').trigger('reset'); ;   
                                        }
                                      
                                        function createAccount(){

                                        var _data = $('#userAccountForm').serialize();
                                        var jqxhr = $.post("../controller/controller.php",
                                                _data);
                                        jqxhr.done(function (result) {
                                        if (result != 0){
                                            loadAccountManagement();
                                        toastr.success(result, '');
                                           jQuery('#userAccountForm').trigger('reset');                                    
                                       } else{
                                        toastr.error('Phone number or Username already exist!', 'Failed');
                                        }
                                        }
                                        );
                                        jqxhr.fail(function () {
                                        toastr.success('Network Error', 'Try again!');
                                        });
                                        jqxhr.always(function (data) {


                                        });
                                        }

function deleteUser(id){
          Swal.fire({
                                title: 'Are you sure to delete user?',
                                        showCancelButton: true,
                                        confirmButtonText: 'Yes',
                                }).then((result) => {
                                /* Read more about isConfirmed, isDenied below */
                                if (result.isConfirmed) {
                                $.get('../controller/get_controller.php',{delete_user:1, id:id}, function (result) {
                                    toastr.success(result,'');
                                   loadAccountManagement();
                              });
                                }
                                
                                }
                                )
}

</script>