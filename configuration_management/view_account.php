<?php
include_once '../model/db_conn.php';
$id = $_REQUEST['id'];
$user = _getUser($id);
?>
<div class="container-fluid">
    <div class="row" id="accountContent">

        <div class="col-xl-12">

            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">User Info</h4>
                    <table class="table">
                        <tr>
                            <th>First Name</th>
                            <th><?php echo $user['first_name']; ?></th>
                        </tr>
                        <tr>
                            <th>Last Name</th>
                            <th><?php echo $user['last_name']; ?></th>
                        </tr>
                        <tr>
                            <th>Phone Number</th>
                            <th><?php echo $user['phone_number']; ?></th>
                        </tr>
                        <tr>
                            <th>Username</th>
                            <th><?php echo $user['username']; ?></th>
                        </tr>
                        <tr>
                            <th>Role</th>
                            <th><?php echo $user['role']; ?></th>
                        </tr>
                    </table>
                </div>
                <div class="border-top">
                    <div class="card-body">
                        <button class="btn btn-secondary" onclick="loadAccountManagement()">Back</button>
                        <button class="btn btn-success" onclick="jQuery('#changePasswordModal').css('display', 'block')">Change Password</button>
                        <button class="btn btn-success" onclick="jQuery('#resetPasswordModal').css('display', 'block')">Reset Password</button>
                        <button class="btn btn-success" onclick="loadPrivilegeModal(<?php echo $user['id']; ?>)">Change Privilege</button>
                    </div>
                </div>
                </form>
            </div>



            <div class="modal_ " id="changePasswordModal">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title">Change Password</h4>
                            <button type="button" class="close" onclick="jQuery('#changePasswordModal').css('display', 'none')"
                                    aria-hidden="true">&times;</button>
                        </div>
                        <form id="changePasswordForm" action="javascript:changePassword()" method="post">
                            <div class="modal-body">

                                <div class="col-sm-12">

                                    <input type="hidden" name="id" value="<?php echo $user['id']; ?>">

                                    <div class="form-group row">
                                        <label >Current Password</label>

                                        <div class="input-group">
                                            <input type="password" required="" id="password" class="form-control" placeholder="Current Password"
                                                   name="password">
                                            <div class="input-group-append">
                                                <span class="input-group-text"><i  class=" showPassword fa  fa-eye"></i></span>
                                            </div>
                                        </div>


                                    </div>

                                    <div class="form-group row">
                                        <label >New Password</label>

                                        <div class="input-group">
                                            <input type="password" required=""  class="npasswordform-control" placeholder="New Password"
                                                   name="new_password">
                                            <div class="input-group-append">
                                                <span class="input-group-text"><i  class="showNewPassword fa  fa-eye"></i></span>
                                            </div>
                                        </div>


                                    </div>



                                    <div class="modal-footer">
                                        <input type="hidden"  name="change_password">
                                        <button type="submit" class="btn btn-success waves-effect"
                                                >Change</button>
                                        <button type="button" class="btn btn-secondary waves-effect"
                                                onclick="jQuery('#changePasswordModal').css('display', 'none')">Close</button>

                                    </div>

                                </div>
                            </div>
                        </form>

                    </div>
                </div>
            </div>


            <div class="modal_ " id="resetPasswordModal">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title">Reset Password</h4>
                            <button type="button" class="close" onclick="jQuery('#resetPasswordModal').css('display', 'none')"
                                    aria-hidden="true">&times;</button>
                        </div>
                        <form id="resetPasswordForm" action="javascript:resetPassword()" method="post">
                            <div class="modal-body">

                                <div class="col-sm-12">

                                    <input type="hidden" name="id" value="<?php echo $user['id']; ?>">
                                    <div class="form-group row">
                                        <label >New Password</label>

                                        <div class="input-group">
                                            <input type="password" required=""  class="npassword form-control" placeholder="New Password"
                                                   name="new_password">
                                            <div class="input-group-append">
                                                <span class="input-group-text"><i  class="showNewPassword fa  fa-eye"></i></span>
                                            </div>
                                        </div>


                                    </div>

                                    <div class="modal-footer">
                                        <input type="hidden"  name="reset_password">
                                        <button type="submit" class="btn btn-success waves-effect"
                                                >Reset</button>
                                        <button type="button" class="btn btn-secondary waves-effect"
                                                onclick="jQuery('#resetPasswordModal').css('display', 'none')">Close</button>

                                    </div>

                                </div>
                            </div>
                        </form>

                    </div>
                </div>
            </div>

            <div class="modal_ " id="privilegeModal">

            </div>
            <script>
                function loadPrivilegeModal(id) {
                    jQuery('#privilegeModal').load('privilege.php', {id: id});
                    $("#privilegeModal").css("display", "block");
                }

                function changePassword() {

                    var _data = $('#changePasswordForm').serialize();
                    var jqxhr = $.post("../controller/controller.php",
                            _data);
                    jqxhr.done(function (result) {
                        if (result != 0) {
                            jQuery('#changePasswordForm').trigger('reset');
                            ;
                            jQuery('#changePasswordModal').css('display', 'none');
                            toastr.success(result, '');

                        } else {
                            toastr.error('Incorect Password!', 'Failed');
                        }
                    }
                    );
                    jqxhr.fail(function () {
                        toastr.success('Network Error', 'Try again!');
                    });
                    jqxhr.always(function (data) {


                    });
                }

                function resetPassword() {

                    var _data = $('#resetPasswordForm').serialize();
                    var jqxhr = $.post("../controller/controller.php",
                            _data);
                    jqxhr.done(function (result) {
                        if (result != 0) {
                            jQuery('#resetPasswordForm').trigger('reset');
                            ;
                            jQuery('#resetPasswordModal').css('display', 'none');
                            toastr.success(result, '');

                        } else {
                            toastr.error('Incorect Password!', 'Failed');
                        }
                    }
                    );
                    jqxhr.fail(function () {
                        toastr.success('Network Error', 'Try again!');
                    });
                    jqxhr.always(function (data) {


                    });
                }

                $(".showPassword").click(function () {


                    var input = $('#password');
                    if (input.attr("type") == "password") {
                        input.attr("type", "text");
                        $('.showPassword').removeClass("fa-eye");
                        $('.showPassword').addClass("fa-eye-slash");
                    } else {
                        input.attr("type", "password");
                        $('.showPassword').addClass("fa-eye");
                        $('.showPassword').removeClass("fa-eye-slash");
                    }
                });

                $(".showNewPassword").click(function () {


                    var input = $('.npassword');
                    if (input.attr("type") == "password") {
                        input.attr("type", "text");
                        $('.showNewPassword').removeClass("fa-eye");
                        $('.showNewPassword').addClass("fa-eye-slash");
                    } else {
                        input.attr("type", "password");
                        $('.showNewPassword').addClass("fa-eye");
                        $('.showNewPassword').removeClass("fa-eye-slash");
                    }
                });
            </script>