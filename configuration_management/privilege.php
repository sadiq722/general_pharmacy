<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title">User Privilege</h4>
            <button type="button" class="close" onclick="jQuery('#privilegeModal').css('display', 'none')"
                    aria-hidden="true">&times;</button>

        </div>
        <?php
        include_once '../model/db_conn.php';
        $id = $_REQUEST['id'];
        $user = _getUserPrivilege($id);
        ?>
        <form id="changePrivilegeForm" action="javascript:changePrivilege()" method="post">
            <div class="modal-body">

                <div class="col-sm-12">
                    <div class="form-group">
                        <div class="">
                            <div class="custom-control custom-checkbox mr-sm-2">
                               <?php if($user['record']!=0){ ?>
                                <input type="checkbox" checked="" class="custom-control-input" name="record" value="record" id="customControlAutosizing1">
                               <?php } else{?>
                                <input type="checkbox" class="custom-control-input" name="record" value="record" id="customControlAutosizing1">
                               <?php } ?>
                                <label class="custom-control-label" for="customControlAutosizing1">Record <small>(Create patient account, search and book appointment)</small></label>
                            </div>
                            <div class="custom-control custom-checkbox mr-sm-2">
                                <?php if($user['nurse']!=0){ ?>
                                <input type="checkbox" checked="" class="custom-control-input" name="nurse" value="nurse"
                                       id="customControlAutosizing2">
                                <?php } else{?>
                                <input type="checkbox" class="custom-control-input" name="nurse" value="nurse"
                                       id="customControlAutosizing2">
                                <?php } ?>
                                <label class="custom-control-label" for="customControlAutosizing2">Nurse <small>(Record vitals,assign and change bed location,document in-patient)</small></label>
                            </div>
                            <div class="custom-control custom-checkbox mr-sm-2">
                               <?php if($user['doctor']!=0){ ?>
                                <input type="checkbox" checked="" class="custom-control-input" name="doctor" value="doctor"
                                       id="customControlAutosizing3">
                               <?php } else{ ?>
                                <input type="checkbox" class="custom-control-input" name="doctor" value="doctor"
                                       id="customControlAutosizing3">
                               <?php }?>
                                <label class="custom-control-label" for="customControlAutosizing3">Doctor<small>(Search, document and admit and discharge patient. Make request for prescription and other investigations)</small></label>
                            </div>

                            <div class="custom-control custom-checkbox mr-sm-2">
                                <?php if($user['pharmacy']!=0){ ?>
                                <input type="checkbox" checked="" class="custom-control-input" name="pharmacy" value="pharmacy"
                                       id="customControlAutosizing4">
                                <?php } else{ ?>
                                <input type="checkbox" class="custom-control-input" name="pharmacy" value="pharmacy"
                                       id="customControlAutosizing4">
                                <?php }?>
                                <label class="custom-control-label" for="customControlAutosizing4">Pharmacy<small>(Create,search and fill prescription request)</small></label>
                            </div>
                            <div class="custom-control custom-checkbox mr-sm-2">
                                <?php if($user['pharmacy_admin']!=0){ ?>
                                <input type="checkbox" checked="" class="custom-control-input" name="pharmacy_admin" value="pharmacy_admin"
                                       id="customControlAutosizing5">
                                <?php } else { ?>
                                 <input type="checkbox" class="custom-control-input" name="pharmacy_admin" value="pharmacy_admin"
                                       id="customControlAutosizing5">
                         <?php   } ?>
                                <label class="custom-control-label" for="customControlAutosizing5">Pharmacy+<small>(All features of pharmacy, plus Stock Management)</small></label>
                            </div>

                            <div class="custom-control custom-checkbox mr-sm-2">
                                <?php if($user['lab']!=0){ ?>
                                <input type="checkbox" checked="" class="custom-control-input" name="lab" value="lab"
                                       id="customControlAutosizing6">
                                <?php } else {?>
                                <input type="checkbox" class="custom-control-input" name="lab" value="lab"
                                       id="customControlAutosizing6">
                                <?php }?>
                                <label class="custom-control-label" for="customControlAutosizing6">Lab<small>(Create,search and fill lab investigation request)</small></label>
                            </div>
                            <div class="custom-control custom-checkbox mr-sm-2">
                                 <?php if($user['lab_admin']!=0){ ?>
                                <input type="checkbox" checked="" class="custom-control-input" name="lab_admin" value="lab_admin"
                                       id="customControlAutosizing7">
                                 <?php } else {?>
                                <input type="checkbox"  class="custom-control-input" name="lab_admin" value="lab_admin"
                                       id="customControlAutosizing7">
                                <?php }?>
                                <label class="custom-control-label" for="customControlAutosizing7">Lab+<small>(All features of lab, plus Lab Management)</small></label>
                            </div>

                            <div class="custom-control custom-checkbox mr-sm-2">
                                <?php if($user['radiology']!=0){ ?>
                                <input type="checkbox" checked="" class="custom-control-input" name="radiology" value="radiology"
                                       id="customControlAutosizing8">
                                <?php } else {?>
                                <input type="checkbox" class="custom-control-input" name="radiology" value="radiology"
                                       id="customControlAutosizing8">
                            <?php } ?>
                                <label class="custom-control-label" for="customControlAutosizing8">Radiology<small>(Create,search and fill imaging investigation request)</small></label>
                            </div>
                            <div class="custom-control custom-checkbox mr-sm-2">
                                 <?php if($user['radiology_admin']!=0){ ?>
                                <input type="checkbox" checked="" class="custom-control-input" name="radiology_admin" value="radiology_admin"
                                       id="customControlAutosizing9">
                                 <?php } else{ ?>
                                <input type="checkbox"  class="custom-control-input" name="radiology_admin" value="radiology_admin"
                                       id="customControlAutosizing9">
                                 <?php }?>
                                <label class="custom-control-label" for="customControlAutosizing9">Radiology+<small>(All features of radiology, plus Imaging Management)</small></label>
                            </div>
                            
                            <div class="custom-control custom-checkbox mr-sm-2">
                                <?php if($user['bill']!=0){ ?>
                                <input type="checkbox" checked="" class="custom-control-input" name="bill" value="bill"
                                       id="customControlAutosizing10">
                                <?php } else {?>
                                <input type="checkbox" class="custom-control-input" name="bill" value="bill"
                                       id="customControlAutosizing10">
                            <?php } ?>
                                <label class="custom-control-label" for="customControlAutosizing10">Billing<small>(search, debit and credit patient account, print receipt)</small></label>
                            </div>
                            <div class="custom-control custom-checkbox mr-sm-2">
                                 <?php if($user['bill_admin']!=0){ ?>
                                <input type="checkbox" checked="" class="custom-control-input" name="bill_admin" value="bill_admin"
                                       id="customControlAutosizing11">
                                 <?php } //else{ ?>
<!--                                <input type="checkbox"  class="custom-control-input" name="bill_admin" value="bill_admin"
                                       id="customControlAutosizing11">-->
                                 <?php// }?>
<!--                                <label class="custom-control-label" for="customControlAutosizing11">Billing+<small>(All features of radiology, plus Imaging Management)</small></label>-->
                            </div>
                            
                            <div class="custom-control custom-checkbox mr-sm-2">
                                <?php if($user['admin']!=0){ ?>
                                <input type="checkbox" checked="" class="custom-control-input" name="admin" value="admin"
                                       id="customControlAutosizing12">
                                <?php } else {?>
                                 <input type="checkbox" class="custom-control-input" name="admin" value="admin"
                                       id="customControlAutosizing12">
                          <?php  } ?>
                                <label class="custom-control-label" for="customControlAutosizing12">User Account<small>(Create user account and change privilege)</small></label>
                            </div>

                            <div class="custom-control custom-checkbox mr-sm-2">
                                <?php if($user['super_admin']!=0){ ?>
                                <input type="checkbox" checked="" class="custom-control-input" name="super_admin" value="super_admin"
                                       id="customControlAutosizing13">
                                <?php } else {?>
                                <input type="checkbox"  class="custom-control-input" name="super_admin" value="super_admin"
                                       id="customControlAutosizing13">
                            <?php } ?>
                                <label class="custom-control-label" for="customControlAutosizing13">User Account+<small>(All features of user account, plus delete user account).</small></label>
                            </div>
                        </div>
                    </div>
                    <input type="hidden" name="id" value="<?php echo $_REQUEST['id']; ?>">

                    <div class="modal-footer">
                        <input type="hidden"  name="change_privilege">
                        <button type="submit" class="btn btn-success waves-effect"
                                >Save</button>
                        <button type="button" class="btn btn-secondary waves-effect"
                                onclick="jQuery('#privilegeModal').css('display', 'none')">Close</button>

                    </div>

                </div>
            </div>
        </form>

    </div>
</div>

<script>
    function changePrivilege() {
        var _data = $('#changePrivilegeForm').serialize();
        var jqxhr = $.post("../controller/controller.php",
                _data);
        jqxhr.done(function (result) {
            if (result != 0) {
                jQuery('#privilegeModal').css('display', 'none');
                toastr.success(result, '');
                // jQuery('#accountContent').load('view_account.php?id=<?php //echo $item['id'];  ?>')                                
            } else {
                toastr.error('', 'Failed');
            }
        }
        );
        jqxhr.fail(function () {
            toastr.success('Network Error', 'Try again!');
        });
        jqxhr.always(function (data) {


        });
    }
</script>