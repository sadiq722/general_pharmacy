<?php
session_start();
include_once '../model/db_conn.php';
include_once '../controller/utils.php';
?> 
<div class="container-fluid">
    <div class="row">

        <!-- Column -->
        <div class="col-md-6 col-lg-3">
            <div class="card card-hover">
                <div id="record" class="box bg-secondary  bg-success text-center" onclick="loadReportBackup()">
                    <h6 class="text-white">Report</h6>
                </div>
            </div>
        </div>
        <!-- Column -->
        <?php if($_SESSION['role']['admin']==1 && $_SESSION['role']['super_admin']==1){ ?>
<!--        <div class="col-md-6 col-lg-3">
            <div class="card card-hover">
                <div id="procedure" class="box  bg-secondary text-center" onclick="loadProcedure()">
                    <h6 class="text-white">Download Backup</h6>
                </div>
            </div>
        </div>-->
        <!-- Column -->
        <?php } ?>

    </div>

    <div class="card" >
        <div class="card-body" id="reportContent">

           <div class="btn-group col-sm-12 col-md-2 ">
                                        <button type="button" class="btn btn-success dropdown-toggle"
                                            data-toggle="dropdown" aria-haspopup="true"
                                            aria-expanded="false">Report Type</button>
                                        <div class="dropdown-menu">
                                           <?php if($_SESSION['role']['pharmacy']==1 || $_SESSION['role']['pharmacy_admin']){?>
                                                <a class="dropdown-item" href="#" onclick="jQuery('#reportTable').load('pharmacy_report.php')">Pharmacy</a>
                                            <?php } ?>                                          
                                        </div>
                                    </div>
            <div class="col-xs-12">.</div>

            
            
            <div id="reportTable">
                
            </div>
        </div>
    </div>
</div>


<!--    DataTables Plugins  -->
<!--<script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.2/js/dataTables.buttons.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.html5.min.js"></script>-->

<script src="dataTable/js/jquery.dataTables.min.js"></script>
<script src="dataTable/js/dataTables.buttons.min.js"></script>
<script src="dataTable/js/jszip.min.js"></script>
<script src="dataTable/js/pdfmake.min.js"></script>
<script src="dataTable/js/vfs_fonts.js"></script>
<script src="dataTable/js/buttons.html5.min.js"></script>
<script>

                                        $('#zero_config').DataTable(
            {
                            dom: 'Bfrtip',
                                    buttons: [
                                            'copyHtml5',
                                            'excelHtml5',
                                            'csvHtml5',
                                            'pdfHtml5'

                                    ]
                            }                                    
            );
                                        function filterPatientTable(){
                                            if($.trim(jQuery('#from').val())=="" && $.trim(jQuery('#to').val())=="" && $.trim(jQuery('#by').val())=="" & $.trim(jQuery('#emr_id').val())==""){
                                                jQuery('#from').addClass('is-invalid');
                                                jQuery('#to').addClass('is-invalid');
                                                jQuery('#by').addClass('is-invalid');
                                                jQuery('#emr_id').addClass('is-invalid');
                                            } else{
                                                 jQuery('#from').removeClass('is-invalid');
                                                jQuery('#to').removeClass('is-invalid');
                                                jQuery('#by').removeClass('is-invalid');
                                                jQuery('#emr_id').removeClass('is-invalid');
                                            var data= $('#patientFilterForm').serialize();
                                        $("#reportTableFilter").load("ajax_response.php", data);
                                            }
                                        }
                                        function openNewClinicModal(){
                                         $("#newClinicModal").css("display", "block");   
                                        }
                                        function openNewRoomModal(){
                                            $("#newRoomModal").css("display", "block");
                                        }
                                        function loadRecordServiceModal(id, name){
                                        jQuery('#recordServiceModal').load('ajax_response.php', {change_record_service_price:id, name:name});
                                        $("#recordServiceModal").css("display", "block");
                                        }
                                        function loadEditProcedureModal(id,name, price){
                                           jQuery('#editProcedureModal').load('ajax_response.php', {edit_procedure:id, name:name,price:price});
                                        $("#editProcedureModal").css("display", "block");  
                                        }
                                        function loadEditClinicModal(id,name){
                                           jQuery('#editClinicModal').load('ajax_response.php', {edit_clinic:id, name:name});
                                        $("#editClinicModal").css("display", "block");  
                                        }
                                        function loadProcedure(){
                                            jQuery('#SettingsContent').load('ajax_response.php?veiw_procedure=1');
                                            jQuery('#record').removeClass('bg-success');
                                            jQuery('#clinic').removeClass('bg-success');
                                            jQuery('#procedure').addClass('bg-success');
                                            
                                        }
                                        function loadClinic(){
                                           jQuery('#SettingsContent').load('ajax_response.php?veiw_clinic=1');
                                            jQuery('#record').removeClass('bg-success');
                                            jQuery('#clinic').addClass('bg-success');
                                            jQuery('#procedure').removeClass('bg-success'); 
                                        }
                                        function addProcedure(){

                                        var _data = $('#procedureForm').serialize();
                                        var jqxhr = $.post("../controller/controller.php",
                                                _data);
                                        jqxhr.done(function (result) {
                                        if (result != 0){
                                            loadProcedure();
                                        toastr.success(result, '');
                                           jQuery('#procedureForm').trigger('reset');                                    
                                       } else{
                                        toastr.error('Procedure name already exist!', 'Failed');
                                        }
                                        }
                                        );
                                        jqxhr.fail(function () {
                                        toastr.success('Network Error', 'Try again!');
                                        });
                                        jqxhr.always(function (data) {


                                        });
                                        }



</script>