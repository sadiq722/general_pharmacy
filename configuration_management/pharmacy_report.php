<?php
session_start();
include_once '../model/db_conn.php'; ?>
<div class="col-xs-12">
                <div class="row">
                    <div class="col-xs-12 col-lg-3">
                                                <div class="bg-dark p-10 text-white text-center">
                                                   
                                                    <h5 class="m-b-0 m-t-5"><?php echo _getPrescriptionCount(); ?></h5>
                                                    <small class="font-light">All Prescription</small>
                                                </div>
                    </div>
                        
                    <div class="col-xs-12 col-lg-2">
                                                <div class="bg-dark p-10 text-white text-center">
                                                   
                                                    <h5 class="m-b-0 m-t-5"><?php echo _getCurrentPrescriptionCount(); ?></h5>
                                                    <small class="font-light">Today </small>
                                                </div>
                        </div>
                   
                  
                
                </div>
            </div>
                <div class="col-xs-12">.</div>
                <div class="col col-sm-12 ">
                      <form id="prescriptionFilterForm">
                                    <div class="row">
<!--                                        <div class="col  col-sm-12 col-md-2">
                                                <input type="text" class="form-control" id="emr_id" name="drug_name" placeholder>
                                              
                                            </div>-->
                                        
                                        <div class="col col-sm-12 col-md-2">
                          
                            <select class="select2 form-control custom-select"
                                    style="width: 100%; height:36px;" name="drug_name"  id="emr_id"required="">
                                <option value="">Drug</option>
                                <?php foreach (_fetchDrug() as $drug){ ?>
                                
                                    <option value="<?php echo $drug['name'] ?>"><?php echo $drug['name'] ?></option>
                                <?php }?>
                            </select>

                        </div>
                                        
                                        <div class="col  col-sm-12 col-md-6" >
                                            <div class=" row">
                                                <label>Filter By Date</label>
                                                <div class="col  col-sm-12 col-md-5"> 
                                                    
                                                <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">From</span>
                                        </div>
                                        <input type="date" id="from"  name="from" class="form-control ">
                                    </div>
                                                </div>
                                            
                                            
                                        <div class="col  col-sm-12 col-md-5">
                                                <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">To</span>
                                        </div>
                                        <input type="date" name="to" id="to" class="form-control">
                                    </div>
                                            </div>
                                            
                                             </div>   
                                            </div>
                                        
<!--                                        <div class="col  col-sm-12 col-md-2">
                                            <select class="form-control" id="by" name="by" >
                                                <option value="">Requested By</option>
                                                                              <?php
                 //   if ($_SESSION['role']['admin'] == 1 || $_SESSION['role']['super_admin'] == 1) {
                   //     $users = _fetchUsernames();
                     //   foreach ($users as $user) {
                            ?>
                            <option><?php// echo $user['username']; ?></option>
                        <?php //}
                    //} else { ?>
                        <option><?php// echo $_SESSION['signature'] ?></option>
<?php //} ?>
                                            </select>
                                              
                                            </div>-->
                                       
                                        <div class="col  col-sm-12 col-md-2">
                                            <div class="col-md-12">
                                                <input type="hidden" name="filter_prescription" >
                                                <input type="button" class="btn btn-sm btn-success" value="Filter" onclick="filterPrescriptionTable()">
                                                                 </div>
                                           
                                        </div>
                                    </div>
            
                                </form>
                            </div>
                <div class="col-xs-12">.</div>
                <div class="table-responsive" id="reportTableFilter">
                    <table id="zero_config" class="table table-striped table-bordered">
                        <thead>

                    <tr>
                        <th>Date</th>
                        <th>Batch No</th>
                      <th>Drug</th>
                        <th>Served Quantity</th>
                        <th>Price</th>
                     

                        
                    </tr>

                </thead>
                        <tbody>
                            <?php foreach (_fetchAllPrescription() as $item) { ?>
                                <tr>
                                        <td><?php echo $item['prescription_date']; ?></td>
                                <td><?php echo $item['batch_no']; ?></td>
                                    <td><?php echo $item['drug_name']; ?></td>
                               
                                   <td><?php echo $item['served_quantity']; ?></td>
                                   <td><?php echo $item['price']; ?></td>
                                </tr>
                            <?php }
                            ?>
                        </tbody></table>

                </div>
                
                <script>
                                               $('#zero_config').DataTable(
            {
                            dom: 'Bfrtip',
                                    buttons: [
                                            'copyHtml5',
                                            'excelHtml5',
                                            'csvHtml5',
                                            'pdfHtml5'

                                    ]
                            });
                                function filterPrescriptionTable(){
                                    
                                            if($.trim(jQuery('#from').val())=="" && $.trim(jQuery('#to').val())=="" && $.trim(jQuery('#by').val())=="" && $.trim(jQuery('#emr_id').val())==""){
                                                jQuery('#from').addClass('is-invalid');
                                                jQuery('#to').addClass('is-invalid');
                                                jQuery('#by').addClass('is-invalid');
                                                jQuery('#emr_id').addClass('is-invalid');
                                            } else{
                                                 jQuery('#from').removeClass('is-invalid');
                                                jQuery('#to').removeClass('is-invalid');
                                                jQuery('#by').removeClass('is-invalid');
                                                jQuery('#emr_id').removeClass('is-invalid');
                                            var data= $('#prescriptionFilterForm').serialize();
                                        $("#reportTableFilter").load("ajax_response.php", data);
                                            }
                                        }
                    </script>