<?php
session_start();
include_once '../model/db_conn.php';
include_once '../controller/utils.php';
if (isset($_REQUEST['filter_prescription'])) {

    $drug_name = $_REQUEST['drug_name'];
    $from = (isset($_REQUEST['from']) && trim($_REQUEST['from']) != '') ? $_REQUEST['from'] . ' ' . date('h:i:s') : NULL;
    $to = (isset($_REQUEST['to']) && trim($_REQUEST['to']) != '') ? $_REQUEST['to'] . ' ' . date('h:i:s') : NULL;

    $item = array();
    if ($from != null && $to != NULL && $drug_name != NULL) {
        $item = _filterPrescriptionRequestByDateAndEMRID($drug_name, $from, $to);
    } elseif ($from != null && $to != NULL) {
        $item = _filterPrescriptionRequestByDate($from, $to);
    } elseif ($drug_name != null) {
        $item = _getPrescriptions($drug_name);
    }
    ?> 
    <div class="table-responsive">

        <table id="filter_config" class="table table-striped table-bordered">
           <thead>

                    <tr>
                        <th>Date</th>
                        <th>Batch No</th>
                      <th>Drug</th>
                        <th>Served Quantity</th>
                        <th>Price</th>
                     

                        
                    </tr>

                </thead>
            <tbody>
    <?php
    foreach ($item as $prescription) {
        ?>
                    <tr>
                        <td><?php echo $prescription['prescription_date']; ?></td>
                        <td><?php echo $prescription['batch_no']; ?></td>
                        <td><?php echo $prescription['drug_name']; ?></td>
                        <td><?php echo $prescription['served_quantity']; ?></td>
                        <td><?php echo $prescription['price']; ?></td>
                    </tr>
        <?php
    }
    ?>
            </tbody></table>
    </div>
        <script>

            $('#filter_config').DataTable(
                    {"bSort": false,
                        "bLengthChange": false,
                        dom: 'Bfrtip',
                        buttons: [
                            'copyHtml5',
                            'excelHtml5',
                            'csvHtml5',
                            'pdfHtml5'

                        ]
                    }
            );</script>

    <?php
    } elseif (isset($_REQUEST['filter_lab'])) {
        $emr_id = isset($_REQUEST['emr_id']) ? getEMRID($_REQUEST['emr_id']) : NULL;
        $from = (isset($_REQUEST['from']) && trim($_REQUEST['from']) != '') ? $_REQUEST['from'] : NULL;
        $to = (isset($_REQUEST['to']) && trim($_REQUEST['to']) != '') ? $_REQUEST['to'] : NULL;
        $by = (isset($_REQUEST['by'])) ? $_REQUEST['by'] : NULL;
        $items = array();
        if ($from != null && $to != NULL && $emr_id != NULL && $by != NULL) {
            $items = _filterLabByDateAndEMRIDBy($emr_id, $from, $to, $by);
        } elseif ($from != null && $to != NULL && $emr_id != NULL) {
            $items = _filterLabByDateAndEMRID($emr_id, $from, $to);
        } elseif ($from != null && $to != NULL && $by != null && $emr_id == NULL) {
            $items = _filterLabByDateAndBy($by, $from, $to);
        } elseif ($from != null && $to != NULL && $by == null && $emr_id == NULL) {
            $items = _filterLabByDate($from, $to);
        } elseif ($emr_id != null && $by != null && $from == null && $to == NULL) {
            $items = _filterLabByEMRIDAndBy($emr_id, $by);
        } elseif ($emr_id != null && $by == null && $from == null && $to == NULL) {

            $items = _filterLabByEMRID($emr_id);
        } elseif ($by != null && $emr_id == null && $from == null && $to == NULL) {
            echo $by;
            $items = _filterLabBY($by);
        }
        ?>
        <table id="zero_config" class="table table-striped table-bordered">
            <thead>

                <tr>
                    <th>Date</th>
                    <th>EMR ID</th>
                    <th>Lab</th>
                    <th>Requested By</th>


                </tr>

            </thead>
            <tbody>
    <?php foreach ($items as $item) { ?>
                    <tr>
                        <td><?php echo $item['request_date']; ?></td>
                        <td><?php echo $item['emr_id']; ?></td>
                        <td><?php echo $item['lab_test']; ?></td>
                        <td><?php echo $item['requested_by']; ?></td>

                    </tr>
    <?php }
    ?>
            </tbody></table>
        <script>

            $('#zero_config').DataTable(
                    {"bSort": false,
                        "bLengthChange": false,
                        dom: 'Bfrtip',
                        buttons: [
                            'copyHtml5',
                            'excelHtml5',
                            'csvHtml5',
                            'pdfHtml5'

                        ]
                    }
            );</script>
    <?php
    } elseif (isset($_REQUEST['filter_radiology'])) {
        $emr_id = isset($_REQUEST['emr_id']) ? getEMRID($_REQUEST['emr_id']) : NULL;
        $from = (isset($_REQUEST['from']) && trim($_REQUEST['from']) != '') ? $_REQUEST['from'] : NULL;
        $to = (isset($_REQUEST['to']) && trim($_REQUEST['to']) != '') ? $_REQUEST['to'] : NULL;
        $by = (isset($_REQUEST['by'])) ? $_REQUEST['by'] : NULL;
        $items = array();
        if ($from != null && $to != NULL && $emr_id != NULL && $by != NULL) {
            $items = _filterRadiologyByDateAndEMRIDBy($emr_id, $from, $to, $by);
        } elseif ($from != null && $to != NULL && $emr_id != NULL) {
            $items = _filterRadiologyByDateAndEMRID($emr_id, $from, $to);
        } elseif ($from != null && $to != NULL && $by != null && $emr_id == NULL) {
            $items = _filterRadiologyByDateAndBy($by, $from, $to);
        } elseif ($from != null && $to != NULL && $by == null && $emr_id == NULL) {
            $items = _filterRadiologyByDate($from, $to);
        } elseif ($emr_id != null && $by != null && $from == null && $to == NULL) {
            $items = _filterRadiologyByEMRIDAndBy($emr_id, $by);
        } elseif ($emr_id != null && $by == null && $from == null && $to == NULL) {
            $items = _filterRadiologyByEMRID($emr_id);
        } elseif ($by != null && $emr_id == null && $from == null && $to == NULL) {
            echo $by;
            $items = _filterRadiologyBY($by);
        }
        ?>
        <table id="zero_config" class="table table-striped table-bordered">
            <thead>

                <tr>
                    <th>Date</th>
                    <th>EMR ID</th>
                    <th>Name</th>
                    <th>Requested By</th>


                </tr>

            </thead>
            <tbody>
                <?php foreach ($items as $item) { ?>
                    <tr>
                        <td><?php echo $item['request_date']; ?></td>
                        <td><?php echo $item['emr_id']; ?></td>
                        <td><?php echo $item['name']; ?></td>
                        <td><?php echo $item['requested_by']; ?></td>

                    </tr>
    <?php }
    ?>
            </tbody></table>
        <script>

            $('#zero_config').DataTable(
                    {"bSort": false,
                        "bLengthChange": false,
                        dom: 'Bfrtip',
                        buttons: [
                            'copyHtml5',
                            'excelHtml5',
                            'csvHtml5',
                            'pdfHtml5'

                        ]
                    }
            );</script>
    <?php
    } elseif (isset($_REQUEST['filter_procedure'])) {
        $emr_id = isset($_REQUEST['emr_id']) ? getEMRID($_REQUEST['emr_id']) : NULL;
        $from = (isset($_REQUEST['from']) && trim($_REQUEST['from']) != '') ? $_REQUEST['from'] : NULL;
        $to = (isset($_REQUEST['to']) && trim($_REQUEST['to']) != '') ? $_REQUEST['to'] : NULL;
        $by = (isset($_REQUEST['by'])) ? $_REQUEST['by'] : NULL;
        $items = array();
        if ($from != null && $to != NULL && $emr_id != NULL && $by != NULL) {
            $items = _filterProcedureByDateAndEMRIDBy($emr_id, $from, $to, $by);
        } elseif ($from != null && $to != NULL && $emr_id != NULL) {
            $items = _filterProcedureByDateAndEMRID($emr_id, $from, $to);
        } elseif ($from != null && $to != NULL && $by != null && $emr_id == NULL) {
            $items = _filterProcedureByDateAndBy($by, $from, $to);
        } elseif ($from != null && $to != NULL && $by == null && $emr_id == NULL) {
            $items = _filterProcedureByDate($from, $to);
        } elseif ($emr_id != null && $by != null && $from == null && $to == NULL) {
            $items = _filterProcedureByEMRIDAndBy($emr_id, $by);
        } elseif ($emr_id != null && $by == null && $from == null && $to == NULL) {
            $items = _filterProcedureByEMRID($emr_id);
        } elseif ($by != null && $emr_id == null && $from == null && $to == NULL) {
            echo $by;
            $items = _filterProcedureBY($by);
        }
        ?>
        <table id="zero_config" class="table table-striped table-bordered">
            <thead>

                <tr>
                    <th>Date</th>
                    <th>EMR ID</th>
                    <th>Name</th>
                    <th>Requested By</th>


                </tr>

            </thead>
            <tbody>
                <?php foreach ($items as $item) { ?>
                    <tr>
                        <td><?php echo $item['schedule_date']; ?></td>
                        <td><?php echo $item['emr_id']; ?></td>
                        <td><?php echo $item['name']; ?></td>
                        <td><?php echo $item['requested_by']; ?></td>

                    </tr>
    <?php }
    ?>

            </tbody>
            <script>

                $('#zero_config').DataTable(
                        {"bSort": false,
                            "bLengthChange": false,
                            dom: 'Bfrtip',
                            buttons: [
                                'copyHtml5',
                                'excelHtml5',
                                'csvHtml5',
                                'pdfHtml5'

                            ]
                        }
                );</script>
        </table>
<?php } ?>
    <script>

        //    $('#zero_config').DataTable(
        //            {
        //                dom: 'Bfrtip',
        //                buttons: [
        //                    'copyHtml5',
        //                    'excelHtml5',
        //                    'csvHtml5',
        //                    'pdfHtml5'
        //
        //                ]
        //            }
        //    );

        function changeRecordServicePrice() {
            if (jQuery('#recordServicePrice').val() < 1) {
                jQuery('#recordServicePrice').addClass('is-invalid');
            } else {
                jQuery('#recordServicePrice').removeClass('is-invalid');
                var _data = $('#recordServiceForm').serialize();
                var jqxhr = $.post("../controller/controller.php",
                        _data);

                jqxhr.done(function (result) {
                    if (result != 0) {
                        toastr.success(result, '');
                        loadOtherSetting();
                    } else {
                        toastr.error('Try again!', 'Failed');
                    }
                }
                );
                jqxhr.fail(function () {
                    toastr.success('Network Error', 'Try again!');
                });
                jqxhr.always(function (data) {


                });
            }
        }
        function updateProcedureService() {
            if (jQuery('#procedurePrice').val() < 1) {
                jQuery('#procedurePrice').addClass('is-invalid');
            } else {
                jQuery('#procedurePrice').removeClass('is-invalid');
                var _data = $('#updateProcedureForm').serialize();
                var jqxhr = $.post("../controller/controller.php",
                        _data);
                jqxhr.done(function (result) {
                    if (result != 0) {
                        toastr.success(result, '');
                        loadProcedure();
                        jQuery('#editProcedureModal').css('display', 'none')
                    } else {
                        toastr.error('Procedure Name already exist!', 'Failed');
                    }
                }
                );
                jqxhr.fail(function () {
                    toastr.success('Network Error', 'Try again!');
                });
                jqxhr.always(function (data) {


                });
            }
        }
        function addClinic() {
            var _data = $('#clinicForm').serialize();
            var jqxhr = $.post("../controller/controller.php",
                    _data);
            jqxhr.done(function (result) {
                if (result != 0) {
                    toastr.success(result, '');
                    loadClinic();
                    jQuery('#clinicForm').trigger('reset');
                } else {
                    toastr.error('Clinic Name already exist!', 'Failed');
                }
            }
            );
            jqxhr.fail(function () {
                toastr.success('Network Error', 'Try again!');
            });
            jqxhr.always(function (data) {


            });

        }

        function editClinic() {
            var _data = $('#editClinicForm').serialize();
            var jqxhr = $.post("../controller/controller.php",
                    _data);
            jqxhr.done(function (result) {
                if (result != 0) {
                    toastr.success(result, '');
                    loadClinic();
                    jQuery('#clinicForm').trigger('reset');
                    jQuery('#editClinicModal').css('display', 'none');
                } else {
                    toastr.error('Clinic Name already exist!', 'Failed');
                }
            }
            );
            jqxhr.fail(function () {
                toastr.success('Network Error', 'Try again!');
            });
            jqxhr.always(function (data) {


            });
        }
        function addRoom() {
            var _data = $('#roomForm').serialize();
            var jqxhr = $.post("../controller/controller.php",
                    _data);
            jqxhr.done(function (result) {
                if (result != 0) {
                    toastr.success(result, '');
                    jQuery('#roomForm').trigger('reset');
                    loadClinic();
                } else {
                    toastr.error('Clinic Room already exist!', 'Failed');
                }
            }
            );
            jqxhr.fail(function () {
                toastr.success('Network Error', 'Try again!');
            });
            jqxhr.always(function (data) {


            });
        }
        function loadEditRoomModal(id, room, clinic_id) {
            jQuery('#editRoomModal').load('ajax_response.php', {edit_room: id, room: room, clinic_id: clinic_id});
            $("#editRoomModal").css("display", "block");
        }
        function editRoom() {
            var _data = $('#editRoomForm').serialize();
            var jqxhr = $.post("../controller/controller.php",
                    _data);
            jqxhr.done(function (result) {
                if (result != 0) {
                    toastr.success(result, '');
                    jQuery('#editRoomForm').trigger('reset');
                    loadClinic();
                } else {
                    toastr.error('Clinic Room already exist!', 'Failed');
                }
            }
            );
            jqxhr.fail(function () {
                toastr.success('Network Error', 'Try again!');
            });
            jqxhr.always(function (data) {


            });
        }

    </script>