<?php
session_start();
include_once '../model/db_conn.php';
include_once '../controller/utils.php';
?> 


<div id="">
    <a href="#" class="btn btn-sm btn-success " onclick="openItemModal()">Add Drug</a>
    <a href="#" class="btn btn-sm btn-success " onclick="openDrugImportModal()">Import Drug</a>
    <div class="table-responsive">

        <table id="zero_config" class="table table-striped table-bordered">
            <thead>

                <tr>
                    <th>#</th>
                    <th>Name</th>
                     <th>Unit</th>
                    <th>Re-order Level</th>
                    <th>Updated By</th>
                    <th>Action</th>
                   
<!--                    <th>Last Update By</th>-->
<!--                    <th>Action</th>-->


                </tr>

            </thead>
            <tbody>
                <?php
                $settings= _getinventorySettings();
                
                $sn = 0;
                foreach (_fetchDrug() as $drug) {
                   
           $sn++;
                    ?>
                    <tr>
                        <td><?php echo $sn ?></td>
                        <td><?php echo $drug['name']; ?></td>
                                               <td><?php echo $drug['unit_of_measure']; ?></td>
                        <td><?php echo $drug['reorder_level']?></td>
                                               <td><?php echo $drug['updated_by']; ?></td>                          
                       <td>
                            <div class="btn-group col-sm-12 col-md-2 ">
                                <button type="button" class="btn btn-sm btn-success dropdown-toggle"
                                        data-toggle="dropdown" aria-haspopup="true"
                                        aria-expanded="false">Action</button>
                                <div class="dropdown-menu">
                                       <a href="#" class="dropdown-item" onclick="loadUpdateDrugModal(<?php echo $drug['id'] ?>)" >Edit</a>
                                       <?php if(_getDrugBatchCount($drug['id'])==0){ ?>
                                    <a href="#" class="dropdown-item" onclick="deleteDrug(<?php echo $drug['id']; ?>)">Delete</a>
                <?php }?>
                                </div>
                            </div>                        </td>

                    </tr>
                    <?php
                }
                ?>
            </tbody></table>


    </div>
</div>


<div class="modal_  " id="drugImportModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title"> IMPORT DRUG LIST </h4>
                
                <button type="button" class="close" data-dismiss="modal"
                        aria-hidden="true" onclick="loadDrug_Consumable()">&times;</button>
            </div>
            <form id="batchForm" action="" method="post" enctype="multipart/form-data">
                <div class="modal-body">
                <small></small>
                    <div class="col-sm-12">
                       
<div class="form-group col-lg-12">
                            <label>Must be an excel file, using this <a href="../assets/DRUGTEMPLATE.xlsx">template</a> </label>
                            
                        </div>
                        <div class="form-group col-lg-12">
                            <label>Upload Drug Batches</label>
                            <div class="custom-file">
                                <input type="file" onchange="uploadDrug()" id="file" name="attachment"  required>
                                <div id="attachmentStatus" style="display: none" class="text-danger">Attachment can only be image file with png, jpg or jpeg extension</div>
                                
                            </div>
                        </div>
                      
                       
                       
                    </div> </div>

                <div class="modal-footer">
                    <input type="hidden"  name="add_batch">
                   
                    <button type="button" class="btn btn-secondary waves-effect"
                            onclick="loadDrug_Consumable()">Close</button>

                </div>
            </form>    
        </div>
    </div>
</div>

<div class="modal_  " id="itemModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">New Item</h4>
                <button type="button" class="close" data-dismiss="modal"
                        aria-hidden="true" onclick="loadDrugList()">&times;</button>
            </div>
            <form id="drugForm" action="javascript:addDrug()" method="post">
                <div class="modal-body">
                    <div class="col-sm-12">

                        <div class="form-group  col-lg-12">

                            <label>Name</label>
                            <input type="text" placeholder="Item Name" class="form-control " name="name"  required="">
                        </div>

                        <div class="form-group col-lg-12">
                            <label>Unit of Measure <a href="#"  onclick="openMeasureModal()">Add New</a></label>
                            <select class="select2 form-control custom-select"
                                    style="width: 100%; height:36px;" name="unit_of_measure" required="">
                                <option value="">Select</option>
                                <?php foreach (_fetchDrugMeasure() as $measure) { ?>
                                    <option value="<?php echo $measure['name'] ?>"><?php echo $measure['name'] ?></option>

                                <?php }
                                ?>
                            </select>

                        </div>
                        <div class="form-group col-lg-12">
                            <label>Reorder Level </label>
                            <input type="number" id="reorder" class="form-control"  name="reorder_level" placeholder="Reorder Level" required="">
                        </div>
                    </div> </div>

                <div class="modal-footer">
                    <input type="hidden"  name="add_drug">
                    <button type="submit" class="btn btn-success waves-effect"
                            >Add</button>
                    <button type="button" class="btn btn-secondary waves-effect"
                            onclick="loadDrugList()">Close</button>

                </div>
            </form>    
        </div>
    </div>
</div>









<div class="modal_  " id="inventorySettingsModal">

</div>

<div class="modal_  " id="editDrugModal">

</div>

<script src="../assets/extra-libs/DataTables/datatables.min.js"></script>
<script>

                                $('#zero_config').DataTable({
                                    "bSort": false,
                                    "bLengthChange": false
                                });


                                function openItemModal() {
                                    $("#itemModal").css("display", "block");
                                }
                                function openDrugImportModal() {
                                    $("#drugImportModal").css("display", "block");
                                }
                                                                function openBatchModal() {
                                    $("#batchModal").css("display", "block");
                                }

                                function openMeasureModal() {
                                    $("#itemModal").css("display", "none");
                                    $("#measureModal").css("display", "block");
                                }
                                function addMeasure() {
                                    var _data = $('#measureForm').serialize();
                                    var jqxhr = $.post("../controller/controller.php",
                                            _data
                                            );
                                    jqxhr.done(function (result) {
                                        if (result != 0) {
                                            jQuery('#measureForm').trigger("reset");
                                            toastr.success(result, '');
                                        } else {
                                            toastr.error('Name already Exist', 'Failed');
                                        }
                                    }
                                    );
                                    jqxhr.fail(function () {
                                        toastr.success('Network Error', 'Try again!');
                                    });
                                    jqxhr.always(function (data) {
                                    });
                                }

                                
                                
                                function addBatch() {
                                    if (!$.isNumeric(jQuery('#unitPrice').val())) {
                                        jQuery('#unitPrice').addClass('is-invalid');
                                        toastr.error('', 'Invalid Price');
                                    } else {
                                        jQuery('#unitPrice').removeClass('is-invalid');
                                        var _data = $('#batchForm').serialize();
                                        var jqxhr = $.post("../controller/controller.php",
                                                _data
                                                );
                                        jqxhr.done(function (result) {
                                            if (result != 0) {
                                                //jQuery('#drugForm').trigger("reset");
                                                toastr.success(result, '');
                                            } else {
                                                toastr.error('Batch No already Exist', 'Failed');
                                            }
                                        }
                                        );
                                        jqxhr.fail(function () {
                                            toastr.success('Network Error', 'Try again!');
                                        });
                                        jqxhr.always(function (data) {
                                        });
                                    }
                                }


                                function loadIncrementQuantityModal(id) {
                                    jQuery('#incrementQuantityModal').load('ajax_response.php', {increment_quantity: id});
                                    jQuery('#incrementQuantityModal').css('display', 'block');
                                }

                                function loadInventorySettings() {
                                    jQuery('#inventorySettingsModal').load('ajax_response.php', {inventory_settings: 1});
                                    jQuery('#inventorySettingsModal').css('display', 'block');
                                }

                                function addQuantity() {
                                    if (jQuery('#quantityIncrement').val() < 1) {
                                        jQuery('#quantityIncrement').addClass('is-invalid');
                                        toastr.error('', 'Invalid Quantity');
                                    } else {
                                        //jQuery('#unitPrice').removeClass('is-invalid');
                                        var _data = $('#incrementQuantityForm').serialize();
                                        var jqxhr = $.post("../controller/controller.php",
                                                _data
                                                );
                                        jqxhr.done(function (result) {

                                            loadDrug_Consumable();
                                            toastr.success(result, '');

                                        }
                                        );
                                        jqxhr.fail(function () {
                                            toastr.success('Network Error', 'Try again!');
                                        });
                                        jqxhr.always(function (data) {
                                        });
                                    }
                                }

                                function loadUpdateDrugModal(id) {
                                    jQuery('#editDrugModal').load('ajax_response.php', {edit_drug: id});
                                    jQuery('#editDrugModal').css('display', 'block');
                                }

                                function updateInventorySettings() {
                                    var _data = $('#updateInventorySettings').serialize();
                                    var jqxhr = $.post("../controller/controller.php",
                                            _data
                                            );
                                    jqxhr.done(function (result) {
                                        if (result != 0) {
                                            loadDrug_Consumable();
                                            toastr.success(result, '');
                                        } else {
                                            toastr.error('', 'Failed');
                                        }
                                    }
                                    );
                                    jqxhr.fail(function () {
                                        toastr.success('Network Error', 'Try again!');
                                    });
                                    jqxhr.always(function (data) {
                                    });
                                }
                                function updateDrug() {

                                    if (!$.isNumeric(jQuery('#_unitPrice').val())) {
                                        jQuery('#_unitPrice').addClass('is-invalid');
                                        toastr.error('', 'Invalid Price');
                                    } else {
                                        jQuery('#_unitPrice').removeClass('is-invalid');
                                        var _data = $('#updateDrugForm').serialize();
                                        var jqxhr = $.post("../controller/controller.php",
                                                _data
                                                );
                                        jqxhr.done(function (result) {
                                            if (result != 0) {
                                                loadDrug_Consumable();
                                                toastr.success(result, '');
                                            } else {
                                                toastr.error('Item name already exist', 'Failed');
                                            }
                                        }
                                        );
                                        jqxhr.fail(function () {
                                            toastr.success('Network Error', 'Try again!');
                                        });
                                        jqxhr.always(function (data) {
                                        });
                                    }
                                }

 function deleteDrug(id) {
                                         Swal.fire({
                                title: 'Are you sure to delete drug item?',
                                        showCancelButton: true,
                                        confirmButtonText: 'Yes',
                                }).then((result) => {
                                /* Read more about isConfirmed, isDenied below */
                                if (result.isConfirmed) {
                               
                                  $.get('../controller/get_controller.php',{delete_drug:id}, function (result) {
                                            
                                   toastr.success('', 'Drug item deleted!');       
                                   loadDrugList();
                                   
                              });
                               
                                }
                                })
                                }


                                
            function uploadDrug(){
                var name = document.getElementById("file").files[0].name;
               
               
               var ext = name.split('.').pop().toLowerCase();
               if (jQuery.inArray(ext, ['xlsx', 'xls']) == -1)
               {
                   jQuery('#attachmentStatus').css('display',"block");
               }
                   
               else
               {
                var form_data = new FormData();
                form_data.append("drugFile", document.getElementById('file').files[0]); 
                form_data.append("filename", name);  
                //alert(document.getElementById('file').files[0].name);
                $.ajax({
                       url: "../controller/controller.php",
                       method: "POST",
                       data: form_data,
                       contentType: false,
                       cache: false,
                       processData: false,
                       beforeSend: function () {
                           $('#attachmentStatus').html("<label class='text-success'>Processing Drug.....</label>");
   jQuery('#attachmentStatus').css('display',"block");                   
   },
   success: function (data)
                       {
                        $('#attachmentStatus').html(data);
                        jQuery('#attachmentStatus').css('display',"block");  
                           toastr.success(data, 'Successful');
          // jQuery('#visitContent').load('procedure_attachment.php',{procedure_id:<?php// echo $_SESSION['procedure_id']?>})

                       },
                });
               }
                
           }
 
</script>