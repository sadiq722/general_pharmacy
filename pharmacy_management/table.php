<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
 require __DIR__ . '/../assets/escpos/autoload.php';

        use Mike42\Escpos\Printer;
        use Mike42\Escpos\PrintConnectors\WindowsPrintConnector;
        use Mike42\Escpos\EscposImage;

use Mike42\Escpos\PrintConnectors\FilePrintConnector;
use Mike42\Escpos\CapabilityProfile;
try{
// Enter connector and capability profile (to match your printer)
    $connector = new WindowsPrintConnector("GeneralPrinter");
//$connector = new FilePrintConnector("GeneralPrinter");
$profile = CapabilityProfile::load("default");
$verbose = false; // Skip tables which iconv wont convert to (ie, only print characters available with UTF-8 input)

/* Print a series of receipts containing i18n example strings - Code below shouldn't need changing */
$printer = new Mike42\Escpos\Printer($connector, $profile);
$codePages = $profile -> getCodePages();
$first = true; // Print larger table for first code-page.
foreach ($codePages as $table => $page) {
    /* Change printer code page */
    $printer -> selectCharacterTable(255);
    $printer -> selectCharacterTable($table);
    /* Select & print a label for it */
    $label = $page -> getId();
    if (!$page -> isEncodable()) {
        $label= " (not supported)";
    }
    $printer -> setEmphasis(true);
    $printer -> textRaw("Table $table: $label\n");
    $printer -> setEmphasis(false);
    if (!$page -> isEncodable() && !$verbose) {
        continue; // Skip non-recognised
    }
    /* Print a table of available characters (first table is larger than subsequent ones */
    if ($first) {
        $first = false;
        compactCharTable($printer, 1, true);
    } else {
        compactCharTable($printer);
    }
}
$printer -> cut();
$printer -> close();


}   catch (Exception $e) {
            echo "Couldn't print to this printer: " . $e->getMessage() . "\n";
        }
        
        function compactCharTable($printer, $start = 4, $header = false)
{
    /* Output a compact character table for the current encoding */
    $chars = str_repeat(' ', 256);
    for ($i = 0; $i < 255; $i++) {
        $chars[$i] = ($i > 32 && $i != 127) ? chr($i) : ' ';
    }
    if ($header) {
        $printer -> setEmphasis(true);
        $printer -> textRaw("  0123456789ABCDEF0123456789ABCDEF\n");
        $printer -> setEmphasis(false);
    }
    for ($y = $start; $y < 8; $y++) {
        $printer -> setEmphasis(true);
        $printer -> textRaw(strtoupper(dechex($y * 2)) . " ");
        $printer -> setEmphasis(false);
        $printer -> textRaw(substr($chars, $y * 32, 32) . "\n");
    }

}