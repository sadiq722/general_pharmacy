<?php
session_start();
include_once '../model/db_conn.php';
include_once '../controller/utils.php';
?> 
<div class="card-title">
     <a href="#" class="btn btn-sm btn-secondary btn-success " id="drugBatch" onclick="loadBatch()">Drug Batch</a>
     <a href="#" class="btn btn-sm btn-secondary " id="drugList" onclick="loadDrugList()">Drug List</a>
    <a href="#" class="btn btn-sm btn-cyan " onclick="loadInventorySettings()">COPAY RATE</a>
</div>

<div id="drugContent">
    <a href="#" class="btn btn-sm btn-success " onclick="openBatchModal()">Add Batch</a>
    <a href="#" class="btn btn-sm btn-success " onclick="openImportBatchModal()">Import Batch</a>
    <div class="table-responsive">

        <table id="zero_config" class="table table-striped table-bordered">
            <thead>

                <tr>
                    <th>Batch No</th>
                    <th>Name</th>
                     <th>Expiration Date</th>
                    <th>Quantity</th>
                    <th>Unit Price</th>
                    <th>Total Price</th>
                     <th>Updated By</th>
                    <th>Action</th>
                   
<!--                    <th>Last Update By</th>-->
<!--                    <th>Action</th>-->


                </tr>

            </thead>
            <tbody>
                <?php
                $settings= _getinventorySettings();
                
                $sn = 0;
                foreach (_fetchDrugBatch() as $drug) {
                   
           $drug_info=  _getDrugInfo($drug['drug_id']);
                    
                     $today=date('Y-m-d ');
  $exp_date=$drug['expiration_date']; 
  $expiration_date= date_create("$exp_date");
  $date_diff= date_diff(date_create("$today"),$expiration_date)->format("%R%a") + 0;

                    ?>
                    <tr>
                        <td><?php echo $drug['batch_no'] ?></td>
                        <td><?php echo $drug['drug_name']; ?></td>
                         <td><?php echo $xpire_date=($date_diff<=($settings['item_expiration']*30))? '<span class="text-danger"><b>' . $drug['expiration_date'] .' '.$date_diff.'days to expiration !</b></span>' :$drug['expiration_date'];  ?></td>

                        <td><?php echo $qty = ($drug['quantity'] <= $drug_info['reorder_level']) ? '<span class="text-danger"><b>' . $drug['quantity'] . ' Low !</b></span>' : $drug['quantity']; ?></td>
                        <td><?php echo $drug['unit_price']; ?></td>
                        <td><?php echo $drug['unit_price'] * $drug['quantity'] ?></td>
                                               <td><?php echo $drug['updated_by']; ?></td>                          
                       <td>
                            <div class="btn-group col-sm-12 col-md-2 ">
                                <button type="button" class="btn btn-sm btn-success dropdown-toggle"
                                        data-toggle="dropdown" aria-haspopup="true"
                                        aria-expanded="false">Action</button>
                                <div class="dropdown-menu">
                                       <a href="#" class="dropdown-item" onclick="loadUpdateDrugModal(<?php echo $drug['id'] ?>)" >Edit</a>
                                    <a href="#" class="dropdown-item" onclick="deleteBatch(<?php echo $drug['id']; ?>)">Delete</a>

                                </div>
                            </div>                        </td>

                    </tr>
                    <?php
                }
                ?>
            </tbody></table>


    </div>
</div>







<div class="modal_  " id="batchModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">New Batch of Drug</h4>
                <button type="button" class="close" data-dismiss="modal"
                        aria-hidden="true" onclick="loadDrug_Consumable()">&times;</button>
            </div>
            <form id="batchForm" action="javascript:addBatch()" method="post">
                <div class="modal-body">
                    <div class="col-sm-12">
                        <input type="hidden" name="drug_id" value="<?php echo $drug['id']; ?>">
<div class="form-group col-lg-12">
                            <label>Drug Name </label>
                            <select class="select2 form-control custom-select"
                                    style="width: 100%; height:36px;" name="drug_name"  id="drugName" required="">
                                <option value="">Select</option>
                                <?php foreach (_fetchDrug() as $drug){ ?>
                                
                                    <option value="<?php echo $drug['name'].' ('.$drug['unit_of_measure'].')' ?>"><?php echo $drug['name'].' ('.$drug['unit_of_measure'].')' ?></option>
                                <?php }?>
                            </select>

                        </div>
                         <div class="form-group col-lg-12">
                            <label>Expiration Date </label>
                            <input type="date" id="expireDate" class="form-control"  name="expire_date" placeholder="Expiration Date" required="">
                        </div>
                        <div class="form-group  col-lg-12">

                            <label>Batch Number</label>
                            <input type="text" placeholder="Batch Number" class="form-control " name="batch"  required="">
                        </div>

                        
                        <div class="form-group col-lg-12">
                            <label>Quantity </label>
                            <input type="number" class="form-control"  name="quantity" placeholder="Quantity" required="">
                        </div>
                        <div class="form-group col-lg-12">
                            <label>Unit Price </label>
                            <input type="text" id="unitPrice" class="form-control"  name="unit_price" placeholder="Unit Price" required="">
                        </div>
                       
                       
                    </div> </div>

                <div class="modal-footer">
                    <input type="hidden"  name="add_batch">
                    <button type="submit" class="btn btn-success waves-effect"
                            >Add</button>
                    <button type="button" class="btn btn-secondary waves-effect"
                            onclick="loadDrug_Consumable()">Close</button>

                </div>
            </form>    
        </div>
    </div>
</div>


<div class="modal_  " id="batchImportModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title"> Import Batch </h4>
                
                <button type="button" class="close" data-dismiss="modal"
                        aria-hidden="true" onclick="loadDrug_Consumable()">&times;</button>
            </div>
            <form id="batchForm" action="javascript:importBatch()" method="post" enctype="multipart/form-data">
                <div class="modal-body">
                <small></small>
                    <div class="col-sm-12">
                       
<div class="form-group col-lg-12">
                            <label>Must be an excel file, using this <a href="../assets/BATCHTEMPLATE.xlsx">template</a> </label>
                            
                        </div>
                        <div class="form-group col-lg-12">
                            <label>Upload Drug Batches</label>
                            <div class="custom-file">
                                <input type="file" onchange="uploadAttachment()" id="file" name="attachment"  required>
                                <div id="attachmentStatus" style="display: none" class="text-danger">Attachment can only be image file with png, jpg or jpeg extension</div>
                                
                            </div>
                        </div>
                      
                       
                       
                    </div> </div>

                <div class="modal-footer">
                    <input type="hidden"  name="add_batch">
                   
                    <button type="button" class="btn btn-secondary waves-effect"
                            onclick="loadDrug_Consumable()">Close</button>

                </div>
            </form>    
        </div>
    </div>
</div>



<div class="modal_  " id="measureModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">New Measure</h4>
                <button type="button" class="close" data-dismiss="modal"
                        aria-hidden="true" onclick="loadDrug_Consumable()">&times;</button>
            </div>
            <form id="measureForm" action="javascript:addMeasure()" method="post">
                <div class="modal-body">
                    <div class="col-sm-12">

                        <div class="form-group  col-lg-12">

                            <label>Name</label>
                            <input type="text" placeholder="Name" class="form-control " name="name"  required="">
                        </div>

                    </div> </div>

                <div class="modal-footer">
                    <input type="hidden"  name="add_measure">
                    <button type="submit" class="btn btn-success waves-effect"
                            >Add</button>
                    <button type="button" class="btn btn-secondary waves-effect"
                            onclick="loadDrug_Consumable()">Close</button>

                </div>
            </form>    
        </div>
    </div>
</div>


<div class="modal_  " id="incrementQuantityModal">

</div>
<div class="modal_  " id="inventorySettingsModal">

</div>

<div class="modal_  " id="updateDrugModal">

</div>

<script src="../assets/extra-libs/DataTables/datatables.min.js"></script>
<script>
    $(".select2").select2();

                                $('#zero_config').DataTable({
                                    "bSort": false,
                                    "bLengthChange": false
                                });


                                function openItemModal() {
                                    $("#itemModal").css("display", "block");
                                }
                                function openBatchModal() {
                                    $("#batchModal").css("display", "block");
                                }
                                function openImportBatchModal(){
                                    $("#batchImportModal").css("display", "block");
                                }

                                function openMeasureModal() {
                                    $("#itemModal").css("display", "none");
                                    $("#measureModal").css("display", "block");
                                }
                                function addMeasure() {
                                    var _data = $('#measureForm').serialize();
                                    var jqxhr = $.post("../controller/controller.php",
                                            _data
                                            );
                                    jqxhr.done(function (result) {
                                        if (result != 0) {
                                            jQuery('#measureForm').trigger("reset");
                                            toastr.success(result, '');
                                        } else {
                                            toastr.error('Name already Exist', 'Failed');
                                        }
                                    }
                                    );
                                    jqxhr.fail(function () {
                                        toastr.success('Network Error', 'Try again!');
                                    });
                                    jqxhr.always(function (data) {
                                    });
                                }

                                function addDrug() {
                                   
                                       
                                        var _data = $('#drugForm').serialize();
                                        var jqxhr = $.post("../controller/controller.php",
                                                _data
                                                );
                                        jqxhr.done(function (result) {
                                            if (result != 0) {
                                                //jQuery('#drugForm').trigger("reset");
                                                toastr.success(result, '');
                                            } else {
                                                toastr.error('Item name already Exist', 'Failed');
                                            }
                                        }
                                        );
                                        jqxhr.fail(function () {
                                            toastr.success('Network Error', 'Try again!');
                                        });
                                        jqxhr.always(function (data) {
                                        });
                                    
                                }
                                 
                                function addBatch() {
                                    if (!$.isNumeric(jQuery('#unitPrice').val())) {
                                        jQuery('#unitPrice').addClass('is-invalid');
                                        toastr.error('', 'Invalid Price');
                                    } else {
                                        
                                             Swal.fire({
                                title: 'Once you  click Yes, you wil not be able to correct any mistake on the form. Are you sure to save?',
                                        showCancelButton: true,
                                        confirmButtonText: 'Yes',
                                }).then((result) => {
                                /* Read more about isConfirmed, isDenied below */
                                if (result.isConfirmed) {
                               
                                 jQuery('#unitPrice').removeClass('is-invalid');
                                        var _data = $('#batchForm').serialize();
                                        var jqxhr = $.post("../controller/controller.php",
                                                _data
                                                );
                                        jqxhr.done(function (result) {
                                            if (result != 0) {
                                                //jQuery('#drugForm').trigger("reset");
                                                toastr.success(result, '');
                                            } else {
                                                toastr.error('Batch No already Exist', 'Failed');
                                            }
                                        }
                                        );
                                        jqxhr.fail(function () {
                                            toastr.success('Network Error', 'Try again!');
                                        });
                                        jqxhr.always(function (data) {
                                        });
                               
                                }
                                
                                }
                                )
                                       
                                    }
                                }


                                function deleteBatch(id) {
                                         Swal.fire({
                                title: 'Are you sure to delete drug batch?',
                                        showCancelButton: true,
                                        confirmButtonText: 'Yes',
                                }).then((result) => {
                                /* Read more about isConfirmed, isDenied below */
                                if (result.isConfirmed) {
                               
                                  $.get('../controller/get_controller.php',{delete_drug_batch:id}, function (result) {
                                            
                                     toastr.success('', 'Drug Batch deleted!');       
                                   loadBatch();
                                   
                              });
                               
                                }
                                })
                                }
                                function loadInventorySettings() {
                                    jQuery('#inventorySettingsModal').load('ajax_response.php', {inventory_settings: 1});
                                    jQuery('#inventorySettingsModal').css('display', 'block');
                                }

                                function addQuantity() {
                                    if (jQuery('#quantityIncrement').val() < 1) {
                                        jQuery('#quantityIncrement').addClass('is-invalid');
                                        toastr.error('', 'Invalid Quantity');
                                    } else {
                                        //jQuery('#unitPrice').removeClass('is-invalid');
                                        var _data = $('#incrementQuantityForm').serialize();
                                        var jqxhr = $.post("../controller/controller.php",
                                                _data
                                                );
                                        jqxhr.done(function (result) {

                                            loadDrug_Consumable();
                                            toastr.success(result, '');

                                        }
                                        );
                                        jqxhr.fail(function () {
                                            toastr.success('Network Error', 'Try again!');
                                        });
                                        jqxhr.always(function (data) {
                                        });
                                    }
                                }

                                function loadUpdateDrugModal(id) {
                                    jQuery('#updateDrugModal').load('ajax_response.php', {update_drug: id});
                                    jQuery('#updateDrugModal').css('display', 'block');
                                }

                                function updateInventorySettings() {
                                    var _data = $('#updateInventorySettings').serialize();
                                    var jqxhr = $.post("../controller/controller.php",
                                            _data
                                            );
                                    jqxhr.done(function (result) {
                                        if (result != 0) {
                                            loadDrug_Consumable();
                                            toastr.success(result, '');
                                        } else {
                                            toastr.error('', 'Failed');
                                        }
                                    }
                                    );
                                    jqxhr.fail(function () {
                                        toastr.success('Network Error', 'Try again!');
                                    });
                                    jqxhr.always(function (data) {
                                    });
                                }
                                function updateDrug() {

                                    if (!$.isNumeric(jQuery('#_unitPrice').val())) {
                                        jQuery('#_unitPrice').addClass('is-invalid');
                                        toastr.error('', 'Invalid Price');
                                    } else {
                                        jQuery('#_unitPrice').removeClass('is-invalid');
                                        var _data = $('#updateDrugForm').serialize();
                                        var jqxhr = $.post("../controller/controller.php",
                                                _data
                                                );
                                        jqxhr.done(function (result) {
                                            if (result != 0) {
                                                loadDrug_Consumable();
                                                toastr.success(result, '');
                                            } else {
                                                toastr.error('Item name already exist', 'Failed');
                                            }
                                        }
                                        );
                                        jqxhr.fail(function () {
                                            toastr.success('Network Error', 'Try again!');
                                        });
                                        jqxhr.always(function (data) {
                                        });
                                    }
                                }


function loadBatch() {
              jQuery('#pharmacyContent').load('drug_consumable.php');
              
            }
         
            function loadDrugList() {        
                
                jQuery('#drugBatch').removeClass('btn-success');
                jQuery('#drugList').addClass('btn-success');
                jQuery('#drugContent').load('drug_list.php');
            }

            function uploadAttachment(){
                var name = document.getElementById("file").files[0].name;
               
               
               var ext = name.split('.').pop().toLowerCase();
               if (jQuery.inArray(ext, ['xlsx', 'xls']) == -1)
               {
                   jQuery('#attachmentStatus').css('display',"block");
               }
                   
               else
               {
                var form_data = new FormData();
                form_data.append("file", document.getElementById('file').files[0]); 
                form_data.append("filename", name);  
                //alert(document.getElementById('file').files[0].name);
                $.ajax({
                       url: "../controller/controller.php",
                       method: "POST",
                       data: form_data,
                       contentType: false,
                       cache: false,
                       processData: false,
                       beforeSend: function () {
                           $('#attachmentStatus').html("<label class='text-success'>Processing Batch.....</label>");
   jQuery('#attachmentStatus').css('display',"block");                   
   },
   success: function (data)
                       {
                        $('#attachmentStatus').html(data);
                        jQuery('#attachmentStatus').css('display',"block");  
                           toastr.success(data, 'Successful');
          // jQuery('#visitContent').load('procedure_attachment.php',{procedure_id:<?php// echo $_SESSION['procedure_id']?>})

                       },
                });
               }
                
           }
</script>