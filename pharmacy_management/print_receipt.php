
<html lang="en">
    <head>


        <title></title>
    </head>	
    <body>
        <?php
        session_start();

//Check whether the session variable SESS_MEMBER_ID is present or not

        /* Change to the correct path if you copy this example! */
        require __DIR__ . '/../assets/escpos/autoload.php';

        use Mike42\Escpos\Printer;
        use Mike42\Escpos\PrintConnectors\WindowsPrintConnector;
        use Mike42\Escpos\EscposImage;

date_default_timezone_set('Africa/Lagos');
        include_once '../model/db_conn.php';

        if (isset($_POST['print_receipt'])) {
            $id = $_POST['id'];
            $prescription=  _fetchPrescription($id);
             $pt=_getPtInfo($id);
            $copy = $_POST['copy'];
            $share_name = $_POST['share_name'];
            $update_share_name = $_POST['update_share_name'];
            
       


        $emrid =  $pt['phone_number'];
        $name = ucwords($pt['full_name']);
        $today = date('Y-m-d h:i: a');
        
        $desc = '';
        foreach ($prescription as $pres){
            $desc .=$pres['drug_name'].'X'.$pres['served_quantity'];
        }
        $user = _getUser($_SESSION['id']);
        $amount = _getPrescriptionPrice($id)['SUM(price)'];

        /**
         * Install the printer using USB printing support, and the "Generic / Text Only" driver,
         * then share it (you can use a firewall so that it can only be seen locally).
         *
         * Use a WindowsPrintConnector with the share name to print.
         *
         * Troubleshooting: Fire up a command prompt, and ensure that (if your printer is shared as
         * "Receipt Printer), the following commands work:
         *
         *  echo "Hello World" > testfile
         *  copy testfile "\\%COMPUTERNAME%\Receipt Printer"
         *  del testfile
         */
//COMMIT 1

        try {
            // Enter the share name for your USB printer here
            // $connector = null;
            $connector = new WindowsPrintConnector($share_name);
for($i=1; $i<=$copy; $i++){
            /* Print a "Hello world" receipt" */
            $printer = new Printer($connector);
            //  $printer -> text("Hello World!\n");

            $printer->setJustification(Printer::JUSTIFY_CENTER);
//$printer -> graphics($logo);

            $path = dirname(__FILE__) . '/hsmb.png';
            $img_logo = EscposImage::load($path, false);


            $printer->setJustification(Printer::JUSTIFY_CENTER);
            $printer->selectPrintMode(Printer::MODE_DOUBLE_WIDTH);
            $printer->bitImageColumnFormat($img_logo);
          //  $printer->text("\n");

            /* Name of org */
            $printer->selectPrintMode(Printer::MODE_DOUBLE_WIDTH);
            $printer->text("RETAINERSHIP CLINIC\n");
            $printer->text("Payment Invoice\n");
            $printer->selectPrintMode();

            $printer->feed();

            /* Title of receipt */
//            $printer->setEmphasis(true);
//            $printer->text();
//            $printer->setEmphasis(false);

            /* Items */
            $printer->setJustification(Printer::JUSTIFY_LEFT);
            $printer->setEmphasis(true);

            $printer->setEmphasis(false);

            $printer->text("Transaction Number: " . $id . "\n");
            $printer->feed();
            $printer->text("Patient Name: " . $name . "\n");
            $printer->feed();
            $printer->text("Transaction Date: " . $today . "\n");
            $printer->feed();
            
            $printer->text("Amount : NGN" . $amount . "\n");
            $printer->feed();
            $printer->setJustification(Printer::JUSTIFY_CENTER);
            $printer->text("DESCCRIPTION\n");
            $printer->feed();
            $sn=0;
            foreach ($prescription as $pres){
                $sn++;
            $printer->text($sn.' '.$pres['drug_name'].' X '.$pres['served_quantity']."=NGN".$pres['price']."\n");
            $printer->feed();
        }
           // 
            

           
            $printer->feed();


            $printer->selectPrintMode(Printer::MODE_DOUBLE_WIDTH);

            $printer->selectPrintMode();

            /* Footer */
            $printer->feed(2);
            $printer->setJustification(Printer::JUSTIFY_CENTER);
            $printer->text("served By: " . $user['first_name']." ".$user['last_name']);
            $printer->feed(2);

            $printer->text("Thank you for your patronage");
            $printer->feed(2);

            $printer->cut();

            /* Close printer */
            $printer->close();
}
        } catch (Exception $e) {
            echo "Couldn't print to this printer: " . $e->getMessage() . "\n";
        }
        if($update_share_name==1){
            _changePrinterShareName($share_name);
        }
        }
        ?>
    </body>
</html>
