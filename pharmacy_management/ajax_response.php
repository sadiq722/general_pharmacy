<?php
if(session_status()!=PHP_SESSION_ACTIVE){
session_start();
}
include_once '../model/db_conn.php';
include_once '../controller/utils.php';
if (isset($_REQUEST['fill_prescription'])) {
    //echo $_REQUEST['fill_prescription'];
   $psid=$_REQUEST['fill_prescription'];
   $prescriptions=_fetchPrescriptions($_REQUEST['fill_prescription']);
    ?>

<div class="card" id="prescription">
    <div class="card-body">
             <div class="card-title">
<!--                <a href="#" class="btn btn-sm btn-success" onclick="addPrescriptionModal(<?php //echo $_REQUEST['fill_prescription']?>)">Add</a>-->
                <?php if(count($prescriptions)>0){ ?>
                 <a href="#" class="btn btn-sm btn-success" onclick="dispensePrescription(<?php echo $_REQUEST['fill_prescription']?>)">Confirm</a>
                <?php } ?>
            </div>
        <div class="row">
            <div class="col col-xs-12 col-sm-8">
                
            <div id="prescriptionRequestTable">
                <div>Total:<?php echo _getPrescriptionPrice($_REQUEST['fill_prescription'])['SUM(price)']; ?></div>
                <div class="table-responsive">

                   <table id="zero_config" class="table table-striped table-bordered">
                <thead>

                    <tr>
                        <th>#</th>
                       
                        <th>Drug</th>
                        <th>Unit</th>
                        <th>Qty</th>
                        <th>Price</th>
                        
<th>Action</th>

                        
                    </tr>

                </thead>
                <tbody>
                    <?php
                    $sn=0;
                    foreach ($prescriptions as $prescription) {
                        $sn++
                                                    ?>
                            <tr>
                                <td><?php echo $sn; ?></td>
                    
                                <td><?php echo $prescription['drug_name']; ?></td>
                                <td><?php echo $prescription['unit']; ?></td>
                                <td><?php echo $prescription['served_quantity']; ?></td>
                                <td><?php echo $prescription['price']; ?></td>
                               
                                <td>
                                    <div class="btn-group col-sm-12 col-md-2 ">
                                            <button type="button" class="btn btn-sm btn-cyan dropdown-toggle"
                                                    data-toggle="dropdown" aria-haspopup="true"
                                                    aria-expanded="false">Action</button>
                                            <div class="dropdown-menu">
                                               <a href="#" class="dropdown-item" onclick="deletePrescriptionRequest(<?php echo $prescription['id'] ?>,<?php echo $psid ?>)" >Cancel</a>
                                            </div>
                                        </div>
                                </td>

                            </tr>
                        <?php
                        }
                                                ?>
                </tbody></table>


                </div>
            </div>
            
            </div>
            <div class="col col-xs-12 col-sm-4">
                <div class="card-body" style="border: 2px solid green">
            <div class="modal-header">
                <h4 class="modal-title">Add Prescription</h4>
               
            </div>
           <form id="prescriptionFrm" action="javascript:addPrescription(<?php echo $_REQUEST['fill_prescription']?>)" method="post">
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-12 ">

                        <input type="hidden" class="form-control " name="prescription_slip_id" value="<?php echo $_REQUEST['fill_prescription']?>"  >
                       

                        <div class="form-group col-lg-12">
                            <label>Drug Name </label>
                            <select class="select2 form-control custom-select"
                                        style="width: 100%; height:36px;" name="drug_name"  id="drugName" required="">
                                <option value="">Select</option>
                                <?php foreach (_fetchAvailableDrugBatch() as $drug){ ?>
                                
                                    <option value="<?php echo $drug['id'] ?>"><?php echo $drug['drug_name'] ?></option>
                                <?php }?>
                            </select>

                        </div>
                        <div class="form-group col-lg-12">
                            <label>Unit </label>
                            <select class="select2 form-control custom-select"
                                        style="width: 100%; height:36px;" name="unit"  id="unit" required="">
                                <option value="">Select</option>
                                <?php foreach (_fetchDrugMeasure() as $measure){ ?>
                                
                                    <option value="<?php echo $measure['name'] ?>"><?php echo $measure['name']?></option>
                                <?php }?>
                            </select>

                        </div>
 <div class="form-group  col-lg-12">  
                                          <label>Quantity   <div class=" text-success text-center" id="drugPrice" style="font-weight:bold">
                </div> </label>
                                          <input type="number" id="qty" placeholder="Quantity" class="form-control "   name="quantity" required="">
                                </div>

                    </div>

                </div>
            </div>
            <div class="modal-footer">
                <input type="hidden"  name="new_prescription">
                <button type="submit" class="btn btn-success waves-effect"
                        >Add</button>
                
                
            </div>
        </form>

    </div>
            </div>
        </div>
        </div></div>
<script src="../assets/libs/select2/dist/js/select2.full.min.js"></script>
    <script src="../assets/libs/select2/dist/js/select2.min.js"></script>
    <script src="../assets/extra-libs/DataTables/datatables.min.js"></script>
    <script> $(".select2").select2();   </script>


<?php }    elseif (isset ($_REQUEST['filter_prescription_request'])) {
    
    $drug_name=  $_REQUEST['drug_name'];
     $from=  (isset($_REQUEST['from']) && trim($_REQUEST['from'])!='')?$_REQUEST['from'].' '.date('h:i:s'):NULL;
     $to=  (isset($_REQUEST['to']) && trim($_REQUEST['to'])!='')?$_REQUEST['to'].' '.date('h:i:s'):NULL;
     
    $item=array();
    if($from!=null && $to!=NULL && $drug_name!=NULL){ $item= _filterPrescriptionRequestByDateAndEMRID($drug_name, $from, $to);}
elseif ($from!=null && $to!=NULL ) { $item= _filterPrescriptionRequestByDate($from, $to);}
elseif ($drug_name!=null) {$item= _getPrescriptions($drug_name);}    ?> 
<div class="table-responsive">
    
            <table id="filter_config" class="table table-striped table-bordered">
                <thead>

                    <tr>
                        <th>Date</th>
                      <th>Drug</th>
                        <th>Served Quantity</th>
                        <th>Price</th>
                     

                        
                    </tr>

                </thead>
                <tbody>
                    <?php
                    foreach ($item as $prescription) {
                                                    ?>
                            <tr>
                                <td><?php echo $prescription['prescription_date']; ?></td>
           
                                <td><?php echo $prescription['drug_name']; ?></td>
                                <td><?php echo $prescription['served_quantity']; ?></td>
                                <td><?php echo $prescription['price']; ?></td>
                              
                               

                            </tr>
                        <?php
                        }
                                                ?>
                </tbody></table>
    <script>
        
              $('#filter_config').DataTable(
                                                       {
                "bSort": false,
                "bLengthChange": false
            },
            {
                            dom: 'Bfrtip',
                                    buttons: [
                                            'copyHtml5',
                                            'excelHtml5',
                                            'csvHtml5',
                                            'pdfHtml5'

                                    ]
                            });
                            

                                       </script>

    
    
        </div>
<?php } elseif (isset ($_REQUEST['edit_drug'])) { 
    
    $drug =  _getDrugInfo($_REQUEST['edit_drug']);
    ?>
    
   <div class="modal-dialog" >
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Edit Drug </h4>
                <button type="button" class="close"
                        aria-hidden="true" onclick="jQuery('#labResultModal').css('display', 'none')">&times;</button>
            </div>
            
             <form id="editdrugForm" action="javascript:editDrug()" method="post">
                <div class="modal-body">
                    <div class="col-sm-12">

                        <div class="form-group  col-lg-12">
                            <input type="hidden" value="<?php echo $drug['id'];?>" name="id">
                                   <label>Name</label>
                            <input type="text" value="<?php echo $drug['name']; ?>" class="form-control " name="name"  required="">
                        </div>

                        <div class="form-group col-lg-12">
                            <label>Unit of Measure <a href="#"  onclick="openMeasureModal()">Add New</a></label>
                            <select class="select2 form-control custom-select"
                                    style="width: 100%; height:36px;" name="unit_of_measure" required="">
                                <option value="<?php echo $drug['unit_of_measure']; ?>"><?php echo $drug['unit_of_measure']; ?></option>
                                <?php foreach (_fetchDrugMeasure() as $measure) { ?>
                                    <option value="<?php echo $measure['name'] ?>"><?php echo $measure['name'] ?></option>

                                <?php }
                                ?>
                            </select>

                        </div>
                        <div class="form-group col-lg-12">
                            <label>Reorder Level </label>
                            <input type="number" id="reorder" class="form-control" value="<?php echo $drug['reorder_level']; ?>"  name="reorder_level" placeholder="Reorder Level" required="">
                        </div>
                    </div> </div>

                <div class="modal-footer">
                    <input type="hidden"  name="edit_drug">
                    <button type="submit" class="btn btn-success waves-effect"
                            >Update</button>
                    <button type="button" class="btn btn-secondary waves-effect"
                            onclick="loadDrugList()">Close</button>

                </div>
            </form>    
              
        </div>
    </div>
    
    
<?php } elseif (isset ($_REQUEST['inventory_settings'])) {
    $settings=  _getinventorySettings();
    ?>
   
        <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Inventory Settings</h4>
                <button type="button" class="close" data-dismiss="modal"
                        aria-hidden="true" onclick="jQuery('#inventorySettingsModal').css('display','none')">&times;</button>
            </div>
            <form id="updateInventorySettings" action="javascript:updateInventorySettings()" method="post">
                <div class="modal-body">
                    <div class="col-sm-12">

                        <div class="form-group  col-lg-12">
                            

                            <label>Item Expiration</label> <small>How long in month(s) from expiration should you be notified?</small>
                            <input type="number" placeholder="Eg. 1" value="<?php echo $settings['item_expiration']; ?>" class="form-control " name="item_expiration"  required="">
                        </div>

<!--                       
                        <div class="form-group col-lg-12">
                            <label>Low quantity threshold</label> <small>What figure should item quantity be less than before you get notified?</small> 
                            <input type="number" value="<?php //echo $settings['low_qty_threshold']; ?>" class="form-control"  name="low_qty_threshold" placeholder="Eg. 40" required="">
                        </div>-->
                        <div class="form-group col-lg-12">
                            <label>COPAY Rate</label> <small>What percentage of items price should be charged?</small>
                            <input type="number" id="_unitPrice" value="<?php echo $settings['copay_rate']; ?>" class="form-control"  name="copay_rate" placeholder="COPAY RATE" required="">
                        </div>
                    </div> </div>

                <div class="modal-footer">
                    <input type="hidden"  name="update_inventory_setting">
                    <button type="submit" class="btn btn-success waves-effect"
                            >Update</button>
                    <button type="button" class="btn btn-secondary waves-effect"
                            onclick="jQuery('#inventorySettingsModal').css('display','none')">Close</button>

                </div>
            </form>    
        </div>
    </div>
            
<?php }  elseif (isset ($_REQUEST['update_drug'])) {
   
    $drug=  _getBatchDrugInfo($_REQUEST['update_drug']);
    ?>
   
        <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Edit Item  </h4>
                <button type="button" class="close" data-dismiss="modal"
                        aria-hidden="true" onclick="jQuery('#updateDrugModal').css('display','none')">&times;</button>
            </div>
            <form id="updateDrugForm" action="javascript:updateDrug()" method="post">
                <div class="modal-body">
                    <div class="col-sm-12">

                        <div class="form-group  col-lg-12">
                            <input type="hidden" value="<?php echo $_REQUEST['update_drug']; ?>" name="id"  required="">
 </div>
                        
                        
                        <div class="form-group col-lg-12">
                            <label>Drug Name </label>
                            <select class="select2 form-control custom-select"
                                    style="width: 100%; height:36px;" name="drug_name"  id="drugName" required="">
                                <option value="<?php echo $drug['drug_name']; ?>"><?php echo $drug['drug_name']; ?></option>
                                <?php foreach (_fetchDrug() as $dru){ ?>
                                
                                    <option value="<?php echo $dru['name'].' ('.$dru['unit_of_measure'].')' ?>"><?php echo $dru['name'].' ('.$dru['unit_of_measure'].')' ?></option>
                                <?php }?>
                            </select>

                        </div>
                         <div class="form-group col-lg-12">
                            <label>Expiration Date </label>
                            <input type="text" id="expireDate" class="form-control"  value="<?php echo $drug['expiration_date']; ?>"  name="expiration_date" placeholder="<?php echo $drug['expiration_date']; ?>" required="">
                        </div>
                        <div class="form-group  col-lg-12">

                            <label>Batch Number</label>
                            <input type="text" placeholder="Batch Number" class="form-control " value="<?php echo $drug['batch_no']; ?>" name="batch_no"  required="">
                        </div>

                        
                         <div class="form-group col-lg-12">
                            <label>Quantity </label>
                            <input type="number" value="<?php echo $drug['quantity']; ?>" class="form-control"  name="quantity" placeholder="Quantity" required="">
                        </div>
                        <div class="form-group col-lg-12">
                            <label>Unit Price </label>
                            <input type="text" id="_unitPrice" value="<?php echo $drug['unit_price']; ?>" class="form-control"  name="unit_price" placeholder="Unit Price" required="">
                        </div>
                   
                    </div> </div>

                <div class="modal-footer">
                    <input type="hidden"  name="update_drug">
                    <button type="submit" class="btn btn-success waves-effect"
                            >Update</button>
                    <button type="button" class="btn btn-secondary waves-effect"
                            onclick="jQuery('#updateDrugModal').css('display','none')">Close</button>

                </div>
            </form>    
        </div>
    </div>
            
<?php } elseif (isset ($_REQUEST['add_prescription'])){ 
$id=$_REQUEST['add_prescription']; ?>
  <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Add Prescription </h4>
                <button type="button" class="close" onclick=" closeFillModal(<?php echo $id ?>);"
                        aria-hidden="true">&times;</button>
            </div>
         
  
        </div>
    </div>

<?php } elseif (isset ($_REQUEST['printer_choice'])) { ?>
<div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Select Printer</h4>
                  <button type="button" class="close" onclick="loadPrescriptionRequest()"
                        aria-hidden="true">&times;</button>
                </div>
                <form id="printerForm" >
                    <div class="modal-body">
                        <div class="col-sm-12">

                            <input type="hidden" class="form-control " name="id" value="<?php echo $_REQUEST['printer_choice']; ?>"  >

                            <div class="form-group col-lg-12">
                                <label>Select Printer</label>
                                <select class="select2 form-control custom-select" id="printerChoice"
                                        style="width: 100%; height:36px;"  required="">
                                    <option value=""></option>
                                    <option value="A4">A4 Printer</option>
                                    <option value="thermal">Thermal Printer</option>
                                                                    </select>

                            </div>
                           
                        </div> </div>

                    <div class="modal-footer">
                      
                        <button type="button" class="btn btn-success waves-effect" onclick="showPrintSettings(<?php echo $_REQUEST['printer_choice']; ?>)"
                                >Proceed</button>
                        <!-- <button type="button" class="btn btn-secondary waves-effect" onclick="jQuery('#printerModal').css('display', 'none')">Close</button> -->
                    </div>
                </form>    
            </div>
<?php }  elseif (isset ($_REQUEST['print_setting'])) { ?>
<div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Print Receipt</h4>
                    <!-- <button type="button" class="close" onclick="jQuery('#printerModal').css('display', 'none')">&times;</button> -->
                </div>
                <form id="printerForm" action="javascript:printReceipt()" method="post">
                    <div class="modal-body">
                        <div class="col-sm-12">

                            <input type="hidden" class="form-control " name="id" value="<?php echo $_REQUEST['print_setting']; ?>"  >

                            <div class="form-group col-lg-12">
                                <label>No of copies</label>
                                <input type="number" name="copy" class="form-control" required="">
                            </div>

                            <div class="form-group col-lg-12">
                                <label>Select Printer</label>
                                <select class="select2 form-control custom-select"
                                        style="width: 100%; height:36px;"  required="">
                                   
                                    <option>Thermal Printer</option>
                                                                    </select>

                            </div>
                            <div class="form-group col-lg-12" id="">
                                <label>Printer Share Name </label>
                                <input type="text" name="share_name" value="<?php echo  _getPrinterShareName()['share_name'] ?>"  class="form-control" >
                            </div>
                            <div class="custom-control custom-checkbox mr-sm-2">
                               <input type="checkbox" checked="" class="custom-control-input" name="update_share_name" value="1"
                                       id="customControlAutosizing2">
                               
                                <label class="custom-control-label" for="customControlAutosizing2">use printer share name for subsequent printing</label>
                            </div>
                        </div> </div>

                    <div class="modal-footer">
                        <input type="hidden"  name="print_receipt">
                        <button type="submit" class="btn btn-success waves-effect"
                                >Proceed</button>
                        <!-- <button type="button" class="btn btn-secondary waves-effect" onclick="jQuery('#printerModal').css('display', 'none')">Close</button> -->
                    </div>
                </form>    
            </div>
<?php } ?>
<script>
 function showPrintSettings(id){
   
     if($('#printerChoice').val()==='A4'){
         window.open("invoice.php?id="+ id, "_blank");
     } else{
         jQuery('#printSettings').load('ajax_response.php?print_setting=' + id);
     }
 }
    function addPrescription(id){
                                         
        var _data = $('#prescriptionFrm').serialize();
        var jqxhr = $.post("../controller/controller.php",
                _data
                );
        jqxhr.done(function (result) {
           if(result!=0){
        toastr.success('Drug item added', '');
        $('#pharmacyContent').load('ajax_response.php', {fill_prescription:id});
    } else{
        toastr.error('Failed', 'Quantity exceed stock quantity');
    }
    }     
        );
        jqxhr.fail(function () {
            toastr.success('Network Error', 'Try again!');
        });
        jqxhr.always(function (data) {


        }); 
       }
  

function editDrug() {
                                   
                                       
                                        var _data = $('#editdrugForm').serialize();
                                        var jqxhr = $.post("../controller/controller.php",
                                                _data
                                                );
                                        jqxhr.done(function (result) {
                                            if (result != 0) {
                                                //jQuery('#drugForm').trigger("reset");
                                                toastr.success(result, '');
                                            } else {
                                                toastr.error('Item name already Exist', 'Failed');
                                            }
                                        }
                                        );
                                        jqxhr.fail(function () {
                                            toastr.success('Network Error', 'Try again!');
                                        });
                                        jqxhr.always(function (data) {
                                        });
                                    
                                }
</script>