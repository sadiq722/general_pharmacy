<?php
session_start();
include_once '../assets/includes/header.php';
include_once 'sidebar.php';
include_once '../model/db_conn.php';

if (!isset($_SESSION['signature']) || $_SESSION['signature'] == '') {
    header('location: ../index.php');
}
?>
<link rel="stylesheet" type="text/css" href="../assets/libs/quill/dist/quill.snow.css">
<link rel="stylesheet" type="text/css" href="../assets/libs/select2/dist/css/select2.min.css">
<link href="../assets/libs/datatables.net-bs4/css/dataTables.bootstrap4.css" rel="stylesheet">
<link href="../dist/css/mymodal.css" rel="stylesheet">
<script src="../assets/libs/select2/dist/js/select2.full.min.js"></script>
<script src="../assets/libs/select2/dist/js/select2.min.js"></script>





<!-- ============================================================== -->
<!-- Page wrapper  -->
<!-- ============================================================== -->
<div class="page-wrapper">
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Sales Cards  -->
        <!-- ============================================================== -->

        <div class="row" id="contentHolder">

        </div>
        <!-- END MODAL -->
        <?php include '../assets/includes/footer.php'; ?>

        <script src="../assets/extra-libs/DataTables/datatables.min.js"></script>
        <script type="text/javascript" src="../dist/js/sweetalert.js"></script>
        <script type="text/javascript">

            jQuery('document').ready(function () {
                $body = jQuery("body");

                jQuery(document).on({
                    ajaxStart: function () {
                        $body.addClass("loading");
                    },
                    ajaxStop: function () {
                        $body.removeClass("loading");
                    }
                });
                jQuery('#contentHolder').load('prescription_request.php');
            });

            function loadPrescriptionRequest() {
                jQuery('#contentHolder').load('prescription_request.php');
            }
         
            function loadDrug_Consumable() {
                jQuery('#prescriptionRequest').removeClass('bg-success');
                jQuery('#searchPrescription').removeClass('bg-success');
                jQuery('#externalRequest').removeClass('bg-success');
                jQuery('#drugConsumable').addClass('bg-success');
                jQuery('#pharmacyContent').load('drug_consumable.php');
            }
        </script>
        </body>

        </html>