<?php
session_start();
include_once '../model/db_conn.php';
include_once '../controller/utils.php';
?> 
<div class="container-fluid">
    <div class="row">

        <!-- Column -->
        <div class="col-md-6 col-lg-3">
            <div class="card card-hover">
                <div id="prescriptionRequest" class="box bg-secondary  bg-success text-center" onclick="loadPrescriptionRequest()">
                    <h1 class="font-light text-white"><i class="fas fa-notes-medical"></i></h1>
                    <h6 class="text-white">Prescription Request</h6>
                </div>
            </div>
        </div

    

     
        <!-- Column -->
        <?php if ($_SESSION['role']['pharmacy_admin'] == 1) { ?>
            <div class="col-md-6 col-lg-3">
                <div class="card card-hover">
                    <div id="drugConsumable" class="box bg-secondary text-center" onclick="loadDrug_Consumable()">
                        <h1 class="font-light text-white"><i class=" fas fa-pills"></i></h1>
                        <h6 class="text-white">Drugs & Consumable</h6>
                    </div>
                </div>
            </div>
        <?php } ?>

    </div>

    <div class="card" >
        <div class="card-body" id="pharmacyContent">

            <div class="card-title">
                <a href="#" class="btn btn-sm btn-success" onclick="openNewPrescriptionModal()">New Request</a>
            </div>



        </div>
    </div>
</div>


<div class="modal_ " id="newPrescriptionModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">New Prescription Request</h4>
                <button type="button" class="close" onclick="loadPrescriptionRequest()"
                        aria-hidden="true">&times;</button>
            </div>
            <form id="prescriptionForm" action="javascript:_addPrescription()" method="post">
                <div class="modal-body">
                    <div class="col-sm-12">

                        <div class="form-group  col-lg-12">

                            <label>Insurance Number</label>
                            <input type="number" placeholder="Insurance Number" class="form-control " name="phone_no" onkeyup="verifyEmr(this.value)"   required="">
                        </div>


                        <div class="form-group col-lg-12">
                            <label>Full Name</label>

                            <input name="full_name" placeholder="Full Name" id="dose" class=" form-control " type="text" required="">
                        </div>



                    </div> </div>

                <div class="modal-footer">
                    <input type="hidden"  name="add_prescription">
                    <button type="submit" class="btn btn-success waves-effect"
                            >Next</button>
                    <button type="button" class="btn btn-secondary waves-effect"
                            onclick="loadPrescriptionRequest()">Close</button>

                </div>
            </form>    
        </div>
    </div>
</div>

<div class="modal_ " id="fillPrescriptionModal">

    

</div>

 <div class="modal_ " id="printerModal">
        <div class="modal-dialog" id="printSettings">

        </div>
    </div>


<script src="../assets/libs/select2/dist/js/select2.full.min.js"></script>
    <script src="../assets/libs/select2/dist/js/select2.min.js"></script>
    <script src="../assets/extra-libs/DataTables/datatables.min.js"></script>
    <script>
                                    $(".select2").select2();


                                $('#zero_config').DataTable({
                                    "bSort": false,
                                    "bLengthChange": false
                                });

                                function openNewPrescriptionModal() {
                                    $("#newPrescriptionModal").css("display", "block");
                                }


                                function closeModal() {
                                    $("#fillPrescriptionModal").css("display", "none");
                                }
                                function _addPrescription(){

                                var _data = $('#prescriptionForm').serialize();
                                        var jqxhr = $.post("../controller/controller.php",
                                                _data
                                                );
                                        jqxhr.done(function (result) {

                                        if (result != 0){
                                        $('#newPrescriptionModal').css("display", "none");
                                                $('#pharmacyContent').load('ajax_response.php', {fill_prescription:result});
//           
//        } else{
//            toastr.error('Failed to request prescription',result);
//        }
                                        }
                                    }
                                        );
                                                jqxhr.fail(function () {
                                                toastr.info('Network Error', 'Try again!');
                                                });
                                                jqxhr.always(function (data) {


                                                });
                                        
                                        }




function addPrescriptionModal(id){
    $('#fillPrescriptionModal').load("ajax_response.php",{add_prescription: id});
              $("#fillPrescriptionModal").css("display", "block");                       
}
                                
                                    
                                    function fillPrescription() {

                                        var _data = $('#fillPrescriptionForm').serialize();
                                        var jqxhr = $.post("../controller/controller.php",
                                                _data
                                                );
                                        jqxhr.done(function (result) {
                                            if(result!=0){
                                            toastr.success(result, '');
                                            jQuery('#fillPrescriptionForm').trigger("reset");
                                            loadPrescriptionRequest();
                                        }else{
                                            toastr.error('Drug Item mey be out of stock or quantity exceeds stock quantity','Failed to fill prescription');
                                        }
                                        }
                                        );
                                        jqxhr.fail(function () {
                                            toastr.info('Network Error', 'Try again!');
                                        });
                                        jqxhr.always(function (data) {


                                        });
                                    }
                                    
                                     function deletePrescriptionRequest(id,psid) {
                                     Swal.fire({
                                title: 'Are you sure to cancel this request?',
                                        showCancelButton: true,
                                        confirmButtonText: 'Yes',
                                }).then((result) => {
                                /* Read more about isConfirmed, isDenied below */
                                if (result.isConfirmed) {
                                $.get('../controller/get_controller.php',{cancel_prescription_request:id}, function (result) {
                                  
                                   $('#pharmacyContent').load('ajax_response.php', {fill_prescription:psid});
                              });
                                }
                                
                                }
                                )
                                    }
                                    function dispensePrescription(id){
                                         $.get('../controller/get_controller.php',{dispense_prescription:id}, function (result) {
                                            
                                            jQuery('#printerModal').css('display', 'block');
                                    jQuery('#printSettings').load('ajax_response.php?printer_choice=' + id);
                                   
                                   
                              });
                                }
                                        function printReceipt(){
                                    var _data = $('#printerForm').serialize();
                                    var jqxhr = $.post("print_receipt.php",
                                            _data
                                            );
                                    jqxhr.done(function (result) {
                                        alert(result);
                                //    toastr.success('Printing..', result);
                                //   loadPrescriptionRequest();
                                   
                                    }

                                    );
                                    jqxhr.fail(function () {
                                    toastr.error('Network Error', 'Try again!');
                                    });
                                    jqxhr.always(function (data) {


                                    });
                                    }
</script>