<?php session_start();
if (!isset($_SESSION['signature']) || $_SESSION['signature'] == '') {
    header('location: ../index.php');
}
if (!isset($_GET['id']) || $_SESSION['id'] == '') {
    header('location: ../index.php');
}

?>
<html dir="ltr" lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Tell the browser to be responsive to screen width -->
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="robots" content="noindex,nofollow">
        <title>EMR PLUS</title>
        <!-- Favicon icon -->
        <link rel="icon" type="image/png" sizes="16x16" href="../assets/images/favicon.png">
        <!-- Custom CSS -->
        <link href="../assets/libs/flot/css/float-chart.css" rel="stylesheet">
        <!-- Custom CSS -->
        <link href="../dist/css/style.min.css" rel="stylesheet">
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
        <!-- loader -->
        <link rel="stylesheet" href="print.css">
        <link href="../dist/css/loader.css" rel="stylesheet">
        <!-- end of loader -->
        <link href="../assets/libs/toastr/build/toastr.min.css" rel="stylesheet">
        <link href="../assets/dataTable/dataTable.css" rel="stylesheet">
        <link href="../assets/dataTable/buttons.datatables.min.css" rel="stylesheet">
        <link href="../assets/dataTable/jquery.dataTables.min.css" rel="stylesheet">
        <link href="../dist/css/mymodal.css" rel="stylesheet">
         <script src="../assets/libs/jquery/dist/jquery.min.js"></script>
    </head>

    <body>
        <?php
        include_once '../model/db_conn.php';
        $id = $_GET['id'];

        $prescription = _fetchPrescription($id);
        $pt = _getPtInfo($id);

        $emrid = $pt['phone_number'];
        $name = ucwords($pt['full_name']);
        $today = date('Y-m-d h:i: a');

        $desc = '';
        foreach ($prescription as $pres) {
            $desc .=$pres['drug_name'] . 'X' . $pres['served_quantity'];
        }

        $amount = _getPrescriptionPrice($id)['SUM(price)'];
        ?>
        <!-- ============================================================== -->
        <!-- Main wrapper - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <div id="main-wrapper">

            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container">
                <div class="container">
                    <div class="container">
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="card card-body printableArea">
                            <div class="row">
                                <div class="col-md-12">
                                    <center>
                                        <div class="pull-right text-right">
                                            <address>
                                                <center>
                                                    <img src="../assets/images/hsmb_logo.jpg" width="80" height="80">
                                                    <h2><b>General Hospital Katsina</b></h2>
                                                    <h4 class="text-muted m-l-30">Pharmacy Department</h4>

                                            </address>
                                        </div>
                                    </center>
                                </div>
                            </div>
                                <div class="row" >
                               
                                   <div class="col-md-9 pull-left">
                                    
                                        <address>
                                            <h4><?php echo $name ?></h4>
                                        <span class="m-t-30" style="margin-top: -15px">Transaction ID:<?php echo $id; ?></span><br>
                                        <span class="m-t-30"><b>Date :</b> <?php echo $today; ?></span>
                                        </address>
                                   
                                   </div>
                                   <div class="col-md-3 pull-right">
                                   
                                        <div id="outputbox" style="width: 150px; height: 150px; ">
                                   
                                    </div>
                            
                                   </div>
                               
                                </div>
                                <div class="col-md-12">
                                    <div class="table-responsive m-t-40" style="clear: both;">
                                        <table class="table table-hover">
                                            <thead>
                                                <tr>
                                                    <th class="text-center">#</th>
                                                    <th>Description</th>
                                                    <th class="text-right">Quantity</th>
                                                    <th class="text-right">Unit Cost</th>
                                                    <th class="text-right">Cost</th>
                                                                                                    </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                $sn = 0;
                                                $total = 0;
                                                foreach ($prescription as $pres) {
                                                    $sn++;
                                                    $total+=  $pres['price'];
                                                    //  $desc .=$pres['drug_name'].'X'.$pres['served_quantity'];
                                                    ?>
                                                    <tr>
                                                        <td class="text-center"><?php echo $sn; ?></td>
                                                        <td><?php echo $pres['drug_name']; ?></td>
                                                        <td class="text-right"><?php echo $pres['served_quantity']; ?></td>
                                                        <td class="text-right"><?php echo ($pres['price']/$pres['served_quantity']); ?></td>
                                                        <td class="text-right"><?php echo $pres['price']; ?></td>
                                                        
                                                    </tr>
                                                <?php } ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="pull-right m-t-30 text-right">

                                        <hr>
                                        <h3 ><b>Total :</b>&#8358<span id="total"><script> $('#total').html((<?php echo $total ?>).toLocaleString());</script></span> </h3>
                                    </div>
                                    <div class="clearfix"></div>
                                    <hr>
                                    <div class="text-right">
                                        <a href="javascript:window.print()" class="btn btn-danger hidden-print" > Print </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Right sidebar -->
                <!-- ============================================================== -->
                <!-- .right-sidebar -->
                <!-- ============================================================== -->
                <!-- End Right sidebar -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->

            <!-- ============================================================== -->
            <!-- End footer -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    <?php
   // include_once '../assets/includes/footer.php';
    ?>
     
     <script src="../assets/libs/qrcode.min.js"></script>
     <script ype="text/javascript">
         
        jQuery('document').ready( function(){
            var qrcode = new QRCode("outputbox",{
                width:150,
                height:150
            });
    qrcode.makeCode(<?php echo '\''.$desc.'\''; ?>);
        });
        </script>