<!DOCTYPE html>
<html lang="en">
<head>


	<title>General Report</title>
</head>	
	<body>
<?php
session_start();

//Check whether the session variable SESS_MEMBER_ID is present or not
if (!isset($_SESSION['user'])) {
    header("location:../../index.php");
    exit();
	
}
/* Change to the correct path if you copy this example! */
require __DIR__ . '/../../autoload.php';
use Mike42\Escpos\Printer;
use Mike42\Escpos\PrintConnectors\WindowsPrintConnector;
use Mike42\Escpos\EscposImage;
date_default_timezone_set('Africa/Lagos');
//ALTERATION 2for ($x = 1; $x <= 2; $x++) {
  


if(isset($_POST['generate_reciept'])){
                                    
    
        $emrid=$_POST['emr'];
        $name= ucwords($_POST['name']);
        $amount1=$_POST['amount'];
        $today=date('d-m-Y');
        $today1=date('Y-m-d');
        $datetime=date('Y-m-d h:i:a');
		$time = date("h:i a");
        $method= $_POST['pmethod'];
        $desc= $_POST['desc'];
		$user = $_SESSION['user'];
		$amount = number_format($amount1, 2);
		
/**
 * Install the printer using USB printing support, and the "Generic / Text Only" driver,
 * then share it (you can use a firewall so that it can only be seen locally).
 *
 * Use a WindowsPrintConnector with the share name to print.
 *
 * Troubleshooting: Fire up a command prompt, and ensure that (if your printer is shared as
 * "Receipt Printer), the following commands work:
 *
 *  echo "Hello World" > testfile
 *  copy testfile "\\%COMPUTERNAME%\Receipt Printer"
 *  del testfile
 */
//COMMIT 1
for ($x = 1; $x <= 1; $x++) {
try {
    // Enter the share name for your USB printer here
   // $connector = null;
    $connector = new WindowsPrintConnector("XP-76C");

    /* Print a "Hello world" receipt" */
    $printer = new Printer($connector);
  //  $printer -> text("Hello World!\n");

  $printer -> setJustification(Printer::JUSTIFY_CENTER);
//$printer -> graphics($logo);

$path = dirname(__FILE__).'/kdr.png'; 
$img_logo = EscposImage::load( $path, false); 

      
      $printer->setJustification(Printer::JUSTIFY_CENTER);
      $printer->selectPrintMode(Printer::MODE_DOUBLE_WIDTH);
      $printer->bitImageColumnFormat($img_logo);
	  $printer -> text("\n");

      /* Name of org */
$printer -> selectPrintMode(Printer::MODE_DOUBLE_WIDTH);
$printer -> text("KSIRPH");
$printer -> selectPrintMode();

$printer -> feed();

/* Title of receipt */
$printer -> setEmphasis(true);
$printer -> text("Payment Reciept\n\n");
$printer -> setEmphasis(false);

/* Items */
$printer -> setJustification(Printer::JUSTIFY_LEFT);
$printer -> setEmphasis(true);

$printer -> setEmphasis(false);

    $printer -> text("Transaction ID: ".$emrid."\n");
    $printer -> feed();
    $printer -> text("Patient Name: ".$name."\n");
    $printer -> feed();
     $printer -> text("Transaction Date: ".$today."\n");
     $printer -> feed();
	  $printer -> text("Time : ".$time."\n");
     $printer -> feed();

      $printer -> text("Amount : #".$amount."\n");
      $printer -> feed();
	  $printer -> text("DESC: ".$desc."\n");
      $printer -> feed();
      $printer -> text("Payment Method: ".$method."\n");
	  $printer -> feed();
	   $printer -> text("Prepared By: ".$user);
	   $printer -> cut();

$printer -> setEmphasis(true);

$printer -> setEmphasis(false);
$printer -> feed();


$printer -> selectPrintMode(Printer::MODE_DOUBLE_WIDTH);

$printer -> selectPrintMode();

/* Footer */
$printer -> feed(2);
$printer -> setJustification(Printer::JUSTIFY_CENTER);
$printer -> text("Thank you for your patronage\n");
$printer -> feed(2);

$printer -> text("ICT NHS\n");
$printer -> feed(2);

    $printer -> cut(); 
    
    /* Close printer */
    $printer -> close();
	
	
} catch (Exception $e) {
    echo "Couldn't print to this printer: " . $e -> getMessage() . "\n";
}




}

//ALTERATION 1}
include "conn.php";
$name = str_replace("'", '', $name);
 $sql = "INSERT INTO payment_record (patients,unit,amount,pmethod,ddate,user_on_seat,transaction_no,dtime) VALUES('$name', '$desc', '$amount1','$method','$today1','$user','$emrid','$datetime')";

	if (mysqli_query($conn, $sql)) {
        echo "Error: " . $sql . "<br>" . mysqli_error($conn);
  echo ("<script LANGUAGE='JavaScript'>
					window.alert('Printing....');
				    window.location.href='../../print.php';
           </script>");
	  
		
	} else {
    echo "Error: " . $sql . "<br>" . mysqli_error($conn);
	
    } 
}
?>
</body>
</html>
