<?php 
session_start();
if(!isset($_SESSION['signature']) || $_SESSION['signature']==''){
    header('location: ../index.php');
     exit();
}
include_once '../model/db_conn.php';
$_SESSION['role']=_getUserPrivilege($_SESSION['id']);
$role=_getUserPrivilege($_SESSION['id']);
include_once '../assets/includes/header.php';
include_once 'sidebar.php';



?>

        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                
                <!-- ============================================================== -->
                <!-- Sales chart -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="d-md-flex align-items-center">
                                    <div>
                                        <h4 class="card-title">Welcome!</h4>
                                        <h5 class="card-subtitle">Logged in as <?php echo $_SESSION['signature']; ?></h5>
                                    </div>
                                </div>
                                <div class="row">
                                    <!-- column -->
<!--                                    <div class="col-lg-6">
                                        <div class="flot-chart">
                                            <h6>How to work around a new feature</h6>
                                             <div class="col-lg-6 video-box align-self-baseline" data-aos="fade-right" data-aos-delay="100">
            <iframe width="560" height="315" src="" frameborder="0" allowfullscreen></iframe>
                                                 <video  width="470" height="270" controls>
  <source src="movie.mp4" type="video/mp4">
  <source src="movie.ogg" type="video/ogg">
Your browser does not support the video tag.
</video>
          </div>
                                        </div>
                                    </div>-->
                                    
                                     <div class="col-lg-3">
                                        <div class="row">
                                           
                                            
                                            <div class="col-6">
                                                <div class="bg-dark p-10 text-white text-center">
                                                   
                                                    <h5 class="m-b-0 m-t-5"><?php echo _getCurrentPrescriptionCount() ?></h5>
                                                    <small class="font-light">Prescription</small>
                                                </div>
                                            </div>
                                       <!--       <div class="col-6 m-t-15">
                                                <div class="bg-dark p-10 text-white text-center">
                                                   
                                                  <h5 class="m-b-0 m-t-5"><?php //echo _getTodayImagingCount() ?></h5>
                                                    <small class="font-light">Radiology</small>
                                                </div>
                                            </div> -->
                                         <?php //if($_SESSION['role']['admin']==1 || $_SESSION['role']['super_admin']==1){ ?>
                                            <div class="col-6">
                                                <div class="bg-dark p-10 text-white text-center">
                                                   
                                                    <h5 class="m-b-0 m-t-5"><?php
                                                    $price=_getTodayPhPayment()['SUM(price)'];
                                                    echo $returns=$price==""?0:$price;  ?></h5>
                                                    <small class="font-light">Today Payment </small>
                                                </div>
                                            </div>
                                         <?php// } ?>
                                    <!--          <div class="col-6 m-t-15">
                                                <div class="bg-dark p-10 text-white text-center">
                                                   
                                                    <h5 class="m-b-0 m-t-5">8540</h5>
                                                    <small class="font-light">Null</small>
                                                </div>
                                            </div>-->
                                        </div>
                                    </div>
                                    <!-- column -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- Sales chart -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Recent comment and chats -->
                <!-- ============================================================== -->
                <div class="row">
                    <!-- column -->
                    <div class="col-lg-6">
                        
                        <!-- card new -->
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title m-b-0">Recent Memos</h4>
                            </div>
                            <ul class="list-style-none">
                                <li class="d-flex no-block card-body">
                                    <i class="fa fa-check-circle w-30px m-t-5"></i>
                                    <div>
                                        <a href="#" class="m-b-0 font-medium p-0">Memo Feature.</a>
                                        <span class="text-muted">Feature No available on this version</span>
                                    </div>
                                    <div class="ml-auto">
                                        <div class="tetx-right">
                                            <h5 class="text-muted m-b-0">20</h5>
                                            <span class="text-muted font-16">Jan</span>
                                        </div>
                                    </div>
                                </li>
                                                           </ul>
                        </div>
                    </div>
                    <!-- column -->

                    <div class="col-lg-6">
                        <!-- Card -->
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Chat Option <small class="text-muted">Feature No available on this version</small></h4>
                                <div class="chat-box scrollable" style="height:475px;">
                                    <!--chat Row -->
                                    <ul class="chat-list">
                                        <!--chat Row -->
                                        <li class="chat-item">
                                            <div class="chat-img"><img src="../assets/images/users/1.jpg" alt="user">
                                            </div>
                                            <div class="chat-content">
                                                <h6 class="font-medium">ICT Department</h6>
                                                <div class="box bg-light-info">Hello There,Kindly chat us on any issue you are experiencing. and we shall get back to you within the next 10 minutes.
                                                    </div>
                                            </div>
                                            <div class="chat-time">10:56 am</div>
                                        </li>
                                        
                                        <!--chat Row -->
                                        <li class="odd chat-item">
                                            <div class="chat-content">
                                                
                                                <div class="box bg-light-inverse">Ok then. Thank you.</div>
                                                <br>
                                            </div>
                                            <div class="chat-time">11:00 am</div>
                                        </li>
                                        <!--chat Row -->
                                       
                                        
                                        <!--chat Row -->
                                    </ul>
                                </div>
                            </div>
                            <div class="card-body border-top">
                                <div class="row">
                                    <div class="col-9">
                                        <div class="input-field m-t-0 m-b-0">
                                            <textarea id="textarea1" placeholder="Type and enter"
                                                class="form-control border-0"></textarea>
                                        </div>
                                    </div>
                                    <div class="col-3">
                                        <a class="btn-circle btn-lg btn-success float-right text-white"
                                            href="javascript:void(0)"><i class="fas fa-paper-plane"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- card -->
                                          </div>
                </div>
                <!-- ============================================================== -->
                <!-- Recent comment and chats -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <?php include_once '../assets/includes/footer.php';
