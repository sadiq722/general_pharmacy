<!DOCTYPE html>
<html dir="ltr">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Tell the browser to be responsive to screen width -->
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>EMR Plus</title>
        <!-- Favicon icon -->
        <link rel="icon" type="image/png" sizes="16x16" href="../../assets/images/favicon.png">
        <!-- Custom CSS -->
        <link href="dist/css/style.min.css" rel="stylesheet">
        <link href="assets/libs/toastr/build/toastr.min.css" rel="stylesheet">
        <link href="dist//landingpage.css" rel="stylesheet">
      
    </head>

    <body style="background-color:lightgray">
          
        <div class="main-wrapper" >
            <!-- ============================================================== -->
            <!-- Preloader - style you can find in spinners.css -->
            <!-- ============================================================== -->
            <div class="preloader">
                <div class="lds-ripple">
                    <div class="lds-pos"></div>
                    <div class="lds-pos"></div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- Preloader - style you can find in spinners.css -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Login box.scss -->
            <!-- ============================================================== -->
 
            <div class="row" style="margin-top: 75px;">
                <div class="col-xs-12 col-lg-4 offset-lg-2 " style="background-image: url('assets/images/background.jpg') !important;
                 background-repeat: no-repeat;">
                    <div class="row badge-success text-center text-white" style="opacity: 0.7; height: 378px">
                        <div class="col col-xs-12" style="padding: 20px;position: absolute; bottom: 5px; ">
                            <h4 style="font-weight: 900; color: white">EMR Plus</h4>
                        <h4>Simplified Health Record</h4> 
                        <h6 >&copy All Rights Reserved</h4></div></div>
                </div>
                <div class="col-xs-12 col-lg-4 bg " style="border-left: 5px solid green; background-color: white" >
                
                <div class="card" style="z-index: 2;  ">
                    <div id="loginform">
                        <div class="text-center p-t-20 p-b-20">
                            <span class="db"><img src="assets/images/logo.png"  alt="logo" /></span>
                        </div>
                        <h5 style="padding-top: 10px">Login in to your account</h5><hr
                            <!-- Form -->
                            <form class="form-horizontal m-t-20" id="loginForm" action="javascript:login()">
                            <div class="row p-b-30">
                                <div class="col-12">
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text bg-success text-white" id="basic-addon1"><i class="ti-user"></i></span>
                                        </div>
                                        <input type="text" name="username" class="form-control form-control-lg" placeholder="Username" aria-label="Username" aria-describedby="basic-addon1" required="">
                                    </div>
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text bg-success text-white" id="basic-addon2"><i class="ti-lock"></i></span>
                                        </div>
                                        <input type="password" name="password" class="form-control form-control-lg" placeholder="Password" aria-label="Password" aria-describedby="basic-addon1" required="">
                                    </div>
                                </div>
                            </div>
                            <div class="row border-top border-secondary">
                                <div class="col-12">
                                    <div class="form-group">
                                        <div class="p-t-20">
                                            <input type="hidden" name="login">
                                            <button class="btn btn-secondary float-right"  type="button" onclick="resetFrom()"> Reset</button>
                                            <button class="btn btn-success float-right" type="submit" style="margin-right: 5px">Login</button>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>

                </div>
             
            </div>
            </div>
            


        </div>
        <!-- ============================================================== -->
        <!-- All Required js -->
        <!-- ============================================================== -->
        <script src="assets/libs/jquery/dist/jquery.min.js"></script>
        <!-- Bootstrap tether Core JavaScript -->
        <script src="assets/libs/popper.js/dist/umd/popper.min.js"></script>
        <script src="assets/libs/bootstrap/dist/js/bootstrap.min.js"></script>
        <!-- ============================================================== -->
        <!-- This page plugin js -->
        <!-- ============================================================== -->
        <script src="assets/libs/toastr/build/toastr.min.js"></script>
        <script>

            $('[data-toggle="tooltip"]').tooltip();
            $(".preloader").fadeOut();
            // ============================================================== 
            // Login and Recover Password 
            // ============================================================== 
function resetFrom(){
     jQuery('#loginForm').trigger("reset");
}
            function login() {
                var _data = $('#loginForm').serialize();
                var jqxhr = $.post("controller/controller.php", _data);
                jqxhr.done(function (result) {
               
                    if (result != 0) {
window.location="stats/index.php";
                    } else {

                        toastr.error('Incorrect Credentials!', 'Denied');
                    }
                }
                );
                jqxhr.fail(function () {
                    toastr.info('Network Error', 'Try again!');
                });
                jqxhr.always(function (data) {


                });
            }
        </script>

    </body>

</html>